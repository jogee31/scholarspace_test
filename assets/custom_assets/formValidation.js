$('form').submit(function (e) {
    var temp_this = $(this);
    var form_id = "#" + temp_this.attr('id');
    if (temp_this.hasClass('user_form')) {
        var temp_notification_count = 0;
        if (!textValidation(form_id) || $("#form_status").val() == 0) {
            e.preventDefault();
            temp_notification_count++;
            $("html, body").animate({
                scrollTop: 0
            }, 1000);
        }
        if (!selectValidation(form_id) || $("#form_status").val() == 0) {
            e.preventDefault();
            temp_notification_count++;
        }
        if (temp_notification_count) {
            show_danger_alert("Please enter required and valid details.");
        }
    }
    else if (temp_this.hasClass('bootstrap_modal')) {
        if (!textValidation(form_id) || !selectValidation(form_id) || !textAreaValidation(form_id)) {
            e.preventDefault();
        }
    }
    else {
        if (!textValidation(form_id) || !selectValidation(form_id) || !textAreaValidation(form_id)) {
            e.preventDefault();
            show_danger_alert("Please enter required and valid details.");
            $("html, body").animate({
                scrollTop: 0
            }, 1000);
        }
    }
});

/* stop autofill option in form */
$("form").attr('autocomplete', 'off');

/* reset button to reset the form */
$('body').on("click", ".reset", function () {
    $('form')[0].reset();
});

/* remove the required error msg */
$("input, textarea, select").focus(function () {
    $(".has-error").removeClass("has-error");
    $(".required_error_span").text("");
});


function textValidation(form_id) {
    var status = 1;
    $(form_id + ' input[type="text"]').each(function () {
        var temp_this = $(this);
        if (temp_this.hasClass("text_required") && temp_this.val() == '') {
            status = 0;
            $(temp_this).parent('div').siblings('label').children('.required_error_span').text("(This field is Required)");
            $(temp_this).parent('div').parent('div').addClass('has-error');
        }
    });
    return status;
}

/*
 function emailValidation(form_id) {
 var status = 0;
 var temp_this = $(this);
 $(form_id + ' input[type="email"]').each(function () {
 if (temp_this.hasClass("email_required")) {
 alert("Working");
 }
 });
 return status;
 }
 */

function selectValidation(form_id) {
    var status = 1;
    $(form_id + ' select').each(function () {
        var temp_this = $(this);
        if (temp_this.hasClass("select_required") && (temp_this.val() == '' || temp_this.val() == null)) {
            status = 0;
            $(temp_this).parent('div').siblings('label').children('.required_error_span').text("(This field is Required)");
            $(temp_this).parent('div').parent('div').addClass('has-error');
        }
    });
    return status;
}

function textAreaValidation(form_id) {
    var status = 1;
    $(form_id + ' textarea').each(function () {
        var temp_this = $(this);
        if (temp_this.hasClass("text_area_required") && (temp_this.text() == '' || temp_this.text() == null)) {
            status = 0;
            $(temp_this).parent('div').siblings('label').children('.required_error_span').text("(This field is Required)");
            $(temp_this).parent('div').parent('div').addClass('has-error');
        }
    });
    return status;
}

function checkBoxValidation() {
    var status = 0;
    $('.days_checkbox').each(function () {
        var temp_this = $(this);
        if (temp_this.is(':checked')) {
            status = 1;
        }
    });
    return status;
}

/* Email validation function */
function isValidEmailAddress(emailAddress) {
    //var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
    return pattern.test(emailAddress);
}

/* Mobile validation function */
function isValidMobileNumber(mobile) {
    var pattern = new RegExp(/^[7-9][0-9]{9}$/);
    return pattern.test(mobile);
}

function show_success_alert(message) {
    $("#success_alert_message").text(message);
    $('.success_alert').slideDown(400);
    $('.success_alert').delay(3000).slideUp(400);
}

function show_danger_alert(message) {
    $("#danger_alert_message").text(message);
    $('.danger_alert').slideDown(400);
    $('.danger_alert').delay(3000).slideUp(400);
}