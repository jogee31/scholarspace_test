$(document).ready(function () {
    /* Root Path */
    var site_url = $('#site_url').val();
    var base_url = $('#base_url').val();
    var stream_courses_list_table;

    /* Add stream courses */
    $(".add_stream_course_btn").click(function () {
        var temp_this = $(this);
        var stream_id = temp_this.attr('stream_id');
        var stream_name = temp_this.attr('stream_name');
        $("#stream_span_add_stream_course_modal").text(stream_name);
        $("#stream_id_add_stream_course_modal").val(stream_id);
    });

    /* Initialization of datatable */
    $('#view_course_table').dataTable({
        "bSort": true,
        "bFilter": true,
        "bPaginate": true
    });

    /* add more corses in add course modal*/
    $(".add_more_course_name_btn").click(function () {
        var temp_html = '<div class="row">\
                        <div class="col-md-9">\
                            <div class="form-group">\
                                <div><input type="text" name="course_name[]" class="form-control text_required" id="exampleInputEmail1" placeholder="Enter Course Name"></div>\
                            </div>\
                        </div>\
                        <div class="col-md-3">\
                            <div class="form-group">\
                            <button type="button" class="btn btn-danger pull-right remove_more_course_name_btn"><i class="fa fa-times"></i>&nbsp;&nbsp; Remove</button>\
                            </div>\
                        </div>\
                    </div>';
        $("#add_more_courses_content_div").append(temp_html);
    });

    /* remove button, for add more stream courses on add course modal */
    $('body').on("click", ".remove_more_course_name_btn", function () {
        var temp_this = $(this);
        temp_this.parents().eq(2).remove();
    });

    /* delete stream's courses by cross button */
    $('body').on("click", ".delete_stream_course", function () {
        var temp_this = $(this);
        var course_id = temp_this.attr('course_id');
        var from = 'page';
        if (temp_this.hasClass("from_modal")) {
            from = 'modal';
            var target_row = temp_this.closest("tr").get(0); // this line did the trick
        }
        bootbox.confirm("Are you sure to delete this course?", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: site_url + "super-admin/stream/delete_stream_course",
                    data: {
                        course_id: course_id,
                        from: from
                    },
                    success: function (data) {
                        if (temp_this.hasClass("from_modal")) {
                            if (data.trim() == '1') {
                                var aPos = stream_courses_list_table.fnGetPosition(target_row);
                                stream_courses_list_table.fnDeleteRow(aPos);
                                $("#modal_success_alert_message").text("Course Deleted successfully.");
                                $('.modal_success_alert').slideDown(400);
                                $('.modal_success_alert').delay(2000).slideUp(400);
                            } else {
                                $("#modal_danger_alert_message").text("Failed to delete Course");
                                $('.modal_danger_alert').slideDown(400);
                                $('.modal_danger_alert').delay(2000).slideUp(400);
                            }
                        } else {
                            if (data.trim() == '1') {
                                window.location.href = window.location.href;
                            } else {
                                $("#danger_alert_message").text("Failed to delete Course");
                                $('.danger_alert').slideDown(400);
                                $('.danger_alert').delay(2000).slideUp(400);
                            }
                        }
                    }
                });
            }
        });
    });

    /**/
    $(".view_all_stream_courses").click(function () {
        var temp_this = $(this);
        var stream_id = temp_this.attr('stream_id');
        var stream_name = temp_this.attr('stream_name');
        $.ajax({
            type: "POST",
            url: site_url + "super-admin/stream/get_stream_all_courses",
            data: {
                stream_id: stream_id
            },
            success: function (data) {
                $(".stream_courses_list_table").dataTable().fnDestroy();
                $(".stream_courses_content_tbody").html('');
                $(".stream_courses_content_tbody").html(data.trim());
                stream_courses_list_table = $(".stream_courses_list_table").dataTable({
                    "bAutoWidth": false,
                    "aoColumns": [
                        {"sWidth": "8%"},
                        {"sWidth": "72%"},
                        {"sWidth": "20%"},
                    ],
                });
                $(".stream_courses_list_table").css("width", "100%");
            }
        });
        $("#stream_span_show_stream_courses_modal").text(stream_name);
        $("#show_stream_courses_modal").modal("show");
    });

    $(".edit_stream_btn").click(function () {
        var temp_this = $(this);
        var stream_id = temp_this.attr('stream_id');
        var stream_title = temp_this.attr('stream_title');
        var study_type = temp_this.attr('study_type');
        var study_type_count = temp_this.attr('study_type_count');

        if (study_type == "semester") {
            $("#editStModal_study_type_semester").attr("checked", "checked");
            $("#editStModal_study_type_semester").attr("semester", study_type_count);
            $("#editStModal_study_type_year").attr("year", "");
            var temp_html = '<label for="exampleInputEmail1">Number of Semester * <span class="required_error_span"></span></label>\
                                <div><input type="text" name="semester" value="' + study_type_count + '" class="form-control text_required number_of_semester" placeholder="Enter Semester"></div>';
            $(".editModal_year_or_semester_div").html(temp_html);
        } else if (study_type == "year") {
            $("#editStModal_study_type_year").attr("checked", "checked");
            $("#editStModal_study_type_semester").attr("year", study_type_count);
            $("#editStModal_study_type_semester").attr("semester", "");
            var temp_html = '<label for="exampleInputEmail1">Number of Year * <span class="required_error_span"></span></label>\
                                <div><input type="text" name="year" value="' + study_type_count + '" class="form-control text_required number_of_year" placeholder="Enter Year"></div>';
            $(".editModal_year_or_semester_div").html(temp_html);
        }

        $("#edit_modal_stream_id").val(stream_id);
        $("#edit_modal_stream_title").val(stream_title);
        $("#edit_stream_modal").modal("show");
    });

    $(".delete_stream_btn").click(function () {
        var temp_this = $(this);
        var stream_id = temp_this.attr('stream_id');
        bootbox.confirm("Are you sure.?", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: site_url + "super-admin/stream/delete_stream",
                    data: {
                        stream_id: stream_id
                    },
                    success: function (data) {
                        if (data.trim() == "1") {
                            window.location.href = window.location.href;
                        } else {
                            $("#danger_alert_message").text("Failed to delete Stream.");
                            $('.danger_alert').slideDown(400);
                            $('.danger_alert').delay(2000).slideUp(400);
                        }
                    }
                });
            }
        });
    });

    $("body").on("click", ".edit_stream_courses_btn", function () {
        var temp_this = $(this);
        var course_id = temp_this.attr('course_id');
        var course_name = temp_this.attr('course_name');
        $("#edit_modal_stream_course_title").val(course_name);
        $("#edit_modal_stream_course_id").val(course_id);
        $("#edit_stream_course_modal").modal("show");
    });

    $("#edit_stream_course_form").submit(function (e) {
        e.preventDefault();
        var course_name = $("#edit_modal_stream_course_title").val();
        var course_id = $("#edit_modal_stream_course_id").val();
        $.ajax({
            type: "POST",
            url: site_url + "super-admin/stream/submit_edit_stream_courses",
            data: {
                course_id: course_id,
                course_name: course_name
            },
            success: function (data) {
                if (data.trim() == '1') {
                    $("#modal_success_alert_message").text("Coures updated successfuly.");
                    $('.modal_success_alert').slideDown(400);
                    $('.modal_success_alert').delay(2000).slideUp(400);
                } else {
                    $("#modal_danger_alert_message").text("Failed to update Course.");
                    $('.modal_danger_alert').slideDown(400);
                    $('.modal_danger_alert').delay(2000).slideUp(400);
                }
                $("#edit_stream_course_modal").modal("hide");
            }
        });
    });

    $(".study_system_checkbox").change(function () {
        var temp_this = $(this);
        var selected_value = temp_this.val();
        var temp_html = '';

        if (selected_value == 'year') {
            temp_html = '<label for="exampleInputEmail1">Number of Year * <span class="required_error_span"></span></label>\
                                <div><input type="text" name="year" class="form-control text_required number_of_year" placeholder="Enter Year"></div>';

            if (temp_this.attr('year') == '' || temp_this.attr('year') > 0) {
                $(".editModal_year_or_semester_div").html(temp_html);
                var year = temp_this.attr('year');
                $(".number_of_year").val(year);
            } else {
                $(".year_or_semester_div").html(temp_html);
            }
        } else if (selected_value == 'semester') {
            temp_html = '<label for="exampleInputEmail1">Number of Semester * <span class="required_error_span"></span></label>\
                                <div><input type="text" name="semester" class="form-control text_required number_of_semester" placeholder="Enter Semester"></div>';

            if (temp_this.attr('semester') == '' || temp_this.attr('semester') > 0) {
                $(".editModal_year_or_semester_div").html(temp_html);
                var semester = temp_this.attr('semester');
                $(".number_of_semester").val(semester);
            } else {
                $(".year_or_semester_div").html(temp_html);
            }
        }
    });
});