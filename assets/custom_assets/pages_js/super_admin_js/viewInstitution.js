$(function () {
    var site_url = $('#site_url').val();
    $('#submit_of_university').click(function () {
        $.ajax({
            url: site_url + "super-admin/university_add_name/add_university",
            type: 'post',
            data: $('form#university_form_data').serialize(),
            success: function (data) {
                if (data.trim() == 1) {
                    window.location.href = window.location.href;
                } else {
                    $("#danger_alert_message").text("Failed to add course.");
                    $('.danger_alert').slideDown(400);
                    $('.danger_alert').delay(2000).slideUp(400);
                }
            }
        });
    });

    $('.update_content').click(function () {
        var id_of_update = $(this).val();
        var name_of_university = $(this).attr("name");
        $(".update_place").val(name_of_university);
        $(".update_id").val(id_of_update);
    });



    $('.delete_content').click(function () {
        var id_for_delete = $(this).val();
        $.ajax({
            url: site_url + "super-admin/university_add_name/delete_university",
            type: 'post',
            data: {id: id_for_delete},
            success: function (data) {
                if (data.trim() == 1) {
                    window.location.href = window.location.href;
                } else {
                    $("#danger_alert_message").text("Failed to add course.");
                    $('.danger_alert').slideDown(400);
                    $('.danger_alert').delay(2000).slideUp(400);
                }
            }
        });
    });

    $('.reactive_content').click(function () {
        var id_for_delete = $(this).val();
        $.ajax({
            url: site_url + "super-admin/university_add_name/reactive_university",
            type: 'post',
            data: {id: id_for_delete},
            success: function (data) {
                if (data.trim() == 1) {
                    window.location.href = window.location.href;
                } else {
                    $("#danger_alert_message").text("Failed to add course.");
                    $('.danger_alert').slideDown(400);
                    $('.danger_alert').delay(2000).slideUp(400);
                }
            }
        });
    });

    $('#submit_update_university').click(function () {
        $.ajax({
            url: site_url + "super-admin/university_add_name/update_university",
            type: 'post',
            data: $('form#update_university_form_data').serialize(),
            success: function (data) {
                if (data.trim() == 1) {
                    window.location.href = window.location.href;
                } else {
                    $("#danger_alert_message").text("Failed to add course.");
                    $('.danger_alert').slideDown(400);
                    $('.danger_alert').delay(2000).slideUp(400);
                }
            }
        });
    });

    $(".active_status_btn").click(function () {
        var temp_this = $(this);
        var institution_id = temp_this.attr('institution_id');
        var active_status = temp_this.attr('active_status');
        $.ajax({
            type: "POST",
            url: site_url + "super-admin/institution/update_institution_active_status",
            data: {
                active_status: active_status,
                institution_id: institution_id
            },
            success: function (data) {
                if (data.trim() == '1') {
                    if (active_status == '1') {
                        temp_this.attr("active_status", "0");
                        temp_this.removeClass("btn-danger");
                        temp_this.addClass("btn-success");
                        temp_this.html("<i class=\"fa fa-check\"></i> Activate");
                    } else {
                        temp_this.attr("active_status", "1");
                        temp_this.addClass("btn-danger");
                        temp_this.removeClass("btn-success");
                        temp_this.html("<i class=\"fa fa-ban\"></i> Deactivate");
                    }
                    $("#success_alert_message").text("Active Status changed successfully.");
                    $('.success_alert').slideDown(400);
                    $('.success_alert').delay(2000).slideUp(400);
                } else {
                    $("#danger_alert_message").text("Failed to change Active Status.");
                    $('.danger_alert').slideDown(400);
                    $('.danger_alert').delay(2000).slideUp(400);
                }
            }
        });
    });

    $(".edit_institution_btn").click(function () {
        var temp_this = $(this);
        var institution_id = temp_this.attr('institution_id');
        var institution_title = temp_this.attr('institution_title');
        $("#edit_modal_institution_id").val(institution_id);
        $("#edit_modal_institution_title").val(institution_title);
        $("#edit_institution_modal").modal("show");
    });

    $(".delete_institution_btn").click(function () {
        var temp_this = $(this);
        var institution_id = temp_this.attr('institution_id');
        bootbox.confirm("Are you sure.?", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: site_url + "super-admin/institution/delete_institution",
                    data: {
                        institution_id: institution_id
                    },
                    success: function (data) {
                        if (data.trim() == "1") {
                            window.location.href = window.location.href;
                        } else {
                            $("#danger_alert_message").text("Failed to delete Institution.");
                            $('.danger_alert').slideDown(400);
                            $('.danger_alert').delay(2000).slideUp(400);
                        }
                    }
                });
            }
        });
    });
});