var table = $("#college_request_table").dataTable();

var site_url = $("#site_url").val();
var base_url = $("#base_url").val();
$(".edit_college_details_btn").click(function () {
    var temp_this = $(this);
    var college_id = temp_this.attr('college_id');
    var college_name = temp_this.attr('college_name');
    var college_city = temp_this.attr('college_city');
    var college_state = temp_this.attr('college_state');
    var college_zipcode = temp_this.attr('college_zipcode');

    $("#eClgDtlMdl_college_id").val(college_id);
    $("#eClgDtlMdl_college_name").val(college_name);
    $("#eClgDtlMdl_college_city").val(college_city);
    $("#eClgDtlMdl_college_state").val(college_state);
    $("#eClgDtlMdl_college_zipcode").val(college_zipcode);

    $("#edit_college_modal").modal("show");
});

$(".accept_college_request_btn").click(function () {
    var temp_this = $(this);
    var target_row = temp_this.closest("tr").get(0);
    var college_id = temp_this.attr('college_id');
    bootbox.confirm("Are you sure to accept this college request.?", function (result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: site_url + "super-admin/college/accept_college_request",
                data: {
                    college_id: college_id
                },
                success: function (data) {
                    if (data.trim() == '1') {
                        var aPos = table.fnGetPosition(target_row);
                        table.fnDeleteRow(aPos);
                        $("#success_alert_message").text("College request ignored Successfully.");
                        $('.success_alert').slideDown(400);
                        $('.success_alert').delay(2000).slideUp(400);
                    } else {
                        $("#danger_alert_message").text("Failed to ignore college request.");
                        $('.danger_alert').slideDown(400);
                        $('.danger_alert').delay(2000).slideUp(400);
                    }
                }
            });
        }
    });
});

$(".ignore_college_details_btn").click(function () {
    var temp_this = $(this);
    var target_row = temp_this.closest("tr").get(0);
    var college_id = temp_this.attr('college_id');
    bootbox.confirm("Are you sure to ignore this request.?", function (result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: site_url + "super-admin/college/ignore_college_request",
                data: {
                    college_id: college_id
                },
                success: function (data) {
                    if (data.trim() == '1') {
                        var aPos = table.fnGetPosition(target_row);
                        table.fnDeleteRow(aPos);
                        $("#success_alert_message").text("College request accepted Successfully.");
                        $('.success_alert').slideDown(400);
                        $('.success_alert').delay(2000).slideUp(400);
                    } else {
                        $("#danger_alert_message").text("Failed to accept college request.");
                        $('.danger_alert').slideDown(400);
                        $('.danger_alert').delay(2000).slideUp(400);
                    }
                }
            });
        }
    });

});