$(document).ready(function () {
    var table = $('#view_user_table').dataTable({
        "bSort": false,
    });

    var site_url = $("#site_url").val();
    var base_url = $("#base_url").val();

    $(".active_status_btn").click(function () {
        var temp_this = $(this);
        var user_id = temp_this.attr('user_id');
        var active_status = temp_this.attr('active_status');
        $.ajax({
            type: "POST",
            url: site_url + "super-admin/user/change_active_status",
            data: {
                user_id: user_id,
                active_status: active_status
            },
            success: function (data) {
                if (data.trim() == "1") {
                    if (active_status == "1") {
                        temp_this.html("<i class=\"fa fa-check\"></i> Activate");
                        temp_this.attr('active_status', '0');
                        temp_this.removeClass('btn-danger');
                        temp_this.addClass('btn-success');
                    } else {
                        temp_this.html("<i class=\"fa fa-ban\"></i> Deactivate");
                        temp_this.attr('active_status', '1');
                        temp_this.removeClass('btn-success');
                        temp_this.addClass('btn-danger');
                    }
                    $("#success_alert_message").text("User active status changed successfully.");
                    $('.success_alert').slideDown(400);
                    $('.success_alert').delay(2000).slideUp(400);
                } else {
                    $("#danger_alert_message").text("Failed to change user active status.");
                    $('.danger_alert').slideDown(400);
                    $('.danger_alert').delay(2000).slideUp(400);
                }
            }
        });
    });

    $("body").on("click", ".delete_user_btn", function () {
        var temp_this = $(this);
        var target_row = temp_this.closest("tr").get(0);
        var user_id = temp_this.attr('user_id');
        var user_type = temp_this.attr('user_type');

        bootbox.confirm("Are you sure to delete this user.", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: site_url + "super-admin/user/delete_user",
                    data: {
                        user_id: user_id,
                        user_type: user_type
                    },
                    success: function (data) {
                        if (data.trim() == '1') {
                            var aPos = table.fnGetPosition(target_row);
                            table.fnDeleteRow(aPos);
                            $("#success_alert_message").text("College active status changed successfully.");
                            $('.success_alert').slideDown(400);
                            $('.success_alert').delay(2000).slideUp(400);
                        } else {
                            $("#danger_alert_message").text("Failed to change College active status.");
                            $('.danger_alert').slideDown(400);
                            $('.danger_alert').delay(2000).slideUp(400);
                        }
                    }
                });
            }
        });
    });
});