$(document).ready(function () {
    var site_url = $('#site_url').val();
    var base_url = $('#base_url').val();

    $("#email").keyup(function () {
        var temp_this = $(this);
        temp_this.parent('div').siblings('label').children('.required_error_span').text("");
        temp_this.parent('div').parent('div').removeClass('has-error');
        var email_value = temp_this.val();
        if (email_value != '') {
            $.ajax({
                type: "POST",
                url: site_url + "super-admin/user/check_email_exist",
                data: {
                    email: email_value
                },
                success: function (data) {
                    if (data.trim() > '0') {
                        temp_this.parent('div').siblings('label').children('.required_error_span').text("(Email is already exist, Try other email)");
                        temp_this.parent('div').parent('div').addClass('has-error');
                    }
                }
            });
        }
    });
    $("#submit_id").click(function () {
        var email_value = $("#email").val();
        if (email_value != '') {
            $.ajax({
                type: "POST",
                url: site_url + "super-admin/user/check_email_exist",
                data: {
                    email: email_value
                },
                success: function (data) {
                    if (data.trim() > '0') {
                        $("#email").parent('div').siblings('label').children('.required_error_span').text("(Email is already exist, Try other email)");
                        $("#email").parent('div').parent('div').addClass('has-error');
                        $("#form_status").val(0);
                        $("#add_data_entry_user").submit();
                    } else {
                        $("#form_status").val(1);
                        $("#add_data_entry_user").submit();
                    }
                }
            });
        } else {
            $("#add_data_entry_user").submit();
        }
    });
});