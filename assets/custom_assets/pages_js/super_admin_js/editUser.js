var site_url = $('#site_url').val();
var base_url = $('#base_url').val();

$("#email").keyup(function () {
    var temp_this = $(this);
    temp_this.parent('div').siblings('label').children('.required_error_span').text("");
    temp_this.parent('div').parent('div').removeClass('has-error');
    var email_value = temp_this.val();
    var user_id = $("#user_id").val();
    if (email_value != '') {
        $.ajax({
            type: "POST",
            url: site_url + "super-admin/user/check_edit_email_exist",
            data: {
                email: email_value,
                user_id: user_id
            },
            success: function (data) {
                if (data.trim() > '0') {
                    temp_this.parent('div').siblings('label').children('.required_error_span').text("(Email is already exist, Try other email)");
                    temp_this.parent('div').parent('div').addClass('has-error');
                }
            }
        });
    }
});

$("#submit_id").click(function () {
    var email_value = $("#email").val();
    var user_id = $("#user_id").val();
    if (email_value != '') {
        $.ajax({
            type: "POST",
            url: site_url + "super-admin/user/check_edit_email_exist",
            data: {
                email: email_value,
                user_id: user_id
            },
            success: function (data) {
                if (data.trim() > '0') {
                    $("#email").parent('div').siblings('label').children('.required_error_span').text("(Email is already exist, Try other email)");
                    $("#email").parent('div').parent('div').addClass('has-error');
                    $("#form_status").val(0);
                    $("#edit_data_entry_user").submit();
                } else {
                    $("#form_status").val(1);
                    $("#edit_data_entry_user").submit();
                }
            }
        });
    } else {
        $("#edit_data_entry_user").submit();
    }
});