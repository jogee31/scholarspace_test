var site_url = $("#site_url").val();
var base_url = $("#base_url").val();

$(document).ready(function () {
    var dataTable = $('#college_list_table').DataTable({
//        "sDom": "Rlrtip",
        "bDeferRender": true,
//        "sScrollX": "100%",
        "bProcessing": true,
        "bServerSide": true,
        "sServerMethod": "GET",
        "sAjaxSource": site_url + 'super-admin/college/fetch_datatable_college_list',
        "iDisplayLength": 10,
        "aLengthMenu": [[10, 20, 50, 100], [10, 20, 50, 100]],
        "aaSorting": [[0, 'asc']],
    });

    $("body").on("click", ".college_edit_btn", function () {
        var temp_this = $(this);
        var college_id = temp_this.attr("college_id");
        var college_name = temp_this.attr("college_name");
        var city = temp_this.attr("college_city");
        var state = temp_this.attr("college_state");
        var zipcode = temp_this.attr("college_zipcode");
        $("#eClgMdl_college_id").val(college_id);
        $("#eClgMdl_college_name").val(college_name);
        $("#eClgMdl_college_city").val(city);
        $("#eClgMdl_college_state").val(state);
        $("#eClgMdl_college_zipcode").val(zipcode);
        $("#edit_college_modal").modal("show");
    });

    $("body").on("click", ".active_status_college_btn", function () {
        var temp_this = $(this);
        var college_id = temp_this.attr("college_id");
        var active_status = temp_this.attr("active_status");
        bootbox.confirm("Are you sure to delete this college.", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: site_url + 'super-admin/college/change_college_active_status',
                    data: {
                        college_id: college_id,
                        active_status: active_status
                    },
                    success: function (data) {
                        if (data.trim() == '1') {
                            if (active_status == "1") {
                                temp_this.html("<i class=\"fa fa-check\"></i> Activate");
                                temp_this.attr('active_status', '0');
                                temp_this.removeClass('btn-danger');
                                temp_this.addClass('btn-success');
                            } else {
                                temp_this.html("<i class=\"fa fa-ban\"></i> Deactivate");
                                temp_this.attr('active_status', '1');
                                temp_this.removeClass('btn-success');
                                temp_this.addClass('btn-danger');
                            }
                            $("#success_alert_message").text("College active status changed successfully.");
                            $('.success_alert').slideDown(400);
                            $('.success_alert').delay(2000).slideUp(400);
                        } else {
                            $("#danger_alert_message").text("Failed to change College active status.");
                            $('.danger_alert').slideDown(400);
                            $('.danger_alert').delay(2000).slideUp(400);
                        }
                    }
                });
            }
        });
    });
});