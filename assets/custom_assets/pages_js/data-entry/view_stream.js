var course_table = $("#view_stream_course_table").DataTable();
//view_stream_table
$("#view_stream_table").DataTable();

var base_url = $("#base_url").val();
var site_url = $("#site_url").val();

$("#stream_select").change(function () {
    var temp_this = $(this);
    var stream_value = temp_this.val();
    $.ajax({
        type: "POST",
        url: site_url + "data-entry/stream/get_stream_corses",
        data: {
            stream: stream_value
        },
        success: function (data) {
            $("#view_stream_course_table").dataTable().fnDestroy();
            $("#view_stream_course_tbody").html('');
            $("#view_stream_course_tbody").html(data.trim());
            course_table = $("#view_stream_course_table").dataTable({
                "bAutoWidth": false,
                "aoColumns": [
                    {"sWidth": "100%"},
                ],
            });
            $("#view_stream_course_table").css("width", "100%");
        }
    });
});

//$("#add_new_stream_course_btn").click(function () {
//    var stream_id = $("#stream_select").val();
//    var stream_course = $("#");
//});
$("#add_stream_course_form").submit(function (e) {
    e.preventDefault();
    var stream_id = $("#stream_select").val();
    var stream_course = $("#stream_course").val();
    $.ajax({
        type: "POST",
        url: site_url + "data-entry/stream/add_new_stream_course",
        data: {
            stream_id: stream_id,
            stream_course: stream_course
        },
        success: function (data) {
            if (data.trim() == '1') {
                addrow();
                $("#success_alert_message").text("Course added Successfully.");
                $('.success_alert').slideDown(400);
                $('.success_alert').delay(2000).slideUp(400);
            } else {
                $("#danger_alert_message").text("Failed to add Course");
                $('.danger_alert').slideDown(400);
                $('.danger_alert').delay(2000).slideUp(400);
            }
            $("#add_new_course_modal").modal("hide");
            $("#stream_course").val('');
        }
    });
});

function addrow() {
    course_table.fnAddData([
        $("#stream_course").val()
    ]);
}