var selected_item_array = [];
var generated_select_stream_courses = [];
$(document).ready(function () {
    $(".id_span").css("display", "none");
//Courses Offered by stream select box
    var site_url = $('#site_url').val();
    $('#selectbox').select2().on("change", function (e) {
        // mostly used event, fired to the original element when the value changes
        var id_and_stream_id = $(this).val();
        $.each(id_and_stream_id, function (index, value) {
            if ($.inArray(value, selected_item_array) == -1) {
                selected_item_array.push(value);
                $.ajax({
                    type: "post",
                    url: site_url + "data-entry/college/fetch_selected_courses",
                    data: {
                        id_stream_id: value
                    },
                    success: function (data) {
                        $("#courses_offered_by").append(data);
                        $('#course_tab-' + value).select2()
                    }
                });
            }
        });
    });
//    $(".select_courses_offered").select2();
    /* More Videoes Link */
    //add more video button
    $(".add-more-video-btn").click(function () {
        var html = '<div class="video_link_box">\
                                                        <div class="col-md-4">\
                                                            <label>\
                                                                <a style="margin-left: 256px;cursor: pointer" class="pull-left remove-more-video-btn"><i class="fa fa-times"></i> Remove </a>\
                                                            </label>\
                                                        </div>\
                                                        <div class="col-md-8">\
                                                            <div class="form-group">\
                                                                <input name="video[]" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">\
                                                            </div>\
                                                        </div>\
                                                    </div>';
        $("#more_video").append(html);
    });
    $("body").on("click", ".remove-more-video-btn", function () {
        var temp_this = $(this);
        temp_this.parents().eq(2).remove();
    });
    /* More Clubs and Activities : */
    $(".add_more_club_and_activities_btn").click(function () {
        var html = '<div class="club_and_Activities_box">\
                        <div class="col-md-4">\
                            <label>\
                                &nbsp;\
                            </label>\
                            <a style="font-weight:700;cursor: pointer" class="pull-right remove_more_club_and_activities_btn"><i class="fa fa-times"></i> Remove&nbsp;&nbsp;&nbsp; </a>\
                        </div>\
                        <div class="col-md-8">\
                            <div class="row">\
                                <div class="col-md-6">\
                                    <div class="form-group">\
                                        <input type="text" name="clubs_activity_title[]" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">\
                                    </div>\
                                </div>\
                                <div class="col-md-6">\
                                    <div class="form-group">\
                                        <textarea name="clubs_activity_description[]" class="form-control" rows="1" placeholder="Enter Description ..."></textarea>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>';
        $("#more_club_and_activities").append(html);
    });
    $("body").on("click", ".remove_more_club_and_activities_btn", function () {
        var temp_this = $(this);
        temp_this.parents().eq(1).remove();
    });
    /* Laboratories add more */
    $(".add_more_laboratories").click(function () {
        var html = '<div class="laboratories_box">\
                        <div class="col-md-4">\
                            <label>\
                                &nbsp;\
                            </label>\
                            <a style="font-weight:700;cursor: pointer" class="pull-right remove_more_laboratories_btn"><i class="fa fa-times"></i> Remove&nbsp;&nbsp;&nbsp; </a>\
                        </div>\
                        <div class="col-md-8">\
                            <div class="row">\
                                <div class="col-md-6">\
                                    <div class="form-group">\
                                        <input type="text" name="laboratories_title[]" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">\
                                    </div>\
                                </div>\
                                <div class="col-md-6">\
                                    <div class="form-group">\
                                        <textarea name="laboratories_description[]" class="form-control" rows="1" placeholder="Enter Description ..."></textarea>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>';
        $("#more_laboratories").append(html);
    });
    $("body").on("click", ".remove_more_laboratories_btn", function () {
        var temp_this = $(this);
        temp_this.parents().eq(1).remove();
    });
    /* Festivals add more */
    $(".add_festivals").click(function () {
        var html = '<div class="festivals_box">\
                        <div class="col-md-4">\
                            <label>\
                                &nbsp;\
                            </label>\
                            <a style="font-weight:700;cursor: pointer" class="pull-right remove_more_laboratories_btn"><i class="fa fa-times"></i> Remove&nbsp;&nbsp;&nbsp; </a>\
                        </div>\
                        <div class="col-md-8">\
                            <div class="row">\
                                <div class="col-md-6">\
                                    <div class="form-group">\
                                        <input type="text" name="festivals_title[]" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">\
                                    </div>\
                                </div>\
                                <div class="col-md-6">\
                                    <div class="form-group">\
                                        <textarea name="festivals_description[]" class="form-control" rows="1" placeholder="Enter Description ..."></textarea>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>';
        $("#more_festivals").append(html);
    });
    $("body").on("click", ".remove_more_laboratories_btn", function () {
        var temp_this = $(this);
        temp_this.parents().eq(1).remove();
    });
    /* Add Extra Curricular */
    $(".add_extra_curricular").click(function () {
        var html = '<div class="video_link_box">\
                                                        <div class="col-md-4">\
                                                            <label class="pull-right">\
                                                                <a style="font-weight:700;cursor: pointer" class="remove_more_extra_curricular_btn"><i class="fa fa-times"></i> Remove&nbsp;&nbsp;&nbsp; </a>\
                                                            </label>\
                                                        </div>\
                                                        <div class="col-md-8">\
                                                            <div class="row">\
                                                                <div class="col-md-6">\
                                                                    <div class="form-group">\
                                                                        <input type="text" name="extra_curricular_title[]" class="form-control" placeholder="Enter Title">\
                                                                    </div>\
                                                                </div>\
                                                                <div class="col-md-6">\
                                                                    <div class="form-group">\
                                                                        <textarea name="extra_curricular_description[]" class="form-control" rows="1" placeholder="Enter Description ..."></textarea>\
                                                                    </div>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>';
        $("#more_extra_curricular").append(html);
    });
    $("body").on("click", ".remove_more_extra_curricular_btn", function () {
        var temp_this = $(this);
        temp_this.parents().eq(2).remove();
    });
    /* Faculty add more */
    $(".add_faculty").click(function () {
        var html = '<div class="faculty_box">\
                                                                <div class="col-md-2">\
                                                                    <a style="font-weight:700; cursor: pointer; margin-left: 30%;" class="remove_more_faculty_btn"><i class="fa fa-times"></i> Remove </a>\
                                                                </div>\
                                                                <div class="col-md-4">\
                                                                    <div class="form-group">\
                                                                        <input type="text" name="faculty_title[]" class="form-control" id="exampleInputEmail1" placeholder="Enter Title">\
                                                                    </div>\
                                                                </div>\
                                                                <div class="col-md-6">\
                                                                    <div class="form-group">\
                                                                        <textarea name="faculty_description[]" class="form-control" rows="1" placeholder="Enter Description ..."></textarea>\
                                                                    </div>\
                                                                </div>\
                                                            </div>';
        $("#more_faculty").append(html);
    });
    $("body").on("click", ".remove_more_faculty_btn", function () {
        var temp_this = $(this);
        temp_this.parents().eq(1).remove();
    });

    /* Hostel radio */
    $(".hostel_facilities_radio").change(function () {
        var temp_this = $(this);
        var this_value = temp_this.val();
        if (this_value == 'yes') {
            $(".hostel_description_div").html('<textarea name="hostal_facility_description" class="form-control" rows="2" placeholder="Description..."></textarea>');
        } else {
            $(".hostel_description_div").html('');
        }
    });

    /* Aicte Approved Check */
    $(".aicte_approved_radio").change(function () {
        var temp_this = $(this);
        var this_value = temp_this.val();
        if (this_value == 'yes') {
            $(".aicte_approved_or_not_div").html('<textarea name="aicte_approved_or_not" class="form-control" rows="2" placeholder="Description..."></textarea>');
        } else {
            $(".aicte_approved_or_not_div").html('');
        }
    });



    $("body").on('click', '.set_college_name', function () {
        var col_name = $(this).text();
        $('input[name=college_name]').val(col_name);
        $('.show_auto_search').hide();
    });

    $("#college_details_save_btn").click(function () {
        $("#extra_flag_field_div").html("<input type='hidden' value='1' name='flag'/>");
    });

    $("body").on('click', '.delete_img', function () {
        var temp_this = $(this);
        var img_id = temp_this.attr("img_id");

        bootbox.confirm("You really want to delete this Image?", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: site_url + "data-entry/college/delete_college_img",
                    data: {
                        img_id: img_id
                    },
                    success: function (data) {
                        if (data.trim()) {
                            temp_this.parent('a').remove();
                            $("#success_alert_message").text("Image successfully deleted.");
                            $('.success_alert').slideDown(400);
                            $('.success_alert').delay(2000).slideUp(400);
                        } else {
                            $("#danger_alert_message").text("Failed To delete Image .");
                            $('.danger_alert').slideDown(400);
                            $('.danger_alert').delay(2000).slideUp(400);
                        }
                        $("html, body").animate({
                            scrollTop: 0
                        }, 1000);
                    }
                });
            }
        });
    });
});

$(function () {
    var site_url = $('#site_url').val();
    $("#college_id").autocomplete({
        source: function (request, response)
        {
            var this_value = $("#college_id").val();
            $("#existing_college_id").val("0");
            $.ajax({
                type: "POST",
                url: site_url + "data-entry/college/show_college_names",
                dataType: "json",
                data: {
                    name: request.term,
                },
                success: function (data)
                {
                    response(data);
                    $('#college_registration_form')[0].reset();
                    $("#college_id").val(this_value);
                    $("#existing_college_id").val("0");
                    $(".more_content").empty();
                }
            });
        }
    });
    /* temporary code added by jogendra */
    $("#stream_selectbox option").click(function () {
        var temp_this = $(this);
        var stream_id = temp_this.val();
        var stream_name = temp_this.text();
        if ($.inArray(stream_id, generated_select_stream_courses) !== -1) {
            $("#" + stream_id).select2('destroy');
            $("." + stream_id).remove();
            generated_select_stream_courses.splice($.inArray(stream_id, generated_select_stream_courses), 1);
        } else {
            var value = $("#stream_selectbox").val();
            if (value.length == 1) {
                $.each(generated_select_stream_courses, function (index, value) {
                    $("#" + value).select2('destroy');
                    $("." + value).remove();
                });
                generated_select_stream_courses = [];
                generated_select_stream_courses.push(stream_id);
            }
            $.ajax({
                type: "POST",
                url: site_url + "data-entry/college/fetch_selected_courses",
                data: {
                    stream_id: stream_id,
                    stream_name: stream_name
                },
                success: function (data) {
                    $("#courses_content").append(data);
                    if (value.length != 1) {
                        generated_select_stream_courses.push(stream_id);
                    }
                    $('#' + stream_id).select2();
                }
            });
        }
    });
});