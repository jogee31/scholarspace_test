var site_url = $("#site_url").val();
var base_url = $("#base_url").val();
var college_id = $("#college_id").val();

/* Request confirm btn for user */
$(".user_request_confirm").click(function () {
    var temp_this = $(this);
    var user_id = temp_this.attr("user_id");
    var college_id = temp_this.attr("college_id");

});

/* Delete user request from the admin */
$(".user_request_delete").click(function () {
    var temp_this = $(this);
    var request_id = temp_this.attr("request_id");
    var requested_user_type = temp_this.attr("requested_user_type");

    $.ajax({
        type: "POST",
        url: site_url + "user/request/delete_request",
        data: {
            request_id: request_id,
            requested_user_type: requested_user_type
        },
        success: function (data) {
            if (data.trim() == "1") {
                temp_this.parents().eq(1).remove();
                $("#success_alert_message").text("Request deleted successfully.");
                $('.success_alert').slideDown(400);
                $('.success_alert').delay(4000).slideUp(400);
                if ($("#request_count").val() > 0) {
                    $("#request_count").val($("#request_count").val() - 1);
                    if ($("#request_count").val() == 0) {
                        var temp_html = '<div class="user_announce">\
                                            <div class="announce_details">\
                                            <h6>No pending request found.</h6>\
                                            </div>\
                                        </div>';
                        $("#request_div_content").html(temp_html);
                    }
                }
            } else {
                $("#danger_alert_message").text("Failed to delete request.Try later.");
                $('.danger_alert').slideDown(400);
                $('.danger_alert').delay(4000).slideUp(400);
            }
        }
    });
});

/* Accept the user request */
$(".user_request_confirm").click(function () {
    var temp_this = $(this);
    var request_id = temp_this.attr("request_id");
    var requested_user_type = temp_this.attr("requested_user_type");
    var college_id = temp_this.attr("college_id");
    if (request_id != '' && college_id != '') {
        $.ajax({
            type: "POST",
            url: site_url + "user/request/accept_admin_user_request",
            data: {
                request_id: request_id,
                college_id: college_id,
                requested_user_type: requested_user_type
            },
            success: function (data) {
                alert(data);

                if (data.trim() == "1") {
                    temp_this.parents().eq(1).remove();
                    $("#success_alert_message").text("Request Accepted successfully.");
                    $('.success_alert').slideDown(400);
                    $('.success_alert').delay(4000).slideUp(400);
                    if ($("#request_count").val() > 0) {
                        $("#request_count").val($("#request_count").val() - 1);
                        if ($("#request_count").val() == 0) {
                            var temp_html = '<div class="user_announce">\
                                            <div class="announce_details">\
                                            <h6>No pending request found.</h6>\
                                            </div>\
                                        </div>';
                            $("#request_div_content").html(temp_html);
                        }
                    }
                } else {
                    $("#danger_alert_message").text("Failed to Accept request.Try later.");
                    $('.danger_alert').slideDown(400);
                    $('.danger_alert').delay(4000).slideUp(400);
                }
            }
        });
    }
});

/****************************** Mahadev **********************************/
/* Get details before we confirm request */
$(".CheckBeforConfirm").click(function () {
    var temp_this = $(this);
    var request_id = temp_this.attr("request_id");
    var requested_user_type = temp_this.attr("requested_user_type");
    var college_id = temp_this.attr("college_id");
    var user_id = temp_this.attr("user_id");
    if (request_id != '' && college_id != '') {
        $.ajax({
            type: "POST",
            url: site_url + "user/request/CheckBeforConfirm_method",
            data: {
                user_id: user_id,
                request_id: request_id,
                college_id: college_id,
                requested_user_type: requested_user_type
            },
            success: function (data) {
//                console.log(data);
                $('#AppendData_Div').html('');
                $('#AppendData_Div').append(data);
                $('#UserRequestConfirm_Submit').attr('user_id', user_id);
                $('#UserRequestConfirm_Submit').attr('request_id', request_id);
                $('#UserRequestConfirm_Submit').attr('requested_user_type', requested_user_type);
                $('#UserRequestConfirm_Submit').attr('college_id', college_id);
                $('#CheckBeforConfirm_Modal').modal('show');
            }
        });
    }
});

//student user request on change of stream select box
$("body").on("change", "#SelectStreams", function () {
    var temp_this = $(this);
    var this_value = temp_this.val();
    $.ajax({
        type: "POST",
        url: site_url + "college/get_college_stream_courses",
        dataType: 'json',
        data: {
            stream_id: this_value,
            college_id: college_id
        },
        success: function (data) {
            var temp_course_select_html = '<option style="display:none" value="">-- Choose Your Course --</option>';
            var temp_semester_select_html = '<option style="display:none" value="">-- Choose Your Semester --</option>';
            if (data != 0) {
                $.each(data, function (index, value) {
                    if (index == "stream_course") {
                        $.each(value, function (inner_index, inner_value) {
                            temp_course_select_html += '<option value="' + inner_index + '">' + inner_value + '</option>';
                        });
                    }
                    if (index == "stream_semester") {
                        var temp_array = ['', 'First', 'Second', 'Third', 'Forth', 'Fifth', 'Sixth', 'Seventh', 'Eighth', 'Ninth', 'Tenth', 'Eleventh'];
                        $.each(value, function (inner_index, inner_value) {
                            if (inner_index == "semester") {
                                temp_semester_select_html = '<option style="display:none" value="">-- Choose Your Semester --</option>';
                            } else {
                                temp_semester_select_html = '<option style="display:none" value="">-- Choose Your Year --</option>';
                            }
                            for (var i = 1; i <= inner_value; i++) {
                                temp_semester_select_html += '<option value="' + i + '">' + temp_array[i] + '</option>';
                            }
                        });
                    }

                });
            }
            $("body #SelectCourse").html(temp_course_select_html);
            $("body #SelectSemester").html(temp_semester_select_html);
        }
    });
});

$("#UserRequestConfirm_Submit").click(function () {
    var temp_this = $(this);
    var Stream = $('#AppendData_Div #SelectStreams').val();
    var Course = $('#AppendData_Div #SelectCourse').val();
    var request_id = temp_this.attr("request_id");
    var requested_user_type = temp_this.attr("requested_user_type");
    var college_id = temp_this.attr("college_id");
    var Semester = "";

    if (requested_user_type == "alumni") {
        Semester = $('#AppendData_Div #SelectYearOfPassing').val();
    } else {
        Semester = $('#AppendData_Div #SelectSemester').val();
    }
    if (request_id != '' && college_id != '') {
        $.ajax({
            type: "POST",
            url: site_url + "user/request/accept_admin_user_request",
            data: {
                request_id: request_id,
                college_id: college_id,
                requested_user_type: requested_user_type,
                Stream: Stream,
                Course: Course,
                Semester: Semester
            },
            success: function (data) {
                if (data.trim() == "1") {
                    temp_this.parents().eq(1).remove();
                    $("#success_alert_message").text("Request Accepted successfully.");
                    $('.success_alert').slideDown(400);
                    $('.success_alert').delay(4000).slideUp(400);
                    if ($("#request_count").val() > 0) {
                        $("#request_count").val($("#request_count").val() - 1);
                        if ($("#request_count").val() == 0) {
                            var temp_html = '<div class="user_announce">\
                                            <div class="announce_details">\
                                            <h6>No pending request found.</h6>\
                                            </div>\
                                        </div>';
                            $("#request_div_content").html(temp_html);
                        }
                    }
                    var user_id = temp_this.attr('user_id');
                    $('.user_announce .request_handling_btns button[user_id=' + user_id + ']').parents('div.user_announce').fadeOut();
                    $('#CheckBeforConfirm_Modal').modal('hide');
                } else {
                    $("#danger_alert_message").text("Failed to Accept request.Try later.");
                    $('.danger_alert').slideDown(400);
                    $('.danger_alert').delay(4000).slideUp(400);
                }
            }
        });
    }

});