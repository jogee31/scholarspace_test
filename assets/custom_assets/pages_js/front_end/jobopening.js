$(document).ready(function () {
    var site_url = $("#site_url").val();
    var edit_jobopening_clicked_flag = null;

    //add new jobopening
    $("#addNewJobBtn").click(function () {
        $("#addNewJobOpeningModal").modal('show');
    });

    //add new course modal form submit
    $("#addNewJobOpeningModalForm").submit(function (e) {
        e.preventDefault();
        var valid_count = 0;
        if (!textValidation("#addNewJobOpeningModalForm")) {
            valid_count++;
        }
        if (!textAreaValidation("#addNewJobOpeningModalForm")) {
            valid_count++;
        }
        if (valid_count == 0) {
            var job_title = $("#jobTitleModalText").val();
            var job_description = $("#jobDescriptionModalTextarea").val();
            $.ajax({
                type: "POST",
                url: site_url + "user/jobopening/submit_add_new_jobopening",
                data: {
                    job_title: job_title,
                    job_description: job_description
                },
                success: function (data) {
                    if (data.trim() != 0) {
                        var job_id = data.trim();
                        var temp_html = '<li class="job_opening_list_li" jobopening_id = "' + job_id + '">\
                                            <h6>' + job_title + '</h6>\
                                            <p>' + job_description + '</p>\
                                            <div class="jobopening_action_btn_div">\
                                                <div class="row">\
                                                    <div class="col-lg-8"></div>\
                                                    <div class="col-lg-4">\
                                                        <button style="margin-left: 5px;" class="btn btn-default btn-xs pull-right delete_job_opening"><i class="fa fa-trash"></i></button>\
                                                        <button class="btn btn-default btn-xs pull-right edit_job_opening"><i class="fa fa-edit"></i></button>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </li>';
                        $("#job_opening_list_ul").prepend(temp_html);
                    }
                    $("#addNewJobOpeningModal").modal('hide');
                    $("#addNewJobOpeningModalForm")[0].reset();
                }
            });
        }
    });

    // edit jobopening
    $("body").on("click", ".edit_job_opening", function () {
        var temp_this = $(this);

        edit_jobopening_clicked_flag = temp_this;

        var jobopening_id = temp_this.parents().eq(3).attr('jobopening_id');
        var job_title = temp_this.parents().eq(2).siblings('h6').text();
        var job_description = temp_this.parents().eq(2).siblings('p').text();

        $("#jobopeningIdEditModalText").val(jobopening_id);
        $("#jobTitleEditModalText").val(job_title);
        $("#jobDescriptionEditModalTextarea").val(job_description);
        $("#editJobOpeningModal").modal("show");
    });

    /* submit edit jobopening id */
    $("#editJobOpeningModalForm").submit(function (e) {
        e.preventDefault();
        var temp_this = $(this);

        var valid_count = 0;
        if (!textValidation("#editJobOpeningModalForm")) {
            valid_count++;
        }
        if (!textAreaValidation("#editJobOpeningModalForm")) {
            valid_count++;
        }
        if (valid_count == 0) {
            var formData = $(this).serialize();
            $.ajax({
                type: "post",
                url: site_url + "user/jobopening/submit_edit_jobopening",
                data: formData,
                success: function (data) {
                    $("#editJobOpeningModal").modal('hide');
                    if (data.trim() == "1") {
                        var job_title = $("#jobTitleEditModalText").val();
                        var job_description = $("#jobDescriptionEditModalTextarea").val();
                        edit_jobopening_clicked_flag.parents().eq(2).siblings('h6').text(job_title);
                        edit_jobopening_clicked_flag.parents().eq(2).siblings('p').text(job_description);
                        alert("Job updated successfully.");
                    }
                    $("#editJobOpeningModalForm")[0].reset();
                }
            });
        }
    });

    // delete jobopening
    $("body").on("click", ".delete_job_opening", function () {
        var temp_this = $(this);
        var confirm_flag = confirm("Are you sure you want to delete this job?");
        if (confirm_flag) {
            var jobopening_id = temp_this.parents().eq(3).attr('jobopening_id');
            $.ajax({
                type: "post",
                url: site_url + "user/jobopening/delete_jobopening",
                data: {
                    jobopening_id: jobopening_id
                },
                success: function (data) {
                    if (data.trim() == "1") {
                        temp_this.parents().eq(3).remove();
                        alert("Job deleted successfully.");
                    }
                }
            });
        }
    });

    /* remove the required error msg */
    $("input, textarea, select").focus(function () {
        var temp_this = $(this);
        $(temp_this).siblings('label').children('.required_error_span').text("");
    });

    /* Validation check for text box */
    function textValidation(form_id) {
        var status = 1;
        $(form_id + ' input[type="text"]').each(function () {
            var temp_this = $(this);
            var title = temp_this.attr("title");
            if (temp_this.val().trim() == '') {
                status = 0;
                $(temp_this).siblings('label').children('.required_error_span').text("* " + title + " is Required");
            }
        });
        return status;
    }

    /* Validation check for textarea */
    function textAreaValidation(form_id) {
        var status = 1;
        $(form_id + ' textarea').each(function () {
            var temp_this = $(this);
            var title = temp_this.attr("title");
            if (temp_this.val().trim() == '') {
                status = 0;
                $(temp_this).siblings('label').children('.required_error_span').text("* " + title + " is Required");
            }
        });
        return status;
    }
});