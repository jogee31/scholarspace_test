var site_url = $("#site_url").val();
var base_url = $("#base_url").val();
var email_status = 1;
var phone_status = 1;

$("#reg_clg_email").on('keyup focusout', function () {
    var email = $(this).val();
    //check_unique_email
    $.ajax({
        type: "POST",
        url: site_url + "front_home/check_unique_email",
        data: {
            email: email
        },
        success: function (data) {
            email_status = data.trim();
        }
    });
});
$("#reg_clg_phone").on('keyup focusout', function () {
    var phone = $(this).val();
    //check_unique_email
    $.ajax({
        type: "POST",
        url: site_url + "front_home/check_unique_phone",
        data: {
            phone: phone
        },
        success: function (data) {
            phone_status = data.trim();
        }
    });
});
$('#register_college_form').submit(function (e) {
    var temp_this = $(this);
    var check = 0;
    if ($("#reg_clg_f_name").val() == '') {
        $("#reg_clg_f_name").addClass("required_border");
        check++;
    }
    if ($("#reg_clg_l_name").val() == '') {
        $("#reg_clg_l_name").addClass("required_border");
        check++;
    }
    if ($("#reg_clg_email").val() == '') {
        $("#reg_clg_email").addClass("required_border");
        check++;
    }
    if ($("#reg_clg_phone").val() == '') {
        $("#reg_clg_phone").addClass("required_border");
        check++;
    }
    if ($("#reg_clg_state").val() == '') {
        $("#reg_clg_state").addClass("required_border");
        check++;
    }
    if ($("#reg_clg_clg_name").val() == '') {
        $("#reg_clg_clg_name").addClass("required_border");
        check++;
    }
//reg_clg_city
    if ($("#reg_clg_city").val() == '') {
        $("#reg_clg_city").addClass("required_border");
        check++;
    }
    if (check) {
        e.preventDefault();
        $('#danger_message').text("Please fill required fields.");
        $('.danger_alert').slideDown(400);
    } else {
        //if logged_in then these fields won't appear for registraion of college
        //so here logged_in is the filed if not login then it will come 
        //n .length will check is this id is present or not
        if ($('#logged_in').length) {
            var emil_check = isValidEmailAddress($("#reg_clg_email").val());
            var phone_check = isValidPhone($("#reg_clg_phone").val());
            if (emil_check != true) {
                e.preventDefault();
                $('#danger_message').text("Please enter valid email.");
                $('.danger_alert').slideDown(400);
            }
            else if (phone_check != true) {
                e.preventDefault();
                $('#danger_message').text("Please enter valid phone.");
                $('.danger_alert').slideDown(400);
            } else if (email_status == 0) {
                e.preventDefault();
                $('#danger_message').text("Email is already exist, Try other email.");
                $('.danger_alert').slideDown(400);
            } else if (phone_status == 0) {
                e.preventDefault();
                $('#danger_message').text("Phone number is already exist, Try other.");
                $('.danger_alert').slideDown(400);
            }
        }
    }
});

$("#register_college_link").click(function () {
    $("input, textarea, select").val('');
    $(".alert-warning").slideUp(400);
    $('.danger_alert').slideUp(400);
    $(".required_border").removeClass("required_border");
});

/* remove the required error msg */
$("input, textarea, select").focus(function () {
    $(".alert-warning").slideUp(400);
    $('.danger_alert').slideUp(400);
    $(".required_border").removeClass("required_border");
});
/* Email validation function */
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
    return pattern.test(emailAddress);
}

/* Phine validation function */
function isValidPhone(phone) {
    var pattern = new RegExp(/^[7-9][0-9]{9}$/);
    return pattern.test(phone);
}

$(function () {
    var site_url = $('#site_url').val();
    $(".college_name_autocomplete").autocomplete({
        source: function (request, response)
        {
            $.ajax({
                type: "POST",
                url: site_url + "college/show_searched_college_list",
                dataType: "json",
                data: {
                    name: request.term,
                },
                success: function (data)
                {
                    response(data);
                }
            });
        }
    });
});