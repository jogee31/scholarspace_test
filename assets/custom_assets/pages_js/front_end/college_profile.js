$(document).ready(function () {
    var site_url = $("#site_url").val();
    var college_id = $("#college_id").val();
    $("#college_id").remove();
    var user_request_type;
    var user_name = "User";
    if ($("#user_name_hidden").val() != "undefine") {
        user_name = $("#user_name_hidden").val();
    }

    $("#send_college_request_btn").click(function () {
        var temp_this = $(this);
        var request_status = temp_this.attr('request_status');
        if (request_status == "not_sent") {
            $("#college_request_modal").modal("show");
        }
    });
    /* Send Request user type */
    $('body').on("click", ".modal_alum_reg", function () {
        var temp_this = $(this);
        var request_type = temp_this.attr("request_type");
        user_request_type = request_type;
        var footer_temp_html = '<div id="appended_modal_footer" class="modal-footer">\
                        <div class="row">&nbsp;</div>\
                        <div class="row">&nbsp;</div>\
                        <div class="row temp_row contact_info_reg" style="margin-right: 0px;margin-left: 0px;">\
                            <div class="col-md-7">&nbsp;</div>\
                            <div class="col-md-5">\
                                <button type="submit" style="padding-top: 8px; font-family: "open_sanslight"; font-size: 18px; padding-bottom:8px; border-radius: 0px;" class="btn submit_btn btn-primary">Proceed Here &nbsp;<i class="fa fa-long-arrow-right"></i></button>\
                            </div>\
                        </div>\
                    </div>';
        if (temp_this.hasClass('modal_alum_reg_active') == false) {
            $("#content_div").html("");
            $("#appended_modal_footer").remove();
        }

        /* if the request is as Student */
        if (request_type == 'student' && temp_this.hasClass('modal_alum_reg_active') == false) {
            var temp_html = '<div class="reg_agree row temp_row">\
                                <div class="col-md-12">\
                                    <select id="student_stream" name="student_stream">\
                                        <option style="display:none" value="">-- Choose your Stream --</option>\
                                    </select>\
                                    <input type="hidden" name="user_request_type" value="' + user_request_type + '"/>\
                                </div>\
                            </div>\
                            <div class="reg_agree row temp_row">\
                                <div class="col-md-12">\
                                    <select id="student_stream_course" disabled="" name="student_stream_course">\
                                        <option value="">-- Choose your Course --</option>\
                                    </select>\
                                </div>\
                            </div>\
                            <div class="reg_agree row temp_row">\
                                <div class="col-md-12">\
                                    <select id="student_semester_count" disabled="" name="student_semester_or_year">\
                                        <option value="">-- Choose your Semester --</option>\
                                    </select>\
                                </div>\
                            </div>';
            $.ajax({
                type: "POST",
                url: site_url + "college/get_college_stream",
                data: {
                    college_id: college_id
                },
                dataType: 'json',
                success: function (data) {
                    var temp_select_html = '<option style="display:none" value="">-- Choose Your Stream --</option>';
                    if (data != '0') {
                        $.each(data, function (index, value) {
                            temp_select_html += '<option value="' + index + '">' + value + '</option>';
                        });
                    }
                    $("#content_div").html(temp_html);
                    $("#student_stream").html(temp_select_html);
                    $("#request_modal").append(footer_temp_html);
                }
            });
        } else if (request_type == 'teacher' && temp_this.hasClass('modal_alum_reg_active') == false) {
            var temp_html = '';
        } else if (request_type == 'alumni' && temp_this.hasClass('modal_alum_reg_active') == false) {
            var temp_html = '\<div class="reg_agree row temp_row">\
                                <div class="col-md-12">\
                                    <select id="alumni_year" name="alumni_year">\
                                        <option style="display:none" value="">-- Choose your Passing year --</option>';
            var current_year = new Date().getFullYear();
            var start_year = 1990;
            for (var i = start_year; i < current_year; i++) {
                temp_html += '<option value="' + i + '">' + i + '</option>';
            }
            temp_html += '</select>\
                                </div>\
                            </div>\
                            <div class="reg_agree row temp_row">\
                                <div class="col-md-12">\
                                    <select id="alumni_stream" name="alumni_stream">\
                                        <option style="display:none" value="">-- Choose your Stream --</option>\
                                    </select>\
                                    <input type="hidden" name="user_request_type" value="' + user_request_type + '"/>\
                                </div>\
                            </div>\
                            <div class="reg_agree row temp_row">\
                                <div class="col-md-12">\
                                    <select id="alumni_stream_course" disabled="" name="alumni_stream_course">\
                                        <option value="">-- Choose your Course --</option>\
                                    </select>\
                                </div>\
                            </div>';
            $.ajax({
                type: "POST",
                url: site_url + "college/get_college_stream",
                data: {
                    college_id: college_id
                },
                dataType: 'json',
                success: function (data) {
                    var temp_select_html = '<option style="display:none" value="">-- Choose Your Stream --</option>';
                    if (data != '0') {
                        $.each(data, function (index, value) {
                            temp_select_html += '<option value="' + index + '">' + value + '</option>';
                        });
                    }
                    $("#content_div").html(temp_html);
                    $("#alumni_stream").html(temp_select_html);
                    $("#request_modal").append(footer_temp_html);
                }
            });
        }
        $(".modal_alum_reg").removeClass("modal_alum_reg_active");
        temp_this.addClass("modal_alum_reg_active");
    });

//student user request on change of stream select box
    $("body").on("change", "#student_stream", function () {
        var temp_this = $(this);
        var this_value = temp_this.val();
        $.ajax({
            type: "POST",
            url: site_url + "college/get_college_stream_courses",
            dataType: 'json',
            data: {
                stream_id: this_value,
                college_id: college_id
            },
            success: function (data) {
                var temp_course_select_html = '<option style="display:none" value="">-- Choose Your Course --</option>';
                var temp_semester_select_html = '<option style="display:none" value="">-- Choose Your Semester --</option>';
                if (data != 0) {
                    $.each(data, function (index, value) {
                        if (index == "stream_course") {
                            $.each(value, function (inner_index, inner_value) {
                                temp_course_select_html += '<option value="' + inner_index + '">' + inner_value + '</option>';
                            });
                        }
                        if (index == "stream_semester") {
                            var temp_array = ['', 'First', 'Second', 'Third', 'Forth', 'Fifth', 'Sixth', 'Seventh', 'Eighth'];
                            $.each(value, function (inner_index, inner_value) {
                                if (inner_index == "semester") {
                                    temp_semester_select_html = '<option style="display:none" value="">-- Choose Your Semester --</option>';
                                } else {
                                    temp_semester_select_html = '<option style="display:none" value="">-- Choose Your Year --</option>';
                                }
                                for (var i = 1; i <= inner_value; i++) {
                                    temp_semester_select_html += '<option value="' + i + '">' + temp_array[i] + '</option>';
                                }
                            });
                        }

                    });
                }
                $("body #student_stream_course").html(temp_course_select_html);
                $("body #student_stream_course").removeAttr("disabled");
                $("body #student_semester_count").html(temp_semester_select_html);
                $("body #student_semester_count").removeAttr("disabled");
            }
        });
    });

    //alumni user request on change of stream select box
    $("body").on("change", "#alumni_stream", function () {
        var temp_this = $(this);
        var this_value = temp_this.val();
        $.ajax({
            type: "POST",
            url: site_url + "college/get_college_stream_courses",
            dataType: 'json',
            data: {
                stream_id: this_value,
                college_id: college_id
            },
            success: function (data) {
                var temp_course_select_html = '<option style="display:none" value="">-- Choose Your Course --</option>';
                if (data != 0) {
                    $.each(data, function (index, value) {
                        if (index == "stream_course") {
                            $.each(value, function (inner_index, inner_value) {
                                temp_course_select_html += '<option value="' + inner_index + '">' + inner_value + '</option>';
                            });
                        }
                    });
                }
                $("body #alumni_stream_course").html(temp_course_select_html);
                $("body #alumni_stream_course").removeAttr("disabled");
            }
        });
    });

    /* Page From submit */
    $("#user_request_form").submit(function (e) {
        var temp_this_form = $(this);
        e.preventDefault();
        var required_count_flag = 0;
        if (user_request_type == 'student') {
            $("#user_request_form select").each(function () {
                var temp_this = $(this);
                if (temp_this.val() == '') {
                    temp_this.addClass("required_border");
                    required_count_flag++;
                }
            });
            //if all filed has filed
            if (required_count_flag == 0) {
                var formData = temp_this_form.serializeArray();
                //adding college id to formData
                var temp_college_id = {"name": "college_id", "value": college_id};
                formData.push(temp_college_id);
                //submit request using ajax
                $.ajax({
                    type: "POST",
                    url: site_url + "college/send_college_request",
                    data: formData,
                    success: function (data) {
                        if (data.trim() == '1') {
                            $("#send_college_request_btn").css("cursor", "default");
                            $("#send_college_request_btn").css("background-color", "#EAB973");
                            $("#send_college_request_btn").css("border", "1px solid #EAB973");
                            $("#send_college_request_btn").text("Request already sent");
                            $("#send_college_request_btn").attr("request_status", "already_sent");
                            var modal_body_html = '<div class="row">\
                                                        <div class="col-md-12">\
                                                            <span style="font-size: 15px;font-weight: 600;">Success !</span> Dear ' + user_name + ', Your request has been successfully sent for this college.\
                                                        </div>\
                                                    </div>';
                            var modal_footer_html = '<div class="row">\
                                                        <div class="col-md-4"></div>\
                                                        <div class="col-md-4">\
                                                            <button style="width: 100%; border-radius: 0px;" data-dismiss="modal" class="btn btn-success">Okay</button>\
                                                        </div>\
                                                        <div class="col-md-4"></div>\
                                                    </div>';
                            $("#college_request_modal .modal-body").html(modal_body_html);
                            $("#college_request_modal .modal-footer").html(modal_footer_html);
                        }
                    }
                });
            }
        } else if (user_request_type == 'teacher') {
            $.ajax({
                type: "POST",
                url: site_url + "college/send_college_request",
                data: {
                    college_id: college_id,
                    user_request_type: "teacher"
                },
                success: function (data) {
                    if (data.trim() == '1') {
                        $("#send_college_request_btn").css("cursor", "default");
                        $("#send_college_request_btn").css("background-color", "#EAB973");
                        $("#send_college_request_btn").css("border", "1px solid #EAB973");
                        $("#send_college_request_btn").text("Request already sent");
                        $("#send_college_request_btn").attr("request_status", "already_sent");
                        var modal_footer = '<div id="appended_modal_footer" class="modal-footer"></div>';
                        var modal_body_html = '<div class="row">\
                                                        <div class="col-md-12">\
                                                            <span style="font-size: 15px;font-weight: 600;">Success !</span> Dear ' + user_name + ', Your request has been successfully sent for this college.\
                                                        </div>\
                                                    </div>';
                        var modal_footer_html = '<div class="row">\
                                                        <div class="col-md-4"></div>\
                                                        <div class="col-md-4">\
                                                            <button style="width: 100%; border-radius: 0px;" data-dismiss="modal" class="btn btn-success">Okay</button>\
                                                        </div>\
                                                        <div class="col-md-4"></div>\
                                                    </div>';
                        $("#request_modal").append(modal_footer);
                        $("#college_request_modal .modal-body").html(modal_body_html);
                        $("#college_request_modal .modal-footer").html(modal_footer_html);
                    }
                }
            });
        } else if (user_request_type == 'alumni') {
            $("#user_request_form select").each(function () {
                var temp_this = $(this);
                if (temp_this.val() == '') {
                    temp_this.addClass("required_border");
                    required_count_flag++;
                }
            });
            //if all filed has filed
            if (required_count_flag == 0) {
                var formData = temp_this_form.serializeArray();
                //adding college id to formData
                var temp_college_id = {"name": "college_id", "value": college_id};
                formData.push(temp_college_id);
                $.ajax({
                    type: "POST",
                    url: site_url + "college/send_college_request",
                    data: formData,
                    success: function (data) {
                        if (data.trim() == '1') {
                            $("#send_college_request_btn").css("cursor", "default");
                            $("#send_college_request_btn").css("background-color", "#EAB973");
                            $("#send_college_request_btn").css("border", "1px solid #EAB973");
                            $("#send_college_request_btn").text("Request already sent");
                            $("#send_college_request_btn").attr("request_status", "already_sent");
                            var modal_body_html = '<div class="row">\
                                                        <div class="col-md-12">\
                                                            <span style="font-size: 15px;font-weight: 600;">Success !</span> Dear ' + user_name + ', Your request has been successfully sent for this college.\
                                                        </div>\
                                                    </div>';
                            var modal_footer_html = '<div class="row">\
                                                        <div class="col-md-4"></div>\
                                                        <div class="col-md-4">\
                                                            <button style="width: 100%; border-radius: 0px;" data-dismiss="modal" class="btn btn-success">Okay</button>\
                                                        </div>\
                                                        <div class="col-md-4"></div>\
                                                    </div>';
                            $("#college_request_modal .modal-body").html(modal_body_html);
                            $("#college_request_modal .modal-footer").html(modal_footer_html);
                        }
                    }
                });
            }
        }
        if (required_count_flag != 0) {
            $(".modal_danger_alert").slideDown("400");
        }
    });
    //send request as teacher
    $('body').on('click', '#send_teacher_request', function () {
        $("#user_request_form").submit();
    });
    //event is to remove required notification from the form
    $('body').on('change', 'select', function () {
        $("select").removeClass("required_border");
        $(".modal_danger_alert").slideUp("400");
    });
});