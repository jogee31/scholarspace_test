$(document).ready(function () {
    var site_url = $("#site_url").val();
    var hostel_description = $("#hotel_description_hidden").val();
    $("#hotel_description_hidden").remove();

    $(".hostel_details_edit_btn").click(function () {
        $("#hostelDescriptionModalTextarea").val(hostel_description);
        $("#editHostelDetailsModal").modal("show");
    });

    $("#hostelDescriptionModalForm").submit(function (e) {
        e.preventDefault();
        var temp_hostel_description = $("#hostelDescriptionModalTextarea").val();
        if (temp_hostel_description.trim() == '') {
            $("#hostelDescriptionModalTextarea").siblings('label').children('.required_error_span').text("* Hostel Description is Required");
        } else {
            $.ajax({
                type: "POST",
                url: site_url + "user/hostel/hostel_description_submit",
                data: {
                    hostel_description: temp_hostel_description
                },
                success: function (data) {
                    $("#editHostelDetailsModal").modal('hide');
                    if (data.trim() == '1') {
                        hostel_description = temp_hostel_description;
                        $("#hostel_description_details").text(temp_hostel_description);
                        alert("Hostel Description updated Successful.");
                    }
                    $("#hostelDescriptionModalForm")[0].reset();
                }
            });
        }
    });
    /* remove the required error msg */
    $("input, textarea, select").focus(function () {
        var temp_this = $(this);
        $(temp_this).siblings('label').children('.required_error_span').text("");
    });
});