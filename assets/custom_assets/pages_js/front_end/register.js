var site_url = $("#site_url").val();
var base_url = $("#base_url").val();
var email_unique_status = 1;
$(".captcha_response").parent('div').css("display", "none");
$(".trem_check_error").parent('div').css("display", "none");

$('form').submit(function (e) {
    var temp_this = $(this);
    var form_id = "#" + temp_this.attr('id');
    var temp_status = 1;
    $(".captcha_response").parent('div').css("display", "none");
    $(".captcha_response").text("");
    if (!textValidation(form_id)) {
        e.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
        temp_status = 0;
    }

    //email validation
    var emailAddress = $(".valid_email").val();
    if (emailAddress != '' && isValidEmailAddress(emailAddress) != true) {
        $(".email_error_msg").text("Please enter valid email.");
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
        temp_status = 0;
    }

    //phone validation
    var phone = $(".valid_phone").val();
    if (phone != '' && isValidPhone(phone) != true) {
        e.preventDefault();
        $(".phone_error_msg").text("Please enter valid Mobile Number.");
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
        temp_status = 0;
    }

    //password validation
    if ($("#password_check").val() != '' && $("#confirm_password").val() != '' && $("#password_check").val() != $("#confirm_password").val()) {
        e.preventDefault();
        $(".confirm_password_error_alert").text("Password doesn't match.");
        temp_status = 0;
    }

    //password length check
    if ($("#password_check").val() != '' && $("#password_check").val().length < 5) {
        e.preventDefault();
        $(".password_error_alert").text("Password length should be minimum 5.");
        temp_status = 0;
    }

    if (!email_unique_status) {
        e.preventDefault();
        $(".email_error_msg").text("Email alreay exist, try other.");
        temp_status = 0;
    }

    if (!$("#terms_check_box").is(":checked")) {
        e.preventDefault();
        $(".trem_check_error").text("In order to use our services, you must agree to our Terms of Service.");
        $(".trem_check_error").parent('div').css("display", "block");
        temp_status = 0;
    }

    if (temp_status) {
        $.ajax({
            type: "POST",
            url: site_url + "register/captcha_validation",
            async: false,
            data: {
                recaptcha_challenge_field: $('#recaptcha_challenge_field').val(),
                recaptcha_response_field: $('#recaptcha_response_field').val()
            },
            success: function (data) {
                if (data.trim() != '1') {
                    e.preventDefault();
                    Recaptcha.reload();
                    $(".captcha_response").parent('div').css("display", "block");
                        $(".captcha_response").text("Invalid Captch code.");
                }
            }
        });
    }
});

function textValidation(form_id) {
    var status = 1;
    $(form_id + ' input[type="text"],input[type="number"],input[type="password"]').each(function () {
        var temp_this = $(this);
        var this_id = temp_this.attr('id');
        if (this_id != "recaptcha_response_field" && temp_this.val() == '') {
            status = 0;
            $(temp_this).parent('div').siblings('.error_msg').text("This field is Required");
        }
        if (this_id == "recaptcha_response_field" && temp_this.val() == '') {
            status = 0;
            $(".captcha_error_msg").text("Captch code field is Required");
        }
    });
    return status;
}

/* Email validation function */
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
    return pattern.test(emailAddress);
}

function isValidPhone(phone) {
    var pattern = new RegExp(/^[7-9][0-9]{9}$/);
    return pattern.test(phone);
}

$("body").on("focus", "input", function () {
    $(".error_msg").text("");
});

$("#email").keyup(function () {
    var emailAddress = $(this).val();
    $.ajax({
        type: "POST",
        url: site_url + "register/check_unique_email",
        data: {
            email: emailAddress
        },
        success: function (data) {
            if (data.trim() == "0") {
                email_unique_status = 0;
                $(".email_error_msg").text("Email alreay exist, try other.");
            } else {
                email_unique_status = 1;
            }
        }
    });
});

var onloadCallback = function () {
    request_additional_info = grecaptcha.render('request_additional_info', {
        'sitekey': '6LfFBQgTAAAAALCGsNxskGwNGHyaDCGvNBtB1vZz',
    });
};

$('#terms_check_box').click(function () {
    if ($(this).prop("checked") == true) {
        $(".trem_check_error").text('');
        $(".trem_check_error").parent('div').css("display", "none");
    }
});