var site_url = $("#site_url").val();
var base_url = $("#base_url").val();
var college_id = $("#college_id").val();

$(document).ready(function () {
    $('.DeleteUserFromCollege_Button').click(function () {
        var ele = $(this);
        var user_id = ele.attr('user_id');
        var intranet_id = ele.attr('intranet_id');
        bootbox.confirm('Are you sure you want to Delete this?', function (ok) {
            if (ok) {
                if (user_id != "" && intranet_id != "") {
                    $.ajax({
                        type: "POST",
                        url: site_url + "user/members_list/DeleteMemberFromCollege_Method",
                        data: {
                            user_id: user_id,
                            intranet_id: intranet_id
                        },
                        success: function (data) {
                            if (data == '1') {
                                ele.parents('div.people_page_info').fadeOut();
                                $('body').animate({scrollTop: 0}, 'slow');
                                $('.success_alert').fadeIn();
                                setTimeout(function () {
                                    $('.success_alert').fadeOut();
                                }, 2500);
                            } else {
                                $('.danger_alert').fadeIn();
                                setTimeout(function () {
                                    $('.danger_alert').fadeOut();
                                }, 2500);
                            }

                        }
                    });
                }
            }
        });

    });
});