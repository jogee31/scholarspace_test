$(document).ready(function () {
    var value_check_post = 0;
    var site_url = $("#site_url").val();
    var base_url = $("#base_url").val();

    //group name
    var fixed_group_name = $("#fixed_group_name").val();
    $("#fixed_group_name").remove();

    var target_flag = 0;
    var content_flag = 1;
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            if (content_flag) {
                target_flag++;
                $(".scroller_loading_div").css("display", "block");
                $.ajax({
                    type: "POST",
                    url: site_url + "user/" + fixed_group_name + "/get_post_on_scroll",
                    data: {
                        target_flag: target_flag
                    },
                    success: function (data) {
                        $(".scroller_loading_div").css("display", "none");
                        if (data.trim() != 0) {
                            $("#all_post_content").append(data);
                        } else if (data.trim() == 0) {
                            content_flag = 0;
                        }
                    }
                });
            }

        }
    });

    $("body textarea").keyup(function (e) {
        $(this).height(20);
        $(this).height(this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth")));
    });

    $("body .share_text2").keyup(function (e) {
        $(this).height(20);
        $(this).height(this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth")));
    });

    $("#group_post_update_form").submit(function (e) {
        e.preventDefault();
        var temp_this = $(this);
        var formData = $(this).serialize();
        if (value_check_post) {
            $.ajax({
                type: "POST",
                url: site_url + "user/" + fixed_group_name + "/save_update_post",
                data: formData,
                success: function (data) {
                    if (data.trim() != '0') {
                        $('.share_box').css("display", "block");
                        $('.show_share').css("display", "none");
                        temp_this[0].reset();
                        $(".uploaded_file_li_post").remove();
                        $(".add_files").css("display", "none");
                        temp_this.children('.poll_share').children('.active_post_btn').removeClass('active_post_btn');
                        $("#all_post_content").prepend(data);
                    }
                }
            });
        }
    });
    $("body").on("keyup", "#update_textarea", function () {
        var temp_this = $(this);
        if (temp_this.val() != '') {
            value_check_post = 1;
            $("#update_post").addClass("active_post_btn");
        } else {
            value_check_post = 0;
            $("#update_post").removeClass("active_post_btn");
        }
    });
    $("body").on("keyup", ".reply_textarea", function () {
        var temp_this = $(this);
        if (temp_this.val() != '') {
            temp_this.parent('div').siblings('.reply_post_button').addClass("active_post_btn");
        } else {
            temp_this.parent('div').siblings('.reply_post_button').removeClass("active_post_btn");
        }
    });
    $("body").on("submit", ".reply_form", function (e) {
        e.preventDefault();
        var temp_this = $(this);
        var formData = temp_this.serialize();
        var reply_message = temp_this.children('.text_poll2').children('.reply_textarea').val();
        if (reply_message != '') {
            $.ajax({
                type: "POST",
                url: site_url + "user/" + fixed_group_name + "/save_post_reply",
                data: formData,
                success: function (data) {
                    if (data.trim() != '0') {
                        temp_this.parents().eq(1).siblings('.all_reply').append(data);
                        temp_this[0].reset();
                        temp_this.children('.reply_post_button').removeClass('active_post_btn');
                        temp_this.parent().siblings('.comment_in1').css("display", "block");
                        temp_this.parent().css("display", "none");
                    }
                }
            });
        }
    });
    //delete_post
    $("body").on("click", ".delete_post", function () {
        var temp_this = $(this);
        var result = confirm("Are you sure you want to delete this message?");
        if (result) {
            var post_id = temp_this.parents().eq(2).attr("post_id");
            $.ajax({
                type: "POST",
                url: site_url + "user/" + fixed_group_name + "/delete_post",
                data: {
                    post_id: post_id
                },
                success: function (data) {
                    if (data.trim() == "1") {
                        temp_this.parents().eq(6).remove();
                        $('.success_alert').slideUp(100);
                        $("#success_alert_message").text("The message has been deleted.");
                        $('.success_alert').slideDown(400);
                        $('.success_alert').delay(2000).slideUp(400);
                    }
                }
            });
        }
    });
    //delete post reply
    $("body").on("click", ".delete_post_reply", function () {
        var temp_this = $(this);
        var result = confirm("Are you sure you want to delete this message?");
        if (result) {
            var post_reply_id = temp_this.parents().eq(2).attr("post_reply_id");
            $.ajax({
                type: "POST",
                url: site_url + "user/" + fixed_group_name + "/delete_post_reply",
                data: {
                    post_reply_id: post_reply_id
                },
                success: function (data) {
                    if (data.trim() == "1") {
                        temp_this.parents().eq(6).remove();
                        $('.success_alert').slideUp(100);
                        $("#success_alert_message").text("The message has been deleted.");
                        $('.success_alert').slideDown(400);
                        $('.success_alert').delay(2000).slideUp(400);
                    }
                }
            });
        }
    });
    /* Group function */
    $("body").on("click", ".comment_in1", function () {
        $(this).siblings(".more_share").css('display', 'block');
        $(this).siblings(".more_share").children(".reply_form").children("div").children(".reply_textarea").focus();
        $(this).css('display', 'none');
    });
    $("body").on("click", ".click_more", function () {
        var temp_this = $(this);
        if (temp_this.hasClass('active_more')) {
            $(this).siblings('.more_list').hide();
            $(this).removeClass("active_more");
        } else {
            $('.more_list').hide();
            $(this).siblings('.more_list').show();
            $(".click_more").removeClass("active_more");
            temp_this.addClass("active_more");
        }
    });
    //post likes click
    $("body").on("click", ".post_like", function () {
        var temp_this = $(this);
        var post_id = temp_this.parent().attr("post_id");
        var like_action = temp_this.attr("like_action");
        var like_count = temp_this.attr("like_count");
        $.ajax({
            type: "POST",
            url: site_url + "user/" + fixed_group_name + "/like_post",
            data: {
                post_id: post_id,
                like_action: like_action
            },
            success: function (data) {
                if (data.trim() == '1') {
                    if (like_action == "like") {
                        temp_this.text("Unlike.");
                        temp_this.attr("like_action", "unlike");
                        like_count++;
                        temp_this.attr("like_count", like_count);
                        if (like_count == 1) {
                            $("#post_like_body-" + post_id).children('.current_user_like').text("You ");
                            $("#post_like_div-" + post_id).css("display", "block");
                        }
                    } else {
                        temp_this.text("Like.");
                        temp_this.attr("like_action", "like");
                        like_count--;
                        temp_this.attr("like_count", like_count);
                        if (like_count == 0) {
                            $("#post_like_div-" + post_id).css("display", "none");
                            $("#post_like_body-" + post_id).children('.current_user_like').text("");
                        }
                    }
                }
            }
        });
    });
    //post reply like
    $("body").on("click", ".post_reply_like", function () {
        var temp_this = $(this);
        var post_id = temp_this.parent().attr("post_id");
        var post_reply_id = temp_this.parent().attr("post_reply_id");
        var like_action = temp_this.attr("like_action");
        var like_count = temp_this.attr("like_count");
        $.ajax({
            type: "POST",
            url: site_url + "user/" + fixed_group_name + "/like_post_reply",
            data: {
                post_id: post_id,
                post_reply_id: post_reply_id,
                like_action: like_action
            },
            success: function (data) {
                if (data.trim() == '1') {
                    if (like_action == "like") {
                        temp_this.text("Unlike.");
                        temp_this.attr("like_action", "unlike");
                        like_count++;
                        temp_this.attr("like_count", like_count);
                        if (like_count == 1) {
                            $("#post_reply_like_body-" + post_reply_id).children('.current_user_post_reply_like').text("You ");
                            $("#post_reply_like_div-" + post_reply_id).css("display", "block");
                        }
                    } else {
                        temp_this.text("Like.");
                        temp_this.attr("like_action", "like");
                        like_count--;
                        temp_this.attr("like_count", like_count);
                        if (like_count == 0) {
                            $("#post_reply_like_div-" + post_reply_id).css("display", "none");
                            $("#post_reply_like_body-" + post_reply_id).children('.current_user_post_reply_like').text("");
                        }
                    }
                }
            }
        });
    });
    $('.pin2').click(function () {
        var temp_this = $(this);
        if (temp_this.hasClass('showing')) {
            temp_this.removeClass("showing");
            $('.attach_list').hide();
            $('.pin2').css('box-shadow', 'none');
        } else {
            temp_this.addClass("showing");
            $('.attach_list').show();
            $('.pin2').css('box-shadow', '0 1px 2px 1px rgba(0,0,0,.2)');
        }
    });
    $('body').on("click", ".expand_msg", function () {
        var temp_this = $(this);
        temp_this.parent('.truncated_body').css("display", "none");
        temp_this.parent('.truncated_body').siblings('.complete_body').css("display", "block");
    });
    $('body').on("click", ".collapse_msg", function () {
        var temp_this = $(this);
        temp_this.parent('.complete_body').css("display", "none");
        temp_this.parent('.complete_body').siblings('.truncated_body').css("display", "block");
    });
    /* file upload code start here */
    $("body").on("click", ".upload_file_from_computer", function () {
        $("#upload_post_file").trigger("click");
    });
    $("body").on("change", "#upload_post_file", function () {
        $('.attach_list').hide();
        $('.pin2').css('box-shadow', 'none');
        $(".add_files").css("display", "block");
        $(".pin2").removeClass("showing");
        $("#image_upload_form").trigger("submit");
    });
    $("#image_upload_form").submit(function (e) {
        e.preventDefault();
        $("#upload_processing_li").css("display", "block");
        process_li_append();
        var data = new FormData($("#image_upload_form")[0]);
        var file_name = ($("#upload_post_file").val()).split('\\');
        $.ajax({
            type: "POST",
            url: site_url + "user/" + fixed_group_name + "/image_upload_form",
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.trim() != 0) {
                    var temp_html = '<li class="uploaded_file_li_post">\
                                        <div class="company image_upload_item" id="remove_com">\
                                            <div class="row">\
                                                <div class="col-md-2">\
                                                    <div class="top_user_img">\
                                                        <img src="' + base_url + data + '">\
                                                        <input type="hidden" name="uploaded_file[]" value="' + data + '"/>\
                                                    </div>\
                                                </div>\
                                                <div class="col-md-8">\
                                                    <div class="pull-left uploaded_img_message_div front-end-font">\
                                                        <a>' + file_name[2] + '</a>\
                                                        <h5 class="file_msg">File attached successfully.</h5>\
                                                    </div>\
                                                </div>\
                                                <div class="col-md-2">\
                                                    <span file_path="' + data + '" class="file_upload_cancel pop_close pull-right"></span>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </li>';
                    process_li_remove();
                    $("#file_upload_content_ul").append(temp_html);
                    $('#image_upload_form')[0].reset();
                    $("#update_post").addClass("active_post_btn");
                    value_check_post++;
                }
            }
        });
    });
    $('body').on("click", ".file_upload_cancel", function () {
        var temp_this = $(this);
        var file_path = temp_this.attr("file_path");
        if (file_path != '') {
            $.ajax({
                type: "POST",
                url: site_url + "user/" + fixed_group_name + "/remove_file_upload",
                data: {
                    file_path: file_path
                },
                success: function (data) {
                    if (data.trim() == "1") {
                        var count = 0;
                        temp_this.parents().eq(3).remove();
                        $(".uploaded_file_li_post").each(function () {
                            count++;
                        });
                        if (count == 0) {
                            $(".add_files").css("display", "none");
                            $("#update_post").removeClass("active_post_btn");
                            if ($("#update_textarea").val() == '') {
                                value_check_post = 0;
                            }
                        }
                    }
                }
            });
        }
    });
    //append progres bar list
    function process_li_append() {
        var temp_proccess_li = '<li id="temp_upload_processing_li">\
                                    <div class="company image_upload_item" id="remove_com">\
                                        <div class="row">\
                                            <div class="col-md-2">\
                                                <div class="top_user_img">\
                                                    <img src="http://localhost/scholarspace/assets_front/image/upload_file1.png">\
                                                </div>\
                                            </div>\
                                            <div class="col-md-10">\
                                                <div style="width: 100%" class="pull-left uploaded_img_message_div front-end-font">\
                                                    <h5>\
                                                        <div class="progress progress-sm active" style="margin-top: 13px; width: 100%; height: 15px !important;">\
                                                            <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 100%;border-radius: 5px;">\
                                                                <span class="sr-only">20% Complete</span>\
                                                            </div>\
                                                        </div>\
                                                    </h5>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </li>';
        $("#file_upload_content_ul").append(temp_proccess_li);
    }

    //remove progres bar list
    function process_li_remove() {
        $("#temp_upload_processing_li").remove();
    }
    /* file upload code Ends here */

    $('body .fancybox-thumbs').fancybox({
        prevEffect: 'none',
        nextEffect: 'none',
        closeBtn: false,
        arrows: false,
        nextClick: true,
        helpers: {
            thumbs: {
                width: 50,
                height: 50
            }
        }
    });
});
