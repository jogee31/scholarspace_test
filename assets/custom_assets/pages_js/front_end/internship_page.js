$(document).ready(function () {
    var site_url = $("#site_url").val();
    var edit_click = null;

    $("#addNewInternship").click(function () {
        $("#addIntershipModal").modal('show');
    });

    $("#addInternshipModalForm").submit(function (e) {
        e.preventDefault();
        var internshipTitle = $("#internshipTitleModalText").val();
        if (internshipTitle.trim() == '') {
            $("#internshipTitleModalText").siblings('label').children('.required_error_span').text("* Internship title is Required");
        } else {
            $.ajax({
                type: "POST",
                url: site_url + "user/internship/submit_add_internship",
                data: {
                    internshipTitle: internshipTitle
                },
                success: function (data) {
                    $("#addIntershipModal").modal('hide');
                    if (data.trim() != '0') {
                        var temp_html_list = '<div class="row" style="padding-bottom: 7px">\
                                                    <div class="col-md-8"><li id="li-#' + data.trim() + '">' + internshipTitle + '</li></div>\
                                                    <div class="col-md-4">\
                                                        <button style="margin-left: 5px;" class="delete_internship btn btn-default btn-xs pull-right"><i class="fa fa-trash"></i></button>\
                                                        <button class="edit_internship btn btn-default btn-xs pull-right"><i class="fa fa-edit"></i></button>\
                                                    </div>\
                                                </div>';
                        if ($('.odd_list .row').length == $('.even_list .row').length || $('.odd_list .row').length < $('.even_list .row').length) {
                            $('.odd_list').append(temp_html_list);
                        }
                        else if ($('.odd_list .row').length > $('.even_list .row').length) {
                            $('.even_list').append(temp_html_list);
                        }
                        alert("Intership added Successful.");
                    }
                    $("#addInternshipModalForm")[0].reset();
                }
            });
        }
    });

    $("body").on("click", ".edit_internship", function () {
        var temp_this = $(this);
        edit_click = temp_this;
        var internship_title = temp_this.parent('div').siblings('.col-md-8').children('li').text();
        var internship_id = temp_this.parent('div').siblings('.col-md-8').children('li').attr("id");

        $("#internshipIdEditModalText").val(internship_id);
        $("#internshipTitleEditModalText").val(internship_title);
        $("#editIntershipModal").modal("show");
    });

    $("#editInternshipModalForm").submit(function (e) {
        e.preventDefault();
        var internshipTitle = $("#internshipTitleEditModalText").val();
        var internshipTempID = $("#internshipIdEditModalText").val();
        var internshipID = internshipTempID.split("-#");
        if (internshipTitle.trim() == '') {
            $("#internshipTitleEditModalText").siblings('label').children('.required_error_span').text("* Internship title is Required");
        } else if (internshipID[1] != '') {
            $.ajax({
                type: "POST",
                url: site_url + "user/internship/submit_edit_internship",
                data: {
                    internshipTitle: internshipTitle,
                    internshipID: internshipID[1]
                },
                success: function (data) {
                    $("#editIntershipModal").modal('hide');
                    if (data.trim() == '1') {
                        edit_click.parent('div').siblings('.col-md-8').children('li').text(internshipTitle);
                        edit_click = null;
                        alert("Intership Updated Successful.");
                    }
                    $("#editInternshipModalForm")[0].reset();
                }
            });
        }
    });

    $("body").on("click", ".delete_internship", function () {
        var temp_this = $(this);
        var temp_internship_id = temp_this.parent('div').siblings('.col-md-8').children('li').attr("id");

        var internshipID = temp_internship_id.split("-#");
        if (internshipID[1] != '') {
            var result = confirm("Are you sure, to delete this internship ?");
            if (result) {
                $.ajax({
                    type: "POST",
                    url: site_url + "user/internship/delete_internship",
                    data: {
                        internshipID: internshipID[1]
                    },
                    success: function (data) {
                        if (data.trim() == '1') {
                            temp_this.parents().eq(1).remove();
                        }
                    }
                });
            }
        }
    });
});