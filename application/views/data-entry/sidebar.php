<!-- =============================================== -->
<?php
//current controller and method name
$current_location = "data-entry/" . $this->router->fetch_class() . "/" . $this->router->fetch_method();
?>
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image"></div>
            <div class="pull-left info">
                <p><i class="fa fa-fw fa-user"></i>Admin</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="<?php echo ($current_location == 'data-entry/home/index') ? "active" : ""; ?>"><a href="<?php echo site_url('data-entry/home'); ?>"><i class="fa fa-home"></i> Dashboard</a></li>
        </ul>
        <ul class="sidebar-menu">
            <li class="<?php echo ($current_location == 'data-entry/college/register') ? "active" : ""; ?>"><a href="<?php echo site_url('data-entry/college/register'); ?>"><i class="fa fa-edit"></i> Register College</a></li>
        </ul>
        <ul class="sidebar-menu">
            <li class="<?php echo ($current_location == 'data-entry/stream/index') ? "active" : ""; ?>"><a href="<?php echo site_url('data-entry/stream/index'); ?>"><i class="fa fa-book"></i> Manage Stream</a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->