<?php
$this->load->view("data-entry/header");
$this->load->view("data-entry/sidebar");
?>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<style type="text/css">
    .danger_alert{
        display: none;
    }
    .success_alert{
        display: none;
    }
</style>
<div class="content-wrapper" style="min-height: 948px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-fw fa-book"></i>
            Manage Stream
        </h1> 
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#">Manage Stream</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="success_alert alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <h4>  <i class="icon fa fa-check"></i> Alert!</h4>
            <span id="success_alert_message"></span>
        </div>
        <div class="danger_alert alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <span id="danger_alert_message"></span>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h4 class="box-title">
                                    Stream List
                                </h4>
                                <button class="btn btn-success pull-right" data-toggle="modal" data-target="#add_new_stream_modal"><i class="fa fa-plus"></i> Add new Stream</button>
                            </div>
                            <div class="box-body">
                                <table id="view_stream_table" class="table table-bordered table-hover dataTable" aria-describedby="example2_info">
                                    <thead>
                                        <tr>
                                            <th style="width: 15%">S.No.</th>
                                            <th>Stream List</th>
                                        </tr>
                                    </thead>
                                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                                        <?php
                                        $count = 1;
                                        foreach ($stream_list as $key => $value) {
                                            ?>
                                            <tr>
                                                <td><?php echo $count; ?></td>
                                                <td><?php echo $value->title; ?></td>
                                            </tr>
                                            <?php
                                            $count++;
                                        }
                                        foreach ($contemporary_stream_list as $key => $value) {
                                            ?>
                                            <tr>
                                                <td><?php echo $count; ?></td>
                                                <td><?php echo $value->title; ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Course List</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!-- general form elements disabled -->
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <div class="row">
                                    <div class="col-md-3" style="margin-top: 10px;"><h4 class="box-title">
                                            Courses List of :
                                        </h4></div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <div>
                                                <?php
                                                //for getting the first element's key
                                                reset($course_list);
                                                $first_key = key($course_list);
                                                ?>
                                                <select id="stream_select" name="stream" class="form-control">
                                                    <?php
                                                    foreach ($stream_list as $value) {
                                                        ?>
                                                        <option <?php echo ($value->id == $first_key) ? "selected" : ""; ?> value="<?php echo $value->id . "-existing"; ?>"><?php echo $value->title; ?></option>
                                                        <?php
                                                    }
                                                    foreach ($contemporary_stream_list as $key => $value) {
                                                        ?>
                                                        <option value="<?php echo $value->id . "-contemporaty"; ?>"><?php echo $value->title; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <button class="btn btn-success pull-right" id="add_new_stream_course_btn" data-toggle="modal" data-target="#add_new_course_modal"><i class="fa fa-plus"></i> Add new Course</button>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div id="example2_wrapper" class="dataTables_wrapper form-inline" role="grid">
                                    <table id="view_stream_course_table" class="table table-bordered table-hover dataTable" aria-describedby="example2_info">
                                        <thead>
                                            <tr>
                                                <th>Course List</th>
                                            </tr>
                                        </thead>
                                        <tbody id="view_stream_course_tbody" role="alert" aria-live="polite" aria-relevant="all">
                                            <?php
                                            foreach ($course_list[$first_key] as $key => $value) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $value->title; ?></td>
                                                </tr>
                                                <?php
                                                $count++;
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Course List</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

        </div>
        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>"/>
        <input type="hidden" id="site_url" value="<?php echo site_url(); ?>"/>
    </section>
</div>
<?php
$this->load->view("data-entry/view_stream_modal");
$this->load->view("data-entry/footer");
?>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/formValidation.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/data-entry/view_stream.js" type="text/javascript"></script>
<?php
$session_flash = '';
$session_flash = $this->session->flashdata('message_success');

if ($session_flash != '') {
    ?>
    <script>
        $("#success_alert_message").text("College Added Successfully.");
        $('.success_alert').slideDown(400);
        $('.success_alert').delay(2000).slideUp(400);
    </script>
    <?php
}

$session_flash = '';
$session_flash = $this->session->flashdata('message_danger');
if ($session_flash != '') {
    ?>
    <script>
        $("#danger_alert_message").text("Failed To Add College Details.");
        $('.danger_alert').slideDown(400);
        $('.danger_alert').delay(2000).slideUp(400);
    </script>
    <?php
}
?>