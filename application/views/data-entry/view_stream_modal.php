<!-- Modal to add new stream -->
<div class="modal fade" id="add_new_course_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add new Course</h4>
            </div>
            <form class="bootstrap_modal" id="add_stream_course_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Course Name * <span class="required_error_span"></span></label>
                                <div>
                                    <input type="text" id="stream_course" name="stream_course" class="form-control text_required" placeholder="Enter Course">
                                    <input type="hidden" id="stream_id" name="stream_id"/>
                                </div><!-- /.input group -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal to add new stream -->
<div class="modal fade" id="add_new_stream_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add new Stream</h4>
            </div>
            <form action="<?php echo site_url('data-entry/stream/add_new_stream'); ?>" method="post" class="bootstrap_modal" id="add_stream_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Stream Name * <span class="required_error_span"></span></label>
                                <div><input type="text" id="stream" name="stream" class="form-control text_required" placeholder="Enter Stream"></div><!-- /.input group -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>