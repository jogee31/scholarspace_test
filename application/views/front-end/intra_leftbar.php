<?php include('header2.php') ?>
<div class="intranet_details">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="intra_sidebar">
                    <div class="intra_user">
                        <div class="intra_user_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                        </div>
                        <h6>
                            <?php
                            $user = $this->ion_auth->user()->row();
                            echo $user->first_name . " " . $user->last_name;
                            ?>
                        </h6>
                    </div>
                    <div class="boards_tab">

                        <div class="groups">
                            <a href="#">  Groups </a> <span class="plus"></span>
                        </div>

                        <div id="board_list">
                            <!-- Nav tabs -->
                            <ul>
                                <li class="<?php echo ("user/" . $current_location == "user/dashboard/index") ? "active" : ""; ?>" > <a href="<?php echo site_url("user/dashboard/"); ?>">Home</a></li>
                                <li class="<?php echo ("user/" . $current_location == "user/teacher/index") ? "active" : ""; ?>" > <a href="<?php echo site_url("user/teacher"); ?>">Add Teacher</a></li>
                                <li class="<?php echo ("user/" . $current_location == "user/request/index") ? "active" : ""; ?>" > <a href="<?php echo site_url("user/request/"); ?>">Users Request
                                        <?php
                                        if ($total_requst_count) {
                                            ?>
                                            <span class="board_no"><?php echo $total_requst_count; ?></span>
                                            <?php
                                        }
                                        ?>
                                    </a> 
                                </li>
                                <li class="<?php echo ("user/" . $current_location == "user/members_list/college") ? "active" : ""; ?>" > <a href="<?php echo site_url("user/members_list/college/"); ?>">College Users</a></li>
                                <li><a href="<?php echo site_url("user/dashboard/"); ?>">College Profile</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- end of boards tab -->

                    <div class="browse_pop"> <span class="br_icon"></span> <a href="#">Browse Groups</a> </div>
                    <div class="create_pop"> <span class="br_icon2"></span> <a href="#" data-toggle="modal" data-target="#myModal">Create Group</a> </div>

                    <div class="create_pop"> 
                        <ul>
                            <li><a href="#">Tools</a></li>
                            <li><a href="intra_course.php">Courses</a></li>
                            <li><a style="<?php echo ("user/" . $current_location == "user/jobopening/index") ? "color:#3e75c7" : ""; ?>" href="<?php echo site_url("user/jobopening"); ?>">Job Opening</a></li>
                            <li><a style="<?php echo ("user/" . $current_location == "user/internship/index") ? "color:#3e75c7" : ""; ?>" href="<?php echo site_url("user/internship"); ?>">Internship</a></li>
                            <li><a style="<?php echo ("user/" . $current_location == "user/hostel/index") ? "color:#3e75c7" : ""; ?>" href="<?php echo site_url("user/hostel"); ?>">Hostel</a></li>
                            <li><a href="fee_structure_page.php">Fees</a></li>
                        </ul>
                    </div>
                </div>
            </div>










