<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Scholar Space | Home page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link href="<?php echo base_url(); ?>assets_front/css/style.css" rel="stylesheet" type="text/css"/>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/bootstrap.css">
        <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400italic' rel='stylesheet' type='text/css'>
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <style type="text/css">
            .error_alert{
                color: #D04141;
                font-family: 'PT Sans', sans-serif;
                font-size: inherit;
            }
            .incorrect_error_msg p{
                text-align: center;
                font-size: 18px;
            }
        </style>
        <div class="header">

            <div class="top">

                <div class="row">

                    <div class="col-md-5">

                        <div class="logo">
                            <a href="<?php echo site_url("/"); ?>"><img src="<?php echo base_url(); ?>assets_front/image/logo.png" alt=""></a>
                        </div>

                    </div>          

                    <div class="col-md-7">

                        <div class="top_links">

                            <ul>
                                <li> <a href="#">Skill Maps</a> </li>
                                <li> <a href="#">College Ranking</a> </li>
                                <li> <a href="#">Admission Calendar</a> </li>
                                <li id="no_border"> <a href="#">Discussion Forum</a> </li>
                                <?php
                                if (!$this->ion_auth->logged_in()) {
                                    ?>
                                    <li id="log_box"> <a href="#" id="login_register_btn" data-toggle="modal" data-target="#myModal_login">Login / Register</a> </li>
                                    <?php
                                } else {
                                    $user_details = $this->ion_auth->user()->row();
                                    ?>
                                    <li><a id="logout_user" style="border: 0px; cursor: pointer; font-family: inherit;">Logout : <?php echo $user_details->first_name . " " . $user_details->last_name; ?></a></li>
                                    <?php
                                }
                                ?>
                                <!--<a href="#" data-toggle="modal" data-target="#myModal_login">Login / Register</a>-->
                            </ul>
                            <?php
                            if (!$this->ion_auth->logged_in()) {
                                ?>
                                <div class="social_icons">
                                    <ul>
                                        <li> <a href="#"><span class="fb"></span></a> </li>
                                        <li> <a href="#"><span class="tw"></span></a> </li>
                                        <li> <a href="#"><span class="go"></span></a> </li>
                                        <li> <a href="#"><span class="li"></span></a> </li>
                                    </ul>

                                </div>
                                <?php
                            }
                            ?>
                        </div>

                    </div>



                </div>

            </div>
            <!-- end of top -->

            <div class="middle">
                <div class="main_menu">
                    <!-- Static navbar -->
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <!--    <a class="navbar-brand" href="#">Project name</a> -->
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="#">engineering</a></li>
                                    <li><a href="#">mba</a></li>
                                    <li><a href="#">medicine</a></li>
                                    <li><a href="#">humanities</a></li>
                                    <li><a href="#">economics</a></li>
                                    <li><a href="#">ias</a></li>
                                    <li><a href="#">bank</a></li>
                                    <li><a href="#">scholarship</a></li>
                                    <li><a href="#">my college space</a></li>

                                    <!--           <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                  <li><a href="#">Action</a></li>
                                                  <li><a href="#">Another action</a></li>
                                                  <li><a href="#">Something else here</a></li>
                                                  <li role="separator" class="divider"></li>
                                                  <li class="dropdown-header">Nav header</li>
                                                  <li><a href="#">Separated link</a></li>
                                                  <li><a href="#">One more separated link</a></li>
                                                </ul>
                                              </li> -->
                                </ul>
                                <!--   <ul class="nav navbar-nav navbar-right">
                                    <li class="active"><a href="./">Default <span class="sr-only">(current)</span></a></li>
                                    <li><a href="../navbar-static-top/">Static top</a></li>
                                    <li><a href="../navbar-fixed-top/">Fixed top</a></li>
                                  </ul> -->
                            </div><!--/.nav-collapse -->
                        </div><!--/.container-fluid -->
                    </nav>

                </div>


            </div>

        </div>
        <!-- end of header -->
        <?php
        if (!$this->ion_auth->logged_in()) {
            ?>
            <div class="main_page_login">
                <div class="modal fade" id="myModal_login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div style="border-radius: 0px" class="modal-content">
                            <div class="modal-header">
                                <div class="row">
                                    <div class="col-md-12" style="text-align: center">
                                        <span style="font-family: 'PT Sans', sans-serif;
                                              font-size: 20px;
                                              color: #466677;">Login to your account</span>
                                        <span class="pull-right" style="cursor: pointer" data-dismiss="modal" aria-label="Close" >X</span>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-body">

                                <form id="login_form_pop" action="<?php echo site_url("auth/login"); ?>" method="post" accept-charset="utf-8">
                                    <div class="login_input">
                                        <span class="error_alert incorrect_error_msg"></span>
                                        <div class="col-md-12">
                                            <label> Username <span class="username_error_alert error_alert"></span></label>
                                            <input type="text" name="identity" value="" id="identity" style="padding-left: 10px;"  class="log_text"></div>
                                    </div>
                                    <div class="login_input">
                                        <div class="col-md-12">
                                            <label> Password <span class="password_error_alert error_alert"></span></label>
                                            <input style="padding-left: 10px;"  value="" id="password" type="password" class="log_text" name="password"></div>
                                    </div>
                                    <div>
                                        <div style="margin-top: 15px;" class="col-md-6"> <a style="cursor: pointer" id="forgot_link">Forgot Password?</a> </div>
                                        <div style="margin-top: 10px;" class="col-md-6">
                                            <button type="submit" style="border-radius: 0px;width: 100%;" class="btn btn-primary">Login Here</button>
                                        </div>
                                    </div>
                                </form>
                                <div class="login_input">
                                    <div class="col-md-12"><h6> <span>Or</span> </h6></div>
                                </div>
                                <div class="login_social">
                                    <div class="col-md-12">
                                        <div class="fb_login">
                                            <span class="fb_log"></span>
                                            <a href="#"> Login with Facebook </a>
                                        </div>
                                    </div>
                                    <div style="margin-top: 5px;margin-bottom: 5px" class="col-md-12">
                                        <div class="go_login">
                                            <span class="go_log"></span>
                                            <a href="#"> Login with Google </a>
                                        </div>
                                    </div>
                                    <div style="margin-top: 5px;margin-bottom: 20px;" class="col-md-12">
                                        <div style="background-color: #308cbd; text-align: center" class="go_login">
                                            <a href="<?php echo site_url("register/"); ?>" style="cursor: pointer; border-left:none"> Create a Scholarspace account </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="row">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>