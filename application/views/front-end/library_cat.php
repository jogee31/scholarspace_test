<?php include('header1.php') ?> 

<div class="body_height">

<div class="college_details">
<div class="row">
<div class="col-md-1">
<?php include('left_sidebar2.php') ?>

</div>

<div class="col-md-9" style="margin-left: -56px;">

<div class="about_colleges" style="padding: 15px 18px 0px;">

<div class="coll_info" style="border-bottom:none; margin-top: 27px;">

<div class="col-md-2">

<div class="coll_logo">

<img src="image/coll_logo5.png" alt="">

</div>

</div>

<div class="col-md-10">

<div class="course_details">
<div class="course_social">
<h5> Common Admission Test (CAT) <span class="cat_color">National Level Exam/Admission for P.G</span> </h5>

<div class="fg_share">
<a href="#"> <span class="f_like"></span> </a> <a href="#"> <span class="share_fb"></span> </a>
</div>

</div>

<div class="web_location">
<p>The Common Admission Test (CAT) is a national Level online test conducted by the Indian Institutes of Management as a pre-requisite for admission to various management programmes of IIMs and other Top Bschools of Country.The admission of the 13 IIMs... <a href="#">Readmore</a></p>

</div>

<!-- <div class="rating"><h6>Rating </h6> <span class="star"></span>   <button class="btn">Add your Rating</button>  
</div> -->

</div>


</div>
</div>
<!-- end of coll-info 1 -->

<div class="detail_tab" id="second_tab">

<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home1" aria-controls="home1" role="tab" data-toggle="tab">About
    </a> <span class="pointer"></span>   </li>
    <li role="presentation"><a href="#profile1" aria-controls="profile1" role="tab" data-toggle="tab">Dates & Results </a> <span class="pointer"></span>   </li>
    <li role="presentation"><a href="#messages1" aria-controls="messages1" role="tab" data-toggle="tab">Fees </a> 
      <span class="pointer"></span>  </li>
    <li role="presentation"><a href="#settings1" aria-controls="settings1" role="tab" data-toggle="tab">Elibility</a> <span class="pointer"></span>  </li>

<li role="presentation"><a href="#top_coll" aria-controls="top_coll" role="tab" data-toggle="tab">Top colleges</a> <span class="pointer"></span>  </li>

<li role="presentation"><a href="#eligible" aria-controls="eligible" role="tab" data-toggle="tab">Elibility</a> <span class="pointer"></span></li>

  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home1">

  <p>CAT 2015 - CAT or the Common Admission Test is the most competitive MBA entrance exam of India taken by around 2 lakh candidates each year. Conducted by the Indian Institutes of Management, CAT is the most popular entrance exam for the admission to 21 IIMs and around 100 B-schools across India. <br>
<span>CAT 2015 Eligibility Criteria:</span> The CAT 2015 Eligibility Criteria requires minimum 50% aggregate mark in graduation in any discipline from a recognized institute. The candidates from SC, ST, OBC and DA category need an aggregate of 45% marks. Final year graduation students can also apply provided they can submit the graduation final marks to the institutes within the requisite schedule.</p>

    </div>


    <div role="tabpanel" class="tab-pane" id="profile1">
       
        <p>CAT 2015 - CAT or the Common Admission Test is the most competitive MBA entrance exam of India taken by around 2 lakh candidates each year. Conducted by the Indian Institutes of Management, CAT is the most popular entrance exam for the admission to 21 IIMs and around 100 B-schools across India. 
CAT 2015 Eligibility Criteria: The CAT 2015 Eligibility Criteria requires minimum 50% aggregate mark in graduation in any discipline from a recognized institute. The candidates from SC, ST, OBC and DA category need an aggregate of 45% marks. Final year graduation students can also apply provided they can submit the graduation final marks to the institutes within the requisite schedule.</p>

    </div>


    <div role="tabpanel" class="tab-pane" id="messages1">
      <ul>     
  <li> Wells stocked library </li>
  <li> Separate hostel facility for boys & girls  </li>
  <li> State of the art laboratories </li>
  <li> Workshop  </li>
  <li> Computer centre </li>
  <li> Digital Classroom </li>
  <li> Wi-Fi facility </li>
  <li> Bank/ATM </li>
  <li> Indoor & Outdoor sports facility </li>
  <li> Medical facility </li>
  <li> Transportation facility </li>
  <li> Cafeteria </li>

      </ul>

    </div>


    <div role="tabpanel" class="tab-pane" id="settings1">

      <ul>     
  <li> Wells stocked library </li>
  <li> Separate hostel facility for boys & girls  </li>
  <li> State of the art laboratories </li>
  <li> Workshop  </li>
  <li> Computer centre </li>
  <li> Digital Classroom </li>
  <li> Wi-Fi facility </li>
  <li> Bank/ATM </li>
  <li> Indoor & Outdoor sports facility </li>
  <li> Medical facility </li>
  <li> Transportation facility </li>
  <li> Cafeteria </li>

      </ul>

    </div>


    <div role="tabpanel" class="tab-pane" id="top_coll"></div>

<div role="tabpanel" class="tab-pane" id="eligible"></div>


  </div>

</div>

</div>
<!-- end of second tab -->

<div class="coll_last_img">

<div class="coll_link">
<div class="coll_logo2">
<img src="image/coll_logo6.png" alt="">
</div>

<a href="#">XAT</a>
</div>


<div class="coll_link">
<div class="coll_logo2">
<img src="image/coll_logo7.png" alt="">
</div>

<a href="#">CMAT</a>
</div>

<div class="coll_link">
<div class="coll_logo2">
<img src="image/coll_logo8.png" alt="">
</div>

<a href="#">IIFT</a>
</div>

<div class="coll_link">
<div class="coll_logo2">
<img src="image/coll_logo9.png" alt="">
</div>

<a href="#">IBSAT</a>
</div>



</div>



</div>
<!-- end of about_college -->

</div>
<!-- end of college listing details -->

<div class="col-md-2" style="margin-left:15px">

<div class="refine_tree" style="padding: 29px 0px 0px;">

<div class="refine_search2" style="margin-bottom: 0px;">
   <div style="border-bottom: 1px solid #d3d3d3; width:100%; float:left">
 <h5> <span class="msg"></span> Recommended discussions</h5>
  </div>


<ul>
<li>The plane is divided into convex heptagons of unit diameter......</li>
<li>How does it find the shortest path between places on a map....</li>
<li>Unit diameter.Prove that the number of heptagons inside.........</li>
<li>What is the constant of triple point of water</li>
</ul>


</div>

</div>
<!--  end of refine tree1 -->


<div class="refine_tree" style="padding: 29px 0px 0px;">

<div class="refine_search2" style="margin-bottom: 0px;">

   <div style="border-bottom: 1px solid #d3d3d3; width:100%; float:left">
 <h5> <span class="article"></span> Recommended articles</h5>
  </div>


<ul>
<li>The plane is divided into convex heptagons of unit diameter......</li>
<li>How does it find the shortest path between places on a map....</li>
<li>Unit diameter.Prove that the number of heptagons inside.........</li>
<li>What is the constant of triple point of water</li>
</ul>

</div>

</div>
<!--  end of refine tree2 -->


</div>


</div>

</div>
</div>

<?php include('footer.php') ?>




