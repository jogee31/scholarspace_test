<div class="left_search_bar">
<div class="first_search">
<span class="find_icon"></span>
 <h6>SEARCH BY</h6>
</div>
<div class="detail_list">

<ul>
<li id="degree_drop1"> <span class="degree_icon"></span> <h6>Streams <span class="arrow_down"></span>  </h6> 

<div class="degree_list" id="degree_list1">
 <form role="form">
<ul>
<li>
<div class="checkbox">
    <label><input type="checkbox" value="">Management(501)</label>
 </div>
</li>

<li>
<div class="checkbox">
    <label><input type="checkbox" value="">Engineering(415)</label>
 </div>
</li>

<li>
<div class="checkbox">
    <label><input type="checkbox" value="">Computer Applications(312)</label>
 </div>
</li>

<li>
<div class="checkbox">
    <label><input type="checkbox" value="">Science(303)</label>
 </div>
</li>

<li>
<div class="checkbox">
    <label><input type="checkbox" value="">Commerce(279)</label>
 </div>
</li>

<li>
<div class="checkbox">
    <label><input type="checkbox" value="">Mass Communications(105)</label>
 </div>
</li>



</ul>
  
</form>

</div>

</li>

<li id="degree_drop2"> <span class="flag_icon"></span> <h6> States <span class="arrow_down"></span> </h6>

<div class="degree_list" id="degree_list2">
 <form role="form">
<ul>
<li>
<div class="checkbox">
    <label><input type="checkbox" value="">Maharashtra</label>
 </div>
</li>

<li>
<div class="checkbox">
    <label><input type="checkbox" value="">Karnataka</label>
 </div>
</li>

<li>
<div class="checkbox">
    <label><input type="checkbox" value="">Kerala</label>
 </div>
</li>

</ul>
  
</form>

</div>

 </li>


<li id="degree_drop3"> <span class="build_icon"></span> <h6>  City  <span class="arrow_down"></span>  </h6>  

<div class="degree_list" id="degree_list3">
 <form role="form">
<ul>
<li>
<div class="checkbox">
    <label><input type="checkbox" value="">Pune</label>
 </div>
</li>

<li>
<div class="checkbox">
    <label><input type="checkbox" value="">Bangalore</label>
 </div>
</li>

<li>
<div class="checkbox">
    <label><input type="checkbox" value="">Cochin</label>
 </div>
</li>

</ul>
  
</form>

</div>


</li>


<li id="degree_drop4">  <span class="currency_icon"></span> <h6>  Fee <span class="arrow_down"></span>  </h6> 

<div class="degree_list" id="degree_list4">
 <form role="form">
<ul>
<li>
<div class="checkbox">
    <label><input type="checkbox" value="">2,00,000</label>
 </div>
</li>

<li>
<div class="checkbox">
    <label><input type="checkbox" value="">5,00,000</label>
 </div>
</li>

<li>
<div class="checkbox">
    <label><input type="checkbox" value="">10,00,000</label>
 </div>
</li>

</ul>
  
</form>

</div>

  </li>


<li id="degree_drop5">  <span class="edit_icon"></span>  <h6> Exams  <span class="arrow_down"></span>  </h6>

<div class="degree_list" id="degree_list5">
 <form role="form">
<ul>
<li>
<div class="checkbox">
    <label><input type="checkbox" value="">CAT</label>
 </div>
</li>

<li>
<div class="checkbox">
    <label><input type="checkbox" value="">XAT</label>
 </div>
</li>

<li>
<div class="checkbox">
    <label><input type="checkbox" value="">IIFT</label>
 </div>
</li>

</ul>
  
</form>

</div>

 </li>


<li id="degree_drop6">  <span class="load_icon"></span>  <h6>Duration  <span class="arrow_down"></span> </h6>

<div class="degree_list" id="degree_list6">
 <form role="form">
<ul>
<li>
<div class="checkbox">
    <label><input type="checkbox" value="">2 yrs</label>
 </div>
</li>

<li>
<div class="checkbox">
    <label><input type="checkbox" value="">4yrs</label>
 </div>
</li>

<li>
<div class="checkbox">
    <label><input type="checkbox" value="">3yrs</label>
 </div>
</li>

</ul>
  
</form>

</div>

 </li>


</ul>

</div>



</div>