<?php include('intra_leftbar.php') ?>
<div class="col-md-6">
    <div class="boards_tab"> 
        <div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="batch">
                    <div class="announce">
                        <h5>Announcements</h5>
                        <div class="total_anounce">
                            <div class="user_announce">
                                <div class="announce_img">
                                    <img src="<?php echo base_url(); ?>assets_front/image/recent_user2.png" alt="">
                                </div>

                                <div class="announce_details">
                                    <a href="#">Iron core- Reason?</a>
                                    <h6>posted by Sayan Chaudhuri</h6>
                                </div>

                                <div class="like_msg">
                                    <div class="msg_space"><h6><span class="likes"></span> 2</h6></div>
                                    <div class="msg_space"><h6><span class="likes2"></span>4</h6></div>
                                </div>
                            </div>
                            <!-- user announce 1 -->

                            <div class="user_announce">
                                <div class="announce_img">
                                    <img src="<?php echo base_url(); ?>assets_front/image/recent_user3.png" alt="">
                                </div>

                                <div class="announce_details">
                                    <a href="#">Iron core- Reason?</a>
                                    <h6>posted by Sayan Chaudhuri</h6>
                                </div>

                                <div class="like_msg">
                                    <div class="msg_space"><h6><span class="likes"></span> 2</h6></div>
                                    <div class="msg_space"><h6><span class="likes2"></span>4</h6></div>
                                </div>
                            </div>
                            <!-- user announce 2 -->
                            <div class="user_announce">
                                <div class="announce_img">
                                    <img src="<?php echo base_url(); ?>assets_front/image/recent_user4.png" alt="">
                                </div>

                                <div class="announce_details">
                                    <a href="#">Iron core- Reason?</a>
                                    <h6>posted by Sayan Chaudhuri</h6>
                                </div>

                                <div class="like_msg">
                                    <div class="msg_space"><h6><span class="likes"></span> 2</h6></div>
                                    <div class="msg_space"><h6><span class="likes2"></span>4</h6></div>
                                </div>
                            </div>
                            <!-- user announce 3 -->

                        </div>     
                    </div>
                    <!--   end of announcements -->

                    <div class="update_tab">
                        <div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">  <span class="point_top"></span> 
                                    <a href="#update" aria-controls="update" role="tab" data-toggle="tab"> 
                                        <span class="update_icon1"></span>Update </a>
                                </li>

                                <li role="presentation"> <span class="point_top"></span> 
                                    <a href="#poll" aria-controls="poll" role="tab" data-toggle="tab"> 
                                        <span class="update_icon2"></span>Poll </a> 
                                </li>

                                <li role="presentation"> <span class="point_top"></span>
                                    <a href="#praise" aria-controls="praise" role="tab" data-toggle="tab"> 
                                        <span class="update_icon3"></span> Praise </a> 
                                </li>

                                <li role="presentation"> <span class="point_top"></span>
                                    <a href="#announce_tab" aria-controls="announce_tab" role="tab" data-toggle="tab"> 
                                        <span class="update_icon3"></span> Announcement </a> 
                                </li>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content" style="margin-top: 10px;">

                                <div role="tabpanel" class="tab-pane active" id="update">

                                    <div class="share_box">
                                        <a href="#">Share something with this group?</a>
                                        <span class="pin"></span>
                                    </div>

                                </div>
                                <!-- end of first tab -->


                                <div role="tabpanel" class="tab-pane" id="poll">
                                    <form action="#" method="post">
                                        <div class="poll_share">
                                            <div class="text_poll">  
                                                <textarea class="share_text" placeholder="Whats your question ? " > 
                                                </textarea> 
                                                <span class="pin2"></span> 
                                            </div>
                                            <div class="box_a">
                                                <span class="answer_box"> A </span>
                                                <input type="text" placeholder="Answer" class="poll_text" name="answer">
                                            </div>
                                            <div class="box_a">
                                                <span class="answer_box"> B</span>
                                                <input type="text" placeholder="Answer" class="poll_text" name="answer">
                                            </div>
                                            <div class="box_a">
                                                <span class="answer_box"> C </span>
                                                <input type="text" placeholder="Answer" class="poll_text" name="answer">
                                            </div>
                                            <div class="add_company">
                                                <input type="text" placeholder="Add a group and/or people" class="add_text" id="notify_id2">
                                                <div class="notify_pop2" id="second_notify">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="side_icon">  
                                                                <span class="group_icon"></span>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-10">
                                                            <div class="people_list">
                                                                <p>You can only add one group</p>
                                                                <ul>
                                                                    <li>
                                                                        <div class="notify_img">
                                                                            <img src="<?php echo base_url(); ?>assets_front/image/company2.png" alt="">
                                                                        </div>
                                                                        <h5>All Company </h5> <h6>This is the default </h6>
                                                                    </li>
                                                                    <li>
                                                                        <div class="notify_img">
                                                                            <img src="<?php echo base_url(); ?>assets_front/image/hr_img.png" alt="">
                                                                        </div>
                                                                        <h5>HR </h5> 
                                                                    </li>

                                                                </ul>
                                                                <!--end of list of group for the company -->
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="side_icon">  
                                                                <span class="single_user"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-10">
                                                            <div class="people_list">

                                                                <ul>
                                                                    <li> 
                                                                        <div class="notify_img">
                                                                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user3.png" alt="">
                                                                        </div>
                                                                        <h5>Haneef </h5> <h6>Web developer</h6>
                                                                    </li>

                                                                    <li> 
                                                                        <div class="notify_img">
                                                                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user2.png" alt="">
                                                                        </div>
                                                                        <h5>Andrew </h5> <h6>Web developer</h6>
                                                                    </li>

                                                                    <li> 
                                                                        <div class="notify_img">
                                                                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                                                                        </div>
                                                                        <h5>Haneef </h5> <h6>Web developer</h6>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- add a group or people notify 2 -->

                                                <div class="company" id="remove_com"> 
                                                    <span class="color_grp"></span> 
                                                    <h6> All company </h6> 
                                                    <span class="pop_close"></span> 
                                                </div>
                                                <input type="text" placeholder="Add people to notify" class="poll_text" id="notify_id">
                                                <!-- popup for add people to notify -->
                                                <div class="notify_pop">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="side_icon">  
                                                                <span class="group_icon"></span>
                                                                <span class="single_user"></span>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-10">
                                                            <div class="people_list">
                                                                <p>You can only add one group</p>
                                                                <ul>
                                                                    <li> 
                                                                        <div class="notify_img">
                                                                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user3.png" alt="">
                                                                        </div>
                                                                        <h5>Haneef </h5> <h6>Web developer</h6>
                                                                    </li>

                                                                    <li> 
                                                                        <div class="notify_img">
                                                                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user2.png" alt="">
                                                                        </div>
                                                                        <h5>Andrew </h5> <h6>Web developer</h6>
                                                                    </li>

                                                                    <li> 
                                                                        <div class="notify_img">
                                                                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                                                                        </div>
                                                                        <h5>Haneef </h5> <h6>Web developer</h6>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>





                                            </div>

                                            <div class="box_a" id="open_topic">
                                                <input type="text" placeholder="Add topics" class="poll_text" name="topic">
                                            </div>

                                            <div class="add_topics" id="topic1"> <span class="join_pin"></span> Add topics</div> 
                                            <button type="submit" class="btn">Post</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- end of second tab -->

                                <div role="tabpanel" class="tab-pane" id="praise">
                                    <form>
                                        <div class="poll_share">
                                            <div class="text_poll">  
                                                <input class="share_value" placeholder="Who would you like to praise ? " > 
                                                <span class="pin2"></span> 
                                            </div>

                                            <div class="text_poll"> 
                                                <div class="praise_image">
                                                    <span class="praise_prev"></span>
                                                    <span class="praise_icons"></span>
                                                    <span class="praise_next"></span>
                                                </div> 
                                                <textarea class="praise_text" placeholder="What are you praising them for ? "></textarea> 
                                            </div>

                                            <div class="add_company">
                                                <div class="company">
                                                    <span class="color_grp"></span> 
                                                    <h6> All company </h6> 
                                                    <span class="pop_close"></span> 
                                                </div>

                                                <input type="text" placeholder="Add people to notify" class="poll_text" name="note">
                                            </div>
                                            <div class="box_a" id="open_topic2">
                                                <input type="text" placeholder="Add topics" class="poll_text">
                                            </div>

                                            <div class="add_topics" id="topic2"> <span class="join_pin"></span> Add topics</div> 
                                            <button type="submit" class="btn">Post</button>
                                        </div>
                                    </form>
                                </div>

                                <!-- end of third tab -->


                                <div role="tabpanel" class="tab-pane" id="announce_tab">
                                    <form action="#" method="post">
                                        <div class="poll_share">
                                            <div class="text_poll">  
                                                <textarea class="share_text" placeholder="Whats your question ? " > 
                                                </textarea> 
                                                <span class="pin2"></span> 
                                            </div>


                                            <div class="add_company">
                                                <input type="text" placeholder="Add a group and/or people" class="add_text" id="notify_id2" name="group">
                                                <div class="notify_pop2" id="second_notify">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="side_icon">  
                                                                <span class="group_icon"></span>

                                                            </div>
                                                        </div>

                                                        <div class="col-md-10">
                                                            <div class="people_list">
                                                                <p>You can only add one group</p>

                                                                <ul>
                                                                    <li>
                                                                        <div class="notify_img">
                                                                            <img src="<?php echo base_url(); ?>assets_front/image/company2.png" alt="">
                                                                        </div>
                                                                        <h5>All Company </h5> <h6>This is the default </h6>

                                                                    </li>

                                                                    <li>
                                                                        <div class="notify_img">
                                                                            <img src="<?php echo base_url(); ?>assets_front/image/hr_img.png" alt="">
                                                                        </div>
                                                                        <h5>HR </h5> 
                                                                    </li>

                                                                </ul>
                                                                <!--end of list of group for the company -->


                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="side_icon">  
                                                                <span class="single_user"></span>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-10">
                                                            <div class="people_list">

                                                                <ul>
                                                                    <li> 
                                                                        <div class="notify_img">
                                                                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user3.png" alt="">
                                                                        </div>
                                                                        <h5>Haneef </h5> <h6>Web developer</h6>
                                                                    </li>

                                                                    <li> 
                                                                        <div class="notify_img">
                                                                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user2.png" alt="">
                                                                        </div>
                                                                        <h5>Andrew </h5> <h6>Web developer</h6>
                                                                    </li>

                                                                    <li> 
                                                                        <div class="notify_img">
                                                                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                                                                        </div>
                                                                        <h5>Haneef </h5> <h6>Web developer</h6>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>



                                                    </div>
                                                </div>
                                                <!-- add a group or people notify 2 -->

                                                <div class="company" id="remove_com"> 
                                                    <span class="color_grp"></span> 
                                                    <h6> All company </h6> 
                                                    <span class="pop_close"></span> 
                                                </div>
                                                <input type="text" placeholder="Add people to notify" class="poll_text" id="notify_id" name="note">
                                                <!-- popup for add people to notify -->
                                                <div class="notify_pop">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="side_icon">  
                                                                <span class="group_icon"></span>
                                                                <span class="single_user"></span>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-10">
                                                            <div class="people_list">
                                                                <p>You can only add one group</p>
                                                                <ul>
                                                                    <li> 
                                                                        <div class="notify_img">
                                                                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user3.png" alt="">
                                                                        </div>
                                                                        <h5>Haneef </h5> <h6>Web developer</h6>
                                                                    </li>

                                                                    <li> 
                                                                        <div class="notify_img">
                                                                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user2.png" alt="">
                                                                        </div>
                                                                        <h5>Andrew </h5> <h6>Web developer</h6>
                                                                    </li>

                                                                    <li> 
                                                                        <div class="notify_img">
                                                                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                                                                        </div>
                                                                        <h5>Haneef </h5> <h6>Web developer</h6>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>





                                            </div>

                                            <div class="box_a" id="open_topic">
                                                <input type="text" placeholder="Add topics" class="poll_text" name="topic">
                                            </div>

                                            <div class="add_topics" id="topic1"> <span class="join_pin"></span> Add topics</div> 
                                            <button type="submit" class="btn">Post</button>
                                        </div>
                                    </form>
                                </div>



                            </div>

                        </div>
                    </div>
                    <!-- end of update tab -->


                    <div class="commentors_tab">
                        <div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <div class="tab_pop">See all conversations in your network</div>
                                    <a href="#top_tab" aria-controls="top_tab" role="tab" data-toggle="tab">
                                        Top .</a> </li>
                                <li role="presentation"><a href="#all_tab" aria-controls="all_tab" role="tab" data-toggle="tab">All .</a></li>
                                <li role="presentation"><a href="#follow_tab" aria-controls="follow_tab" role="tab" data-toggle="tab">Following
                                    </a> </li>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content" style="margin-top: 14px;">
                                <div role="tabpanel" class="tab-pane active" id="top_tab">
                                    <div class="global_details">
                                        <h4> <a href="#" id="global_name"> Global Telecom </a> </h4>

                                        <div class="user_popup" id="res_global_name">
                                            <span class="point_top"></span> 
                                            <div class="row">

                                                <div class="col-md-3">
                                                    <div class="intra_user_img" style="padding: 4px 9px;">
                                                        <img src="<?php echo base_url(); ?>assets_front/image/company.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="pop_user_info">
                                                        <h5>All Company</h5>
                                                        <h6>Public group</h6>
                                                        <h6>This is the default group for everyone</h6>

                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="pop_send">
                                                        <!--     <a href="#">Send Message</a> -->
                                                        <a href="#" style="float: right; margin-right: 12px;"> <span class="tick"></span>Joined</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="top_user_img">
                                                    <img src="<?php echo base_url(); ?>assets_front/image/intra_user1.png">
                                                </div>
                                            </div>

                                            <div class="col-md-10">
                                                <div class="top_user_details">
                                                    <h5> <a href="#" id="top_name"> Swapna Srivastava </a> </h5>
                                                    <div class="user_popup" id="res_top_name">
                                                        <span class="point_top"></span> 
                                                        <div class="row">

                                                            <div class="col-md-3">
                                                                <div class="intra_user_img" style="padding: 4px 9px;">
                                                                    <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <div class="pop_user_info">
                                                                    <h5>Haneef</h5>
                                                                    <h6>Web developer</h6>
                                                                    <h6>Groups: Web development design</h6>
                                                                    <h6>Email : <a href="mailto:haneef@hexwhale.com"> haneef@hexwhale.com</a></h6>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="pop_send">
                                                                    <a href="#">Send Message</a>
                                                                    <a href="#"> <span class="tick"></span> Following</a>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>


                                                    <h6>November 12, 2013 at 11:13am</h6>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae elit vel nisi lobortis pellentesque quis in lacus. Donec eu dignissim nunc, sed venenatis neque. Ut ultrices magna id ullamcorper vestibulum. Nam molestie nec justo ac rutrum......</p>

                                                    <a href="#" title="Like">Like.</a>
                                                    <a href="#" title="Reply">Reply.</a>
                                                    <a href="#" title="Share">Share.</a>
                                                    <a href="#" title="More">More</a>

                                                    <div class="commenting_box">
                                                        <div class="joined">
                                                            <div class="more_share edit_more2">
                                                                <form action="#" method="post">
                                                                    <div class="text_poll2">  
                                                                        <input type="text" placeholder="Add topic.." class="poll_text" name="topic">
                                                                        <button type="submit" class="btn">Add</button>
                                                                    </div>

                                                                </form>
                                                            </div>
                                                            <a href="#"> <span class="join_pin"></span> joined</a>

                                                            <div class="edit2"><a href="javascript:void(0);" onclick="myFunction2('Done')" id="text_change2" >Edit Topics</a></div>

                                                        </div>

                                                        <div class="reply_box">
                                                            <div class="announce_img">
                                                                <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                                                            </div>
                                                            <div class="share_box2" id="comment_in1">
                                                                <a href="javascript:void(0);" >Write a reply...</a>
                                                                <span class="reply_pin"></span>
                                                            </div>

                                                            <div class="more_share" id="more_in1">
                                                                <form action="#" method="post">
                                                                    <a href="#">Vishnu Ravi is replying to Haneef</a>
                                                                    <div class="text_poll2">  
                                                                        <textarea class="share_text2"></textarea> 
                                                                        <span class="pin2"></span> 
                                                                        <input type="text" placeholder="Notify additional people" class="poll_text" name="note">
                                                                    </div>
                                                                    <button type="submit" class="btn">Post</button>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- end of global details 1 -->


                                    <div class="global_details">
                                        <h4> <a href="#"> Global Telecom </a> </h4>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="top_user_img">
                                                    <img src="<?php echo base_url(); ?>assets_front/image/intra_user1.png">
                                                </div>
                                            </div>

                                            <div class="col-md-10">
                                                <div class="top_user_details">
                                                    <h5> <a href="#"> Swapna Srivastava </a> </h5>
                                                    <h6>November 12, 2013 at 11:13am</h6>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae elit vel nisi lobortis pellentesque quis in lacus. Donec eu dignissim nunc, sed venenatis neque. Ut ultrices magna id ullamcorper vestibulum. Nam molestie nec justo ac rutrum......</p>

                                                    <a href="#" title="Like">Like.</a>
                                                    <a href="#" title="Reply">Reply.</a>
                                                    <a href="#" title="Share">Share.</a>
                                                    <a href="#" title="More">More</a>

                                                    <div class="commenting_box">
                                                        <div class="joined">
                                                            <div class="more_share edit_more">
                                                                <form action="#" method="post">
                                                                    <div class="text_poll2">  
                                                                        <input type="text" placeholder="Add topic.." class="poll_text" name="topic">
                                                                        <button type="submit" class="btn">Add</button>
                                                                    </div>

                                                                </form>
                                                            </div>

                                                            <a href="#"> <span class="join_pin"></span> joined</a>
                                                            <div class="edit"><a href="javascript:void(0);" onclick="myFunction('Done')" id="text_change" >Edit Topics</a></div>
                                                        </div>

                                                        <div class="reply_box">
                                                            <div class="announce_img">
                                                                <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                                                            </div>
                                                            <div class="share_box2" id="comment_in1">
                                                                <a href="javascript:void(0);" >Write a reply...</a>
                                                                <span class="reply_pin"></span>
                                                            </div>

                                                            <div class="more_share" id="more_in1">
                                                                <form action="#" method="post">
                                                                    <a href="#">Vishnu Ravi is replying to Haneef</a>
                                                                    <div class="text_poll2">  
                                                                        <textarea class="share_text2"></textarea> 
                                                                        <span class="pin2"></span> 
                                                                        <input type="text" placeholder="Notify additional people" class="poll_text" name="notify">
                                                                    </div>
                                                                    <button type="submit" class="btn">Post</button>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- end of global details 2 -->



                                </div>

                                <div role="tabpanel" class="tab-pane" id="all_tab">
                                </div>

                                <div role="tabpanel" class="tab-pane" id="follow_tab">
                                </div>



                            </div>
                        </div>
                    </div>
                    <!--  end of commentors tab -->



                </div>
                <!-- end of tab1 -->

                <div role="tabpanel" class="tab-pane" id="job">
                    <h6>Campus</h6>
                    <p>The institute has following major facilities:</p>
                </div>


                <div role="tabpanel" class="tab-pane" id="know">
                    <h6>History</h6>
                    <p>GVIET is managed and run by Gurukul Vidyapeeth. The institute is affiliated to Punjab Technical University, Jalandhar and recognised by AICTE, New Delhi.</p>
                </div>


                <div role="tabpanel" class="tab-pane" id="blog"></div>


            </div>
        </div>
    </div>


</div>

<div class="col-md-3">

    <div class="right_bar">
        <div class="progress_box">
            <h4>Getting Started</h4>

            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="64"
                     aria-valuemin="0" aria-valuemax="100" style="width:64%">
                    <span class="sr-only">64% Complete</span>
                </div>
            </div>
            <h5>64%</h5>

            <a href="#">Write your first post</a>
            <a href="#">Get Yammer for desktop</a>
            <a href="#">Get the Yammer Mobile App</a>
        </div>
        <!-- end of progress bar -->

        <div class="recent_activity">
            <h5>Recent Activities</h5>

            <div class="activities">
                <div class="act_img">
                    <img src="<?php echo base_url(); ?>assets_front/image/vr_img.png" alt="">
                </div>  
                <p> <a href="#">Vishnu Ravi</a> and Haneef Mp have joined Design.</p> 
            </div>
            <!-- end of 1 activity -->

            <div class="activities">
                <div class="act_img">
                    <img src="<?php echo base_url(); ?>assets_front/image/sa.png" alt="">
                </div>  
                <p> <a href="#">Stella Ammanna</a> changed their Job Title from web designing to Web Developer.</p> 
            </div>
            <!-- end of 2 activity -->

            <div class="activities">
                <div class="act_img">
                    <img src="<?php echo base_url(); ?>assets_front/image/sm.png" alt="">
                </div>  
                <p> <a href="#">shihas Mandottil</a> has joined PHP Development</p> 
            </div>
            <!-- end of 3 activity -->

            <div class="activities">
                <div class="act_img">
                    <img src="<?php echo base_url(); ?>assets_front/image/vr_blue.png" alt="">
                </div>  
                <p> <a href="#">Vishnu Ravi</a> has Created Design Group</p> 
            </div>
            <!-- end of 4 activity -->

        </div>
        <!-- end of recent activity -->
    </div>
    <!-- end of right bar -->
</div>


</div>
</div>
</div>



<?php include('footer2.php') ?>


