<?php
include('intra_leftbar.php');
?>
<style type="text/css">
    .request_handling_btns{
        float: right;
        padding: 8px 0px 0px;
    }
    .recent_activity_modified {
        margin-top: 0px !important;
    }
    .error_msg, .trem_check_error{
        color: #CC1717;
        font-size: 15px;
        font-weight: 600;
    }
</style>
<div class="col-lg-9">
    <div class="boards_tab">
        <div>
            <div class="tab-content">
                <div class="tab-pane active" id="batch" role="tabpanel">
                    <div class="announce">
                        <div class="success_alert alert alert-success alert-dismissable" style="display: none; padding: 8px;margin-bottom: 10px;">
                            <h4 style="padding: 0px; margin: 0px;">
                                <i class="icon fa fa-check"></i> Teacher added successfully. 
                                <span id="success_alert_message" style="font-size: 15px;"></span></h4>
                        </div>
                        <div class="danger_alert alert alert-danger alert-dismissable" style="display: none;padding: 8px;margin-bottom: 10px;">
                            <h4 style="padding: 0px; margin: 0px;">
                                <i class="icon fa fa-warning"></i> Error adding Teacher..! Please try again. 
                                <span id="danger_alert_message" style="font-size: 15px;"></span></h4>
                        </div>
                        <h5><i class="fa fa-user-plus"></i> Add New Teacher</h5>
                        <div class = "total_anounce">
                            <div class = "col-md-12">
                                <form id = "AddNewTeacher_Form" method = "POST" action = "<?php echo site_url("user/teacher"); ?>">
                                    <div class = "col-md-12">
                                        <div class = "course_message">
                                            <div class = "col-md-2"><?php echo form_label('First Name *', 'first_name'); ?></div>
                                            <div class="col-md-5"><?php echo form_input(array("class" => "reg_text", "name" => "first_name", "placeholder" => "Enter First Name", "value" => set_value('first_name'))); ?></div>
                                            <div class="col-md-5 error_msg"><?php echo form_error('first_name'); ?></div>
                                        </div>
                                        <div class="course_message">
                                            <div class="col-md-2"><?php echo form_label('Last Name *', 'last_name'); ?></div>
                                            <div class="col-md-5"><?php echo form_input(array("class" => "reg_text", "name" => "last_name", "placeholder" => "Enter Last Name", "value" => set_value('last_name'))); ?></div>
                                            <div class="col-md-5 error_msg"><?php echo form_error('last_name'); ?></div>
                                        </div>
                                        <div class="course_message">
                                            <div class="col-md-2"><?php echo form_label('Email *', 'email'); ?></div>
                                            <div class="col-md-5"><?php echo form_input(array("class" => "reg_text valid_email", "id" => "email", "name" => "email", "placeholder" => "Enter Email", "value" => set_value('email'))); ?></div>
                                            <div class="col-md-5 email_error_msg error_msg"><?php echo form_error('email'); ?></div>
                                        </div>
                                        <div class="course_message">
                                            <div class="col-md-2"><?php echo form_label('Mobile *', 'mobile'); ?></div>
                                            <div class="col-md-1"><span class="num_box">+91</span></div>
                                            <div class="col-md-4"><?php echo form_input(array("class" => "reg_text valid_phone", "type" => "number", "name" => "mobile", "placeholder" => "Enter Mobile Number", "value" => set_value('mobile'))); ?></div>
                                            <div class="col-md-5 phone_error_msg error_msg"><?php echo form_error('mobile'); ?></div>
                                        </div>
                                        <div class="reg_agree">
                                            <div class="col-md-12"> 
                                                <div class="checkbox">
                                                    <button type="submit" class="btn btn-primary">Register</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="site_url" value="<?php echo site_url(); ?>"/>
<input type="hidden" id="base_url" value="<?php echo base_url(); ?>"/>
<?php
include('footer2.php');
?>
<?php
$FormSubmit = $this->session->flashdata('FormSubmit');
if ($FormSubmit == 'success') {
    ?>
    <script type="text/javascript">
        $('.success_alert').fadeIn();
        setTimeout(function () {
            $('.success_alert').fadeOut();
        }, 3000);
    </script>
<?php } else if ($FormSubmit == 'failure') { ?>
    <script type="text/javascript">
        $('.danger_alert').fadeIn();
        setTimeout(function () {
            $('.danger_alert').fadeOut();
        }, 3000);
    </script>
<?php }
?>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/teacher.js" type="text/javascript"></script>
