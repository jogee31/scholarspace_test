<div role="tabpanel" class="tab-pane active" id="batch">
    <div class="update_tab">
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">  <span class="point_top"></span> 
                    <a href="#update" aria-controls="update" role="tab" data-toggle="tab"> 
                        <span class="update_icon1"></span>Update </a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="margin-top: 10px;">
                <div role="tabpanel" class="tab-pane active" id="update">
                    <div class="share_box">
                        <a href="javascript:void(0);" id="ontick_share">Share something with this group?</a>
                        <span class="pin"></span>
                    </div>
                    <div class="show_share">
                        <form id="group_post_update_form">
                            <div class="poll_share">
                                <div class="text_poll">  
                                    <textarea ata-autoresize class="share_text" id="update_textarea" name="update_message" placeholder="Whats your question ? "></textarea>
                                    <span class="pin2"></span>
                                    <div class="attach_list">
                                        <ul>
                                            <li> <a class="upload_file_from_computer"> <span class="upload_icon"></span> <h6>Upload a file from computer</h6> </a> </li>
                                            <li> <a href="#"> <span class="file_icon"></span> <h6>Select a file on </h6> </a> </li>
                                            <li> <a href="#"> <span class="note_icon"></span> <h6>Select a note on</h6> </a> </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="add_files" style="display: none">
                                    <ul id="file_upload_content_ul"></ul>
                                </div>
                                <div class="add_company">
                                    <input type="text" placeholder="Add a group and/or people" class="add_text" id="notify_id2" name="group">
                                    <div class="notify_pop2" id="second_notify">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="side_icon">  
                                                    <span class="group_icon"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="people_list">
                                                    <p>You can only add one group</p>
                                                    <ul>
                                                        <li>
                                                            <div class="notify_img">
                                                                <img src="image/company2.png" alt="">
                                                            </div>
                                                            <h5>All Company </h5> <h6>This is the default </h6>
                                                        </li>
                                                        <li>
                                                            <div class="notify_img">
                                                                <img src="image/hr_img.png" alt="">
                                                            </div>
                                                            <h5>HR </h5> 
                                                        </li>
                                                    </ul>
                                                    <!--end of list of group for the company -->
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="side_icon">  
                                                    <span class="single_user"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="people_list">
                                                    <ul>
                                                        <li> 
                                                            <div class="notify_img">
                                                                <img src="image/recent_user3.png" alt="">
                                                            </div>
                                                            <h5>Haneef </h5> <h6>Web developer</h6>
                                                        </li>
                                                        <li> 
                                                            <div class="notify_img">
                                                                <img src="image/recent_user2.png" alt="">
                                                            </div>
                                                            <h5>Andrew </h5> <h6>Web developer</h6>
                                                        </li>
                                                        <li> 
                                                            <div class="notify_img">
                                                                <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                                                            </div>
                                                            <h5>Haneef </h5> <h6>Web developer</h6>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- add a group or people notify 2 -->
                                    <div class="company" id="remove_com"> 
                                        <span class="color_grp"></span> 
                                        <h6> All company </h6> 
                                        <span class="pop_close"></span> 
                                    </div>
                                    <input type="text" placeholder="Add people to notify" class="poll_text" id="notify_id" name="note">
                                    <!-- popup for add people to notify -->
                                    <div class="notify_pop">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="side_icon">  
                                                    <span class="group_icon"></span>
                                                    <span class="single_user"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="people_list">
                                                    <p>You can only add one group</p>
                                                    <ul>
                                                        <li> 
                                                            <div class="notify_img">
                                                                <img src="image/recent_user3.png" alt="">
                                                            </div>
                                                            <h5>Haneef </h5> <h6>Web developer</h6>
                                                        </li>
                                                        <li> 
                                                            <div class="notify_img">
                                                                <img src="image/recent_user2.png" alt="">
                                                            </div>
                                                            <h5>Andrew </h5> <h6>Web developer</h6>
                                                        </li>
                                                        <li> 
                                                            <div class="notify_img">
                                                                <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                                                            </div>
                                                            <h5>Haneef </h5> <h6>Web developer</h6>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box_a" id="open_topic">
                                    <input type="text" placeholder="Add topics" class="poll_text" name="topic">
                                </div>
                                <div class="add_topics" id="topic1"> <span class="join_pin"></span> Add topics</div> 
                                <button type="submit" id="update_post" class="btn">Post</button>
                            </div>
                        </form>
                        <form id="image_upload_form" method="post" enctype="multipart/form-data">
                            <input type="file" name="upload_post_file" id="upload_post_file" style="display:none">
                        </form>
                    </div>
                    <!-- end of share update -->
                </div>
            </div>
        </div>
    </div>