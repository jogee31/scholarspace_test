<?php include('header.php') ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<div class="body_height">
    <div class="banner_content" id="bck_image">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <style>
                        #college_serch_result{
                            width: 51.5%;
                            background-color: #334b58;
                            margin-left: 22.5%;
                        }
                        .search_data{
                            color: #334b58;
                            padding: 2%;
                            margin-bottom: 2px;
                            background-color: #e7e7e7;
                            font-size: 14px;
                        }
                        .required_border{
                            border-color: #D25050 !important;
                        }
                        .danger_alert{
                            display: none;
                        }
                    </style>


                    <div class="banner_network">
                        <h1>scholar space</h1>

                        <h5>Your Education Network</h5>

                        <div class="search_coll">
                            <form class="navbar-form" role="search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="I'm Looking for Engineering College" name="srch-term" id="srch-term1">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" type="submit"> <span class="search"></span> </button>
                                    </div>
                                </div>

                                <div class="login_sign">
                                    <a href="#">SIGN UP</a>
                                    <button type="submit" class="btn">LOGIN</button>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>


            </div>

        </div>

    </div>
    <!-- end of banner network1 -->
    <div class="banner_content" id="back_color">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="banner_network">
                        <h2>my college space</h2>

                        <div class="search_coll" id="find_box">
                            <form class="navbar-form" action="<?php echo site_url("college/search"); ?>" method="get" role="search">
                                <div class="input-group">
                                    <input type="text" class="form-control college_name_autocomplete" placeholder="Search Your College" name="search-term" id="srch-term2">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" type="submit"> <span class="search"></span> </button>
                                    </div>
                                </div>
                                <div class="search-result">
                                    <ul id="college_serch_result"></ul>
                                </div>

                            </form>
                            <div class="find_link">
                                <a href="#" id="register_college_link" data-toggle="modal" data-target="#myModal_register">Can't find? <span>Register with My college space</span> </a>
                            </div>


                        </div>

                    </div>


                </div>


            </div>

        </div>

    </div>
    <!-- end of banner network2 -->


    <div class="about_college">
        <div class="container">
            <div class="row">
                <div class="col-md-6">

                    <div class="about_us">
                        <h3><span>About</span>The Scholar Space</h3>

                        <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>

                        <a href="#">Read more</a>

                    </div>

                </div>



                <div class="col-md-3">

                    <div class="about_us">
                        <h4>Search</h4>

                        <ul>
                            <li><a href="#">Top Engineering colleges </a></li>
                            <li><a href="#">Top Engineering colleges in Bangalore</a></li>
                            <li><a href="#">Top Universities in Karanataka</a></li>
                            <li><a href="#">Colleges Abroad</a></li>
                            <li><a href="#">Top Courses in India and Abroad</a></li>
                            <li><a href="#">MS in USA</a></li>
                            <li><a href="#">CAT schedule </a></li>
                            <li><a href="#">MBA Exams</a></li>
                            <li><a href="#">JEE Advanced Schedule </a></li>
                            <li><a href="#">JEE Mains schedule</a></li>
                        </ul>


                    </div>

                </div>



                <div class="col-md-3">

                    <div class="about_us" style="margin-top: 38px;">

                        <ul>
                            <li><a href="#">Engineering Entrance exam Calender </a></li>
                            <li><a href="#">Medical Entrance Exam Calender</a></li>
                            <li><a href="#">Top 50 Medical Colleges</a></li>
                            <li><a href="#">AICTE Approved Engineering Colleges</a></li>
                            <li><a href="#">AIPMT Schedule</a></li>
                            <li><a href="#">Best Engineering colleges in Bangalore</a></li>
                            <li><a href="#">JEE cutoff</a></li>
                            <li><a href="#">Best Engineering Colleges in India</a></li>
                            <li><a href="#">Medical Colleges </a></li>

                        </ul>



                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
<div class="main_page_login" id="home_reg">
    <div class="modal fade" id="myModal_register" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <form action="<?php echo site_url("front_home/registration_college"); ?>" method="post" id="register_college_form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="reg_close" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Register Your College</h4>
                    </div>
                    <div class="modal-body">
                        <div class="contact_info_reg">
                            <div class="col-md-12">
                                <div class="danger_alert alert alert-danger alert-dismissable">
                                    <h4><i class="icon fa fa-check"></i> Error ! <span id="danger_message" style="font-size: 15px;"></span></h4>
                                </div>
                                <?php if (validation_errors()) { ?>
                                    <div class="alert alert-warning">
                                        <?php echo validation_errors(); ?>
                                    </div>
                                <?php } ?>


                                <?php if ($this->session->flashdata('item')) { ?>
                                    <div class="alert alert-success">
                                        <?php echo $this->session->flashdata('item'); ?>
                                    </div>
                                    <?php
                                }
                                if ($this->session->flashdata('item_error')) {
                                    ?>
                                    <div class="alert alert-danger">
                                        <?php echo $this->session->flashdata('item_error'); ?>
                                    </div>
                                    <?php
                                }
                                //item_error
                                ?>
                                <div class="course_message">
                                    <div class="col-md-12">
                                        <?php echo form_input(array("class" => "required reg_text", "id" => "reg_clg_clg_name", "type" => "text", "name" => "college_name", "placeholder" => "Enter College Name", "value" => set_value('college_name'))); ?>
                                    </div>
                                </div>
                                <?php
                                if (!$this->ion_auth->logged_in()) {
                                    ?>
                                    <input type="hidden" id="logged_in" value="1"/>
                                    <div class="course_message">
                                        <div class="col-md-12">
                                            <?php echo form_input(array("class" => "required reg_text", "id" => "reg_clg_f_name", "type" => "text", "name" => "first_name", "placeholder" => "Enter First Name", "value" => set_value('first_name'))); ?>
                                        </div>
                                    </div>
                                    <div class="course_message">
                                        <div class="col-md-12">
                                            <?php echo form_input(array("class" => "required reg_text", "id" => "reg_clg_l_name", "type" => "text", "name" => "last_name", "placeholder" => "Enter Last Name", "value" => set_value('last_name'))); ?>
                                        </div>
                                    </div>
                                    <div class="course_message">
                                        <div class="col-md-12">
                                            <?php echo form_input(array("class" => "required reg_text", "id" => "reg_clg_email", "type" => "text", "name" => "email", "placeholder" => "Enter Email", "value" => set_value('email'))); ?>
                                        </div>
                                    </div>

                                    <div class="course_message">
                                        <div class="col-md-12">
                                            <?php echo form_input(array("class" => "required reg_text", "id" => "reg_clg_phone", "type" => "number", "name" => "phone", "placeholder" => "+91 - Enter Phone Number", "value" => set_value('phone'))); ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="course_message">
                                    <div class="col-md-12">
                                        <?php echo form_input(array("class" => "required reg_text", "id" => "reg_clg_city", "type" => "text", "name" => "college_city", "placeholder" => "Enter College City", "value" => set_value('college_city'))); ?>
                                    </div>
                                </div>
                                <div class="reg_agree">
                                    <div class="col-md-12">
                                        <select id="reg_clg_state" name="state">
                                            <option value="">-- Choose State --</option>
                                            <?php
                                            foreach ($state_list as $value) {
                                                ?>
                                                <option value="<?php echo $value->title; ?>"><?php echo $value->title; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="course_message">
                                    <div class="col-md-12"><input type="number" id="reg_clg_zipcode" class="reg_text" name="zipcode" placeholder="Enter College Zipcode"></div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <div class="login_input" style="margin: 10px 0px;">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Register</button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<input type="hidden" id="site_url" value="<?php echo site_url(); ?>"/>
<input type="hidden" id="base_url" value="<?php echo base_url(); ?>"/>
<?php include('footer.php') ?>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end_js/home.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/jquery-ui.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/index.js" type="text/javascript"></script>
<?php if (validation_errors()) { ?>
    <script type="text/javascript">
        $("#myModal_register").modal("show");
    </script>
<?php } ?>
<?php
if ($this->session->flashdata('msg_success')) {
    ?>
    <div class="main_page_login" id="home_reg">
        <div class="modal fade" id="success_modal" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div style="background-color: #0C9076" class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="reg_close" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Successful College Registration</h4>
                    </div>
                    <div class="modal-body">
                        <div class="contact_info_reg"><?php echo $this->session->flashdata('msg_success'); ?></div>
                    </div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#success_modal").modal("show");
    </script>
    <?php
}
?>




