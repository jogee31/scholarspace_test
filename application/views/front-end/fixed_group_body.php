
<!-- Group POST SECTION  -->
<div class="commentors_tab">
    <div>
        <div class="post_head">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <div class="tab_pop">See all conversations in your network</div>
                    <a href="#top_tab" aria-controls="top_tab" role="tab" data-toggle="tab">
                        Top .</a> </li>
                <li role="presentation"><a href="#all_tab" aria-controls="all_tab" role="tab" data-toggle="tab">All .</a></li>
                <li role="presentation"><a href="#follow_tab" aria-controls="follow_tab" role="tab" data-toggle="tab">Following
                    </a> </li>
            </ul>
        </div>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="top_tab">
                <div id="all_post_content" class="global_details">
                    <?php
                    //print_r($posts_list);
                    foreach ($posts_list as $value) {
                        ?>
                        <div class="post_content_div">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="top_user_img">
                                        <img src="<?php echo base_url() . $value->profile_picture; ?>">
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="top_user_details">
                                        <h5><a href="#" class="top_name"><?php echo $value->first_name . " " . $value->last_name; ?></a> </h5>
                                        <div class="user_popup res_top_name">
                                            <span class="point_top"></span>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="intra_user_img" style="padding: 4px 9px;">
                                                        <img src="<?php echo base_url() . $value->profile_picture; ?>" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="pop_user_info">
                                                        <h5><?php echo $value->first_name . " " . $value->last_name; ?></h5>
                                                        <h6>Web developer</h6>
                                                        <h6>Groups: Web development design</h6>
                                                        <h6>Email : <a href="mailto:haneef@hexwhale.com"> haneef@hexwhale.com</a></h6>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="pop_send">
                                                        <a href="#">Send Message</a>
                                                        <a href="#"> <span class="tick"></span> Following</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <h6>
                                            <span class="post_time" timestamp="<?php echo strtotime($value->timestamp); ?>">
                                                <?php
                                                $posted_date = date('M j Y', strtotime($value->timestamp));
                                                echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($value->timestamp));
                                                ?>
                                            </span>
                                        </h6>
                                        <div class="post_message_div">
                                            <p class="post_message_p">
                                                <?php
                                                if (str_word_count($value->post_message) >= 110) {
                                                    ?>
                                                    <span class="truncated_body">
                                                        <?php
                                                        echo preg_replace('/\s+?(\S+)?$/', '', substr($value->post_message, 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                                        ?>
                                                    </span>
                                                    <span class="complete_body">
                                                        <?php
                                                        echo $value->post_message . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse</a>";
                                                        ?>
                                                    </span>
                                                    <?php
                                                } else {
                                                    echo $value->post_message;
                                                }
                                                ?>
                                            </p>
                                        </div>
                                        <?php
                                        $file_count = count($posts_file_list[$value->post_id]);
                                        if ($file_count) {
                                            ?>
                                            <div class="post_file_div">
                                                <div class="row">
                                                    <?php
                                                    $count = 1;
                                                    $inner_count = 2;
                                                    foreach ($posts_file_list[$value->post_id] as $file_value) {
                                                        if ($count == 1) {
                                                            ?>
                                                            <div class="col-md-12">
                                                                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo base_url() . $file_value->file_path; ?>">
                                                                    <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . $file_value->file_path; ?>"/>
                                                                </a>
                                                            </div>
                                                            <?php
                                                            $count++;
                                                        } else {
                                                            if ($inner_count == 2) {
                                                                ?>
                                                                <div class="col-md-12">
                                                                    <?php
                                                                }
                                                                ?>
                                                                <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo base_url() . $file_value->file_path; ?>">
                                                                    <img style="margin-bottom: 2px; width: 32.5%; object-fit: cover;" src="<?php echo base_url() . $file_value->file_path; ?>"/>
                                                                </a>
                                                                <?php
                                                                if ($file_count == $inner_count) {
                                                                    ?>
                                                                </div>
                                                                <?php
                                                            }
                                                            $inner_count++;
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <div class="post_action_div" post_id ="<?php echo $encryption_decryption_object->encode($value->post_id); ?>">
                                            <a class="post_like" href="javascript:void(0);" like_count="<?php echo count($posts_like_all_user[$value->post_id]); ?>" like_action ="<?php echo ($posts_like_current_user[$value->post_id] == 1) ? "unlike" : "like"; ?>" title="Like"><?php echo ($posts_like_current_user[$value->post_id] == 1) ? "Unlike" : "Like."; ?></a>
                                            <a href="#" title="Reply">Reply.</a>
                                            <a href="#" title="Share">Share.</a>
                                            <a href="javascript:void(0);" class="click_more" title="More">More</a>
                                            <div class="more_list">
                                                <ul>
                                                    <li class="delete_post"> <a href="javascript:void(0);"> <h6> <span class="stop_icon"></span> Delete</h6> </a> </li>
                                                    <li> <a href="#">  <h6> <span class="stop_icon"></span> Select a file on </h6> </a> </li>
                                                    <li> <a href="#"> <h6> <span class="stop_icon"></span> Select a note on</h6> </a> </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="commenting_box">
                                            <div id="post_like_div-<?php echo $encryption_decryption_object->encode($value->post_id); ?>" class="post_like_div" style="display: <?php echo (!empty($posts_like_all_user[$value->post_id])) ? "block" : "none"; ?>">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="post_like_body-<?php echo $encryption_decryption_object->encode($value->post_id); ?>" class="post_like_body">
                                                            <i class="fa fa-thumbs-up"></i>
                                                            <span class="current_user_like">
                                                                <?php
                                                                //current user like
                                                                if ($posts_like_current_user[$value->post_id] == 1 && count($posts_like_all_user[$value->post_id]) == 2) {
                                                                    ?>
                                                                    You and
                                                                    <?php
                                                                } else if ($posts_like_current_user[$value->post_id] == 1) {
                                                                    ?>You
                                                                    <?php
                                                                }
                                                                ?>
                                                            </span>
                                                            <?php
                                                            //other user like status
                                                            if (count($posts_like_all_user[$value->post_id]) > 1) {
                                                                foreach ($posts_like_all_user[$value->post_id] as $inner_value) {
                                                                    ?>
                                                                    <span><?php echo $inner_value->user_id; ?></span>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                            like this.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="all_reply">
                                                <?php
                                                foreach ($posts_reply_list[$value->post_id] as $post_reply_key => $post_reply_value) {
                                                    $user_details = $this->ion_auth->user($post_reply_value->user_id)->row();
                                                    ?>
                                                    <div class="post_content_reply_div">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="top_user_img pull-right">
                                                                    <img class="reply_user_img" src="<?php echo base_url() . $user_details->profile_picture; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="top_user_details">
                                                                    <h5><a href="#" class="top_name"><?php echo $user_details->first_name . " " . $user_details->last_name; ?></a> </h5>
                                                                    <h6>
                                                                        <span class="post_time" timestamp="<?php echo strtotime($post_reply_value->timestamp); ?>">
                                                                            <?php
                                                                            $posted_date = date('M j Y', strtotime($post_reply_value->timestamp));
                                                                            echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($post_reply_value->timestamp));
                                                                            ?>
                                                                        </span>
                                                                    </h6>
                                                                    <p class="post_message_p">
                                                                        <?php
                                                                        if (str_word_count($post_reply_value->reply_message) >= 80) {
                                                                            ?>
                                                                            <span class="truncated_body">
                                                                                <?php
                                                                                echo preg_replace('/\s+?(\S+)?$/', '', substr($post_reply_value->reply_message, 0, 200)) . "&nbsp;<a class=\"expand_msg\"> Expand >></a>";
                                                                                ?>
                                                                            </span>
                                                                            <span class="complete_body">
                                                                                <?php
                                                                                echo $post_reply_value->reply_message . "&nbsp;<a class=\"collapse_msg\"> << Collapse</a>";
                                                                                ?>
                                                                            </span>
                                                                            <?php
                                                                        } else {
                                                                            echo $post_reply_value->reply_message;
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                    <div post_id="<?php echo $encryption_decryption_object->encode($value->post_id); ?>" post_reply_id ="<?php echo $encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>" class="post_action_div">
                                                                        <a class="post_reply_like" href="javascript:void(0);" like_count ="<?php echo count($posts_reply_like_all_user[$value->post_id][$post_reply_value->post_reply_id]); ?>" like_action="<?php echo ($posts_reply_like_current_user[$value->post_id][$post_reply_value->post_reply_id] == 1) ? "unlike" : "like"; ?>" title="Like"><?php echo ($posts_reply_like_current_user[$value->post_id][$post_reply_value->post_reply_id] == 1) ? "Unlike." : "Like."; ?></a>
                                                                        <a href="#" title="Reply">Reply.</a>
                                                                        <a href="#" title="Share">Share.</a>
                                                                        <a href="javascript:void(0);" class="click_more" title="More">More</a>
                                                                        <div class="more_list">
                                                                            <ul>
                                                                                <li class="delete_post_reply"> <a href="javascript:void(0);"> <h6> <span class="stop_icon"></span> Delete</h6> </a> </li>
                                                                                <li> <a href="#">  <h6> <span class="stop_icon"></span> Select a file on </h6> </a> </li>
                                                                                <li> <a href="#"> <h6> <span class="stop_icon"></span> Select a note on</h6> </a> </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="post_reply_like_div-<?php echo $encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>" style="display: <?php echo (!empty($posts_reply_like_all_user[$value->post_id][$post_reply_value->post_reply_id])) ? "block" : "none"; ?>" class="post_reply_like_div">
                                                            <div class="row">
                                                                <div class="col-md-2"></div>
                                                                <div class="col-md-10">
                                                                    <div id="post_reply_like_body-<?php echo $encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>" class="post_like_body">
                                                                        <i class="fa fa-thumbs-up"></i>
                                                                        <span class="current_user_post_reply_like">
                                                                            <?php
                                                                            //current user like
                                                                            if ($posts_reply_like_current_user[$value->post_id][$post_reply_value->post_reply_id] == 1 && count($posts_reply_like_all_user[$value->post_id][$post_reply_value->post_reply_id]) == 2) {
                                                                                ?>
                                                                                You and
                                                                                <?php
                                                                            } else if ($posts_reply_like_current_user[$value->post_id][$post_reply_value->post_reply_id] == 1) {
                                                                                ?>You
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </span>
                                                                        like this.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <!-- reply comment form  -->
                                            <div class="reply_box">
                                                <div class="announce_img">
                                                    <img src="<?php echo base_url() . $value->profile_picture; ?>" alt="">
                                                </div>
                                                <div class="share_box2 comment_in1">
                                                    <a href="javascript:void(0);" >Write a reply...</a>
                                                    <!--<span class="reply_pin"></span>-->
                                                </div>
                                                <div class="more_share">
                                                    <form class="reply_form">
                                                        <a href="#"><?php echo $value->first_name . " " . $value->last_name; ?> is replying.</a>
                                                        <div class="text_poll2">
                                                            <input type="hidden" name="post_id" value="<?php echo $encryption_decryption_object->encode($value->post_id); ?>"/>
                                                            <textarea name="reply_message" class="share_text2 reply_textarea"></textarea>
                                                            <!--<span class="pin2"></span>-->
                                                            <input type="text" placeholder="Notify additional people" class="poll_text" name="note">
                                                        </div>
                                                        <button type="submit" class="btn reply_post_button">Post</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="scroller_loading_div">
                    <img class="scroller_loading_img" src="<?php echo base_url(); ?>assets_front/image/scroll_loader.GIF" alt=""/>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="all_tab">
            </div>
            <div role="tabpanel" class="tab-pane" id="follow_tab">
            </div>
        </div>
    </div>
</div>
<!--  end of commentors tab -->
</div>