<?php include('intra_leftbar.php') ?>
<link href="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>

<input type="hidden" id="fixed_group_name" value="jobopening"/>
<div class="col-md-6">
    <div class="boards_tab"> 
        <div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="success_alert alert alert-success alert-dismissable" style="padding: 8px;margin-top: 10px;">
                    <h4 style="padding: 0px; margin: 0px;">
                        <i class="icon fa fa-check"></i> Success ! 
                        <span id="success_alert_message" style="font-size: 15px;"></span></h4>
                </div>
                <?php include('fixed_group_head.php') ?>
                <div class="job_opening_list">
                    <div class="row">
                        <div class="col-md-5">
                            <h4>Job oppurtunities</h4>
                        </div>
                        <div class="col-md-7">
                            <button id="addNewJobBtn" class="btn btn-default btn-xs pull-right"><i class="fa fa-plus"></i> Add New Job</button>
                        </div>
                    </div>
                    <ul id="job_opening_list_ul">
                        <?php
                        foreach ($jobopening_list as $jobopening_value) {
                            ?>
                            <li class="job_opening_list_li" jobopening_id = "<?php echo $encryption_decryption_object->encode($jobopening_value->job_id); ?>">
                                <h6><?php echo $jobopening_value->job_title; ?></h6>  
                                <p><?php echo $jobopening_value->job_description; ?></p>
                                <div class="jobopening_action_btn_div">
                                    <div class="row">
                                        <div class="col-lg-8"></div>
                                        <div class="col-lg-4">
                                            <button style="margin-left: 5px;" class="delete_job_opening btn btn-default btn-xs pull-right"><i class="fa fa-trash"></i></button>
                                            <button class="edit_job_opening btn btn-default btn-xs pull-right"><i class="fa fa-edit"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
                <?php include('fixed_group_body.php') ?>
                <!-- end of tab1 -->
                <div role="tabpanel" class="tab-pane" id="job">
                    <h6>Campus</h6>
                    <p>The institute has following major facilities:</p>
                </div>
                <div role="tabpanel" class="tab-pane" id="know">
                    <h6>History</h6>
                    <p>GVIET is managed and run by Gurukul Vidyapeeth. The institute is affiliated to Punjab Technical University, Jalandhar and recognised by AICTE, New Delhi.</p>
                </div>
                <div role="tabpanel" class="tab-pane" id="blog"></div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="right_bar">
        <div class="recent_activity">
            <h5>Recent Activities</h5>
            <div class="activities">
                <div class="act_img">
                    <img src="<?php echo base_url(); ?>assets_front/image/vr_img.png" alt="">
                </div>  
                <p> <a href="#">Vishnu Ravi</a> and Haneef Mp have joined Design.</p> 
            </div>
            <!-- end of 1 activity -->

            <div class="activities">
                <div class="act_img">
                    <img src="<?php echo base_url(); ?>assets_front/image/sa.png" alt="">
                </div>  
                <p> <a href="#">Stella Ammanna</a> changed their Job Title from web designing to Web Developer.</p> 
            </div>
            <!-- end of 2 activity -->

            <div class="activities">
                <div class="act_img">
                    <img src="<?php echo base_url(); ?>assets_front/image/sm.png" alt="">
                </div>  
                <p> <a href="#">shihas Mandottil</a> has joined PHP Development</p> 
            </div>
            <!-- end of 3 activity -->

            <div class="activities">
                <div class="act_img">
                    <img src="<?php echo base_url(); ?>assets_front/image/vr_blue.png" alt="">
                </div>  
                <p> <a href="#">Vishnu Ravi</a> has Created Design Group</p> 
            </div>
            <!-- end of 4 activity -->

        </div>
        <!-- end of recent activity -->
    </div>
    <!-- end of right bar -->
</div>
</div>
</div>
</div>
<?php
include('footer2.php');
$this->load->view("front-end/page_modal/jobopening_modal");
?>
<script src="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.pack.js" type="text/javascript"></script>

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/fancy_box/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/jobopening.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/fixed_group.js" type="text/javascript"></script>



