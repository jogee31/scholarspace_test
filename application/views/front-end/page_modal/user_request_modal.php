
<!---------------Modal after pressing confirm for request----------------->
<div class="group_popup">
    <!-- Modal -->
    <div class="modal fade" id="CheckBeforConfirm_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="UpdateDetailsOfRequest_Form" method="POST">

                    <div class="modal-body" id="AppendData_Div">

                    </div>
                    
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                        <div class="course_details" style="margin: 30px 0px 15px;">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-primary" id="UserRequestConfirm_Submit">Confirm Request</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>