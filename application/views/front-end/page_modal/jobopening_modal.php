<!---------------Modal after pressing confirm for request----------------->
<div class="group_popup">
    <!-- Modal -->
    <div class="modal fade" id="addNewJobOpeningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="addNewJobOpeningModalForm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Add New Job</h4>
                    </div>
                    <div class="modal-body" id="AppendData_Div">
                        <div class="course_message">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Job Title <span class="required_error_span"></span></label>
                                    <input type="text" id="jobTitleModalText" class="pop_text pop_up_taxt" title="Job Title" name="job_title">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Job Description <span class="required_error_span"></span></label>
                                    <textarea ata-autoresize="" id="jobDescriptionModalTextarea" class="share_text pop_up_taxt" title="Job Description" name="update_message"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                        <div class="course_details" style="margin: 30px 0px 15px;">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" id="UserRequestConfirm_Submit">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Job Opening edit modal -->
<div class="group_popup">
    <!-- Modal -->
    <div class="modal fade" id="editJobOpeningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="editJobOpeningModalForm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Add New Job</h4>
                    </div>
                    <div class="modal-body" id="AppendData_Div">
                        <div class="course_message">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Job Title <span class="required_error_span"></span></label>
                                    <input type="text" id="jobTitleEditModalText" class="pop_text pop_up_taxt" title="Job Title" name="job_title">
                                    <input type="hidden" name="job_id" id="jobopeningIdEditModalText"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Job Description <span class="required_error_span"></span></label>
                                    <textarea ata-autoresize="" id="jobDescriptionEditModalTextarea" class="share_text pop_up_taxt" title="Job Description" name="job_description"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                        <div class="course_details" style="margin: 30px 0px 15px;">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>