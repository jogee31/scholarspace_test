<div class="group_popup">
    <!-- Modal -->
    <div class="modal fade" id="addIntershipModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="addInternshipModalForm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Add new internship</h4>
                    </div>
                    <div class="modal-body" id="AppendData_Div">
                        <div class="course_message">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Internship Title <span class="required_error_span"></span></label>
                                    <input type="text" id="internshipTitleModalText" class="pop_text pop_up_taxt" title="Internship Title">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-12" style="margin-bottom: 10px;">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="group_popup">
    <!-- Modal -->
    <div class="modal fade" id="editIntershipModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="editInternshipModalForm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit internship</h4>
                    </div>
                    <div class="modal-body" id="AppendData_Div">
                        <div class="course_message">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Internship Title <span class="required_error_span"></span></label>
                                    <input type="text" id="internshipTitleEditModalText" class="pop_text pop_up_taxt" title="Internship Title">
                                    <input type="hidden" id="internshipIdEditModalText" value=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-12" style="margin-bottom: 10px;">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>