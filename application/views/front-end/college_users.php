<?php include('intra_leftbar.php') ?>

<input type="hidden" id="college_id" value="<?php echo $this->session->userdata('college_id'); ?>"/>
<div class="col-md-9">
    <div class="boards_tab" style="margin-top: 20px;">
        <div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="batch">
                    <!--<div class="people_directory">
                        <div class="row">
                            <div class="col-md-5">
                                <h3>College Users</h3>  
                            </div>
                            <div class="col-md-7">
                                <div class="people_invite">
                                    <p><a href="#">11 people</a>in your company who have not activated</p> 
                                    <a href="#" id="alot_them" data-toggle="modal" data-target="#myModalppl">Invite Them</a>
                                </div>
                            </div>
                        </div>
                    </div>-->

                    <div class="people_tab">
                        <div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#CompleteList_Tab" aria-controls="every_tab" role="tab" data-toggle="tab">All Users </a>   </li>
                                <li role="presentation"><a href="#StudentsList_Tab" aria-controls="few_tab" role="tab" data-toggle="tab">Students </a></li>
                                <li role="presentation"><a href="#AlumniList_Tab" aria-controls="few_tab" role="tab" data-toggle="tab">Alumni </a></li>
                                <li role="presentation"><a href="#TeachersList_Tab" aria-controls="few_tab" role="tab" data-toggle="tab">Teachers </a></li>
                            </ul>
                            <div class="success_alert alert alert-success alert-dismissable" style="display: none;padding: 8px;margin-top: 10px;">
                                <h4 style="padding: 0px; margin: 0px;">
                                    <i class="icon fa fa-check"></i> Member is deleted successfully. 
                                    <span id="success_alert_message" style="font-size: 15px;"></span></h4>
                            </div>
                            <div class="danger_alert alert alert-danger alert-dismissable" style="display: none;padding: 8px;margin-top: 10px;">
                                <h4 style="padding: 0px; margin: 0px;">
                                    <i class="icon fa fa-warning"></i> Error in deleting the member. Please try again. 
                                    <span id="danger_alert_message" style="font-size: 15px;"></span></h4>
                            </div>
                            <!--------------------------------First tab which shows details of all Users---------------------------------->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="CompleteList_Tab">
                                    <div class="total_people_list">
                                        <ul id="demoOne" class="demo">
                                            <div class="list_heading">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h6>People</h6>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h6>Details</h6>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
                                            if (count($CompleteList)) {
                                                foreach ($CompleteList as $key => $value) {
                                                    ?>
                                                    <li>
                                                        <div class="people_page_info">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <div class="intra_user_img">
                                                                        <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                                                                    </div>
                                                                    <div class="pp_details">
                                                                        <h5> 
                                                                            <span class="on_dot" title="offline"></span>
                                                                            <?php
                                                                            echo $value->fname . " " . $value->lname;
                                                                            echo " (" . ucfirst($value->intranet_user_type) . ")";
                                                                            ?>
                                                                        </h5> 
                                                                        <h6>
                                                                            <?php
                                                                            if ($value->intranet_user_type != "teacher")
                                                                                echo "<strong>" . $value->stream_name . "</strong>";
                                                                            ?>
                                                                        </h6>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 pp_details">
                                                                    <p><?php echo "Joined: " . date('d-m-Y', strtotime($value->timestamp)); ?></p>
                                                                    <h6>
                                                                        <?php
                                                                        if ($value->intranet_user_type != "teacher")
                                                                            echo $value->stream_course_name;
                                                                        ?>
                                                                    </h6>
                                                                    <h6>
                                                                        <?php
                                                                        if ($value->intranet_user_type == "student")
                                                                            echo $temp_array[$value->semester_or_year] . " " . $value->study_type;
                                                                        else if ($value->intranet_user_type == "alumni")
                                                                            echo "Pass out year: " . $value->year_of_passing;
                                                                        ?>
                                                                    </h6>
                                                                    <button intranet_id ="<?php echo $value->college_users_intranet_id; ?>" user_id="<?php echo $value->user_id; ?>" class="btn DeleteUserFromCollege_Button"><i class="fa fa-trash-o"></i> Delete</button> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li> 
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <li>
                                                    <div class="people_page_info">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p>No Members</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php }
                                            ?>
                                        </ul>
                                    </div>
                                </div>

                                <!-------------------Second tab which shows details of all Users------------------>
                                <div role="tabpanel" class="tab-pane" id="StudentsList_Tab">
                                    <div class="total_people_list">
                                        <ul id="demoTwo" class="demo">
                                            <div class="list_heading">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h6>People</h6>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h6>Details</h6>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
                                            if (count($StudentsList)) {
                                                foreach ($StudentsList as $key => $value) {
                                                    ?>    
                                                    <li>
                                                        <div class="people_page_info">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <div class="intra_user_img">
                                                                        <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                                                                    </div>
                                                                    <div class="pp_details">
                                                                        <h5> 
                                                                            <span class="on_dot" title="offline"></span>
                                                                            <?php
                                                                            echo $value->fname . " " . $value->lname;
                                                                            ?>
                                                                        </h5> 
                                                                        <h6>
                                                                            <?php
                                                                            echo "<strong>" . $value->stream_name . "</strong>";
                                                                            ?>
                                                                        </h6>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 pp_details">
                                                                    <p><?php echo "Joined: " . date('d-m-Y', strtotime($value->timestamp)); ?></p>
                                                                    <h6>
                                                                        <?php
                                                                        if ($value->intranet_user_type != "teacher")
                                                                            echo $value->stream_course_name;
                                                                        ?>
                                                                    </h6>
                                                                    <h6>
                                                                        <?php
                                                                        if ($value->intranet_user_type == "student")
                                                                            echo $temp_array[$value->semester_or_year] . " " . $value->study_type;
                                                                        else if ($value->intranet_user_type == "alumni")
                                                                            echo "Pass out year: " . $value->year_of_passing;
                                                                        ?>
                                                                    </h6>
                                                                    <button intranet_id ="<?php echo $value->college_users_intranet_id; ?>" user_id="<?php echo $value->user_id; ?>" class="btn DeleteUserFromCollege_Button"><i class="fa fa-trash-o"></i> Delete</button> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <li>
                                                    <div class="people_page_info">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p>No Members</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php }
                                            ?>
                                        </ul>
                                    </div>
                                </div>

                                <!--------------Third tab which shows details of all Users------------------>
                                <div role="tabpanel" class="tab-pane" id="AlumniList_Tab">
                                    <div class="total_people_list">
                                        <ul id="demoTwo" class="demo">
                                            <div class="list_heading">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h6>People</h6>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h6>Details</h6>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
                                            if (count($AlumniList)) {
                                                foreach ($AlumniList as $key => $value) {
                                                    ?>    
                                                    <li>
                                                        <div class="people_page_info">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <div class="intra_user_img">
                                                                        <img src="<?php echo base_url(); ?>assets_front/image/recent_user2.png" alt="">
                                                                    </div>
                                                                    <div class="pp_details">
                                                                        <h5> 
                                                                            <span class="on_dot" title="offline"></span>
                                                                            <?php
                                                                            echo $value->fname . " " . $value->lname;
                                                                            ?>
                                                                        </h5> 
                                                                        <h6>
                                                                            <?php
                                                                            echo "<strong>" . $value->stream_name . "</strong>";
                                                                            ?>
                                                                        </h6>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 pp_details">
                                                                    <p><?php echo "Joined: " . date('d-m-Y', strtotime($value->timestamp)); ?></p>
                                                                    <h6>
                                                                        <?php
                                                                        if ($value->intranet_user_type != "teacher")
                                                                            echo $value->stream_course_name;
                                                                        ?>
                                                                    </h6>
                                                                    <h6>
                                                                        <?php
                                                                        if ($value->intranet_user_type == "student")
                                                                            echo $temp_array[$value->semester_or_year] . " " . $value->study_type;
                                                                        else if ($value->intranet_user_type == "alumni")
                                                                            echo "Pass out year: " . $value->year_of_passing;
                                                                        ?>
                                                                    </h6>
                                                                    <button intranet_id ="<?php echo $value->college_users_intranet_id; ?>" user_id="<?php echo $value->user_id; ?>" class="btn DeleteUserFromCollege_Button"><i class="fa fa-trash-o"></i> Delete</button> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <li>
                                                    <div class="people_page_info">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p>No Members</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php }
                                            ?>
                                        </ul>
                                    </div>
                                </div>

                                <!-------------------Fourth tab which shows details of all Users------------------->
                                <div role="tabpanel" class="tab-pane" id="TeachersList_Tab">
                                    <div class="total_people_list">
                                        <ul id="demoTwo" class="demo">
                                            <div class="list_heading">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h6>People</h6>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h6>Details</h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            if (count($TeacherList)) {
                                                foreach ($TeacherList as $key => $value) {
                                                    ?>    
                                                    <li>
                                                        <div class="people_page_info">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <div class="intra_user_img">
                                                                        <img src="<?php echo base_url(); ?>assets_front/image/recent_user2.png" alt="">
                                                                    </div>
                                                                    <div class="pp_details">
                                                                        <h5> 
                                                                            <span class="on_dot" title="offline"></span>
                                                                            <?php
                                                                            echo $value->fname . " " . $value->lname;
                                                                            ?>
                                                                        </h5> 
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 pp_details">
                                                                    <p><?php echo "Joined: " . date('d-m-Y', strtotime($value->timestamp)); ?></p>
                                                                    <button intranet_id ="<?php echo $value->college_users_intranet_id; ?>" user_id="<?php echo $value->user_id; ?>" class="btn DeleteUserFromCollege_Button"><i class="fa fa-trash-o"></i> Delete</button> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <li>
                                                    <div class="people_page_info">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p>No Members</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php }
                                            ?>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of tab1 -->
            </div>
        </div>
    </div>
</div>
<?php
include('footer2.php');
include ('page_modal/college_users_modal.php');
?>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/college_users.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets_front/js/bootbox.min.js" type="text/javascript"></script>

