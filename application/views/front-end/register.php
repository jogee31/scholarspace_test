<?php $this->load->view('front-end/header1'); ?>
<style type="text/css">
    #recaptcha_response_field{
        width: 100%;
        padding: 3px 8px;
        background-color: #ffffff;
        border: 1px solid #e4e4e4 !important;
    }
    .error_msg, .trem_check_error{
        color: #CC1717;
        font-size: 15px;
        font-weight: 600;
    }
    .captcha_response{
        color: #CC1717;
        font-size: 15px;
        font-weight: 600;
    }
</style>
<div class="body_height">
    <div class="college_details">
        <div class="container">
            <div class="new_registration">
                <h3>New Member Registration</h3>
                <div class="row">
                    <div class="col-md-12">
                        <div class="contact_info_reg">
                            <h5>Contact Information</h5>
                            <?php if (validation_errors()) { ?>
                                <div class="alert alert-warning">
                                    <?php echo validation_errors(); ?>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('item')) { ?>
                                <div class="alert alert-success">
                                    <?php echo $this->session->flashdata('item'); ?>
                                </div>
                                <?php
                            }
                            if ($this->session->flashdata('item_error')) {
                                ?>
                                <div class="alert alert-danger">
                                    <?php echo $this->session->flashdata('item_error'); ?>
                                </div>
                                <?php
                            }
                            //item_error
                            ?>
                            <form id="user_registration_form" action="<?php echo site_url("register/index"); ?>" method="post" accept-charset="utf-8">
                                <div class="col-md-8">
                                    <div class="course_message">
                                        <div class="col-md-3"><?php echo form_label('First Name', 'first_name'); ?></div>
                                        <div class="col-md-5">
                                            <?php echo form_input(array("class" => "reg_text", "name" => "first_name", "placeholder" => "Enter First Name", "value" => set_value('first_name'))); ?>
                                        </div>
                                        <div class="col-md-4 error_msg"></div>
                                    </div>
                                    <div class="course_message">
                                        <div class="col-md-3"><?php echo form_label('Last Name', 'last_name'); ?></div>
                                        <div class="col-md-5">
                                            <?php echo form_input(array("class" => "reg_text", "name" => "last_name", "placeholder" => "Enter Last Name", "value" => set_value('last_name'))); ?>
                                        </div>
                                        <div class="col-md-4 error_msg"></div>
                                    </div>
                                    <div class="course_message">
                                        <div class="col-md-3"><?php echo form_label('Email', 'email'); ?></div>
                                        <div class="col-md-5">
                                            <?php echo form_input(array("class" => "reg_text valid_email", "id" => "email", "name" => "email", "placeholder" => "Enter Email", "value" => set_value('email'))); ?>
                                        </div>
                                        <div class="col-md-4 email_error_msg error_msg"></div>
                                    </div>
                                    <div class="course_message">
                                        <div class="col-md-3"><?php echo form_label('Mobile', 'mobile'); ?></div>
                                        <div class="col-md-1"><span class="num_box">+91</span></div>
                                        <div class="col-md-4">
                                            <?php echo form_input(array("class" => "reg_text valid_phone", "type" => "number", "name" => "mobile", "placeholder" => "Enter Mobile Number", "value" => set_value('mobile'))); ?>
                                        </div>
                                        <div class="col-md-4 phone_error_msg error_msg"></div>
                                    </div>
                                    <div class="course_message">
                                        <div class="col-md-3"><?php echo form_label('Password', 'password'); ?></div>
                                        <div class="col-md-5">
                                            <?php echo form_input(array("class" => "reg_text", "id" => "password_check", "type" => "password", "name" => "password", "placeholder" => "Enter Password", "value" => set_value('password'))); ?>
                                        </div>
                                        <div class="col-md-4 error_msg password_error_alert"></div>
                                    </div>
                                    <div class="course_message">
                                        <div class="col-md-3"><?php echo form_label('Confirm Password', 'confirm_password'); ?></div>
                                        <div class="col-md-5">
                                            <?php echo form_input(array("class" => "reg_text", "type" => "password", "id" => "confirm_password", "name" => "confirm_password", "placeholder" => "Enter Confirm Password", "value" => set_value('confirm_password'))); ?>
                                        </div>
                                        <div class="col-md-4 error_msg confirm_password_error_alert"></div>
                                    </div>
                                    <div class="course_message">
                                        <div class="col-md-8">
                                            <script type="text/javascript">
                                                var RecaptchaOptions = {
                                                    theme: 'clean'
                                                };
                                            </script>
                                            <script type="text/javascript" src="https://www.google.com/recaptcha/api/challenge?k=6LfFBQgTAAAAALCGsNxskGwNGHyaDCGvNBtB1vZz"></script>
                                        </div>
                                        <div class="col-md-4 error_msg captcha_error_msg"></div>
                                    </div>
                                    <div class="reg_agree">
                                        <div class="col-md-12 captcha_response"></div>
                                    </div>
                                    <div class="reg_agree">
                                        <div class="col-md-12">
                                            <div class="checkbox">
                                                <label><input type="checkbox" id="terms_check_box" value="">I agree to <a href="#">terms & conditions</a> </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="reg_agree">
                                        <div class="col-md-12 trem_check_error"></div>
                                    </div>
                                    <div class="reg_agree">
                                        <div class="col-md-12"> 
                                            <div class="checkbox">
                                                <button type="submit" class="btn btn-primary">Register</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of registration page-->
        </div>
    </div>
</div>
<input type="hidden" id="site_url" value="<?php echo site_url(); ?>"/>
<input type="hidden" id="base_url" value="<?php echo base_url(); ?>"/>
<?php $this->load->view('front-end/footer'); ?>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/register.js" type="text/javascript"></script>