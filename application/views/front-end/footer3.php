<!-- .........footer begins............ -->
<div class="alum_footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="alum_footer_info">
                    <p>@2015 IIt Kharangpur<br>
                        Powered  by <a href="#">Scholarspace</a> </p>
                </div>
            </div>

            <div class="col-md-3">
                <div class="alum_footer_info">
                    <h6>LINKS</h6>
                    <ul>
                        <li> <a href="#">About IIT Kharagpur</a> </li>
                        <li> <a href="#">Centres  </a> </li>
                        <li> <a href="#">Programs</a> </li>
                        <li> <a href="#">Gallery    </a> </li>
                        <li> <a href="#">Faculty & Research    </a> </li>
                        <li> <a href="#">Alumini </a> </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3">
                <div class="alum_footer_info">
                    <ul style="padding-top: 30px;">
                        <li> <a href="#">Overview   </a> </li>
                        <li> <a href="#">Give   </a> </li>
                        <li> <a href="#">Alumni Magazine </a> </li>
                        <li> <a href="#">Distinguished Alumnus Award </a> </li>
                        <li> <a href="#">Alumni News </a> </li>
                        <li> <a href="#">My Chapter   </a> </li>
                        <li> <a href="#">Contact Us   </a> </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3">
                <div class="alum_social_icons">
                    <h6>FOLLOW US</h6>
                    <ul>
                        <li><a href="#"> <span class="fb_alum"></span> </a> </li>
                        <li><a href="#"> <span class="tw_alum"></span> </a> </li>
                        <li><a href="#"> <span class="li_alum"></span> </a> </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>

</body>

<script src="<?php echo base_url(); ?>assets_front/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets_front/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets_front/js/jquery.tinycarousel.js"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        var tab = 0;
        $('#slider1').tinycarousel({
            axis: "y"
        });


        $('.plus_drop,news_plus').click(function () {
            var id = $(this).val();
            $('#plus_dropdown' + id).slideToggle();
            $('.plus_new' + id).css('background-position', '-15px 0px');
            return false;
        });
        var g_id = '';
        $('.plus_drop').click(function () {
            var id = $(this).val();

            if (g_id !== '' && g_id !== id) {
                $('#plus_dropdown' + g_id).slideUp(200);
                $('.plus_new' + g_id).css('background-position', '0px 0px');
            }

            else
            {
                $('.plus_new' + g_id).css('background-position', '0px 0px');
            }

            g_id = id;
            g_id = $(this).val();
            return false;
        });

    });
</script>


</html>







