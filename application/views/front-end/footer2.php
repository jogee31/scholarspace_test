<!-- .........footer begins............ -->
<div class="online_footer">
    <div class="container">
        <div class="online_box">
            <div id="open_chat"><span class="online_img"></span> Online Now </div> 
            <div style="float:right; padding: 9px 12px;">
                <span class="on_set"></span>
                <span class="on_dash"></span>
            </div>
        </div>

        <div class="set_pop">
            <span class="point_top"></span> 
            <ul>
                <li>Sounds</li>
                <li>Hide messages</li>
            </ul>
        </div>


        <div class="online_chat">

            <div class="online_search_box">
                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for people" name="srch-term" id="srch_msg">
                    </div>
                </form>
            </div>

            <div class="people_list">
                <ul>
                    <li> 
                        <div class="notify_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                        </div>
                        <h5 id="chat_pop_name">Haneef  <span class="on_dot"></span> </h5> 
                        <div class="user_popup" id="chat_name_res">
                         <!--  <span class="point_top"></span>  -->
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="intra_user_img" style="padding: 4px 9px;">
                                        <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="pop_user_info">
                                        <h5>Haneef</h5>
                                        <h6>Web developer</h6>
                                        <h6>Groups: Web development design</h6>
                                        <h6>Email : <a href="mailto:haneef@hexwhale.com"> haneef@hexwhale.com</a></h6>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="pop_send">
                                        <a href="#">Send Message</a>
                                        <a href="#"> <span class="tick"></span> Following</a>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <h6>Web developer</h6>
                    </li>

                    <li> 
                        <div class="notify_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user2.png" alt="">
                        </div>
                        <h5>Andrew  <span class="on_dot"></span> </h5> 
                        <h6>Web developer</h6>
                    </li>

                    <li> 
                        <div class="notify_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                        </div>
                        <h5>Haneef  <span class="on_dot"></span> </h5> 
                        <h6>Web developer</h6>
                    </li>

                    <li> 
                        <div class="notify_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user3.png" alt="">
                        </div>
                        <h5>Haneef  <span class="on_dot"></span> </h5> 
                        <h6>Web developer</h6>
                    </li>

                    <li> 
                        <div class="notify_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user2.png" alt="">
                        </div>
                        <h5>Andrew  <span class="on_dot"></span> </h5> 
                        <h6>Web developer</h6>
                    </li>

                    <li> 
                        <div class="notify_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                        </div>
                        <h5>Haneef  <span class="on_dot"></span> </h5> 
                        <h6>Web developer</h6>
                    </li>

                    <li> 
                        <div class="notify_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                        </div>
                        <h5>Haneef  <span class="on_dot"></span> </h5> 
                        <h6>Web developer</h6>
                    </li>

                    <li> 
                        <div class="notify_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user3.png" alt="">
                        </div>
                        <h5>Haneef  <span class="on_dot"></span> </h5> 
                        <h6>Web developer</h6>
                    </li>

                    <li> 
                        <div class="notify_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user2.png" alt="">
                        </div>
                        <h5>Andrew  <span class="on_dot"></span> </h5> 
                        <h6>Web developer</h6>
                    </li>

                    <li> 
                        <div class="notify_img">
                            <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                        </div>
                        <h5>Haneef  <span class="on_dot"></span> </h5> 
                        <h6>Web developer</h6>
                    </li>

                </ul>
            </div>
        </div>
        <!-- end of online chat -->

    </div>
</div>
</body>
<script src="<?php echo base_url(); ?>assets_front/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets_front/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/intranet_group.js" type="text/javascript"></script>
<script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
<script type="text/javascript">
    var global = 0;
    var num = 0;
    var res = 0;
    $(function () {

        $('#topic1').click(function () {
            $('#open_topic').css('display', 'block');
            $('#topic1').css('display', 'none');

        });
        $('#topic2').click(function () {
            $('#open_topic2').css('display', 'block');
            $('#topic2').css('display', 'none');

        });

        $('#comment_in1').click(function () {
            $('#more_in1').css('display', 'block');
            $('#comment_in1').css('display', 'none');

        });

        $('#set_box').click(function () {
            $(".setting_list").slideToggle();
            if (global == 0)
            {

                $('.setting_box1').css('box-shadow', ' 0 1px 2px 1px rgba(0,0,0,.2)');
                $('.setting_box1').css('background-color', 'white');

                global++;

            }
            else
            {
                $('.setting_box1').css('box-shadow', 'none');
                $('.setting_box1').css('background-color', 'transparent');
                global--;
            }

        });

        $('#notify_id').click(function () {

            if (global == 0)
            {
                $('.notify_pop').show();
                global++;
            }
            else
            {
                $('.notify_pop').hide();
                global--;
            }

        });
        $('.pop_close').click(function () {
            $('#second_notify').show();
            $('.add_text ').css('display', 'block');
            $('#remove_com').css('display', 'none');
            $('#notify_id').css('display', 'none');

        });

        $('#notify_id2').click(function () {
            $('#second_notify').hide();

        });

        /* code is for name popup */
        $(".top_name").hover(function () {
            var temp_this = $(this);
            if (global == 0)
            {
                temp_this.parent('h5').siblings('div .res_top_name').fadeIn('slow');
                global++;
            }
        });
        $(".res_top_name").mouseleave(function () {
            $(this).fadeOut('slow');
            global--;
        });

        $("#global_name").hover(function () {

            if (global == 0)
            {
                $('#res_global_name').fadeIn('slow');
                global++;
            }

            else
            {
                $('#res_global_name').hide();
                global--;
            }

        });


        $("#global_name").hover(function () {

            if (global == 0)
            {
                $('#res_global_name').fadeIn('slow');
                global++;
            }

            else
            {
                $('#res_global_name').hide();
                global--;
            }

        });



        $('.edit').click(function () {
            $('.edit_more').css('display', 'block');
        });

        $('.edit2').click(function () {
            $('.edit_more2').css('display', 'block');
        });


        $('#open_chat ').click(function () {

            if (num == 0)
            {
                $('.online_chat ').css('display', 'block');
                $('.online_box ').css('bottom', '364px');
                num++;
            }

            else
            {
                $('.online_chat ').css('display', 'none');
                $('.online_box ').css('bottom', '0px');
                $('.set_pop ').hide();
                num--;
            }

        });


        $('.online_box div .on_set').click(function () {

            if (res == 0)
            {
                $('.set_pop ').show();
                res++;
            }

            else
            {
                $('.set_pop ').hide();
                res--;
            }

        });


        $('.on_dash').click(function () {
            $('.online_chat ').css('display', 'none');
            $('.online_box ').css('bottom', '0px');
            $('.set_pop ').hide();
        });


        $("#chat_pop_name").hover(function () {

            if (global == 0)
            {
                $('#chat_name_res').show();
                global++;
            }

            else
            {
                $('#chat_name_res').hide();
                global--;
            }

        });

        $('.praise_icons').click(function () {
            var i = 0;
            if (i == 0)
            {
                $('.praise_icons').css('background-position', '0px -46px');
                i++;
                $('.praise_icons').click(function () {
                    $('.praise_icons').css('background-position', '0px -92px');
                });
            }

        });


        $('.share_box').click(function () {
            $(this).css('display', 'none');
            $('.show_share').show();
        });

//        $('.pin2').click(function () {
//            if (num == 0)
//            {
//                $('.attach_list').show();
//                $('.pin2').css('box-shadow', '0 1px 2px 1px rgba(0,0,0,.2)');
//                num++;
//            }
//            else
//            {
//                $('.attach_list').hide();
//                $('.pin2').css('box-shadow', 'none');
//                num--;
//            }
//        });


        $('.pin').click(function () {
            if (num == 0)
            {
                $('.attach_list').show();
                $('.pin2').css('box-shadow', '0 1px 2px 1px rgba(0,0,0,.2)');
                num++;
            }
            else
            {
                $('.attach_list').hide();
                $('.pin2').css('box-shadow', 'none');
                num--;
            }
        });

    });
</script>


<script>
    function myFunction(name)
    {

        if (global == 0)
        {
            document.getElementById("text_change").innerHTML = " " + name;
            global++;
        }
        else
        {
            document.getElementById("text_change").innerHTML = "Edit topics ";
            global--;
        }

    }


    function myFunction2(name)
    {

        if (global == 0)
        {
            document.getElementById("text_change2").innerHTML = " " + name;
            global++;
        }
        else
        {
            document.getElementById("text_change2").innerHTML = "Edit topics ";
            global--;
        }

    }


</script>

<script>
    $(function () {
//        $("#from").datepicker({
//            defaultDate: "+1w",
//            changeMonth: true,
//            numberOfMonths: 1,
//            dateFormat: 'dd-mm-yy',
//            onClose: function (selectedDate) {
//                $("#to").datepicker("option", "minDate", selectedDate);
//
//            }
//        });
//        $("#to").datepicker({
//            defaultDate: "+1w",
//            changeMonth: true,
//            numberOfMonths: 1,
//            dateFormat: 'dd-mm-yy',
//            onClose: function (selectedDate) {
//                $("#from").datepicker("option", "maxDate", selectedDate);
//            }
//        });
//        $('.click_more').click(function () {
//            if (num == 0) {
//                $('.more_list').hide();
//                $(this).siblings('.more_list').show();
//                num++;
//            }
//            else
//            {
//                $('.more_list').hide();
//                $(this).siblings('.more_list').hide();
//                num--;
//            }
//        });
    });
</script>
</html>