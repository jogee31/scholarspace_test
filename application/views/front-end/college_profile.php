<?php $this->load->view('front-end/header3.php'); ?>
<style type="text/css">
    .danger_alert{
        display: none;
    }
    .temp_row{
        margin-right: 0px;
        margin-left: 0px;
    }
    .modal_alum_reg:hover{
        background-color: #e79521;
        text-shadow: #fff;
    }
    .modal_alum_reg:focus { 
        outline: none;
    }
    .modal_alum_reg_active { 
        background-color: #DA850A;
    }
    .required_border{
        border-color: #D25050 !important;
    }
    .modal_danger_alert{
        display: none;
    }
</style>
<div class="body_top">
    <div class="banner_image">
        <img src="<?php echo base_url(); ?>assets_front/image/banner_img1.png" alt="">

        <div class="banner_heading">
            <h2>INDIAN INSTITUTE OF TECHNOLOGY</h2>
            <h3>KHARAGPUR</h3>
        </div>
    </div>

    <div class="alum_links_reg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="alum_links">
                        <ul>
                            <li><a href="#"> Overview </a></li>
                            <li><a href="#"> Give  </a></li>
                            <li><a href="#"> Alumni Magazine </a></li>
                            <li><a href="#"> Distinguished Alumnus Award</a></li>
                            <li><a href="#"> Alumni News  </a></li>
                            <li><a href="#"> My Chapter </a></li>
                            <li><a href="#"> Contact Us </a></li>          
                        </ul>
                        <div class="alum_reg_log <?php echo ($this->ion_auth->logged_in()) ? "pull-right" : ""; ?>">
                            <?php
                            if ($this->ion_auth->logged_in()) {
                                $user_details = $this->ion_auth->user()->row();
                                $user_type = $user_details->user_type;
                                $user_id = $user_details->id;
                                ?>
                                <input type="hidden" id="user_name_hidden" value="<?php echo $user_details->first_name . " " . $user_details->last_name; ?>"/>
                                <a style="cursor: pointer;<?php echo (isset($requested_user)) ? "cursor:default;background-color:#EAB973; border: 1px solid #EAB973;" : ""; ?>" request_status="<?php echo (isset($requested_user)) ? "already_sent" : "not_sent"; ?>" college_id="<?php echo $college_id; ?>" user_id="<?php echo $user_id; ?>" id="send_college_request_btn" class="alum_reg send_college_request_btn"> <span class="lock2_img"></span><?php echo (isset($requested_user)) ? "Request already sent" : "Send Request here"; ?></a>
                                <?php
                            } else {
                                ?>
                                <a href="#" class="alum_log"> <span class="lock_img"></span> Login</a>
                                <a href="#" class="alum_reg"> <span class="lock2_img"></span> Register</a>
                                <a href="#" class="alum_pass">Forgot password?</a>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- end of alumini links and login -->

    <div class="alum_slider_plus">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="alum_slider">
                        <div class="alum_slide_content">
                            <h3>ALUMINI</h3>
                            <h4>Distinguished Alumnus Award 2014 announced</h4>                  
                        </div>

                        <div id="slider1">


                            <a class="buttons prev" href="#"><span class="alum_arrows_prev"></span></a>
                            <a class="buttons next" href="#"> <span class="alum_arrows_next"></span> </a>


                            <div class="viewport">
                                <ul class="overview">
                                    <li>
                                        <div class="alum_users">
                                            <div class="alum_user_img">
                                                <img src="<?php echo base_url(); ?>assets_front/image/alum_user1.png" alt="">
                                            </div>
                                            <h6>JOHN DOE</h6>
                                            <p>John Doe is the Co-founder and Managing Director of foodpanda India. Foodpanda India provides a service that allows users to select local restaurants </p>
                                            <a href="#">Read more</a>  
                                        </div>
                                        <!--      end of user alum 1 -->
                                        <div style="border-top:1px solid #67b2db; width:73px; float:left; margin-bottom:33px;"></div>
                                        <div class="alum_users">
                                            <div class="alum_user_img">
                                                <img src="<?php echo base_url(); ?>assets_front/image/alum_user1.png" alt="">
                                            </div>
                                            <h6>JOHN DOE</h6>
                                            <p>John Doe is the Co-founder and Managing Director of foodpanda India. Foodpanda India provides a service that allows users to select local restaurants </p>
                                            <a href="#">Read more</a>  
                                        </div>

                                    </li>

                                    <li>     
                                        <div class="alum_users">
                                            <div class="alum_user_img">
                                                <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                                            </div>
                                            <h6>JOHN DOE</h6>
                                            <p>John Doe is the Co-founder and Managing Director of foodpanda India. Foodpanda India provides a service that allows users to select local restaurants </p>
                                            <a href="#">Read more</a>  
                                        </div>
                                        <!--      end of user alum 1 -->
                                        <div style="border-top:1px solid #67b2db; width:73px; float:left; margin-bottom:33px;"></div>
                                        <div class="alum_users">
                                            <div class="alum_user_img">
                                                <img src="<?php echo base_url(); ?>assets_front/image/alum_user1.png" alt="">
                                            </div>
                                            <h6>JOHN DOE</h6>
                                            <p>John Doe is the Co-founder and Managing Director of foodpanda India. Foodpanda India provides a service that allows users to select local restaurants </p>
                                            <a href="#">Read more</a>  
                                        </div>

                                    </li>
                                    <li>
                                        <div class="alum_users">
                                            <div class="alum_user_img">
                                                <img src="<?php echo base_url(); ?>assets_front/image/recent_user1.png" alt="">
                                            </div>
                                            <h6>JOHN DOE</h6>
                                            <p>John Doe is the Co-founder and Managing Director of foodpanda India. Foodpanda India provides a service that allows users to select local restaurants </p>
                                            <a href="#">Read more</a>  
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="alum_slider" style="background-color:#fafafa">
                        <div class="alum_slide_content">
                            <h5>NEWS</h5>
                            <div class="alum_news_plus">
                                <ul>
                                    <li class="plus_drop" value="1">
                                        <div style="width:100%;float:left;"><h6>12 Sep 2015</h6>  <span class="news_plus plus_new1"></span> </div>
                                        <h4>LAWYERS WEEKLY</h4>
                                        <div id="plus_dropdown1" class="news_plus_info">
                                            <p>Leena Yousefi’s entrepreneurial journey of building and growing YLaw and her suggestions for overcoming the fear of starting your own practice are compiled in this inspirational article published February 2015</p>
                                        </div>
                                        <div style="border-top:1px solid #dcdcdc; width:122px; margin:27px 0px;"></div>
                                    </li>

                                    <li class="plus_drop" value="2">
                                        <div style="width:100%;float:left;"><h6>15 Sep 2015</h6>  <span class="news_plus plus_new2"></span> </div>
                                        <h4>NATIONAL POST</h4>
                                        <div id="plus_dropdown2" class="news_plus_info">
                                            <p>Leena Yousefi’s entrepreneurial journey of building and growing YLaw and her suggestions for overcoming the fear of starting your own practice are compiled in this inspirational article published February 2015</p>
                                        </div>
                                        <div style="border-top:1px solid #dcdcdc; width:122px; margin:27px 0px;"></div>                  
                                    </li>

                                    <li class="plus_drop" value="3">
                                        <div style="width:100%;float:left;"><h6>25 Aug 2015</h6>  <span class="news_plus plus_new3"></span> </div>
                                        <h4>LAWYERS WEEKLY</h4>
                                        <div id="plus_dropdown3" class="news_plus_info">
                                            <p>Leena Yousefi’s entrepreneurial journey of building and growing YLaw and her suggestions for overcoming the fear of starting your own practice are compiled in this inspirational article published February 2015</p>
                                        </div>
                                        <div style="border-top:1px solid #dcdcdc; width:122px; margin:27px 0px;"></div>                  
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="site_url" value="<?php echo site_url(); ?>"/>
    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>"/>
    <input type="hidden" id="college_id" value="<?php echo $college_id; ?>"/>
    <?php $this->load->view('front-end/footer3.php'); ?>
    <script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/college_profile.js" type="text/javascript"></script>

    <div class="main_page_login" id="home_reg">
        <div class="modal fade" id="modal_success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div style="background-color: #0C9076" class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="reg_close" aria-hidden="true"></span></button>
                        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-check"></i> Successful!</h4>
                    </div>
                    <div class="modal-body">
                        <div class="contact_info_reg"></div>
                    </div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="main_page_login" id="home_reg">
        <div class="modal fade" id="college_request_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" style="top: 50px; width: 626px;" role="document">
                <form id="user_request_form">
                    <div id="request_modal" class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="reg_close" aria-hidden="true"></span></button>
                            <h4 class="modal-title" id="myModalLabel">Request with following Details</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="modal_danger_alert alert alert-danger alert-dismissable" style="width: 94%;float: right;margin-right: 16px;">
                                        <h4><i class="icon fa fa-check"></i> Error ! <span id="modal_danger_alert_message" style="font-size: 15px;">Please fill required fields.</span></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row temp_row contact_info_reg" style="margin-right: 0px;margin-left: 0px;">
                                <div class="col-md-6">
                                    <a style="cursor: pointer;width: 100%; text-align: center;" request_type="student" class="alum_reg modal_alum_reg"> <span class="lock2_img"></span>As Student</a>
                                </div>
                                <div class="col-md-6">
                                    <a style="cursor: pointer;width: 100%;text-align: center;" request_type="alumni" class="alum_reg modal_alum_reg"> <span class="lock2_img"></span>As Alumni</a>
                                </div>
                            </div>
                            <div id="content_div" style="padding-top: 55px;"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>




