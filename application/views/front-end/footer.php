<!-- .........footer begins............ -->
<div class="footer">
    <input type="hidden" id="site_url" value="<?php echo site_url(); ?>"/>
    <input type="hidden" id="base_url" value="<?php echo site_url(); ?>"/>
    <div class="col-md-9">

        <div class="footer_about">
            <h4>SCHOLAR SPACE</h4>

            <ul>
                <li><a href="#">About</a></li>
                <li><a href="#">News </a></li>
                <li><a href="#"> Contact</a></li>
                <li><a href="#">Privacy policy</a></li>
                <li><a href="#">Terms and Conditions</a></li>
                <li>Copyright@2015<a href="#"> Scholar Space Ltd.</a> All rights reserved</li>

            </ul> 

        </div>

    </div>


    <div class="col-md-3">

        <div class="join_us">

            <h5>Join us on</h5>

            <div class="social_icons2">
                <ul>
                    <li> <a href="#"> <span class="fb2"></span>  </a> </li>
                    <li> <a href="#"> <span class="tw2"></span>  </a> </li>
                    <li> <a href="#"> <span class="go2"></span>  </a> </li>
                </ul>

            </div>

            <h6>Developed by <a href="#">Hexwhale Technologies</a></h6>

        </div>

    </div>

</div>



</body>

<script src="<?php echo base_url(); ?>assets_front/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets_front/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets_front/custom_assets/login.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function () {

        $('#degree_drop1').click(function () {

            $('#degree_drop1 .arrow_down').css('backgroundImage', 'url(image/arrow_down.png)');
            $('#degree_list1').slideToggle();

        });

        $('#degree_drop2').click(function () {

            $('#degree_drop2 .arrow_down').css('backgroundImage', 'url(image/arrow_down.png)');
            $('#degree_list2').slideToggle();

        });

        $('#degree_drop3').click(function () {

            $('#degree_drop3 .arrow_down').css('backgroundImage', 'url(image/arrow_down.png)');
            $('#degree_list3').slideToggle();

        });

        $('#degree_drop4').click(function () {

            $('#degree_drop4 .arrow_down').css('backgroundImage', 'url(image/arrow_down.png)');
            $('#degree_list4').slideToggle();

        });

        $('#degree_drop5').click(function () {

            $('#degree_drop5 .arrow_down').css('backgroundImage', 'url(image/arrow_down.png)');
            $('#degree_list5').slideToggle();

        });

        $('#degree_drop6').click(function () {

            $('#degree_drop6 .arrow_down').css('backgroundImage', 'url(image/arrow_down.png)');
            $('#degree_list6').slideToggle();

        });


        // $(".left_search_bar").mouseout(function(){
        //     $(".degree_list").css("display", "none");

        // });

        $(window).load(function () {

            $(window).scroll(function () {
                //console.log($(window).scrollTop() + $(window).height()+" ");
                //console.log((parseInt($(window).scrollTop()) + parseInt($(window).height())) +" s "+ (parseInt($(document).height())-117)); 
                if ((parseInt($(window).scrollTop()) + parseInt($(window).height())) >= (parseInt($(document).height()) - 116)) {
                    // alert("bottom!");
                    var total = (parseInt($(window).scrollTop()) + parseInt($(window).height())) - (parseInt($(document).height()));
                    $('.left_search_bar ').css('top', '100px');
                    console.log(parseInt(total));
                    $('.left_search_bar ').css('bottom', (116 + parseInt(total)) + 'px');
                }

                else
                {

                    $('.left_search_bar ').css('bottom', '0px');
                }

            });

        });





    });

</script>
</html>







