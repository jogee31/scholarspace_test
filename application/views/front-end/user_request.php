<?php
include('intra_leftbar.php');
?>
<style type="text/css">
    .request_handling_btns{
        float: right;
        padding: 8px 0px 0px;
    }
    .recent_activity_modified {
        margin-top: 0px !important;
    }
</style>
<input type="hidden" id="college_id" value="<?php echo $college_id; ?>"/>
<div class="col-lg-9">
    <div class="boards_tab">
        <div>
            <div class="tab-content">
                <div class="tab-pane active" id="batch" role="tabpanel">
                    <div class="announce">
                        <div class="success_alert alert alert-success alert-dismissable" style="display: none;padding: 8px;margin-bottom: 10px;">
                            <h4 style="padding: 0px; margin: 0px;">
                                <i class="icon fa fa-check"></i> Success ! 
                                <span id="success_alert_message" style="font-size: 15px;"></span></h4>
                        </div>
                        <div class="danger_alert alert alert-danger alert-dismissable" style="display: none;padding: 8px;margin-bottom: 10px;">
                            <h4 style="padding: 0px; margin: 0px;">
                                <i class="icon fa fa-warning"></i> Error ! 
                                <span id="danger_alert_message" style="font-size: 15px;"></span></h4>
                        </div>
                        <h5><i class="fa fa-user-plus"></i> New User Request</h5>
                        <div id="request_div_content" class="total_anounce">
                            <?php
                            $temp_array = array("", "First", "Second", "Third", "Forth", "Fifth", "Sixth", "Seventh", "Eighth", "Ninth", "Tenth", "Eleventh", "Twelth");

                            foreach ($admin_user_requests as $value) {
                                $user = $this->ion_auth->user($value->user_id)->row();
                                if (!empty($user)) {
                                    ?>
                                    <div class="user_announce">
                                        <div class="announce_img">
                                            <img src="<?php echo base_url(); ?>/assets_front/image/user.png" alt="">
                                        </div>
                                        <div class="announce_details">
                                            <a href="#" class="reqDetailsPopUp">
                                                <?php echo $user->first_name . " " . $user->last_name; ?>
                                                <span style="color: #B1A8A8;font-size: 11px;" class="">(Request as <?php echo ucfirst($value->requested_user_type); ?>)</span>
                                            </a>
                                            <h6>
                                                <?php
                                                $requested_date = date('M j Y', strtotime($value->timestamp));
                                                echo (($requested_date == date('M j Y')) ? "Today" : $requested_date) . " at " . date('h:i A', strtotime($value->timestamp));
                                                ?>
                                            </h6>
                                            <?php
                                            if ($value->requested_user_type != 'teacher') {
                                                ?>
                                                <span style="color: #B1A8A8;font-size: 12px;" class="">
                                                    <?php
                                                    if ($value->requested_user_type == "student") {
                                                        echo $student_request_details_array[$value->id]->stream_name . " - " . $student_request_details_array[$value->id]->stream_course_name . " (" . ((key_exists($student_request_details_array[$value->id]->semester_or_year, $temp_array)) ? $temp_array[$student_request_details_array[$value->id]->semester_or_year] : $student_request_details_array[$value->id]->semester_or_year) . " " . ucfirst($student_request_details_array[$value->id]->study_type) . ")";
                                                    } else {
                                                        echo $alumni_request_details_array[$value->id]->stream_name . " - " . $alumni_request_details_array[$value->id]->stream_course_name . " (Year of passing - " . $alumni_request_details_array[$value->id]->year_of_passing . ")";
                                                    }
                                                    ?>
                                                </span>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <div class="request_handling_btns">
                                            <!--<button request_id="<?php echo $value->id; ?>" requested_user_type="<?php echo $value->requested_user_type; ?>" college_id="<?php //echo $value->college_id;  ?>" class="btn btn-primary btn-xs user_request_confirm" data-toggle="modal" data-target="#CheckBeforConfirm_Modal"><i class="fa fa-check"></i> Confirm</button>-->
                                            <button user_id="<?php echo $value->user_id; ?>" request_id="<?php echo $value->id; ?>" requested_user_type="<?php echo $value->requested_user_type; ?>" college_id="<?php echo $value->college_id; ?>" class="btn btn-primary btn-xs CheckBeforConfirm"><i class="fa fa-check"></i> Confirm</button>
                                            <button user_id="<?php echo $value->user_id; ?>" college_id="<?php echo $value->college_id; ?>" class="btn btn-primary btn-xs user_request_confirm"><i class="fa fa-envelope-o"></i> Send Message</button>
                                            <button request_id="<?php echo $value->id; ?>" requested_user_type="<?php echo $value->requested_user_type; ?>" class="btn btn-default btn-xs confirm user_request_delete"><i class="fa fa-trash"></i> Delete Request</button>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            if (count($admin_user_requests) == 0) {
                                ?>
                                <div class="user_announce">
                                    <div class="announce_details">
                                        <h6>No pending request found.</h6>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>     
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="request_count" value="<?php echo count($admin_user_requests); ?>"/>
<input type="hidden" id="site_url" value="<?php echo site_url(); ?>"/>
<input type="hidden" id="base_url" value="<?php echo base_url(); ?>"/>
<?php
include('footer2.php');
include ('page_modal/user_request_modal.php');
?>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/user_request.js" type="text/javascript"></script>