<?php include('intra_leftbar.php') ?>
<link href="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<input type="hidden" id="fixed_group_name" value="hostel"/>
<input type="hidden" id="hotel_description_hidden" value="<?php echo (count($hostel_description)) ? $hostel_description[0]->hostel_description : ""; ?>"/>
<div class="col-md-6">
    <div class="boards_tab">
        <div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="success_alert alert alert-success alert-dismissable" style="padding: 8px;margin-top: 10px;">
                    <h4 style="padding: 0px; margin: 0px;">
                        <i class="icon fa fa-check"></i> Success ! 
                        <span id="success_alert_message" style="font-size: 15px;"></span></h4>
                </div>
                <?php include('fixed_group_head.php') ?>
                <div class="hostel_details">
                    <div class="hostel_heading_div">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="hostel_heading_h3">My Hostel</h3>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-default btn-xs pull-right hostel_details_edit_btn"><i class="fa fa-edit"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="hostel_info"> 
                        <p id="hostel_description_details">
                            <?php
                            if (count($hostel_description)) {
                                echo $hostel_description[0]->hostel_description;
                            } else {
                                echo "Description not addedd.";
                            }
                            ?>
                        </p>
                    </div> 
                </div>
                <?php include('fixed_group_body.php') ?>
                <!-- end of tab1 -->

                <div role="tabpanel" class="tab-pane" id="job">
                    <h6>Campus</h6>
                    <p>The institute has following major facilities:</p>
                </div>


                <div role="tabpanel" class="tab-pane" id="know">
                    <h6>History</h6>
                    <p>GVIET is managed and run by Gurukul Vidyapeeth. The institute is affiliated to Punjab Technical University, Jalandhar and recognised by AICTE, New Delhi.</p>
                </div>


                <div role="tabpanel" class="tab-pane" id="blog"></div>
            </div>
        </div>
    </div>


</div>

<div class="col-md-3">
    <div class="right_bar">
        <div class="recent_activity">
            <h5>Recent Activities</h5>
            <div class="activities">
                <div class="act_img">
                    <img src="<?php echo base_url(); ?>assets_front/image/vr_img.png" alt="">
                </div>  
                <p> <a href="#">Vishnu Ravi</a> and Haneef Mp have joined Design.</p> 
            </div>
            <!-- end of 1 activity -->

            <div class="activities">
                <div class="act_img">
                    <img src="<?php echo base_url(); ?>assets_front/image/sa.png" alt="">
                </div>  
                <p> <a href="#">Stella Ammanna</a> changed their Job Title from web designing to Web Developer.</p> 
            </div>
            <!-- end of 2 activity -->

            <div class="activities">
                <div class="act_img">
                    <img src="<?php echo base_url(); ?>assets_front/image/sm.png" alt="">
                </div>  
                <p> <a href="#">shihas Mandottil</a> has joined PHP Development</p> 
            </div>
            <!-- end of 3 activity -->

            <div class="activities">
                <div class="act_img">
                    <img src="<?php echo base_url(); ?>assets_front/image/vr_blue.png" alt="">
                </div>  
                <p> <a href="#">Vishnu Ravi</a> has Created Design Group</p> 
            </div>
            <!-- end of 4 activity -->

        </div>
        <!-- end of recent activity -->
    </div>
    <!-- end of right bar -->
</div>


</div>
</div>
</div>
<?php include('footer2.php') ?>
<?php include('page_modal/hostel_page_modal.php') ?>
<script src="<?php echo base_url(); ?>assets_front/fancy_box/source/jquery.fancybox.pack.js" type="text/javascript"></script>

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/fancy_box/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/fixed_group.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/front_end/hostel_page.js" type="text/javascript"></script>