<?php
$this->load->helper('college-admin_helper');

$encryption_decryption_object = new Encryption; //autoloaded helper class inside config/autoload.php file

$total_requst_count = college_membership_request();

$current_location = $this->router->fetch_class() . "/" . $this->router->fetch_method();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Scholar Space | Home page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/bootstrap.css">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/style.css"/>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/dev_style.css"/>
        <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/jquery-ui.css">
        <link href="<?php echo base_url(); ?>assets_front/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="second_header">
            <div class="second_top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="institute_logo">
                                <a href="#"> <img src="<?php echo base_url(); ?>assets_front/image/iit_logo.png" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="by_logo">
                                <h6>Powered by</h6>  
                                <div class="logo">
                                    <a href="<?php echo site_url("/"); ?>"><img src="<?php echo base_url(); ?>assets_front/image/logo.png" alt=""></a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end of top -->
            <div class="middle">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">

                            <div class="intranet_menu">
                                <!-- Static navbar -->
                                <nav class="navbar navbar-default">
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <!--  <a class="navbar-brand" href="#">Project name</a> -->
                                    </div>
                                    <div id="navbar" class="navbar-collapse collapse">
                                        <ul class="nav navbar-nav">
                                            <?php $url = trim(basename($_SERVER['PHP_SELF'], '.php') . PHP_EOL); ?>
                                            <li><a href="intranet.php">Home</a></li>
                                            <li><a href="inbox.php"> <span class="inbox"></span> Inbox </a> </li>
                                            <li><a href="batch_cse.php"> <span class="inbox_bell"></span></a></li>
                                            <li class="<?php echo ($current_location == "request/index") ? "active" : "" ?>">
                                                <a href="<?php echo site_url("user/request"); ?>"><i class="fa fa-user-plus"></i>&nbsp;
                                                    <?php
                                                    if ($total_requst_count) {
                                                        ?>
                                                        <span class="counts"><?php echo $total_requst_count; ?></span>
                                                        <?php
                                                    }
                                                    ?>
                                                </a>
                                            </li>
                                        </ul>

                                    </div><!--/.nav-collapse -->
                                </nav>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="middle_box">
                                <div class="middle_search">
                                    <form class="navbar-form" role="search">
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                                <button class="btn btn-default" type="submit"> <span class="search"></span> </button>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Search for people, groups and conversations" name="srch-term" id="srch-term">

                                        </div>
                                    </form>
                                    <div class="setting_box1" id="set_box"> <span class="settings_image"></span> </div> 
                                    <div class="setting_list">
                                        <ul>
                                            <li> <a href="files_page.php"><h6><i class="fa fa-user-plus pull-left"></i>&nbsp;Invite</h6></a></li>
                                            <li> <a href="files_page.php"><h6><i class="fa fa-group pull-left"></i>&nbsp;Group</h6></a></li>
                                            <li> <a href="files_page.php"><h6><i class="fa fa-user pull-left"></i>&nbsp;People</h6></a></li>
                                            <li> <a href="files_page.php"><h6><i class="fa fa-paperclip pull-left"></i>&nbsp;Files</h6></a></li>
                                            <li> <a href="files_page.php"><h6><i class="fa fa-puzzle-piece pull-left"></i>&nbsp;Apps</h6></a></li>
                                            <li> <a href="<?php echo site_url("user/setting"); ?>"><h6><i class="fa fa-gears pull-left"></i>&nbsp;Account Setting</h6></a></li>
                                            <li> <a href="files_page.php"><h6><i class="fa fa-sign-out pull-left"></i>&nbsp;Log Out</h6></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of second header -->

        <div class="group_popup">
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                            <h4 class="modal-title" id="myModalLabel">Create a New Group</h4>
                        </div>
                        <div class="modal-body">
                            <div class="course_message">
                                <div class="col-md-12"><label> Group Name </label></div>
                                <div class="col-md-12"><input type="text" class="pop_text" name="group_name"></div>
                            </div>
                            <div class="course_message">
                                <div class="col-md-12"><label> Group Members </label></div>
                                <div class="col-md-12"><input type="text" class="pop_text" placeholder="+ Add people to this group by name or email" name="group_members"></div>
                            </div>
                            <div class="course_message">
                                <div class="col-md-12"><label style="float:left;padding-top: 4px;"> Visible to </label>
                                    <div class="visible_select">
                                        <select>
                                            <option value="Re-offering earlier Course">Only to me</option>
                                            <option value="Members">Members</option>
                                            <option value="Public">Public</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="course_message">
                                <div class="col-md-12"><label> Who can view this content </label></div>
                                <div class="col-md-12"> 
                                    <div class="radio">
                                        <label><input type="radio" name="optradio"><span>Public</span> - Anyone in this network</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="radio">
                                        <label><input type="radio" name="optradio"><span>Private</span> - Only approved members</label>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <!--    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                            <div class="course_details" style="margin: 30px 0px 15px;">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-primary">Create Group</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="site_url" value="<?php echo site_url(); ?>"/>
        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>"/>