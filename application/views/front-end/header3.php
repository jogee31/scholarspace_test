<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Scholar Space | Home page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/style.css"/>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/bootstrap.css">
        <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/tinycarousel.css" type="text/css" media="screen"/>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <div class="second_header">
            <div class="second_top" style="background-image:none;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="institute_logo">
                                <a href="#"> <img src="<?php echo base_url(); ?>assets_front/image/iit_logo.png" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="by_logo">
                                <h6>Powered by</h6>  
                                <div class="logo">
                                    <a href="<?php echo site_url("/"); ?>"><img src="<?php echo base_url(); ?>assets_front/image/logo.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of top -->
        </div>
        <!-- end of second header -->



