<?php
$this->load->view('supper-admin/header');
$this->load->view('supper-admin/sidebar');
?>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<style>
    .custom_aj_style{
        float: right;
        margin-top: 3%;
        margin-right: 5%;
    }
    .pagination{
        float:right;
    }
</style>
<style type="text/css">
    .danger_alert{
        display: none;
    }
    .success_alert{
        display: none;
    }
</style>
<div class="content-wrapper" style="min-height: 948px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-fw fa-building-o"></i>
            Institution Type
            <small><a style="font-weight: 500; cursor: pointer;color: #11559E" class="" data-toggle="modal" data-target="#add_institution_modal"><i class="fa fa-plus-circle"></i> Add New Institution</a></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#"> Manage Institution</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="success_alert alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <h4>  <i class="icon fa fa-check"></i> Alert!</h4>
            <span id="success_alert_message"></span>
        </div>
        <div class="danger_alert alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <span id="danger_alert_message"></span>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="box box-success">
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline" role="grid">
                            <table id="viewInstitutionTable" class="table table-bordered table-hover dataTable" aria-describedby="example2_info">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">S.no</th>
                                        <th>Courses</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody role="alert" aria-live="polite" aria-relevant="all">
                                    <?php
                                    $count = 1;
                                    foreach ($institution_type_list as $value) {
                                        ?>
                                        <tr>
                                            <td><?php echo $count; ?></td>
                                            <td><?php echo $value->title; ?> </td>
                                            <td>
                                                <button type="button" institution_id="<?php echo $value->id; ?>" institution_title="<?php echo $value->title; ?>" class="btn btn-xs btn-success edit_institution_btn"><i class="fa fa-edit"></i> Edit</button> |
                                                <button type="button" institution_id="<?php echo $value->id; ?>" active_status="<?php echo $value->active_status; ?>" class="btn btn-xs <?php echo ($value->active_status == '1') ? "btn-danger" : "btn-success"; ?> active_status_btn"><i class="fa <?php echo ($value->active_status == '1') ? "fa-ban" : "fa-check"; ?>"></i> <?php echo ($value->active_status == '1') ? "Deactivate" : "Activate"; ?></button> |
                                                <button type="button" institution_id="<?php echo $value->id; ?>" class="btn btn-xs btn-default delete_institution_btn"><i class="fa fa-trash"></i> Delete</button>
                                            </td>
                                        </tr>
                                        <?php
                                        $count++;
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th style="width: 5%;">S.no</th>
                                        <th>Courses</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Trigger the modal with a button -->
        <input type="hidden" value="<?php echo site_url(); ?>" id="site_url">
        <input type="hidden" value="<?php echo base_url(); ?>" id="base_url">
    </section>
</div>
<?php
$this->load->view('supper-admin/footer');
?>
<!-- Page Modal -->
<div class="modal fade" id="add_institution_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form id="addIntitutionForm" class="bootstrap_modal" role="form" method="post" action="<?php echo site_url('super-admin/institution/submit_add_institute'); ?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Institution</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="usr">Institution * <span class="required_error_span"></span></label>
                        <div><input type="text" placeholder="Enter Institution" class="form-control text_required" name="institution"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Add Institution</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_institution_modal" role="dialog">
    <div class="modal-dialog">
        <form role="form" method="post" action="<?php echo site_url('super-admin/institution/submit_edit_institution'); ?>" class="bootstrap_modal" id="update_university_form_data">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Institution</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="usr">Institution * <span class="required_error_span"></span></label>
                        <div>
                            <input type="text" class="form-control update_place text_required" name="institution_title" id="edit_modal_institution_title">
                            <input type="hidden" class="form-control update_id" name="institution_id" id="edit_modal_institution_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Update Institution</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/formValidation.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/super_admin_js/viewInstitution.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $('#viewInstitutionTable').dataTable({
            "bSort": false,
            "bFilter": false,
            //"bPaginate": false
        });
    });
</script>


<?php
//edit_time_status
$session_flash = '';
$session_flash = $this->session->flashdata('message_success');

if ($session_flash != '') {
    ?>
    <script>
        $("#success_alert_message").text("<?php echo $session_flash; ?>");
        $('.success_alert').slideDown(400);
        $('.success_alert').delay(2000).slideUp(400);
    </script>
    <?php
}

$session_flash = '';
$session_flash = $this->session->flashdata('message_danger');
if ($session_flash != '') {
    ?>
    <script>
        $("#danger_alert_message").text("<?php echo $session_flash; ?>");
        $('.danger_alert').slideDown(400);
        $('.danger_alert').delay(2000).slideUp(400);
    </script>
    <?php
}
?>