<?php
$this->load->view('supper-admin/header');
$this->load->view('supper-admin/sidebar');
?>
<style type="text/css">
    .danger_alert{
        display: none;
    }
    .success_alert{
        display: none;
    }
</style>
<div class="content-wrapper" style="min-height: 948px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-fw fa-user"></i>
            Add User
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#">Add User</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="success_alert alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <h4>  <i class="icon fa fa-check"></i> Alert!</h4>
            <span id="success_alert_message"></span>
        </div>
        <div class="danger_alert alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <span id="danger_alert_message"></span>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="box box-success">
                    <form method="post" class="user_form" action="<?php echo site_url("super-admin/user/add_user_submit"); ?>" id="add_data_entry_user" role="form">
                        <div class="box-body">
                            <!-- text input -->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Choose User Type <span class="required_error_span"></span></label>
                                        <div>
                                            <select id="choose_user_type_select" name="user_type" class="form-control select_required">
                                                <option value="">--Choose option--</option>
                                                <?php
                                                foreach ($user_type_list as $value) {
                                                    ?>
                                                    <option value="<?php echo $value->title; ?>"><?php echo $value->description; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>First Name <span class="required_error_span"></span></label>
                                        <div class="input-group">
                                            <input type="text" name="first_name" class="form-control text_required" placeholder="Enter Last Name">
                                            <div class="input-group-addon">
                                                <i class="fa fa-bookmark"></i>
                                            </div>
                                        </div><!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Last Name <span class="required_error_span"></span></label>
                                        <div class="input-group">
                                            <input type="text" name="last_name" class="form-control text_required" placeholder="Enter Last Name">
                                            <div class="input-group-addon">
                                                <i class="fa fa-bookmark"></i>
                                            </div>
                                        </div><!-- /.input group -->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Phone <span class="required_error_span"></span></label>
                                        <div class="input-group">
                                            <input type="text" name="phone" class="form-control text_required" placeholder="Enter Phone">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                        </div><!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email <span class="required_error_span"></span></label>
                                        <div class="input-group">
                                            <input type="text" id="email" name="email" class="form-control text_required" placeholder="Enter Email">
                                            <div class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </div>
                                        </div><!-- /.input group -->
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <button style="margin-left: 1%;" id="submit_id" type="button" class="btn btn-primary pull-right">Save Details</button>
                                    <button type="button" class="reset btn btn-default pull-right"><i class="fa fa-refresh"></i> &nbsp;Reset</button>&nbsp;&nbsp;
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<div id="hidden_field_div">
    <input type="hidden" id="site_url" value="<?php echo site_url(); ?>"/>
    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>"/>
    <!-- For email existence if it's user details form. -->
    <input type="hidden" id="form_status" value="0"/>
</div>
<?php
$this->load->view('supper-admin/footer');
?>
<script src='<?php echo base_url(); ?>assets/custom_assets/formValidation.js'></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/super_admin_js/addUser.js" type="text/javascript"></script>
<?php
//edit_time_status
$session_flash = '';
$session_flash = $this->session->flashdata('message_success');

if ($session_flash != '') {
    ?>
    <script>
        $("#success_alert_message").text("User details added successfully.");
        $('.success_alert').slideDown(400);
        $('.success_alert').delay(2000).slideUp(400);
    </script>
    <?php
}

$session_flash = '';
$session_flash = $this->session->flashdata('message_danger');
if ($session_flash != '') {
    ?>
    <script>
        $("#danger_alert_message").text("Failed to add user details.");
        $('.danger_alert').slideDown(400);
        $('.danger_alert').delay(2000).slideUp(400);
    </script>
    <?php
}
?>