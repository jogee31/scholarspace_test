<!-- Modal -->
<div class="modal fade bootstrap_modal" id="edit_college_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form id="edit_college_details_form" class="bootstrap_modal" role="form" method="post" action="<?php echo site_url('super-admin/college/save_college_details'); ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit College Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">College Name * <span class="required_error_span"></span></label>
                                <div>
                                    <input type="text" id="eClgDtlMdl_college_name" name="college_name" class="form-control text_required" placeholder="Enter College Name">
                                </div>
                                <input type="hidden" id="eClgDtlMdl_college_id" name="college_id" value=""/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">City<span class="required_error_span"></span></label>
                                <div><input type="text" name="college_city" class="form-control" id="eClgDtlMdl_college_city" placeholder="Enter College City"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Choose User</label>
                                <div>
                                    <select id="eClgDtlMdl_college_state" name="college_state" class="form-control select_required">
                                        <option value="">--Choose option--</option>
                                        <?php
                                        foreach ($state_list as $value) {
                                            ?>
                                            <option value="<?php echo $value->title; ?>"><?php echo $value->title; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Zipcode<span class="required_error_span"></span></label>
                                <div><input type="text" name="college_zipcode" class="form-control" id="eClgDtlMdl_college_zipcode" placeholder="Enter College Zipcode"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>