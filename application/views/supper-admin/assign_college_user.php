<?php
$this->load->view('supper-admin/header');
$this->load->view('supper-admin/sidebar');
?>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/custom_assets/pages_css/super_admin_css/select2.css" rel="stylesheet" type="text/css">

<style type="text/css">
    .danger_alert{
        display: none;
    }
    .success_alert{
        display: none;
    }
    .select2-selection{
        width: 100% !important;
        height: 33px !important;
        border-radius: 0px !important;
        border-color: #d2d6de !important;
    }
    .select2{
        width: 100% !important;
    }
</style>
<div class="content-wrapper" style="min-height: 948px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-fw fa-share-square-o"></i>
            Assign College User
            <small><a href="<?php echo site_url('super-admin/user/add'); ?>" style="font-weight: 500; cursor: pointer;color: #11559E" ><i class="fa fa-plus-circle"></i> Add New User</a></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li><li><a href="#"> Assign College User</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="success_alert alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <h4>  <i class="icon fa fa-check"></i> Alert!</h4>
            <span id="success_alert_message"></span>
        </div>
        <div class="danger_alert alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <span id="danger_alert_message"></span>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header ui-sortable-handle" style="cursor: move;">
                        <i class="fa fa-circle-o"></i>
                        <h3 class="box-title">Add a User to a College</h3>
                    </div>
                    <div class="box-body">
                        <form id="assign_college_user_form" method="post" action="<?php echo site_url('super-admin/college/assign_college_user_submit'); ?>">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Choose College <span class="required_error_span"></span></label>
                                        <div>
                                            <select id="choose_college" name="college" class="form-control select_required">
                                                <option value="">--Choose option--</option>
                                                <?php
                                                foreach ($college_list as $value) {
                                                    ?>
                                                    <option value="<?php echo $value->id; ?>"><?php echo $value->college_name; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Choose User <span class="required_error_span"></span></label>
                                        <div>
                                            <select id="choose_user" name="user" class="form-control select_required">
                                                <option value="">--Choose option--</option>
                                                <?php
                                                foreach ($user_list as $value) {
                                                    ?>
                                                    <option value="<?php echo $value->id; ?>"><?php echo $value->first_name . " " . $value->last_name . " ( " . $value->email . " )"; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label style="display: block">&nbsp;</label>
                                        <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-fw fa-share-square-o"></i> Assign User</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline" role="grid">
                            <table id="college_list_table" class="table table-bordered table-hover dataTable" aria-describedby="example2_info">
                                <thead>
                                    <tr>
                                        <th style="width: 30%">College Name</th>
                                        <th>User Name</th>
                                        <th>User Email</th>
                                        <th style="width: 18%">Action</th>
                                    </tr>
                                </thead>
                                <tbody role="alert" aria-live="polite" aria-relevant="all">
                                    <?php
                                    foreach ($assigned_college_user_list as $value) {
                                        ?>
                                        <tr>
                                            <td><?php echo $value->college_name; ?></td>
                                            <td><?php echo $value->first_name . " " . $value->last_name; ?></td>
                                            <td><?php echo $value->email; ?></td>
                                            <td>
                                                <button class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>College Name</th>
                                        <th>User Name</th>
                                        <th>User Email</th>
                                        <th style="width: 18%">Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<input type="hidden" value="<?php echo site_url(); ?>" id="site_url">
<input type="hidden" value="<?php echo base_url(); ?>" id="base_url">
<?php
$this->load->view('supper-admin/footer');
?>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/super_admin_js/select2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/super_admin_js/assign_college_user.js" type="text/javascript"></script>
<script src='<?php echo base_url(); ?>assets/custom_assets/formValidation.js'></script>
<?php
$session_flash = '';
$session_flash = $this->session->flashdata('message_success');

if ($session_flash != '') {
    ?>
    <script>
        $("#success_alert_message").text("<?php echo $session_flash; ?>");
        $('.success_alert').slideDown(400);
        $('.success_alert').delay(2000).slideUp(400);
    </script>
    <?php
}

$session_flash = '';
$session_flash = $this->session->flashdata('message_danger');
if ($session_flash != '') {
    ?>
    <script>
        $("#danger_alert_message").text("<?php echo $session_flash; ?>");
        $('.danger_alert').slideDown(400);
        $('.danger_alert').delay(2000).slideUp(400);
    </script>
    <?php
}
?>