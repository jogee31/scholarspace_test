<!-- =============================================== -->
<?php
//current controller and method name
$current_location = "super-admin/" . $this->router->fetch_class() . "/" . $this->router->fetch_method();

$this->load->helper('super-admin');
$college_registration_request = college_registration_request();
?>
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image"></div>
            <div class="pull-left info">
                <p><i class="fa fa-fw fa-user"></i>Admin</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="<?php echo ($current_location == 'super-admin/home/index') ? "active" : ""; ?>"><a href="<?php echo site_url('super-admin/home'); ?>"><i class="fa fa-home"></i> Dashboard</a></li>
<!--            <li class="<?php echo ($current_location == 'super-admin/college_request/index') ? "active" : ""; ?>">
                <a href="<?php echo site_url('super-admin/college_request/'); ?>"><i class="fa fa-share-square-o"></i><span>College Request</span>
            <?php
            if ($college_registration_request > 0) {
                ?>
                                                                                                                <span class="label label-success pull-right">New <?php echo $college_registration_request; ?></span>
                <?php
            }
            ?>
                </a>
            </li>-->
            <li class="treeview <?php echo ($current_location == 'super-admin/college/index' || $current_location == 'super-admin/college/college_list' || $current_location == 'super-admin/college/assign_user') ? "active" : ""; ?>">
                <a href="#">
                    <i class="fa fa-building"></i> <span>College</span> <i class="fa fa-angle-left pull-right"></i>
                    <?php
                    if ($college_registration_request > 0) {
                        ?>
                        <span class="label label-success pull-right"><?php echo $college_registration_request; ?> New request</span>
                        <?php
                    }
                    ?>
                </a>
                <ul class="treeview-menu <?php echo ($current_location == 'super-admin/college/index' || $current_location == 'super-admin/college/college_list' || $current_location == 'super-admin/college/assign_user') ? "active" : ""; ?>" style="<?php echo ($current_location == 'super-admin/college/index' || $current_location == 'super-admin/college/assign_user' || $current_location == 'super-admin/college/college_list') ? "display:block" : "display:none"; ?>">
                    <li class="<?php echo ($current_location == 'super-admin/college/college_list') ? "active" : ""; ?>"><a href="<?php echo site_url('super-admin/college/college_list'); ?>"><i class="fa fa-circle-o"></i> College List</a></li>
                    <li class="<?php echo ($current_location == 'super-admin/college/index') ? "active" : ""; ?>"><a href="<?php echo site_url('super-admin/college/'); ?>"><i class="fa fa-circle-o"></i> College Request</a></li>
                    <li class="<?php echo ($current_location == 'super-admin/college/assign_user') ? "active" : ""; ?>"><a href="<?php echo site_url('super-admin/college/assign_user'); ?>"><i class="fa fa-circle-o"></i> Assign College User</a></li>
                </ul>
            </li>
            <li class="treeview <?php echo ($current_location == 'super-admin/user/add' || $current_location == 'super-admin/user/view' || $current_location == 'super-admin/user/edit') ? "active" : ""; ?>">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu <?php echo ($current_location == 'super-admin/user/add' || $current_location == 'super-admin/user/edit' || $current_location == 'super-admin/user/view') ? "active" : ""; ?>" style="<?php echo ($current_location == 'super-admin/user/add' || $current_location == 'super-admin/user/edit' || $current_location == 'super-admin/user/view') ? "display:block" : "display:none"; ?>">
                    <li class="<?php echo ($current_location == 'super-admin/user/add') ? "active" : ""; ?>"><a href="<?php echo site_url('super-admin/user/add'); ?>"><i class="fa fa-circle-o"></i> Add User</a></li>
                    <li class="<?php echo ($current_location == 'super-admin/user/view' || $current_location == 'super-admin/user/edit') ? "active" : ""; ?>"><a href="<?php echo site_url('super-admin/user/view'); ?>"><i class="fa fa-circle-o"></i> Manage Users</a></li>

                </ul>
            </li>
            <li class="<?php echo ($current_location == 'super-admin/stream/index') ? "active" : ""; ?>"><a href="<?php echo site_url('super-admin/stream'); ?>"><i class="fa fa-book"></i> Manage Stream</a></li>
            <li class="<?php echo ($current_location == 'super-admin/institution/index') ? "active" : ""; ?>"><a href="<?php echo site_url('super-admin/institution'); ?>"><i class="fa fa-building-o"></i> Manage Institution</a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<!-- =============================================== -->