<?php
$this->load->view('supper-admin/header');
$this->load->view('supper-admin/sidebar');
?>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<style>
    .custom_aj_style{
        float: right;
        margin-top: 3%;
        margin-right: 5%;
    }
</style>
<style type="text/css">
    .danger_alert{
        display: none;
    }
    .success_alert{
        display: none;
    }
    .modal_success_alert{
        display: none;
    }
    .modal_danger_alert{
        display: none;
    }
    .custom_a_btn{
        font-weight: 500; cursor: pointer;padding: 0px 2%;color: #11559E
    }
    .custom_a_btn:hover { 
        color: #2997D8;
    }
</style>
<div class="content-wrapper" style="min-height: 948px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 style="display: inline;">
            <i class="fa fa-fw fa-book"></i>
            Streams <small><a style="font-weight: 500; cursor: pointer;color: #11559E" class="" data-toggle="modal" data-target="#add_stream_modal"><i class="fa fa-plus-circle"></i> Add New Stream</a></small>
            <!--<button data-toggle="modal" data-target="#myModal"  style="margin-right: 5%;" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add New Course</button>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#">Manage Stream</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="success_alert alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <h4>  <i class="icon fa fa-check"></i> Alert!</h4>
            <span id="success_alert_message"></span>
        </div>
        <div class="danger_alert alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <span id="danger_alert_message"></span>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="box box-success">
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline" role="grid">
                            <table id="view_course_table" class="table table-bordered table-hover dataTable" aria-describedby="example2_info">
                                <thead>
                                    <tr>
                                        <th style="width: 40%">Stream</th>
                                        <th style="width: 60%">Courses</th>
                                    </tr>
                                </thead>
                                <tbody role="alert" aria-live="polite" aria-relevant="all">
                                    <?php
                                    $stream_ids = array();
                                    foreach ($stream_list as $value) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $value['stream_name'] . " <span style='color:#11559E'>(" . $value['study_type_count'] . " " . $value['study_type'] . (($value['study_type_count'] > 1) ? "s" : "") . ")</span>"; ?>
                                                <a class="pull-right delete_stream_btn custom_a_btn" stream_id="<?php echo $value['stream_id']; ?>"><i class="fa fa-trash"></i>&nbsp; Delete</a>
                                                <a class="pull-right edit_stream_btn custom_a_btn" stream_id="<?php echo $value['stream_id']; ?>" stream_title="<?php echo $value['stream_name']; ?>" study_type="<?php echo $value['study_type']; ?>" study_type_count="<?php echo $value['study_type_count']; ?>"><i class="fa fa-edit"></i>&nbsp; Edit</a>
                                                <a class="pull-right add_stream_course_btn custom_a_btn" stream_id="<?php echo $value['stream_id']; ?>" stream_name="<?php echo $value['stream_name']; ?>" data-toggle="modal" data-target="#add_stream_courese_modal"><i class="fa fa-plus-square"></i>&nbsp; Add Course</a>
                                            </td>
                                            <td>
                                                <?php
                                                if (count($stream_courses[$value['stream_id']])) {
                                                    $count_course = 1;
                                                    foreach ($stream_courses[$value['stream_id']] as $inner_value) {
                                                        ?>
                                                        <a style="padding: 0px 12px;height: 22px;" class="btn btn-app">
                                                            <span title="Delete this course." course_id="<?php echo $inner_value['course_id']; ?>" class="badge bg-red delete_stream_course">X</span>
                                                            <?php echo $inner_value['course_name']; ?>
                                                        </a>
                                                        <?php
                                                        if ($count_course == 5) {
                                                            break;
                                                        }
                                                        $count_course++;
                                                    }
                                                    ?>
                                                    <br>
                                                    <a class="view_all_stream_courses pull-right" stream_name="<?php echo $value['stream_name']; ?>" stream_id="<?php echo $value['stream_id']; ?>" style="cursor: pointer; margin-left: 4%;font-weight: 900;text-decoration: underline;color: #11559E"><span><i class="fa fa-eye"></i> View all</span></a>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <span style="padding-left: 12px;color: #dd4b39;">Courses not found..!!</span>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Stream</th>
                                        <th>Courses</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<input type="hidden" value="<?php echo site_url(); ?>" id="site_url">
<input type="hidden" value="<?php echo base_url(); ?>" id="base_url">
<?php
$this->load->view('supper-admin/viewStreamModal');
$this->load->view('supper-admin/footer');
?>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src='<?php echo base_url(); ?>assets/custom_assets/formValidation.js'></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/super_admin_js/viewStream.js" type="text/javascript"></script>
<?php
//edit_time_status
$session_flash = '';
$session_flash = $this->session->flashdata('message_success');

if ($session_flash != '') {
    ?>
    <script>
        $("#success_alert_message").text("<?php echo $session_flash; ?>");
        $('.success_alert').slideDown(400);
        $('.success_alert').delay(2000).slideUp(400);
    </script>
    <?php
}

$session_flash = '';
$session_flash = $this->session->flashdata('message_danger');
if ($session_flash != '') {
    ?>
    <script>
        $("#danger_alert_message").text("<?php echo $session_flash; ?>");
        $('.danger_alert').slideDown(400);
        $('.danger_alert').delay(2000).slideUp(400);
    </script>
    <?php
}
?>