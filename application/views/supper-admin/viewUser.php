<?php
$this->load->view('supper-admin/header');
$this->load->view('supper-admin/sidebar');
?>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<style type="text/css">
    .danger_alert{
        display: none;
    }
    .success_alert{
        display: none;
    }
</style>
<div class="content-wrapper" style="min-height: 948px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-fw fa-user"></i>
            View User
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#">View User</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="success_alert alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <h4>  <i class="icon fa fa-check"></i> Alert!</h4>
            <span id="success_alert_message"></span>
        </div>
        <div class="danger_alert alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <span id="danger_alert_message"></span>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="box box-success">
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline" role="grid">
                            <div class="row">
                                <div class="col-xs-6"></div><div class="col-xs-6"></div>
                            </div>
                            <table id="view_user_table" class="table table-bordered table-hover dataTable" aria-describedby="example2_info">
                                <thead>
                                    <tr>
                                        <th style="width: 15%">User Type</th>
                                        <th style="width: 25%">Name</th>
                                        <th style="width: 15%">Email</th>
                                        <th style="width: 15%">Phone</th>
                                        <th style="width: 8%">Password</th>
                                        <th style="width: 22%">Action</th>
                                    </tr>
                                </thead>
                                <tbody role="alert" aria-live="polite" aria-relevant="all">
                                    <?php
                                    foreach ($user_list as $user_value) {
                                        ?>
                                        <tr class="odd">
                                            <td class=" sorting_1"><?php echo ucfirst($user_value->user_type); ?></td>
                                            <td class=" "><?php echo $user_value->first_name . " " . $user_value->last_name; ?></td>
                                            <td class=" "><?php echo $user_value->email; ?></td>
                                            <td class=" "><?php echo $user_value->phone; ?></td>
                                            <td class=" "><?php echo base64_decode($user_value->encoded_pwd); ?></td>
                                            <td>
                                                <?php $btn_class = ($user_value->active) ? "btn-success" : "btn-danger"; ?>
                                                <a href="<?php echo site_url("super-admin/user/edit") . "?ui=" . base64_encode($user_value->id) . "&ut=" . base64_encode($user_value->user_type); ?>"><button class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Edit</button></a> |
                                                <button user_id="<?php echo $user_value->id; ?>" active_status="<?php echo $user_value->active; ?>" class="btn btn-xs <?php echo ($user_value->active == 1) ? "btn-danger" : "btn-success"; ?> active_status_btn"><?php echo ($user_value->active) ? "<i class=\"fa fa-ban\"></i> Deactivate" : "<i class=\"fa fa-check\"></i> Activate"; ?></button> | 
                                                <button user_id ="<?php echo $user_value->id; ?>" user_type="<?php echo $user_value->user_type; ?>" class="btn btn-default btn-xs delete_user_btn"><i class="fa fa-trash"></i> Delete</button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>User Type</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Password</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>"/>
        <input type="hidden" id="site_url" value="<?php echo site_url(); ?>"/>
    </section>
</div>
<?php
$this->load->view('supper-admin/footer');
?>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/custom_assets/pages_js/super_admin_js/viewUser.js" type="text/javascript"></script>
<?php
$session_flash = '';
$session_flash = $this->session->flashdata('message_success');

if ($session_flash != '') {
    ?>
    <script>
        $("#success_alert_message").text("<?php echo $session_flash; ?>");
        $('.success_alert').slideDown(400);
        $('.success_alert').delay(2000).slideUp(400);
    </script>
    <?php
}

$session_flash = '';
$session_flash = $this->session->flashdata('message_danger');
if ($session_flash != '') {
    ?>
    <script>
        $("#danger_alert_message").text("<?php echo $session_flash; ?>");
        $('.danger_alert').slideDown(400);
        $('.danger_alert').delay(2000).slideUp(400);
    </script>
    <?php
}
?>