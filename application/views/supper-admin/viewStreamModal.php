<!-- Modal -->
<div class="modal fade" id="add_stream_courese_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form id="add_stream_course_form" class="bootstrap_modal" role="form" method="post" action="<?php echo site_url('super-admin/stream/submit_add_stream_subject'); ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Stream Courses</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Stream Name &nbsp;&nbsp;: &nbsp; &nbsp; <span id="stream_span_add_stream_course_modal"></span></label>
                                <input type="hidden" name="stream_id" id="stream_id_add_stream_course_modal"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Course Name * <span class="required_error_span"></span></label>
                                <div><input type="text" name="course_name[]" class="form-control text_required" id="exampleInputEmail1" placeholder="Enter Course Name"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label style="display: block;" for="exampleInputEmail1">&nbsp;</label>
                                <button type="button" class="btn btn-primary pull-right add_more_course_name_btn"><i class="fa fa-plus"></i> Add More</button>
                            </div>
                        </div>  
                    </div>
                    <div id="add_more_courses_content_div"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit_of_course" class="btn btn-success">Add Courses</button>
                </div>
            </div>
        </form>
    </div>
</div>


<div class="modal fade" id="add_stream_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form id="add_stream_form" class="bootstrap_modal" role="form" method="post" action="<?php echo site_url('super-admin/stream/submit_add_stream'); ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Stream</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Stream * <span class="required_error_span"></span></label>
                                <div><input type="text" name="stream" class="form-control text_required" id="exampleInputEmail1" placeholder="Enter Stream"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Study System :<span class="required_error_span"></span></label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" class="study_system_checkbox" name="study_type" value="semester" checked="">
                                        Semester
                                    </label>
                                    <label style="margin-left: 10%;">
                                        <input type="radio" class="study_system_checkbox" name="study_type" value="year">
                                        Year
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group year_or_semester_div">
                                <label for="exampleInputEmail1">Number of Semester * <span class="required_error_span"></span></label>
                                <div><input type="text" name="semester" class="form-control text_required" id="exampleInputEmail1" placeholder="Enter Stream"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit_of_course" class="btn btn-success">Add Stream</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Show all courses of the stream on a modal -->
<div class="modal fade" id="show_stream_courses_modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">All Stream Subject</h4>
            </div>
            <div class="modal-body">
                <div class="modal_success_alert alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4>  <i class="icon fa fa-check"></i> Alert!</h4>
                    <span id="modal_success_alert_message"></span>
                </div>
                <div class="modal_danger_alert alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <span id="modal_danger_alert_message"></span>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Stream Name &nbsp;&nbsp;: &nbsp; &nbsp; <span id="stream_span_show_stream_courses_modal"></span></label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table stream_courses_list_table table-bordered table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Course</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="stream_courses_content_tbody"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_stream_modal" role="dialog">
    <div class="modal-dialog">
        <form role="form" method="post" action="<?php echo site_url('super-admin/stream/submit_edit_stream'); ?>" class="bootstrap_modal" id="update_university_form_data">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Stream</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="usr">Stream * <span class="required_error_span"></span></label>
                        <div>
                            <input type="text" class="form-control text_required" name="stream_title" id="edit_modal_stream_title">
                            <input type="hidden" class="form-control" name="stream_id" id="edit_modal_stream_id">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Study System :<span class="required_error_span"></span></label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" class="study_system_checkbox" id="editStModal_study_type_semester" name="study_type" value="semester">
                                        Semester
                                    </label>
                                    <label style="margin-left: 10%;">
                                        <input type="radio" class="study_system_checkbox" id="editStModal_study_type_year" name="study_type" value="year">
                                        Year
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group editModal_year_or_semester_div"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Update Stream</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- stream courses edit modal -->
<div class="modal fade" id="edit_stream_course_modal" role="dialog">
    <div class="modal-dialog">
        <form role="form" method="post" action="<?php echo site_url('super-admin/stream/submit_edit_stream_courses'); ?>" class="bootstrap_modal" id="edit_stream_course_form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Stream Course</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="usr">Course * <span class="required_error_span"></span></label>
                        <div>
                            <input type="text" class="form-control text_required" name="stream_course_title" id="edit_modal_stream_course_title">
                            <input type="hidden" class="form-control" name="stream_course_id" id="edit_modal_stream_course_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Update Course</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


