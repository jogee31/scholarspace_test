<?php
$this->load->view('supper-admin/header');
$this->load->view('supper-admin/sidebar');
?>
<div class="content-wrapper" style="min-height: 948px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-fw fa-home"></i>
            Dashboard
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        </ol>
    </section>
</div>
<?php
$this->load->view('supper-admin/footer');
?>