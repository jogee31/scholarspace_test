<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ($this->ion_auth->logged_in()) {
            redirect("/");
        }
        $this->load->library(array('form_validation', 'session')); // load form lidation libaray & session library
        $this->load->helper(array('url', 'html', 'form'));  // load url,html,form helpers
    }

    function index() {
        $tables = $this->config->item('tables', 'ion_auth');
        $this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[1]|max_length[100]');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[1]|max_length[100]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        $this->form_validation->set_rules('mobile', 'Mobile', 'required|min_length[10]|max_length[20]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('front-end/register');
        } else {
            $username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
            $email = strtolower($this->input->post('email'));
            $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'user_type' => "front-end-user",
                'user_type_id' => 3,
                'phone' => $this->input->post('phone'),
                'encoded_pwd' => base64_encode($password)
            );
            if ($this->ion_auth->register($username, $password, $email, $additional_data)) {
                $remember = 0;
                if ($this->ion_auth->login($email, $password, $remember)) {
                    redirect("/");
                }
            } else {
                $this->session->set_flashdata('item_error', 'Request can not completed, Please try again later.');
                redirect(current_url());
            }
        }
    }

    function check_unique_email() {
        if (!empty($_POST)) {
            $email = $this->input->post('email');
            //custom function 1st argu : table name, 2nd argument : column name, 3rd argument : column value
            if ($this->form_validation->is_unique_email('users', 'email', $email)) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function captcha_validation() {
        $this->load->library('recaptcha_library');
        $publickey = "6LfFBQgTAAAAALCGsNxskGwNGHyaDCGvNBtB1vZz";
        $privatekey = "6LfFBQgTAAAAAJY0R7ju830AwccRPfAIcm_BEvDk";

# the response from reCAPTCHA
        $resp = null;
# the error code from reCAPTCHA, if any
        $error = null;

# was there a reCAPTCHA response?
        if ($_POST["recaptcha_response_field"]) {
            $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
            echo $resp->is_valid;
        }
    }

}

?>