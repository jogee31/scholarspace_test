<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Front_home extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('My_PHPMailer');
        $this->load->library(array('form_validation', 'session')); // load form lidation libaray & session library
        $this->load->helper(array('url', 'html', 'form'));  // load url,html,form helpers
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
    }

    function index() {
        /* Fetch all states */
        $sql_query = "SELECT * FROM `state`";
        $query_result = $this->data_fetch->data_query($sql_query);
        $data['state_list'] = $query_result;

        $this->load->view('front-end/index', $data);
    }

    public function registration_college() {
        /* Fetch all states */
        $sql_query = "SELECT * FROM `state`";
        $query_result = $this->data_fetch->data_query($sql_query);
        $data['state_list'] = $query_result;

        $tables = $this->config->item('tables', 'ion_auth');

//if user is already logged in so no need to take user details while requesting for the college registration
        if (!$this->ion_auth->logged_in()) {
            $this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[1]|max_length[100]');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
            $this->form_validation->set_rules('phone', 'Phone', 'required|min_length[10]|max_length[20]|is_unique[' . $tables['users'] . '.phone]');
        }

        $this->form_validation->set_rules('college_name', 'College Name', 'required|min_length[1]|max_length[100]');
        $this->form_validation->set_rules('college_city', 'College City', 'required|min_length[1]|max_length[100]');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('front-end/index', $data);
        } else if ($this->form_validation->run() == true) {
//if the user is loged in then we are not adding new user in users table
            if (!$this->ion_auth->logged_in()) {
//user details
                $username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
                $email = strtolower($this->input->post('email'));
                $password = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', 5)), 0, 8);

                $additional_data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'user_type' => "front-end-user",
                    'user_type_id' => 3,
                    'phone' => $this->input->post('phone'),
                    'encoded_pwd' => base64_encode($password)
                );

                $user_id = $this->ion_auth->register($username, $password, $email, $additional_data);
                $body = <<<EOD
                            <div style='font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#000000'>
                            <div style='width:680px'>
                            <div style='height:70px;background-color: #308cbd;position:relative;width: 680px;  margin-bottom: 22px;'>
                            <img src='http://spydernet.in/demo/scholarspace_demo/image/logo.png' style='position:absolute;margin-top: 27px;margin-left: 25px;'>
                            </div>
                            <p style='margin-top:0px;margin-bottom:20px; color:#000000 !important;width:100%;'>Dear $username</p>
                            <p style='margin-top:0px;margin-bottom:15px; color:#000000 !important;width:100%;'>Congratulations! Your request to register new college accepted and you have been registered for Scholler Space</p>
                            <p style='margin-top:0px;margin-bottom:30px; color:#000000 !important;width:100%'>Please login to your account using below mentioned link and credentials.<a href='http://localhost/scholerspace/' target='_blank'>Scholler Space</a>
                            </p>
                            <table style='border-collapse:collapse;width:100%;border-top:1px solid #dddddd;margin-bottom:30px;border-left:1px solid #dddddd;'>
                            <thead>
                            <tr>
                            <td style='font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222' colspan='2'>Login Details</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                            <td style='font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;color:#000000 !important;text-align:left;padding:7px'><b>Username:</b> $email </td>
                            <td style='font-size:12px;color:#000000 !important;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px;'><b> Password:</b> $password </td>
                            </tr>
                            </tbody>
                            </table>
                            <p style='margin-top:0px;margin-bottom:5px; color:#000000 !important;width:100%;'>In case of any concerns, reply to this email.</p>
                            <p style='margin-top:0px;margin-bottom:5px; color:#000000 !important;width:100%;'>Regards,</p>
                            <p style='margin-top:0px;margin-bottom:30px; color:#000000 !important;width:100%;'>Team Scholler Space.</p>
                            <div style='width:680px;height:35px;background-color: #182027;margin-top: 50px;display:block!important;'></div>
                            </div>
                            </div>
EOD;
                $this->send_email($email, "Scholler Space user Regestration details.", $body);
            } else {
                $user_id = $this->ion_auth->get_user_id();
            }
            if ($user_id) {
                //college_details
                $college_name = $this->input->post('college_name');
                $college_city = $this->input->post('college_city');
                $college_state = $this->input->post('state');
                $college_zipcode = $this->input->post('zipcode');

                $sql_query = "INSERT INTO `contemporary_college_registration`(`college_name`,`college_city`,`college_state`,`college_zipcode`,`requested_user_id`) VALUES('$college_name','$college_city','$college_state','$college_zipcode','$user_id')";
                $query_result = $this->data_insert->data_query($sql_query);

                if ($query_result) {
                    $this->session->set_flashdata('msg_success', "<b>Congratulations</b>! Your request for college registration has been completed and also you have been registered in Scholarspace. Please check your email for login details.");
                }
            } else {
                $this->session->set_flashdata('msg_error', "Failed complete your reuest for college registration. Please try after some time.");
            }
            redirect("/");
        }
    }

    public function check_unique_email() {
        if (!empty($_POST)) {
            $email = $this->input->post('email');
            //custom function 1st argu : table name, 2nd argument : column name, 3rd argument : column value
            if ($this->form_validation->is_unique_email('users', 'email', $email)) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function check_unique_phone() {
        if (!empty($_POST)) {
            $phone = $this->input->post('phone');
            //custom function 1st argu : table name, 2nd argument : column name, 3rd argument : column value
            if ($this->form_validation->is_unique_email('users', 'phone', $phone)) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function send_email($email, $subject, $body) {
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->IsHTML(true);
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = "jogendra@hexwhale.com";
        $mail->Password = "getinside";
        $mail->setFrom('jogendra@hexwhale.com', 'Scholler Space');
        $mail->addReplyTo('jogendra@hexwhale.com', 'Scholler Space');
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->addAddress($email);
        $mail->send();
    }

    /*

      $body = <<<EOD
      <div style='font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#000000'>
      <div style='width:680px'>
      <div style='height:70px;background-color: #308cbd;position:relative;width: 680px;  margin-bottom: 22px;'>
      <img src='http://spydernet.in/demo/scholarspace_demo/image/logo.png' style='position:absolute;margin-top: 27px;margin-left: 25px;'>
      </div>
      <p style='margin-top:0px;margin-bottom:20px; color:#000000 !important;width:100%;'>Dear $username</p>
      <p style='margin-top:0px;margin-bottom:15px; color:#000000 !important;width:100%;'>Congratulations! Your request to register new college accepted and you have been registered for Scholler Space</p>
      <p style='margin-top:0px;margin-bottom:30px; color:#000000 !important;width:100%'>Please login to your account using below mentioned link and credentials.<a href='http://localhost/scholerspace/' target='_blank'>Scholler Space</a>
      </p>
      <table style='border-collapse:collapse;width:100%;border-top:1px solid #dddddd;margin-bottom:30px;border-left:1px solid #dddddd;'>
      <thead>
      <tr>
      <td style='font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222' colspan='2'>Login Details</td>
      </tr>
      </thead>
      <tbody>
      <tr>
      <td style='font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;color:#000000 !important;text-align:left;padding:7px'><b>Username:</b> $email </td>
      <td style='font-size:12px;color:#000000 !important;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px;'><b> Password:</b> $password </td>
      </tr>
      </tbody>
      </table>
      <p style='margin-top:0px;margin-bottom:5px; color:#000000 !important;width:100%;'>In case of any concerns, reply to this email.</p>
      <p style='margin-top:0px;margin-bottom:5px; color:#000000 !important;width:100%;'>Regards,</p>
      <p style='margin-top:0px;margin-bottom:30px; color:#000000 !important;width:100%;'>Team Scholler Space.</p>
      <div style='width:680px;height:35px;background-color: #182027;margin-top: 50px;display:block!important;'></div>
      </div>
      </div>
      EOD;
      $this->send_email($email, "Scholler Space user Regestration details.", $body);
     */
}
