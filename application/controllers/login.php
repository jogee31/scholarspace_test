<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
    }

    // log the user in
    function index() {
        if (!$this->ion_auth->logged_in()) {
            $this->load->view("login");
        } else {
            //current user details
            $user = $this->ion_auth->user()->row();
            if ($user->user_type == 'super-admin') {
                redirect('super-admin/home/', 'refresh');
            } else if ($user->user_type == 'data-entry') {
                redirect('data-entry/home/', 'refresh');
            }
        }
    }

    function user_login() {
        if (!empty($_POST) && $this->input->post('identity') != '' && $this->input->post('password') != '') {
            $remember = 0;
            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                echo 1;
            } else {
                echo $this->ion_auth->errors();
            }
        }
    }

    function user_logout() {
        if (!empty($_POST) && $this->input->post('flag') != '') {
            if ($this->ion_auth->logout()) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

}
