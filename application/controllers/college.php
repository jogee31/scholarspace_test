<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class College extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
    }

    public function index() {
        show_404();
    }

    //this function will be call from search function of this class
    public function profile() {
        $college_id = base64_decode($this->input->get('cid'));
        //check if the college is exist or not
        $sql_query = "SELECT * FROM `college` WHERE `id` = '$college_id'";
        $query_result = $this->data_fetch->data_query($sql_query);

        if (count($query_result)) {
            $data['college_id'] = $college_id;

            // if the user is already logged in, then  check whether user has already sent request or not, or is already the member of college
            if ($this->ion_auth->logged_in()) {
                $user_details = $this->ion_auth->user()->row();
                $user_id = $user_details->id;

                $sql_query = "SELECT `id` FROM `college_user_request_to_admin` WHERE `user_id` = '$user_id' AND `college_id` = '$college_id'";
                $query_result = $this->data_fetch->data_query($sql_query);
                if (count($query_result)) {
                    $data['requested_user'] = 1;
                }
            } else {
                
            }
            $this->load->view("front-end/college_profile", $data);
        } else {
            show_404();
        }
    }

    public function search() {
        if (!empty($_GET)) {
            $search_term = $this->input->get("search-term");
            $search_term = explode(",", $search_term);
            $search_term = array_map('trim', $search_term);

            $search_clg_name = (key_exists(0, $search_term)) ? $search_term[0] : "";
            $search_clg_city = (key_exists(1, $search_term)) ? $search_term[1] : "";
            $search_clg_state = (key_exists(2, $search_term)) ? $search_term[2] : "";

            $sql_query = "SELECT `id` FROM `college` WHERE `college_name` = '$search_clg_name'";

            if ($search_clg_city != '') {
                $sql_query .= "AND `city` = '$search_clg_city'";
            }
            if ($search_clg_city != '') {
                $sql_query .= "AND `state` = '$search_clg_state' LIMIT 1";
            }

            $query_result = $this->data_fetch->data_query($sql_query);

            //if the college is found set the college_id into session so we can use it in to all the intranet pages
            if (count($query_result)) {
                $session_data = array(
                    'college_id' => $query_result[0]->id
                );
                $this->session->set_userdata($session_data);
            }

            if (count($query_result) && $this->ion_auth->logged_in()) {
                $college_id = $query_result[0]->id;

                $sql_query = "SELECT `user_id` FROM `college_admin` WHERE `college_id` = '$college_id' LIMIT 1";
                $query_result = $this->data_fetch->data_query($sql_query);

                //if the user is college admin
                if (count($query_result) && $this->ion_auth->user()->row()->id == $query_result[0]->user_id) {
                    redirect("user/dashboard", "refresh");
                }
                //if the user is front end user but not the member of college
                else {
                    redirect("college/profile?cid=" . base64_encode($college_id), "refresh");
                }
            } else if (count($query_result) && !$this->ion_auth->logged_in()) {
                redirect("college/profile?cid=" . base64_encode($query_result[0]->id), "refresh");
            } else {
                echo "college not found.";
            }
        } else {
            
        }
    }

    public function show_searched_college_list() {
        $query_variable = $this->input->post("name");
        $query = "SELECT `id`, `college_name`, `city`, `state` FROM `college` WHERE college_name LIKE '$query_variable%' LIMIT 10";
        $data = $this->data_fetch->data_query($query);
        $college_details_array = array();
        foreach ($data as $value) {
            $college = $value->college_name;
            if ($value->city != '') {
                $college .= ", " . $value->city;
            }
            if ($value->state != '') {
                $college .= ", " . $value->state;
            }
            $college_details_array[$value->id] = $college;
        }
        echo json_encode($college_details_array);
    }

    /* User request code from college profile page */

    public function get_college_stream() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && isset($posted_data['college_id']) && !empty($posted_data['college_id'])) {
            $college_id = $posted_data['college_id'];

            //fetch all the stream of the college
            $sql_query = "SELECT a.`stream_id`, b.`title`, b.`study_type`, b.`study_type_count` FROM `college_streams` AS a INNER JOIN `stream` AS b ON a.`stream_id` = b.`id` WHERE a.`college_id` = '$college_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            $data_array = array();
            foreach ($query_result as $value) {
                $data_array[$value->stream_id] = $value->title;
            }
            echo json_encode($data_array);
        } else {
            echo 0;
        }
    }

    public function get_college_stream_courses() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && isset($posted_data['college_id']) && !empty($posted_data['college_id']) && isset($posted_data['stream_id']) && !empty($posted_data['stream_id'])) {
            $college_id = $posted_data['college_id'];
            $stream_id = $posted_data['stream_id'];

            $sql_query = "SELECT a.course_id, b.`title` FROM `college_stream_course` as a INNER JOIN `stream_courses` as b ON a.`course_id` = b.`id` WHERE a.`course_type`='existing' AND a.`stream_id` = '$stream_id' AND a.`college_id` = '$college_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            $data_array = array();
            foreach ($query_result as $value) {
                $data_array['stream_course'][$value->course_id] = $value->title;
            }

            //stream number of semester
            $sql_query = "SELECT `study_type`,`study_type_count` FROM `stream` WHERE `id` = '$stream_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            $data_array['stream_semester'][$query_result[0]->study_type] = $query_result[0]->study_type_count;

            echo json_encode($data_array);
        }
    }

    public function user_request_submit() {
        $posted_data = $this->input->post();
        if (!empty($posted_data)) {
            print_r($posted_data);
        }
    }

    /* Send college request to the college if the user is already logged in scholerspace */

    public function send_college_request() {
        // user is not logged in
        if (!$this->ion_auth->logged_in()) {
            show_404();
        }
        $posted_data = $this->input->post();

        if (!empty($posted_data) && isset($posted_data['user_request_type']) && !empty($posted_data['user_request_type']) && !empty($posted_data['college_id'])) {
            /* request handling for different users */ //first if block is for student
            if ($posted_data['user_request_type'] == "student" && isset($posted_data['student_stream']) && isset($posted_data['student_stream_course']) && isset($posted_data['student_semester_or_year'])) {
                $student_stream = $posted_data['student_stream'];
                $student_stream_course = $posted_data['student_stream_course'];
                $college_id = $posted_data['college_id'];
                $user_id = $this->ion_auth->get_user_id();

                //check the stream semesterwise or year wise
                $sql_query = "SELECT `study_type` FROM `stream` WHERE `id` = '$student_stream'";
                $query_result = $this->data_fetch->data_query($sql_query);

                $study_type = $query_result[0]->study_type;
                $student_semester_or_year = $posted_data['student_semester_or_year'];

                //admin request table insert
                $sql_query = "INSERT INTO `college_user_request_to_admin`(`college_id`, `user_id`, `requested_user_type`) VALUES('$college_id','$user_id','student')";
                $query_result = $this->data_insert->data_query($sql_query);

                if ($query_result) {
                    $request_id = $this->db->insert_id();

                    //admin request table insert
                    $sql_query = "INSERT INTO `college_user_request_to_admin_for_student`(`request_id`, `stream_id`, `stream_course_id`, `study_type`,`semester_or_year`) VALUES('$request_id','$student_stream','$student_stream_course','$study_type','$student_semester_or_year')";
                    $query_result = $this->data_insert->data_query($sql_query);
                }
                if ($query_result) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else if ($posted_data['user_request_type'] == "teacher") {
                $college_id = $posted_data['college_id'];
                $user_request_type = $posted_data['user_request_type'];
                $user_id = $this->ion_auth->get_user_id();

                //admin request table insert
                $sql_query = "INSERT INTO `college_user_request_to_admin`(`college_id`, `user_id`, `requested_user_type`) VALUES('$college_id','$user_id','$user_request_type')";
                $query_result = $this->data_insert->data_query($sql_query);

                if ($query_result) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else if ($posted_data['user_request_type'] == "alumni") {
                $college_id = $posted_data['college_id'];
                $user_request_type = $posted_data['user_request_type'];
                $user_id = $this->ion_auth->get_user_id();

                //alumni details
                $alumni_year = $posted_data['alumni_year'];
                $alumni_stream = $posted_data['alumni_stream'];
                $alumni_stream_course = $posted_data['alumni_stream_course'];

                //admin request table insert
                $sql_query = "INSERT INTO `college_user_request_to_admin`(`college_id`, `user_id`, `requested_user_type`) VALUES('$college_id','$user_id','$user_request_type')";
                $query_result = $this->data_insert->data_query($sql_query);

                if ($query_result) {
                    $request_id = $this->db->insert_id();

                    //admin request table insert
                    $sql_query = "INSERT INTO `college_user_request_to_admin_for_alumni`(`request_id`, `stream_id`, `stream_course_id`,`year_of_passing`) VALUES('$request_id','$alumni_stream','$alumni_stream_course','$alumni_year')";
                    $query_result = $this->data_insert->data_query($sql_query);
                }
                if ($query_result) {
                    echo 1;
                } else {
                    echo 0;
                }
            }
        }
        die();
    }

}

?>