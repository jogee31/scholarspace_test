<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Stream extends MY_Controller {

    function __construct() {
        parent::__construct();
        $user = $this->ion_auth->user()->row();
        if ($user->user_type != "data-entry") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
    }

    public function index() {
//fetch all stream list
        $sql_query = "SELECT * FROM `stream`";
        $query_result = $this->data_fetch->data_query($sql_query);
        $data['stream_list'] = $query_result;

//temp_stream_course
        $sql_query = "SELECT * FROM `contemporary_stream`";
        $query_result = $this->data_fetch->data_query($sql_query);
        $data['contemporary_stream_list'] = $query_result;

        $data['course_list'] = array();

//only one time foreach loop
        foreach ($query_result as $value) {
            $sql_query = "SELECT * FROM `stream_courses` WHERE `stream_id` = '$value->id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            $data['course_list'][$value->id] = $query_result;
            break;
        }
        $this->load->view("data-entry/view_stream", $data);
    }

    public function add_new_stream() {
        if (!empty($_POST)) {
            $stream = $this->input->post('stream');

            $sql_query = "INSERT INTO `contemporary_stream`(`title`) VALUES('$stream')";
            $query_result = $this->data_insert->data_query($sql_query);

            if ($query_result) {
                $this->session->set_flashdata('message_success', "Stream Added Successfully.");
                redirect("data-entry/stream", 'refresh');
            } else {
                $this->session->set_flashdata('message_danger', "Failed to Add Stream.");
                redirect("data-entry/stream", 'refresh');
            }
        }
    }

    public function get_stream_corses() {
        if (!empty($_POST)) {
            $stream = $this->input->post('stream');
            $stream = explode("-", $stream);

            if ($stream[1] == 'existing') {
                $sql_query = "SELECT * FROM `stream_courses` WHERE `stream_id` = '$stream[0]'";
            } else {
                $sql_query = "SELECT * FROM `contemporary_stream_courses` WHERE `contemporary_stream_id` = '$stream[0]'";
            }
            $query_result = $this->data_fetch->data_query($sql_query);
            foreach ($query_result as $key => $value) {
                ?>
                <tr>
                    <td><?php echo $value->title; ?></td>
                </tr>
                <?php
            }
            if ($stream[1] == 'existing') {
                $sql_query = "SELECT * FROM `contemporary_stream_courses` WHERE `contemporary_stream_id` = '$stream[0]' AND `stream_type` = 'existing'";
                $query_result = $this->data_fetch->data_query($sql_query);
                foreach ($query_result as $key => $value) {
                    ?>
                    <tr>
                        <td><?php echo $value->title; ?></td>
                    </tr>
                    <?php
                }
            }
        }
    }

    public function add_new_stream_course() {
        if (!empty($_POST)) {
            $stream_course = $this->input->post('stream_course');
            $stream_id = $this->input->post('stream_id');
            $stream = explode("-", $stream_id);

            if ($stream[1] == 'existing') {
                $sql_query = "INSERT INTO `contemporary_stream_courses`(`title`,`contemporary_stream_id`,`stream_type`) VALUES('$stream_course','$stream[0]]','existing')";
            } else {
                $sql_query = "INSERT INTO `contemporary_stream_courses`(`title`,`contemporary_stream_id`,`stream_type`) VALUES('$stream_course','$stream[0]','contemporary')";
            }
            $query_result = $this->data_insert->data_query($sql_query);
            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

}
