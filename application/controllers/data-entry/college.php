<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class College extends MY_Controller {

    function __construct() {
        parent::__construct();
        $user = $this->ion_auth->user()->row();
        if ($user->user_type != "data-entry") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
    }

    // Dataentry Registaration of college
    public function register() {
        $sql_query = "SELECT * FROM `institution_types`";
        $query_result = $this->data_fetch->data_query($sql_query);
        $data['institution_types'] = $query_result;

        $sql_query = "SELECT * FROM `state`";
        $query_result = $this->data_fetch->data_query($sql_query);
        $data['state_list'] = $query_result;

        //get stream list
        $sql_query = "SELECT * FROM `stream`";
        $query_result = $this->data_fetch->data_query($sql_query);
        $data['stream_list'] = $query_result;

        //get contemporary stream list
        $sql_query = "SELECT * FROM `contemporary_stream`";
        $query_result = $this->data_fetch->data_query($sql_query);
        $data['contemporary_stream_list'] = $query_result;

        $this->load->view('data-entry/register', $data);
    }

    //save registration college
    public function save_college_registration() {
        if (!empty($_POST) && $this->input->post('flag') == 1) {
            //posted data
            $stream_courses = $this->input->post('stream_courses');

            $college_name = $this->input->post('college_name');
            $institute_type = $this->input->post('institute_type');
            $naac_accredition = $this->input->post('naac_accredition');
            $course_offer = $this->input->post('course_offer');
            $address_1 = $this->input->post('address_1');
            $address_2 = $this->input->post('address_2');
            $city = $this->input->post('city');
            $state = $this->input->post('state');
            $zipcode = $this->input->post('zipcode');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $fax = $this->input->post('fax');
            $industry_interaction = $this->input->post('industry_interaction');
            $fee = $this->input->post('fee');
            $placement_details = $this->input->post('placement_details');
            $quality_of_student_and_crowd = $this->input->post('quality_of_student_and_crowd');
            $international_exposure = $this->input->post('international_exposure');
            $international_or_india_tie_up = $this->input->post('international_or_india_tie_up');
            $video = $this->input->post('video');
            $clubs_activity_title = $this->input->post('clubs_activity_title');
            $clubs_activity_description = $this->input->post('clubs_activity_description');
            $hostal_facility_description = $this->input->post('hostal_facility_description');
            $aicte_approved_or_not = $this->input->post('aicte_approved_or_not');
            $laboratories_title = $this->input->post('laboratories_title');
            $laboratories_description = $this->input->post('laboratories_description');
            $festivals_title = $this->input->post('festivals_title');
            $festivals_description = $this->input->post('festivals_description');
            $extra_curricular_title = $this->input->post('extra_curricular_title');
            $extra_curricular_description = $this->input->post('extra_curricular_description');
            $scholarships = $this->input->post('scholarships');
            $facility_stay_near_by = $this->input->post('facility_stay_near_by');
            $sports_infrastructure = $this->input->post('sports_infrastructure');
            $library = $this->input->post('library');
            $faculty_title = $this->input->post('faculty_title');
            $faculty_description = $this->input->post('faculty_description');

            if ($this->input->post('existing_college_id') == 0) {  // 0 is for insert new college
                $sql_query = "INSERT INTO `college`(`college_name`, `institute_type`, `naac_accredition`, `address_1`, `address_2`, `city`, `state`,"
                        . " `zipcode`, `email`, `phone`, `fax`, `industry_interaction`, `fee`, `placement_details`, `quality_of_students_and_crowd`, "
                        . "`international_exposure`, `international_or_india_tie_ups`, `hostel_facility`, `aicte_approved`, `scholarships`, "
                        . "`facility_to_stay_near_by`, `sports_infrastructure`, `library`) VALUES ('$college_name','$institute_type','$naac_accredition','$address_1',"
                        . "'$address_2','$city','$state','$zipcode','$email','$phone','$fax','$industry_interaction','$fee','$placement_details','$quality_of_student_and_crowd',"
                        . "'$international_exposure','$international_or_india_tie_up','$hostal_facility_description','$aicte_approved_or_not',"
                        . "'$scholarships','$facility_stay_near_by','$sports_infrastructure','$library')";

                $query_result = $this->data_insert->data_query($sql_query);
                if ($query_result) {
                    $college_id = $this->db->insert_id();

                    //adding stream and courses
                    foreach ($stream_courses as $key => $value) {
                        $stream_details = $key;
                        $stream_details = explode("-", $stream_details);
                        $stream_id = $stream_details[0];
                        $stream_type = $stream_details[1];

                        $sql_query = "INSERT INTO `college_streams`(`college_id`,`stream_id`,`stream_type`) VALUES('$college_id','$stream_id','$stream_type')";
                        $query_result = $this->data_insert->data_query($sql_query);

                        foreach ($value as $inner_key => $inner_value) {
                            $course_details = $inner_value;
                            $course_details = explode("-", $course_details);
                            $course_id = $course_details[1];
                            $course_type = $course_details[0];

                            $sql_query = "INSERT INTO `college_stream_course`(`course_type`,`course_id`,`stream_id`,`stream_type`,`college_id`) VALUES('$course_type','$course_id','$stream_id','$stream_type','$college_id')";
                            $query_result = $this->data_insert->data_query($sql_query);
                        }
                    }

                    foreach ($video as $key => $value) {
                        $sql_query = "INSERT INTO `college_video`( `college_id`, `video_link`) VALUES ('$college_id','$value')";
                        $this->data_insert->data_query($sql_query);
                    }

                    //College Image upload
                    foreach ($_FILES['images']['name'] as $key => $value) {
                        $target = "img/collegeImg/" . date("Ymdhis");
                        $target.=basename($_FILES['images']['name'][$key]);
                        move_uploaded_file($_FILES['images']['tmp_name'][$key], $target);
                        $sql_query = "INSERT INTO `college_image`(`college_id`, `image`) VALUES ('$college_id','$target')";
                        $this->data_insert->data_query($sql_query);
                    }

                    foreach ($clubs_activity_title as $key => $value) {
                        $sql_query = "INSERT INTO `college_clubs_and_activities`(`college_id`, `title`, `description`) VALUES ('$college_id','$value','$clubs_activity_description[$key]')";
                        $this->data_insert->data_query($sql_query);
                    }

                    foreach ($laboratories_title as $key => $value) {
                        $sql_query = "INSERT INTO `college_laboratories`(`college_id`, `title`, `description`) VALUES ('$college_id','$value','$laboratories_description[$key]')";
                        $this->data_insert->data_query($sql_query);
                    }

                    foreach ($festivals_title as $key => $value) {
                        $sql_query = "INSERT INTO `college_festivals`(`college_id`, `title`, `description`) VALUES ('$college_id','$value','$festivals_description[$key]')";
                        $this->data_insert->data_query($sql_query);
                    }

                    foreach ($extra_curricular_title as $key => $value) {
                        $sql_query = "INSERT INTO `college_extra_curricular`( `college_id`,`title`, `description`) VALUES ('$college_id','$value','$extra_curricular_description[$key]')";
                        $this->data_insert->data_query($sql_query);
                    }

                    foreach ($faculty_title as $key => $value) {
                        $sql_query = "INSERT INTO `college_faculty`( `college_id`, `title`, `description`) VALUES ('$college_id','$value','$faculty_description[$key]')";
                        $this->data_insert->data_query($sql_query);
                    }
                }

                $this->session->set_flashdata('message_success', "Successfully Added College Details.");
                redirect("data-entry/college/register/", 'refresh');
            } else {
                $college_id = $this->input->post('existing_college_id');

                $sql_query = "UPDATE `college` SET `institute_type`='$institute_type',`naac_accredition`='$naac_accredition',`address_1`='$address_1',"
                        . "`address_2`='$address_2',`city`='$city',`state`='$state',`zipcode`='$zipcode',`email`='$email',`phone`='$phone',`fax`='$fax',"
                        . "`industry_interaction`='$industry_interaction',`fee`='$fee',`placement_details`='$placement_details',`quality_of_students_and_crowd`='$quality_of_student_and_crowd',"
                        . "`international_exposure`='$international_exposure',`international_or_india_tie_ups`='$international_or_india_tie_up',`hostel_facility`='$hostal_facility_description',"
                        . "`aicte_approved`='$aicte_approved_or_not',`scholarships`='$scholarships',`facility_to_stay_near_by`='$facility_stay_near_by',`sports_infrastructure`='$sports_infrastructure',"
                        . "`library`='$library' WHERE `id` = '$college_id'";

                $query_result = $this->data_insert->data_query($sql_query);

                if ($query_result) {
                    $sql_query = "DELETE FROM `college_streams` WHERE `college_id` = '$college_id'";
                    $query_result = $this->data_delete->data_query($sql_query);

                    $sql_query = "DELETE FROM `college_stream_course` WHERE `college_id` = '$college_id'";
                    $query_result = $this->data_delete->data_query($sql_query);

                    //updating stream and courses
                    foreach ($stream_courses as $key => $value) {
                        $stream_details = $key;
                        $stream_details = explode("-", $stream_details);
                        $stream_id = $stream_details[0];
                        $stream_type = $stream_details[1];

                        $sql_query = "INSERT INTO `college_streams`(`college_id`,`stream_id`,`stream_type`) VALUES('$college_id','$stream_id','$stream_type')";
                        $query_result = $this->data_insert->data_query($sql_query);

                        foreach ($value as $inner_key => $inner_value) {
                            $course_details = $inner_value;
                            $course_details = explode("-", $course_details);
                            $course_id = $course_details[1];
                            $course_type = $course_details[0];

                            $sql_query = "INSERT INTO `college_stream_course`(`course_type`,`course_id`,`stream_id`,`stream_type`,`college_id`) VALUES('$course_type','$course_id','$stream_id','$stream_type','$college_id')";
                            $query_result = $this->data_insert->data_query($sql_query);
                        }
                    }

                    foreach ($video as $key => $value) {
                        //check if exist or not
                        $sql_query = "SELECT `id` FROM `college_video` WHERE `id` = '$key' AND `college_id` = '$college_id'";
                        $query_result = $this->data_fetch->data_query($sql_query);

                        if ($query_result) {
                            $sql_query = "UPDATE `college_video` SET `video_link` = '$value' WHERE `id` = '$key'";
                            $this->data_insert->data_query($sql_query);
                        } else {
                            $sql_query = "INSERT INTO `college_video`( `college_id`, `video_link`) VALUES ('$college_id','$value')";
                            $this->data_insert->data_query($sql_query);
                        }
                    }

                    //College Image upload
                    foreach ($_FILES['images']['name'] as $key => $value) {
                        $target = "img/collegeImg/" . date("Ymdhis");
                        if ($_FILES['images']['name'][$key] != '') {
                            $target.=basename($_FILES['images']['name'][$key]);
                            move_uploaded_file($_FILES['images']['tmp_name'][$key], $target);
                            $sql_query = "INSERT INTO `college_image`(`college_id`, `image`) VALUES ('$college_id','$target')";
                            $this->data_insert->data_query($sql_query);
                        }
                    }

                    foreach ($clubs_activity_title as $key => $value) {
                        //check if exist or not
                        $sql_query = "SELECT `id` FROM `college_clubs_and_activities` WHERE `id` = '$key' AND `college_id` = '$college_id'";
                        $query_result = $this->data_fetch->data_query($sql_query);

                        if ($query_result) {
                            $sql_query = "UPDATE `college_clubs_and_activities` SET `title` = '$value', `description` = '$clubs_activity_description[$key]' WHERE `id` = '$key'";
                            $this->data_insert->data_query($sql_query);
                        } else {
                            $sql_query = "INSERT INTO `college_clubs_and_activities`( `college_id`,`title`,`description`) VALUES ('$college_id','$value','$clubs_activity_description[$key]')";
                            $this->data_insert->data_query($sql_query);
                        }
                    }

                    foreach ($laboratories_title as $key => $value) {
                        //check if exist or not
                        $sql_query = "SELECT `id` FROM `college_laboratories` WHERE `id` = '$key' AND `college_id` = '$college_id'";
                        $query_result = $this->data_fetch->data_query($sql_query);

                        if ($query_result) {
                            $sql_query = "UPDATE `college_laboratories` SET `title` = '$value', `description` = '$laboratories_description[$key]' WHERE `id` = '$key'";
                            $this->data_insert->data_query($sql_query);
                        } else {
                            $sql_query = "INSERT INTO `college_laboratories`( `college_id`,`title`,`description`) VALUES ('$college_id','$value','$laboratories_description[$key]')";
                            $this->data_insert->data_query($sql_query);
                        }
                    }

                    foreach ($festivals_title as $key => $value) {
                        //check if exist or not
                        $sql_query = "SELECT `id` FROM `college_festivals` WHERE `id` = '$key' AND `college_id` = '$college_id'";
                        $query_result = $this->data_fetch->data_query($sql_query);

                        if ($query_result) {
                            $sql_query = "UPDATE `college_festivals` SET `title` = '$value', `description` = '$festivals_description[$key]' WHERE `id` = '$key'";
                            $this->data_insert->data_query($sql_query);
                        } else {
                            $sql_query = "INSERT INTO `college_festivals`( `college_id`,`title`,`description`) VALUES ('$college_id','$value','$festivals_description[$key]')";
                            $this->data_insert->data_query($sql_query);
                        }
                    }

                    foreach ($extra_curricular_title as $key => $value) {
                        //check if exist or not
                        $sql_query = "SELECT `id` FROM `college_extra_curricular` WHERE `id` = '$key' AND `college_id` = '$college_id'";
                        $query_result = $this->data_fetch->data_query($sql_query);

                        if ($query_result) {
                            $sql_query = "UPDATE `college_extra_curricular` SET `title` = '$value', `description` = '$extra_curricular_description[$key]' WHERE `id` = '$key'";
                            $this->data_insert->data_query($sql_query);
                        } else {
                            $sql_query = "INSERT INTO `college_extra_curricular`( `college_id`,`title`,`description`) VALUES ('$college_id','$value','$extra_curricular_description[$key]')";
                            $this->data_insert->data_query($sql_query);
                        }
                    }

                    foreach ($faculty_title as $key => $value) {
                        //check if exist or not
                        $sql_query = "SELECT `id` FROM `college_faculty` WHERE `id` = '$key' AND `college_id` = '$college_id'";
                        $query_result = $this->data_fetch->data_query($sql_query);

                        if ($query_result) {
                            $sql_query = "UPDATE `college_faculty` SET `title` = '$value', `description` = '$faculty_description[$key]' WHERE `id` = '$key'";
                            $this->data_insert->data_query($sql_query);
                        } else {
                            $sql_query = "INSERT INTO `college_faculty`( `college_id`,`title`,`description`) VALUES ('$college_id','$value','$faculty_description[$key]')";
                            $this->data_insert->data_query($sql_query);
                        }
                    }
                }

                $this->session->set_flashdata('message_success', "Successfully Updated College Details.");
                redirect("data-entry/college/register/", 'refresh');
            }
        } else {
            $this->session->set_flashdata('message_danger', "Failed To Add College Details.");
            redirect("data-entry/college/register/", 'refresh');
        }
    }

    public function fetch_courses() {
        $query = "SELECT * FROM `stream`";
        $res = $this->data_fetch->data_query($query);
        foreach ($res as $value) {
            ?>
            <option class="head_of_stream" value="<?php echo $value->id; ?>"><?php echo $value->title; ?></option>
            <?php
        }
    }

    public function show_college_names() {
        $query_variable = $this->input->post("name");
        $query = "SELECT `id`,`college_name`,`city`,`state` FROM `college` WHERE college_name LIKE '$query_variable%' LIMIT 10";
        $data = $this->data_fetch->data_query($query);
        $college_details_array = array();
        foreach ($data as $value) {
            $college_details_array[$value->id] = $value->college_name . "*/*" . $value->id . "*/*" . $value->city . "*/*" . $value->state;
        }
        echo json_encode($college_details_array);
    }

    /*
      public function fetch_selected_courses() {
      $id_and_str = $this->input->post("id_stream_id");

      $sql_query = "SELECT * FROM `stream` WHERE `id` = '$id_and_str'";
      $query_result = $this->data_fetch->data_query($sql_query);
      $stream_title = $query_result[0]->title;

      $sql_query = "SELECT * FROM `stream_courses` WHERE stream_id = '$id_and_str'";
      $query_result = $this->data_fetch->data_query($sql_query);
      ?>
      <div stream_id="<?php echo $id_and_str; ?>" class='col-md-4 <?php echo $stream_title; ?>'>
      <div class='form-group'>
      <label for="exampleInputEmail1"><?php echo $stream_title; ?> Course *<span class="required_error_span"></span></label>
      <div>
      <select class="select_required" id="course_tab-<?php echo $id_and_str; ?>" multiple="">
      <?php
      foreach ($query_result as $values) {
      ?>
      <option class="form-control" value="<?php echo $values->id; ?>"><?php echo $values->title; ?></option>
      <?php
      }
      ?>
      </select>
      </div>
      </div>
      </div>
      <?php
      }
     */

    public function fetch_selected_courses() {
        $stream_id = $this->input->post('stream_id');
        $stream_name = $this->input->post('stream_name');
        $stream = explode("-", $stream_id);

        if ($stream[1] == 'existing') {
            $sql_query = "SELECT * FROM `stream_courses` WHERE `stream_id` = '$stream[0]'";
        } else {
            $sql_query = "SELECT * FROM `contemporary_stream_courses` WHERE `contemporary_stream_id` = '$stream[0]' AND `stream_type` = 'contemporary'";
        }
        $query_result = $this->data_fetch->data_query($sql_query);

        if ($stream[1] == 'existing') {
            $sql_query = "SELECT * FROM `contemporary_stream_courses` WHERE `contemporary_stream_id` = '$stream[0]' AND `stream_type` = 'existing'";
            $contemporary_query_result = $this->data_fetch->data_query($sql_query);

            foreach ($contemporary_query_result as $value) {
                $query_result[] = $value;
            }
        }
        ?>
        <div class='col-md-12 <?php echo $stream_id; ?>'>
            <div class='form-group'>
                <label for="exampleInputEmail1"><?php echo $stream_name; ?> Course *<span class="required_error_span"></span></label>
                <div>
                    <select class="select_required" name="stream_courses[<?php echo $stream_id; ?>][]" id="<?php echo $stream_id; ?>" multiple="">
                        <?php
                        foreach ($query_result as $values) {
                            ?>
                            <option class="form-control" value="<?php echo (isset($values->stream_type)) ? "contemporary-" . $values->id : "existing-" . $values->id; ?>"><?php echo $values->title; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <?php
    }

    public function get_update_stream_course() {
        if (!empty($_POST)) {
            $stream_id = $this->input->post('stream_id');
            $college_id = $this->input->post('college_id');
            $stream = explode("-", $stream_id);

            if ($stream[1] == 'existing') {
                $sql_query = "SELECT `title` FROM `stream` WHERE `id` = '$stream[0]'";
            } else {
                $sql_query = "SELECT * FROM `contemporary_stream` WHERE `id` = '$stream[0]'";
            }
            $query_result = $this->data_fetch->data_query($sql_query);
            $stream_name = $query_result[0]->title;

            //college stream courses
            $sql_query = "SELECT * FROM `college_stream_course` WHERE `stream_id` = '$stream[0]' AND `stream_type` = '$stream[1]' AND `college_id` = '$college_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            $college_stream_courses['existing'] = array();
            $college_stream_courses['contemporary'] = array();
            foreach ($query_result as $key => $value) {
                if ($value->course_type == 'existing') {
                    $college_stream_courses['existing'][] = $value->course_id;
                } else {
                    $college_stream_courses['contemporary'][] = $value->course_id;
                }
            }

            if ($stream[1] == 'existing') {
                $sql_query = "SELECT * FROM `stream_courses` WHERE `stream_id` = '$stream[0]'";
            } else {
                $sql_query = "SELECT * FROM `contemporary_stream_courses` WHERE `contemporary_stream_id` = '$stream[0]' AND `stream_type` = 'contemporary'";
            }
            $query_result = $this->data_fetch->data_query($sql_query);

            if ($stream[1] == 'existing') {
                $sql_query = "SELECT * FROM `contemporary_stream_courses` WHERE `contemporary_stream_id` = '$stream[0]' AND `stream_type` = 'existing'";
                $contemporary_query_result = $this->data_fetch->data_query($sql_query);

                foreach ($contemporary_query_result as $value) {
                    $query_result[] = $value;
                }
            }
            ?>
            <div class='col-md-12 <?php echo $stream_id; ?>'>
                <div class='form-group'>
                    <label for="exampleInputEmail1"><?php echo $stream_name; ?> Course *<span class="required_error_span"></span></label>
                    <div>
                        <select class="select_required" name="stream_courses[<?php echo $stream_id; ?>][]" id="<?php echo $stream_id; ?>" multiple="">
                            <?php
                            foreach ($query_result as $values) {
                                ?>
                                <option class="form-control" 
                                        <?php
                                        if (isset($values->stream_type) && in_array($values->id, $college_stream_courses['contemporary'])) {
                                            echo "selected";
                                        } else {
                                            if (in_array($values->id, $college_stream_courses['existing'])) {
                                                echo "selected";
                                            }
                                        }
                                        ?> 
                                        value="<?php echo (isset($values->stream_type)) ? "contemporary-" . $values->id : "existing-" . $values->id; ?>"><?php echo $values->title; ?></option>
                                        <?php
                                    }
                                    ?>
                        </select>
                    </div>
                </div>
            </div>
            <?php
        }
    }

    public function delete_college_img() {
        if (!empty($_POST)) {
            $image_id = $this->input->post('img_id');

            $sql_query = "SELECT `image` FROM `college_image` WHERE `id` = '$image_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            $target = $query_result[0]->image;

            $sql_query = "DELETE FROM `college_image` WHERE `id`='$image_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            unlink($target);
            echo $query_result;
        }
    }

    public function set_all_values_on_college_select() {
        $college_id = $this->input->post('college_id');
        $query = "SELECT * FROM `college` WHERE id='$college_id'";

        $college_details_array = array();
        $college_details = $this->data_fetch->data_query($query);

        if (count($college_details)) {
            $college_details_array['institute_type'] = $college_details[0]->institute_type;
            $college_details_array['naac_accredition'] = $college_details[0]->naac_accredition;
            $college_details_array['address_1'] = $college_details[0]->address_1;
            $college_details_array['address_2'] = $college_details[0]->address_2;
            $college_details_array['city'] = $college_details[0]->city;
            $college_details_array['state'] = $college_details[0]->state;
            $college_details_array['zipcode'] = $college_details[0]->zipcode;
            $college_details_array['email'] = $college_details[0]->email;
            $college_details_array['phone'] = $college_details[0]->phone;
            $college_details_array['fax'] = $college_details[0]->fax;
            $college_details_array['industry_interaction'] = $college_details[0]->industry_interaction;
            $college_details_array['fee'] = $college_details[0]->fee;
            $college_details_array['placement_details'] = $college_details[0]->placement_details;
            $college_details_array['quality_of_students_and_crowd'] = $college_details[0]->quality_of_students_and_crowd;
            $college_details_array['international_exposure'] = $college_details[0]->international_exposure;
            $college_details_array['international_or_india_tie_ups'] = $college_details[0]->international_or_india_tie_ups;
            $college_details_array['hostel_facility'] = $college_details[0]->hostel_facility;
            $college_details_array['aicte_approved'] = $college_details[0]->aicte_approved;
            $college_details_array['scholarships'] = $college_details[0]->scholarships;
            $college_details_array['facility_to_stay_near_by'] = $college_details[0]->facility_to_stay_near_by;
            $college_details_array['sports_infrastructure'] = $college_details[0]->sports_infrastructure;
            $college_details_array['library'] = $college_details[0]->library;
        }

        /* update streams and courses */
        $sql_query = "SELECT * FROM `college_streams` WHERE college_id ='$college_id'";
        $query_result = $this->data_fetch->data_query($sql_query);
        $college_details_array['streams'] = array();
        foreach ($query_result as $key => $value) {
            $college_details_array['streams'][] = $value;
        }

        /* update videos */
        $sql_query = "SELECT * FROM `college_video` WHERE college_id ='$college_id'";
        $query_result = $this->data_fetch->data_query($sql_query);
        $college_details_array['videos'] = array();
        foreach ($query_result as $key => $value) {
            $college_details_array['videos'][$value->id] = $value->video_link;
        }

        /* Update images */
        $sql_query = "SELECT * FROM `college_image` WHERE college_id ='$college_id'";
        $query_result = $this->data_fetch->data_query($sql_query);
        $college_details_array['images'] = array();
        foreach ($query_result as $key => $value) {
            $college_details_array['images'][$value->id] = $value->image;
        }

        /* update club and activities */
        $sql_query = "SELECT * FROM `college_clubs_and_activities` WHERE college_id ='$college_id'";
        $query_result = $this->data_fetch->data_query($sql_query);
        $college_details_array['college_clubs_and_activities'] = array();
        foreach ($query_result as $key => $value) {
            $college_details_array['college_clubs_and_activities'][$value->id] = array($value->title, $value->description);
        }

        /* update Laboratories */
        $sql_query = "SELECT * FROM `college_laboratories` WHERE college_id ='$college_id'";
        $query_result = $this->data_fetch->data_query($sql_query);
        $college_details_array['college_laboratories'] = array();
        foreach ($query_result as $key => $value) {
            $college_details_array['college_laboratories'][$value->id] = array($value->title, $value->description);
        }

        /* update Fastivales */
        $sql_query = "SELECT * FROM `college_festivals` WHERE college_id ='$college_id'";
        $query_result = $this->data_fetch->data_query($sql_query);
        $college_details_array['college_festivals'] = array();
        foreach ($query_result as $key => $value) {
            $college_details_array['college_festivals'][$value->id] = array($value->title, $value->description);
        }

        /* update Extra Curricular  */
        $sql_query = "SELECT * FROM `college_extra_curricular` WHERE college_id ='$college_id'";
        $query_result = $this->data_fetch->data_query($sql_query);
        $college_details_array['college_extra_curricular'] = array();
        foreach ($query_result as $key => $value) {
            $college_details_array['college_extra_curricular'][$value->id] = array($value->title, $value->description);
        }

        /* update Faculty  */
        $sql_query = "SELECT * FROM `college_faculty` WHERE college_id ='$college_id'";
        $query_result = $this->data_fetch->data_query($sql_query);
        $college_details_array['college_faculty'] = array();
        foreach ($query_result as $key => $value) {
            $college_details_array['college_faculty'][$value->id] = array($value->title, $value->description);
        }

        echo json_encode($college_details_array);
    }

}
