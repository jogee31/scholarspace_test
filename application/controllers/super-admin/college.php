<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class College extends MY_Controller {

    function __construct() {
        parent::__construct();
        $user = $this->ion_auth->user()->row();
        if ($user->user_type != "super-admin") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
    }

    public function index() {
        //fetch all new requested
        $sql_query = "SELECT * FROM `contemporary_college_registration`";
        $query_result = $this->data_fetch->data_query($sql_query);
        $data['requested_college_list'] = $query_result;

        $sql_query = "SELECT * FROM `state`";
        $query_result = $this->data_fetch->data_query($sql_query);
        $data['state_list'] = $query_result;

        //code to make college registration notification status as readed
        $sql_query = "UPDATE `contemporary_college_registration` SET `is_checked` = '1'";
        $query_result = $this->data_update->data_query($sql_query);

        $this->load->view('supper-admin/college_request', $data);
    }

    public function save_college_details() {
        if (!empty($_POST)) {
            $college_id = $this->input->post('college_id');
            $college_name = $this->input->post('college_name');
            $college_city = $this->input->post('college_city');
            $college_state = $this->input->post('college_state');
            $college_zipcode = $this->input->post('college_zipcode');

            //update college name
            $sql_query = "UPDATE `contemporary_college_registration` SET `college_name`='$college_name', `college_city` = '$college_city', `college_state` = '$college_state', `college_zipcode` = '$college_zipcode' WHERE `id`='$college_id'";
            $query_result = $this->data_update->data_query($sql_query);
            if ($query_result) {
                $this->session->set_flashdata('message_success', "Institute Type Added Successfully.");
                redirect("super-admin/college/", 'refresh');
            } else {
                $this->session->set_flashdata('message_danger', "Failed to add intitution Type.");
                redirect("super-admin/college/", 'refresh');
            }
        }
    }

    public function ignore_college_request() {
        if (!empty($_POST)) {
            $college_id = $this->input->post('college_id');
            //ignore college request
            $sql_query = "DELETE FROM `contemporary_college_registration` WHERE `id`='$college_id'";
            $query_result = $this->data_delete->data_query($sql_query);
            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    public function accept_college_request() {
        if (!empty($_POST)) {
            $college_id = $this->input->post('college_id');
            //select query for geting college detail from `contemporary_college_registration` table
            $sql_query = "SELECT * FROM `contemporary_college_registration` WHERE `id`='$college_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            if ($query_result) {
                $college_name = $query_result[0]->college_name;
                $college_city = $query_result[0]->college_city;
                $college_state = $query_result[0]->college_state;
                $college_zipcode = $query_result[0]->college_zipcode;
                $requested_user_id = $query_result[0]->requested_user_id;
            }
            //insert query to add college into main college table
            if ($query_result) {
                $sql_query = "INSERT INTO `college`(`college_name`,`city`,`state`,`zipcode`) VALUES('$college_name','$college_city','$college_state','$college_zipcode')";
                $query_result = $this->data_insert->data_query($sql_query); //here query result will be the last inserted id in college id
            }
            //insert query to add college requested user into college_registration_request_by_users table, to send notification-
            //-when the college will activate
            if ($query_result) {
                $main_college_id = $this->db->insert_id();
                $sql_query = "INSERT INTO `college_registration_request_by_users`(`college_id`,`user_id`) VALUES('$main_college_id','$requested_user_id')";
                $query_result = $this->data_insert->data_query($sql_query);
            }
            //delete query to remove college from contemporary college table
            if ($query_result) {
                $sql_query = "DELETE FROM `contemporary_college_registration` WHERE `id`='$college_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    //function to load assign admin user to college view
    public function assign_user() {
        //fetch all college list
        $sql_query = "SELECT `id`,`college_name` FROM `college` WHERE `id` NOT IN (SELECT `college_id` FROM `college_admin`) ORDER BY `college_name` ASC";
        $query_result = $this->data_fetch->data_query($sql_query);
        $data['college_list'] = $query_result;

        //fetch all college admin user
        $sql_query = "SELECT `id`,`first_name`,`last_name`,`email` FROM `users` WHERE `id` NOT IN (SELECT `user_id` FROM `college_admin`) AND `user_type_id`='4' AND `user_type`='college-admin' ORDER BY `first_name` ASC";
        $query_result = $this->data_fetch->data_query($sql_query);
        $data['user_list'] = $query_result;

        //assigned user college list
        $sql_query = "select c.id as college_id,c.college_name,u.id as user_id, u.first_name, u.last_name, u.email ,ca.id as college_admin_id from college_admin as ca join college as c on ca.college_id=c.id join users as u on ca.user_id=u.id";
        $query_result = $this->data_fetch->data_query($sql_query);
        $data['assigned_college_user_list'] = $query_result;

        $this->load->view('supper-admin/assign_college_user', $data);
    }

    public function assign_college_user_submit() {
        if (!empty($_POST)) {
            $college_id = $this->input->post('college');
            $user_id = $this->input->post('user');

            $sql_query = "INSERT INTO `college_admin`(`college_id`,`user_id`) VALUES('$college_id','$user_id')";
            $query_result = $this->data_insert->data_query($sql_query);

            if ($query_result) {
                $this->session->set_flashdata('message_success', "College User assigned Successfully.");
                redirect("super-admin/college/assign_user", 'refresh');
            } else {
                $this->session->set_flashdata('message_danger', "Failed to assign College User.");
                redirect("super-admin/college/assign_user", 'refresh');
            }
        }
    }

    public function college_list() {
        //fetch all state list
        $sql_query = "SELECT * FROM `state`";
        $query_result = $this->data_fetch->data_query($sql_query);
        $data['state_list'] = $query_result;

        $this->load->view('supper-admin/college_list', $data);
    }

    public function edit_college_details_submit() {
        if (!empty($_POST)) {
            $college_name = $this->input->post('college_name');
            $college_id = $this->input->post('college_id');
            $college_city = $this->input->post('college_city');
            $college_state = $this->input->post('college_state');
            $college_zipcode = $this->input->post('college_zipcode');

            $sql_query = "UPDATE `college` SET `college_name` = '$college_name', `city` = '$college_city', `state` = '$college_state', `zipcode` = '$college_zipcode' WHERE `id` = '$college_id'";
            $query_result = $this->data_update->data_query($sql_query);
            if ($query_result) {
                $this->session->set_flashdata('message_success', "College details updated Successfully.");
                redirect("super-admin/college/college_list", 'refresh');
            } else {
                $this->session->set_flashdata('message_danger', "Failed to update College Details.");
                redirect("super-admin/college/college_list", 'refresh');
            }
        }
    }

    public function change_college_active_status() {
        if (!empty($_POST)) {
            $college_id = $this->input->post('college_id');
            $active_status = ($this->input->post('active_status') == 1) ? 0 : 1;

            $sql_query = "UPDATE `college` SET `active_status` = '$active_status' WHERE `id` = '$college_id'";
            $query_result = $this->data_update->data_query($sql_query);

            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    public function fetch_datatable_college_list() {
        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
        $aColumns = array('college_name', 'city', 'state', 'zipcode', 'id', 'active_status');
        // DB table to use
        $sTable = 'college';

        $iDisplayStart = $this->input->get_post('iDisplayStart', true);
        $iDisplayLength = $this->input->get_post('iDisplayLength', true);
        $iSortCol_0 = $this->input->get_post('iSortCol_0', true);
        $iSortingCols = $this->input->get_post('iSortingCols', true);
        $sSearch = $this->input->get_post('sSearch', true);
        $sEcho = $this->input->get_post('sEcho', true);

        // Paging
        if (isset($iDisplayStart) && $iDisplayLength != '-1') {
            $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
        }

        // Ordering
        if (isset($iSortCol_0)) {
            for ($i = 0; $i < intval($iSortingCols); $i++) {
                $iSortCol = $this->input->get_post('iSortCol_' . $i, true);
                $bSortable = $this->input->get_post('bSortable_' . intval($iSortCol), true);
                $sSortDir = $this->input->get_post('sSortDir_' . $i, true);

                if ($bSortable == 'true') {
                    $this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        /*
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        if (isset($sSearch) && !empty($sSearch)) {
            for ($i = 0; $i < count($aColumns); $i++) {
                $bSearchable = $this->input->get_post('bSearchable_' . $i, true);

                // Individual column filtering
                if (isset($bSearchable) && $bSearchable == 'true') {
                    $this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
                }
            }
        }
        // Select Data
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $aColumns)), false);
        $rResult = $this->db->get($sTable);


// Data set length after filtering
        $this->db->select('FOUND_ROWS() AS found_rows');
        $iFilteredTotal = $this->db->get()->row()->found_rows;

        // Total data set length
        $iTotal = $this->db->count_all($sTable);

// Output
        $output = array(
            'sEcho' => intval($sEcho),
            'iTotalRecords' => $iTotal,
            'iTotalDisplayRecords' => $iFilteredTotal,
            'aaData' => array()
        );

        foreach ($rResult->result_array() as $aRow) {
            $row = array();

            foreach ($aColumns as $col) {
                if ($col != 'id' && $col != 'active_status') {
                    $row[] = ($aRow[$col] == '') ? "-" : $aRow[$col];
                } else {
                    $temp_html = '<button type="button" college_state="' . $aRow['state'] . '" college_zipcode="' . $aRow['zipcode'] . '" college_city="' . $aRow['city'] . '" college_name="' . $aRow['college_name'] . '" college_id="' . $aRow['id'] . '" class="btn btn-success btn-xs college_edit_btn"><i class="fa fa-edit"></i> Edit</button> |';
                    $temp_html .= ' <button type="button" class="btn ';
                    $temp_html .= ($aRow['active_status'] == 1) ? "btn-danger " : "btn-success ";
                    $temp_html .= 'btn-xs active_status_college_btn" active_status=' . $aRow['active_status'] . ' college_id = ' . $aRow['id'] . '><i class="fa';
                    $temp_html .= ($aRow['active_status'] == 1) ? " fa-ban" : " fa-check";
                    $temp_html .= '"></i> ';
                    $temp_html .= ($aRow['active_status'] == 1) ? "Deactivate" : "Activate";
                    $temp_html .= '</button>';
                    $row[] = $temp_html;
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }

}
