<?php

Class Stream extends MY_Controller {

    function __construct() {
        parent::__construct();
        $user = $this->ion_auth->user()->row();
        if ($user->user_type != "super-admin") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
    }

    public function index() {
        $sql_query = "SELECT a.title as stream_name, a.id as stream_id, a.study_type, a.study_type_count, a.active_status as stream_active_status, b.title as course_name, b.id as course_id, b.active_status as course_active_status FROM `stream` as a left join `stream_courses` as b on a.id = b.stream_id";
        $query_result = $this->data_fetch->data_query($sql_query);

        $stream_array = array();
        $stream_course_array = array();

        foreach ($query_result as $value) {
            if (!key_exists($value->stream_id, $stream_array)) {
                $stream_array[$value->stream_id] = array('stream_id' => $value->stream_id, 'stream_name' => $value->stream_name, 'stream_active_status' => $value->stream_active_status, 'study_type' => $value->study_type, 'study_type_count' => $value->study_type_count);
            }

            //if no courses added for this stream
            if ($value->course_name != NULL) {
                $stream_course_array[$value->stream_id][] = array('stream_id' => $value->stream_id, 'course_id' => $value->course_id, 'course_name' => $value->course_name, 'course_active_status' => $value->course_active_status);
            } else {
                $stream_course_array[$value->stream_id] = array();
            }
        }
        $data['stream_list'] = $stream_array;
        $data['stream_courses'] = $stream_course_array;
        $this->load->view('supper-admin/viewStream', $data);
    }

    public function submit_add_stream() {
        if (!empty($_POST)) {

            $stream = $this->input->post('stream');
            $study_type = $this->input->post('study_type');
            $study_type_count = $this->input->post($study_type);

            $sql_query = "INSERT INTO `stream`(`title`, `active_status`,`study_type`,`study_type_count`) VALUES ('$stream',1,'$study_type','$study_type_count')";
            $query_result = $this->data_insert->data_query($sql_query);
            if ($query_result) {
                $this->session->set_flashdata('message_success', "Stream Added Successfully.");
                redirect("super-admin/stream/", 'refresh');
            } else {
                $this->session->set_flashdata('message_danger', "Failed to Add Stream.");
                redirect("super-admin/stream/", 'refresh');
            }
        }
    }

    //function is to add courses for the streams
    public function submit_add_stream_subject() {
        if (!empty($_POST)) {
            $stream_id = $this->input->post('stream_id');
            $course_name = $this->input->post('course_name');

            $query_result = 0;

            foreach ($course_name as $value) {
                $sql_query = "INSERT INTO `stream_courses`(`title`,`stream_id`,`active_status`) VALUES('$value','$stream_id','1')";
                $query_result = $this->data_insert->data_query($sql_query);
            }
            if ($query_result) {
                $this->session->set_flashdata('message_success', "Courses Added Successfully.");
                redirect("super-admin/stream/", 'refresh');
            } else {
                $this->session->set_flashdata('message_danger', "Failed to Add Course.");
                redirect("super-admin/stream/", 'refresh');
            }
        }
    }

    public function submit_edit_stream() {
        if (!empty($_POST)) {
            $stream_title = $this->input->post('stream_title');
            $stream_id = $this->input->post('stream_id');

            $study_type = $this->input->post('study_type');
            $study_type_count = $this->input->post($study_type);

            $sql_query = "UPDATE `stream` SET `title` = '$stream_title', `study_type` = '$study_type', `study_type_count` = '$study_type_count' WHERE `id`='$stream_id'";
            $query_result = $this->data_insert->data_query($sql_query);
            if ($query_result) {
                $this->session->set_flashdata('message_success', "Stream updated Successfully.");
                redirect("super-admin/stream/", 'refresh');
            } else {
                $this->session->set_flashdata('message_danger', "Failed to update Stream.");
                redirect("super-admin/stream/", 'refresh');
            }
        }
    }

    public function delete_stream() {
        if (!empty($_POST)) {
            $stream_id = $this->input->post('stream_id');
            $sql_query = "DELETE FROM `stream` WHERE `id` = '$stream_id'";
            $query_result = $this->data_delete->data_query($sql_query);
            if ($query_result) {
                $sql_query = "DELETE FROM `stream_courses` WHERE `stream_id` = '$stream_id'";
                $query_result = $this->data_delete->data_query($sql_query);
                if ($query_result) {
                    $this->session->set_flashdata('message_success', "Stream deleted Successfully.");
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        }
    }

    public function delete_stream_course() {
        if (!empty($_POST)) {
            $course_id = $this->input->post('course_id');
            $page_from = $this->input->post('from');
            $sql_query = "DELETE FROM `stream_courses` WHERE `id` = '$course_id'";
            $query_result = $this->data_delete->data_query($sql_query);
            if ($query_result) {
                if ($page_from != '' && $page_from == 'page') {
                    $this->session->set_flashdata('message_success', "Course deleted Successfully.");
                }
                echo 1;
            } else {
                if ($page_from != '' && $page_from == 'page') {
                    $this->session->set_flashdata('message_danger', "Failed to delete Course.");
                }
                echo 0;
            }
        }
    }

    public function get_stream_all_courses() {
        if (!empty($_POST)) {
            $stream_id = $this->input->post('stream_id');

            $sql_query = "SELECT * FROM `stream_courses` WHERE `stream_id` = '$stream_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            $count = 1;
            foreach ($query_result as $value) {
                ?>
                <tr>
                    <td><?php echo $count; ?></td>
                    <td><?php echo $value->title; ?></td>
                    <td>
                        <button class="btn btn-xs btn-success edit_stream_courses_btn" course_name="<?php echo $value->title; ?>" course_id="<?php echo $value->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                        <button class="btn btn-xs btn-danger delete_stream_course from_modal" course_id="<?php echo $value->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                    </td>
                </tr>
                <?php
                $count++;
            }
        }
    }

    public function submit_edit_stream_courses() {
        if (!empty($_POST)) {
            $course_id = $this->input->post('course_id');
            $course_name = $this->input->post('course_name');

            $sql_query = "UPDATE `stream_courses` SET `title` = '$course_name' WHERE `id` = '$course_id'";
            $query_result = $this->data_update->data_query($sql_query);

            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

}
