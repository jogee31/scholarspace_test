<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends MY_Controller {

    function __construct() {
        parent::__construct();
        $user = $this->ion_auth->user()->row();
        if ($user->user_type != "super-admin") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
    }

    //view all user list in super admin panel
    public function view() {

        //query to fatch all the user list
        $sql_query = "SELECT `id`,`first_name`,`last_name`,`email`,`phone`,`encoded_pwd`,`active`,`user_type` FROM `users` WHERE `user_type` != 'super-admin'";
        $query_result = $this->data_fetch->data_query($sql_query);

        $data['user_list'] = $query_result;

        $this->load->view('supper-admin/viewUser', $data);
    }

    //add user page open 
    public function add() {
        //fetch all the user type
        $sql_query = "SELECT * FROM `user_types`";
        $query_result = $this->data_fetch->data_query($sql_query);
        $data['user_type_list'] = $query_result;
        $this->load->view('supper-admin/addUser', $data);
    }

    // add user form submit function
    public function add_user_submit() {
        if (!empty($_POST)) {
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $user_type = $this->input->post('user_type');

            $username = strtolower($first_name) . " " . strtolower($last_name);
            $email = strtolower($email);
            $password = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', 5)), 0, 8);

            $additional_data = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'phone' => $phone,
                'encoded_pwd' => base64_encode($password),
                'user_type' => $user_type,
                'user_type_id' => ($user_type == "data-entry") ? 2 : 4
            );

            if ($this->ion_auth->register($username, $password, $email, $additional_data)) {
                $body = <<<EOD
                            <div style='font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#000000'>
                  <div style='width:680px'>
                  <div style='height:70px;background-color: #308cbd;position:relative;width: 680px;  margin-bottom: 22px;'>
                                <img src='http://spydernet.in/demo/scholarspace_demo/image/logo.png' style='position:absolute;margin-top: 27px;margin-left: 25px;'>
                            </div>
                  <p style='margin-top:0px;margin-bottom:20px; color:#000000 !important;width:100%;'>Dear $username</p>
                                <p style='margin-top:0px;margin-bottom:15px; color:#000000 !important;width:100%;'>Congratulations! You have been registered for Scholler Space</p>
                                <p style='margin-top:0px;margin-bottom:30px; color:#000000 !important;width:100%'>Please login to your account using below mentioned link and credentials.<a href='http://localhost/scholerspace/' target='_blank'>Scholler Space</a>                                
</p>
                  <table style='border-collapse:collapse;width:100%;border-top:1px solid #dddddd;margin-bottom:30px;border-left:1px solid #dddddd;'>
                  <thead>
                  <tr>
                  <td style='font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222' colspan='2'>Login Details</td>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                  <td style='font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;color:#000000 !important;text-align:left;padding:7px'><b>Username:</b> $email </td>
                  <td style='font-size:12px;color:#000000 !important;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px;'><b> Password:</b> $password </td>
                  </tr>
                  </tbody>
                  </table>
                  <p style='margin-top:0px;margin-bottom:5px; color:#000000 !important;width:100%;'>In case of any concerns, reply to this email.</p>
                  <p style='margin-top:0px;margin-bottom:5px; color:#000000 !important;width:100%;'>Regards,</p>
                  <p style='margin-top:0px;margin-bottom:30px; color:#000000 !important;width:100%;'>Team Scholler Space.</p>
                  <div style='width:680px;height:35px;background-color: #182027;margin-top: 50px;display:block!important;'></div>
                  </div>
                  </div>
EOD;
                $this->send_email($email, "Scholler Space user Regestration details.", $body);

                $this->session->set_flashdata('message_success', $this->ion_auth->messages());
                redirect("super-admin/user/add", 'refresh');
            } else {
                $this->session->set_flashdata('message_danger', $this->ion_auth->messages());
                redirect("super-admin/user/add", 'refresh');
            }
        }
    }

    public function delete_user() {
        if (!empty($_POST)) {
            $user_id = $this->input->post('user_id');
            $user_type = $this->input->post('user_type');

            $sql_query = "DELETE FROM `users` WHERE `user_type` = '$user_type' AND `id` = '$user_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            if ($query_result && $user_type == 'front-end-user') {
                $sql_query = "DELETE FROM `college_registration_request_by_users` WHERE `user_id` = '$user_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }

            if ($query_result) {
                $this->session->set_flashdata('message_success', $this->ion_auth->messages());
                echo $query_result;
            } else {
                $this->session->set_flashdata('message_danger', $this->ion_auth->messages());
                echo $query_result;
            }
        }
    }

    public function edit() {
        if (isset($_GET['ui']) && !empty($_GET['ui']) && isset($_GET['ut']) && !empty($_GET['ut'])) {
            $user_id = base64_decode($this->input->get('ui'));
            $user_type = base64_decode($this->input->get('ut'));

            $sql_query = "SELECT `id`,`first_name`,`last_name`,`email`,`phone`,`user_type`,`user_type_id`,`active` FROM `users` WHERE `id` = '$user_id' LIMIT 1";
            $query_result = $this->data_fetch->data_query($sql_query);

            if (!count($query_result)) {
                show_404();
            }
            $data['user_details'] = $query_result[0];

            $sql_query = "SELECT * FROM `user_types` WHERE `id` != '1'";
            $query_result = $this->data_fetch->data_query($sql_query);
            $data['user_type_list'] = $query_result;

            $this->load->view('supper-admin/editUser', $data);
        } else {
            show_404();
        }
    }

    //function is to submit edit user form
    public function edit_user_submit() {
        if (!empty($_POST)) {
            $user_type = $this->input->post('user_type');
            $user_type_id = ($user_type == 'data-entry') ? 1 : (($user_type == 'front-end-user') ? 3 : 4);
            $user_id = $this->input->post('user_id');

            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');

            $sql_query = "UPDATE `users` SET `first_name` = '$first_name', `last_name` = '$last_name', `email` = '$email', `phone` = '$phone',`user_type` = '$user_type', `user_type_id`='$user_type_id' WHERE `id` = '$user_id'";
            $query_result = $this->data_update->data_query($sql_query);

            if ($query_result) {
                $this->session->set_flashdata('message_success', "User details updated successfully.");
                redirect("super-admin/user/edit?ui=" . base64_encode($user_id) . "&ut=" . base64_encode($user_type), 'refresh');
            } else {
                $this->session->set_flashdata('message_danger', "Failed to update User details.");
                redirect("super-admin/user/edit?ui=" . base64_encode($user_id) . "&ut=" . base64_encode($user_type), 'refresh');
            }
        }
    }

    // function is to check email exist or not
    public function check_email_exist() {
        if (!empty($_POST)) {
            $email = $this->input->post('email');
            $sql_query = "SELECT `email` FROM `users` WHERE `email` = '$email'";
            $query_result = $this->data_fetch->data_query($sql_query);

            //if email exist will return more than 0
            echo count($query_result);
        }
    }

    // function is to check email exist or not from edit user pages
    public function check_edit_email_exist() {
        if (!empty($_POST)) {
            $user = $this->input->post('user_id');
            $email = $this->input->post('email');
            $sql_query = "SELECT `email` FROM `users` WHERE `email` = '$email' AND `id`!='$user'";
            $query_result = $this->data_fetch->data_query($sql_query);

            //if email exist will return more than 0
            echo count($query_result);
        }
    }

    //funtion is to active/deactive user account status
    public function change_active_status() {
        if (!empty($_POST)) {
            $user_id = $this->input->post('user_id');
            $active_status = ($this->input->post('active_status')) ? 0 : 1; // if posted is 1 means user is currently active so deactivate it with 0
            $sql_query = "UPDATE `users` SET `active` = '$active_status' WHERE `id` = '$user_id'";
            $query_result = $this->data_update->data_query($sql_query);
            if ($query_result) {
                $this->session->set_flashdata('message_success', $this->ion_auth->messages());
                echo $query_result;
            } else {
                $this->session->set_flashdata('message_danger', $this->ion_auth->messages());
                echo $query_result;
            }
        }
    }

}
