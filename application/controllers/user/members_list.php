<?php

/* class is to mainten the user request from the college admin */

Class Members_list extends MY_Controller {

    private $user = null;

    function __construct() {
        parent::__construct();
        $this->user = $this->ion_auth->user()->row();
        if ($this->user->user_type != "college-admin") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
    }

    // function will load the request list page into the college admin profile
    public function index() {
        
    }

    public function college() {
        $user_id = $this->user->id;

        //get user's college id and college member list
        $sql_query = "SELECT `college_id` FROM `college_admin` WHERE `user_id` = '$user_id'";
        $query_result = $this->data_fetch->data_query($sql_query);
        $college_id = $query_result[0]->college_id;

        $CompleteList = array();
        $StudentsList = array();
        $AlumniList = array();
        $TeacherList = array();

        //Get all members of collage seperately and sort according to first name
        $SelectCollegeUsers = "SELECT t1.* "
                . "FROM `college_users_intranet` t1 "
                . "INNER JOIN users t2 ON t1.user_id = t2.id "
                . "WHERE `college_id` = '$college_id' "
                . "ORDER BY t2.first_name ASC";
        $SelectCollegeUsers_Result = $this->data_fetch->data_query($SelectCollegeUsers);

        foreach ($SelectCollegeUsers_Result as $key => $value) {
            $intranet_user_type = $value->intranet_user_type;
            switch ($intranet_user_type) {
                case 'student':
                    $SelectDetailsOfStudent = "SELECT t1.*, "
                            . "t2.`title` AS `stream_name`, "
                            . "t3.`title` AS `stream_course_name`, "
                            . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, "
                            . "t5.intranet_user_type "
                            . "FROM `college_student_users_intranet` AS t1 "
                            . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                            . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                            . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE t1.college_users_intranet_id = $value->id "
                            . "ORDER BY t4.first_name ASC";
                    $SelectDetailsOfStudent_Result = $this->data_fetch->data_query($SelectDetailsOfStudent);

                    foreach ($SelectDetailsOfStudent_Result as $value1) {
                        $StudentsList[$value1->college_users_intranet_id] = $value1;
                    }
                    break;
                case 'alumni':
                    $SelectDetailsOfAlumni = "SELECT t1.*, "
                            . "t2.`title` AS `stream_name`, "
                            . "t3.`title` AS `stream_course_name`, "
                            . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, "
                            . "t5.intranet_user_type "
                            . "FROM `college_alumni_users_intranet` AS t1 "
                            . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                            . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                            . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE college_users_intranet_id = $value->id "
                            . "ORDER BY t4.first_name ASC";
                    $SelectDetailsOfAlumni_Result = $this->data_fetch->data_query($SelectDetailsOfAlumni);
                    foreach ($SelectDetailsOfAlumni_Result as $value2) {
                        $AlumniList[$value2->college_users_intranet_id] = $value2;
                    }
                    break;
                case 'teacher':
                    $SelectDetailsOfTeacher = "SELECT t1.*, "
                            . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, "
                            . "t5.intranet_user_type "
                            . "FROM `college_teacher_users_intranet` AS t1 "
                            . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE college_users_intranet_id = $value->id "
                            . "ORDER BY t4.first_name ASC";
                    $SelectDetailsOfTeacher_Result = $this->data_fetch->data_query($SelectDetailsOfTeacher);

                    foreach ($SelectDetailsOfTeacher_Result as $value3) {
                        $TeacherList[$value3->college_users_intranet_id] = $value3;
                    }
                    break;
            }
        }


        //Get all members of college and sort according to timestamp
        $SelectCollegeUsersAll = "SELECT * "
                . "FROM `college_users_intranet` "
                . "WHERE `college_id` = '$college_id' "
                . "ORDER BY timestamp DESC";
        $SelectCollegeUsersAll_Result = $this->data_fetch->data_query($SelectCollegeUsersAll);
        foreach ($SelectCollegeUsersAll_Result as $value) {
            $intranet_user_type = $value->intranet_user_type;
            switch ($intranet_user_type) {
                case 'student':
                    $SelectDetailsOfAll = "SELECT t1.*, "
                            . "t2.`title` AS `stream_name`, "
                            . "t3.`title` AS `stream_course_name`, "
                            . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, "
                            . "t5.intranet_user_type "
                            . "FROM `college_student_users_intranet` AS t1 "
                            . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                            . "INNER JOIN `stream_courses` AS t3 ON t1.`stream_course_id` = t3.`id` "
                            . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE t1.college_users_intranet_id = $value->id "
                            . "ORDER BY t1.timestamp DESC";
                    break;
                case 'alumni':
                    $SelectDetailsOfAll = "SELECT t1.*, "
                            . "t2.`title` AS `stream_name`, "
                            . "t3.`title` AS `stream_course_name`, "
                            . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, "
                            . "t5.intranet_user_type "
                            . "FROM `college_alumni_users_intranet` AS t1 "
                            . "INNER JOIN `stream` AS t2 ON t1.`stream_id` = t2.`id` "
                            . "INNER JOIN `stream_courses` as t3 ON t1.`stream_course_id` = t3.`id` "
                            . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE college_users_intranet_id = $value->id "
                            . "ORDER BY t1.timestamp DESC";
                    break;
                case 'teacher':
                    $SelectDetailsOfAll = "SELECT t1.*, "
                            . "t4.id AS user_id, t4.first_name AS fname, t4.last_name AS lname, "
                            . "t5.intranet_user_type "
                            . "FROM `college_teacher_users_intranet` AS t1 "
                            . "INNER JOIN `users` AS t4 ON t4.id = '$value->user_id' "
                            . "INNER JOIN college_users_intranet AS t5 ON t1.college_users_intranet_id = t5.id "
                            . "WHERE college_users_intranet_id = $value->id "
                            . "ORDER BY t1.timestamp DESC";
                    break;
            }
            $SelectDetailsOfAll_Result = $this->data_fetch->data_query($SelectDetailsOfAll);
            foreach ($SelectDetailsOfAll_Result as $value4) {
                $CompleteList[$value4->college_users_intranet_id] = $value4;
            }
        }

        $data['temp_array'] = ['', 'First', 'Second', 'Third', 'Forth', 'Fifth', 'Sixth', 'Seventh', 'Eighth', 'Ninth', 'Tenth', 'Eleventh'];
        $data['StudentsList'] = $StudentsList;
        $data['AlumniList'] = $AlumniList;
        $data['TeacherList'] = $TeacherList;
        $data['CompleteList'] = $CompleteList;
        $this->load->view('front-end/college_users', $data);
    }

    public function DeleteMemberFromCollege_Method() {
        $PostData = $this->input->post();
        $user_id = $PostData['user_id'];
        $intranet_id = $PostData['intranet_id'];
        $college_id = $this->session->userdata('college_id');

        if (!empty($PostData) && isset($PostData['user_id']) && isset($PostData['intranet_id'])) {

            $SelectFromIntranet = "SELECT * FROM college_users_intranet "
                    . "WHERE id = '$intranet_id' "
                    . "AND college_id = '$college_id' "
                    . "AND user_id = '$user_id'";
            $SelectFromIntranet_Result = $this->data_fetch->data_query($SelectFromIntranet);

            $IntranetUserType = $SelectFromIntranet_Result[0]->intranet_user_type;
            $DeleteMember_Result = 1;

            switch ($IntranetUserType) {
                case 'student':
                    $DeleteMember = "DELETE t1.*,t2.* "
                            . "FROM college_users_intranet t1 "
                            . "INNER JOIN college_student_users_intranet t2 ON t1.id = t2.college_users_intranet_id "
                            . "WHERE t1.id = '$intranet_id'";
                    break;
                case 'alumni':
                    $DeleteMember = "DELETE t1.*,t2.* "
                            . "FROM college_users_intranet t1 "
                            . "INNER JOIN college_alumni_users_intranet t2 ON t1.id = t2.college_users_intranet_id "
                            . "WHERE t1.id = '$intranet_id'";
                    break;
                case 'teacher':
                    $DeleteMember = "DELETE t1.*,t2.* "
                            . "FROM college_users_intranet t1 "
                            . "INNER JOIN college_teacher_users_intranet t2 ON t1.id = t2.college_users_intranet_id "
                            . "WHERE t1.id = '$intranet_id'";
                    break;
            }
            $DeleteMember_Result = $this->data_delete->data_query($DeleteMember);

            if ($DeleteMember_Result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

}

?>