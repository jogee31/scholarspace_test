<?php
/* class is to mainten the user request from the college admin */

Class Request extends MY_Controller {

    private $user = null;

    function __construct() {
        parent::__construct();
        $this->user = $this->ion_auth->user()->row();
        if ($this->user->user_type != "college-admin") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
    }

    // function will load the request list page into the college admin profile
    public function index() {
        
        //admin user id
        $user_id = $this->user->id;

        //get the admin college id and request for the college
        $sql_query = "SELECT `college_id` FROM `college_admin` WHERE `user_id`='$user_id'";
        $query_result = $this->data_fetch->data_query($sql_query);

        if (count($query_result)) {
            $college_id = $query_result[0]->college_id;

            //get all the request for the college
            $sql_query = "SELECT * FROM `college_user_request_to_admin` WHERE `college_id`='$college_id' ORDER BY timestamp DESC;";
            $query_result = $this->data_fetch->data_query($sql_query);

            $data['admin_user_requests'] = $query_result;
        }

        $sql_query = "SELECT a.*, b.`title` as `stream_name`, c.`title` as `stream_course_name` FROM `college_user_request_to_admin_for_student` as a INNER JOIN `stream` as b ON a.`stream_id` = b.`id` INNER JOIN `stream_courses` as c ON a.`stream_course_id` = c.`id`";
        $query_result = $this->data_fetch->data_query($sql_query);

        $student_request_details_array = array();
        foreach ($query_result as $value) {
            $student_request_details_array[$value->request_id] = $value;
        }
        $data['student_request_details_array'] = $student_request_details_array;

        $sql_query = "SELECT a.*, b.`title` as `stream_name`, c.`title` as `stream_course_name` FROM `college_user_request_to_admin_for_alumni` as a INNER JOIN `stream` as b ON a.`stream_id` = b.`id` INNER JOIN `stream_courses` as c ON a.`stream_course_id` = c.`id`";
        $query_result = $this->data_fetch->data_query($sql_query);

        $alumni_request_details_array = array();
        foreach ($query_result as $value) {
            $alumni_request_details_array[$value->request_id] = $value;
        }

        $data['alumni_request_details_array'] = $alumni_request_details_array;

        $data['user'] = $this->user;
        $data['college_id'] = $college_id;
        $this->load->view("front-end/user_request", $data);
    }

    public function delete_request() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && isset($posted_data['request_id']) && isset($posted_data['requested_user_type'])) {
            $request_id = $posted_data['request_id'];
            $requested_user_type = $posted_data['requested_user_type'];

            $query_result = 1;
            if ($requested_user_type == "student") {
                $sql_query = "DELETE a.*, b.* FROM `college_user_request_to_admin` as a INNER JOIN `college_user_request_to_admin_for_student` as b ON a.`id` = b.`request_id` WHERE a.`id` = '$request_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            } else if ($requested_user_type == "teacher") {
                $sql_query = "DELETE FROM `college_user_request_to_admin` WHERE `id` = '$request_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            } else if ($requested_user_type == "alumni") {
                $sql_query = "DELETE a.*, b.* FROM `college_user_request_to_admin` as a INNER JOIN `college_user_request_to_admin_for_alumni` as b ON a.`id` = b.`request_id` WHERE a.`id` = '$request_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    //function is to accept user request from admin panel
    public function accept_admin_user_request() {
        $posted_data = $this->input->post();
        
        if (!empty($posted_data) && !empty($posted_data['request_id']) && !empty($posted_data['requested_user_type'])) {
            $request_id = $posted_data['request_id'];
            $college_id = $posted_data['college_id'];
            $requested_user_type = $posted_data['requested_user_type'];

            //get all the request details and proceed accepting request
            if ($requested_user_type == "student") {
                $sql_query = "SELECT a.*,b.* FROM `college_user_request_to_admin` as a INNER JOIN `college_user_request_to_admin_for_student` as b ON a.`id` = b.`request_id` AND a.`id`='$request_id' AND `college_id` = '$college_id' LIMIT 1";
                $query_result = $this->data_fetch->data_query($sql_query);

                if (count($query_result)) {
                    $user_id = $query_result[0]->user_id;
                    $study_type = $query_result[0]->study_type;

                    $stream_id = $posted_data['Stream'];
                    $stream_course_id = $posted_data['Course'];
                    $semester_or_year = $posted_data['Semester'];

                    //insert data into college_users_intranet
                    $sql_query = "INSERT INTO `college_users_intranet`(`college_id`,`user_id`,`intranet_user_type`) VALUES('$college_id','$user_id','$requested_user_type')";
                    $query_result = $this->data_insert->data_query($sql_query);

                    //inserted id use it as forgen key in college_student_users_intranet table
                    $college_users_intranet_id = $this->db->insert_id();

                    //insert into college_student_users_intranet table
                    $sql_query = "INSERT INTO `college_student_users_intranet`(`college_users_intranet_id`,`stream_id`,`stream_course_id`,`study_type`,`semester_or_year`) VALUES('$college_users_intranet_id','$stream_id','$stream_course_id','$study_type','$semester_or_year')";
                    $query_result = $this->data_insert->data_query($sql_query);

                    //after accepting user request form college admin panel DELETE from request table
                    $sql_query = "DELETE a.*, b.* FROM `college_user_request_to_admin` as a INNER JOIN `college_user_request_to_admin_for_student` as b ON a.`id` = b.`request_id` WHERE a.`id` = '$request_id'";
                    $query_result = $this->data_delete->data_query($sql_query);

                    if ($query_result) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                }
            } else if ($requested_user_type == "alumni") {

                $sql_query = "SELECT a.*,b.* FROM `college_user_request_to_admin` as a INNER JOIN `college_user_request_to_admin_for_alumni` as b ON a.`id` = b.`request_id` AND a.`id`='$request_id' AND `college_id` = '$college_id' LIMIT 1";
                $query_result = $this->data_fetch->data_query($sql_query);

                if (count($query_result)) {

                    $user_id = $query_result[0]->user_id;

                    $stream_id = $posted_data['Stream'];
                    $stream_course_id = $posted_data['Course'];
                    $year_of_passing = $posted_data['Semester'];

                    //insert data into college_users_intranet
                    $sql_query = "INSERT INTO `college_users_intranet`(`college_id`,`user_id`,`intranet_user_type`) VALUES('$college_id','$user_id','$requested_user_type')";
                    $query_result = $this->data_insert->data_query($sql_query);

                    //inserted id use it as forgen key in college_alumni_users_intranet teble
                    $college_users_intranet_id = $this->db->insert_id();

                    //insert into college_student_users_intranet table
                    $sql_query = "INSERT INTO `college_alumni_users_intranet`(`college_users_intranet_id`,`stream_id`,`stream_course_id`,`year_of_passing`) VALUES('$college_users_intranet_id','$stream_id','$stream_course_id','$year_of_passing')";
                    $query_result = $this->data_insert->data_query($sql_query);

                    //after accepting user request form college admin panel DELETE from request table
                    $sql_query = "DELETE a.*, b.* FROM `college_user_request_to_admin` as a INNER JOIN `college_user_request_to_admin_for_alumni` as b ON a.`id` = b.`request_id` WHERE a.`id` = '$request_id'";
                    $query_result = $this->data_delete->data_query($sql_query);
                    if ($query_result) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                }
            }
        }
    }

    /*     * ****************************************************************************************  */
    /*                                    Code by - Mahadev Mattikalli                                 */
    /*     * ***************************************************************************************** */

    public function CheckBeforConfirm_method() {

        $request_id = $this->input->post('request_id');
        $user_type = $this->input->post('requested_user_type');
        $user_id = $this->input->post('user_id');
        $temp_array = ['', 'First', 'Second', 'Third', 'Forth', 'Fifth', 'Sixth', 'Seventh', 'Eighth', 'Ninth', 'Tenth', 'Eleventh'];

        if ($user_type == 'student') {
            $SelectQuery = "SELECT t1.stream_id,t1.stream_course_id,t1.study_type,t1.semester_or_year "
                    . "FROM college_user_request_to_admin_for_student t1 "
                    . "WHERE request_id = '$request_id'";
            $QueryResult = $this->data_fetch->data_query($SelectQuery);

            $study_type = $QueryResult[0]->study_type;
            $stream_id = $QueryResult[0]->stream_id;
            $stream_course_id = $QueryResult[0]->stream_course_id;
            $semester = $QueryResult[0]->semester_or_year;

            $SelectStreams = "SELECT * FROM stream WHERE active_status = '1' AND study_type = '$study_type'";
            $Result_SelectStreams = $this->data_fetch->data_query($SelectStreams);
            $study_type_count = $Result_SelectStreams[0]->study_type_count;

            $SelectCourses = "SELECT * FROM stream_courses WHERE active_status = '1' AND stream_id = '$stream_id'";
            $Result_SelectCourses = $this->data_fetch->data_query($SelectCourses);

            $SelectUsers = "SELECT first_name,last_name FROM users WHERE id = '$user_id'";
            $Result_SelectUsers = $this->data_fetch->data_query($SelectUsers);
            ?>

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                <h4 class="modal-title" id="myModalLabel">Request confirmation for user- <?php echo $Result_SelectUsers[0]->first_name . " " . $Result_SelectUsers[0]->last_name . " (" . $user_type . ")"; ?></h4>
            </div>
            <div class="course_message">
                <div class="col-md-12">
                    <label class="col-md-1" style="float:left;padding-top: 4px;"> Stream: </label>
                    <div class="visible_select col-md-10">
                        <select name="SelectStreams" class="SelectStreams vis_select" id="SelectStreams">
                            <?php foreach ($Result_SelectStreams as $key => $value) { ?>
                                <option value="<?php echo $value->id; ?>" <?php
                                if ($value->id == $stream_id)
                                    echo "selected";
                                ?>><?php echo $value->title; ?></option>;
                                        <?php
                                    }
                                    ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="course_message">
                <div class="col-md-12">
                    <label class="col-md-1" style="float:left;padding-top: 4px;"> Course: </label>
                    <div class="visible_select col-md-10">
                        <select name="SelectCourse" class="SelectCourse vis_select" id="SelectCourse">
                            <?php foreach ($Result_SelectCourses as $key => $value) { ?>
                                <option value="<?php echo $value->id; ?>" <?php
                                if ($value->id == $stream_course_id)
                                    echo "selected";
                                ?>><?php echo $value->title; ?></option>;
                                        <?php
                                    }
                                    ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="course_message">
                <div class="col-md-12">
                    <label class="col-md-1" style="float:left;padding-top: 4px;"> Semester: </label>
                    <div class="visible_select col-md-10">
                        <select name="SelectSemester" class="SelectSemester vis_select" id="SelectSemester">
                            <?php for ($i = 1; $i <= $study_type_count; $i++) { ?>
                                <option value="<?php echo $i ?>" <?php if ($i == $semester) echo "selected"; ?>><?php echo $temp_array[$i]; ?></option>
                            <?php }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <?php
        } else if ($user_type == 'alumni') {
            $SelectQuery = "SELECT t1.stream_id,t1.stream_course_id,t1.year_of_passing "
                    . "FROM college_user_request_to_admin_for_alumni t1 "
                    . "WHERE request_id = '$request_id'";
            $QueryResult = $this->data_fetch->data_query($SelectQuery);

            $stream_id = $QueryResult[0]->stream_id;
            $stream_course_id = $QueryResult[0]->stream_course_id;
            $year = $QueryResult[0]->year_of_passing;

            $SelectStreams = "SELECT * FROM stream WHERE active_status = '1'";
            $Result_SelectStreams = $this->data_fetch->data_query($SelectStreams);
            $study_type_count = $Result_SelectStreams[0]->study_type_count;

            $SelectCourses = "SELECT * FROM stream_courses WHERE active_status = '1' AND stream_id = '$stream_id'";
            $Result_SelectCourses = $this->data_fetch->data_query($SelectCourses);
            
            $SelectUsers = "SELECT first_name,last_name FROM users WHERE id = '$user_id'";
            $Result_SelectUsers = $this->data_fetch->data_query($SelectUsers);
            ?>

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_pop" aria-hidden="true"></span></button>
                <h4 class="modal-title" id="myModalLabel">Request confirmation for user- <?php echo $Result_SelectUsers[0]->first_name . " " . $Result_SelectUsers[0]->last_name . " (" . $user_type . ")"; ?></h4>
            </div>
            <div class="course_message">
                <div class="col-md-12">
                    <label class="col-md-3" style="float:left;padding-top: 4px;"> Year of Passing: </label>
                    <div class="visible_select col-md-8">
                        <select name="SelectYearOfPassing" class="SelectYearOfPassing vis_select" id="SelectYearOfPassing">
                            <?php for ($i = 1990; $i < date('Y'); $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($i == $year) echo "selected"; ?> ><?php echo $i; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="course_message">
                <div class="col-md-12">
                    <label class="col-md-3" style="float:left;padding-top: 4px;"> Course: </label>
                    <div class="visible_select col-md-8">
                        <select name="SelectStreams" class="SelectStreams vis_select" id="SelectStreams">
                            <?php foreach ($Result_SelectStreams as $key => $value) { ?>
                                <option value="<?php echo $value->id; ?>" <?php
                                if ($value->id == $stream_id)
                                    echo "selected";
                                ?>><?php echo $value->title; ?></option>;
                                        <?php
                                    }
                                    ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="course_message">
                <div class="col-md-12">
                    <label class="col-md-3" style="float:left;padding-top: 4px;"> Semester: </label>
                    <div class="visible_select col-md-8">
                        <select name="SelectCourse" class="SelectCourse vis_select" id="SelectCourse">
                            <?php foreach ($Result_SelectCourses as $key => $value) { ?>
                                <option value="<?php echo $value->id; ?>" <?php
                                if ($value->id == $stream_course_id)
                                    echo "selected";
                                ?>><?php echo $value->title; ?></option>;
                                        <?php
                                    }
                                    ?>
                        </select>
                    </div>
                </div>
            </div>
            <?php
        }
    }

}
?>