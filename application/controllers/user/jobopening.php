<?php

/* class is to mainten the user request from the college admin */

Class Jobopening extends MY_Fixedgroup {

    function __construct() {
        parent::__construct();
        $this->group_name = "jobopening";
        $this->encryption_decryption_object = new Encryption();
    }

    function index() {
        $data = $this->get_group_post(); //get all the post for this group from MY_Fixedgroup cotroller

        $college_id = $this->college_id;

        /* get all the job posted */
        $sql_query = "SELECT * FROM `college_jobopenings_list` WHERE `college_id` = '$college_id' ORDER BY `job_added_timestamp` DESC";
        $query_result = $this->data_fetch->data_query($sql_query);

        $data['jobopening_list'] = $query_result;
        $this->load->view("front-end/jobopening", $data);
    }

    public function submit_add_new_jobopening() {
        $posted_data = $this->input->post();

        if (!empty($posted_data) && !empty($posted_data['job_title']) && !empty($posted_data['job_description'])) {

            $job_title = mysql_real_escape_string($posted_data['job_title']);
            $job_description = mysql_real_escape_string($posted_data['job_description']);
            $college_id = $this->college_id;
            $timestamp = date('Y-m-d h:i:s');

            /* INSERT INTO jobopening table */
            $sql_query = "INSERT INTO `college_jobopenings_list`(`college_id`,`job_title`,`job_description`,`job_added_timestamp`) VALUES('$college_id','$job_title','$job_description','$timestamp')";
            $query_result = $this->data_insert->data_query($sql_query);

            if ($query_result) {
                echo $this->encryption_decryption_object->encode($this->db->insert_id());
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    /* Submit edit jobopening details form */

    public function submit_edit_jobopening() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['job_id']) && $this->encryption_decryption_object->is_valid_input($posted_data['job_id']) && !empty($posted_data['job_title']) && !empty($posted_data['job_description'])) {
            $job_id = $this->encryption_decryption_object->is_valid_input($posted_data['job_id']);
            $job_title = mysql_real_escape_string($posted_data['job_title']);
            $job_description = mysql_real_escape_string($posted_data['job_description']);

            /* update the job opening details */
            $sql_query = "UPDATE `college_jobopenings_list` SET `job_title` = '$job_title', `job_description` = '$job_description' WHERE `job_id` = '$job_id'";
            $query_result = $this->data_update->data_query($sql_query);

            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    public function delete_jobopening() {
        $posted_data = $this->input->post();

        if (!empty($posted_data) && !empty($posted_data['jobopening_id']) && $this->encryption_decryption_object->is_valid_input($posted_data['jobopening_id'])) {
            $jobopening_id = $this->encryption_decryption_object->is_valid_input($posted_data['jobopening_id']);
            $sql_query = "DELETE FROM `college_jobopenings_list` WHERE `job_id` = '$jobopening_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

}
