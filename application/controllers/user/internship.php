<?php

/* class is to mainten the user request from the college admin */

Class Internship extends MY_Fixedgroup {

    function __construct() {
        parent::__construct();
        $this->group_name = "internship";
        $this->encryption_decryption_object = new Encryption();
    }

    function index() {
        //get all the post for this group from MY_Fixedgroup cotroller
        $data = $this->get_group_post();

        $college_id = $this->college_id;

        //get Internship List
        $sql_query = "SELECT `id`,`internship_title` FROM `college_internship_list` WHERE `college_id` = '$college_id'";
        $query_result = $this->data_fetch->data_query($sql_query);

        $data['internship_list'] = $query_result;

        $this->load->view("front-end/internship_page", $data);
    }

    function submit_add_internship() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['internshipTitle'])) {
            $college_id = $this->college_id;
            $internship_title = mysql_real_escape_string($posted_data['internshipTitle']);

            $sql_query = "INSERT INTO `college_internship_list`(`internship_title`,`college_id`) VALUE('$internship_title','$college_id')";
            $query_result = $this->data_insert->data_query($sql_query);

            if ($query_result) {
                echo $this->encryption_decryption_object->encode($this->db->insert_id());
            } else {
                echo 0;
            }
        }
    }

    function submit_edit_internship() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['internshipID']) && $this->encryption_decryption_object->is_valid_input($posted_data['internshipID'])) {
            $college_id = $this->college_id;

            $internshipID = $this->encryption_decryption_object->is_valid_input($posted_data['internshipID']);
            $internship_title = mysql_real_escape_string($posted_data['internshipTitle']);

            $sql_query = "UPDATE `college_internship_list` SET `internship_title` = '$internship_title' WHERE `id` = '$internshipID' AND `college_id` = '$college_id'";
            $query_result = $this->data_insert->data_query($sql_query);

            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    function delete_internship() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['internshipID']) && $this->encryption_decryption_object->is_valid_input($posted_data['internshipID'])) {
            $college_id = $this->college_id;

            $internshipID = $this->encryption_decryption_object->is_valid_input($posted_data['internshipID']);

            $sql_query = "DELETE FROM `college_internship_list` WHERE `id` = '$internshipID' AND `college_id` = '$college_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

}

?>