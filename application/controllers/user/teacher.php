<?php

/* class is to mainten the user request from the college admin */

Class Teacher extends MY_Controller {

    private $user = null;

    function __construct() {
        parent::__construct();
        $this->user = $this->ion_auth->user()->row();
        if ($this->user->user_type != "college-admin") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
        $this->load->library(array('form_validation', 'session')); // load form lidation libaray & session library
        $this->load->helper(array('url', 'html', 'form'));  // load url,html,form helpers
    }

    public function index() {
        $data['user'] = $this->user;
        $data['college_id'] = $this->session->userdata('college_id');

        $tables = $this->config->item('tables', 'ion_auth');
        $this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[1]|max_length[100]|alpha');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[1]|max_length[100]|alpha');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        $this->form_validation->set_rules('mobile', 'Mobile', 'required|min_length[10]|max_length[20]|callback_numeric_wcomma');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('front-end/teacher');
        } else {
            $college_id = $this->session->userdata('college_id');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $user_type = "front-end-user";

            $username = strtolower($first_name) . " " . strtolower($last_name);
            $email = strtolower($email);
            $password = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', 5)), 0, 8);

            $additional_data = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'phone' => $phone,
                'encoded_pwd' => base64_encode($password),
                'user_type' => $user_type,
                'user_type_id' => 3
            );
            if ($user_id = $this->ion_auth->register($username, $password, $email, $additional_data)) {

                //insert data into college_users_intranet
                $sql_query = "INSERT INTO `college_users_intranet`(`college_id`,`user_id`,`intranet_user_type`) VALUES('$college_id','$user_id','teacher')";
                $query_result = $this->data_insert->data_query($sql_query);

                //inserted id use it as forgen key in college_student_users_intranet table
                $college_users_intranet_id = $this->db->insert_id();

                //insert into college_teacher_users_intranet table
                $sql_query = "INSERT INTO `college_teacher_users_intranet`(`college_users_intranet_id`) VALUES('$college_users_intranet_id')";
                $query_result = $this->data_insert->data_query($sql_query);

                if ($query_result) {
                    $this->session->set_flashdata('FormSubmit', 'success');
                    redirect('user/teacher','refresh');
                } else {
                    $this->session->set_flashdata('FormSubmit', 'failure');
                    redirect('user/teacher','refresh');
                }
            } else {
                $this->session->set_flashdata('item_error', 'Request can not completed, Please try again later.');
                redirect(current_url());
            }
        }
    }

}
