<?php

/* class is to mainten the user request from the college admin */

Class Hostel extends MY_Fixedgroup {

    function __construct() {
        parent::__construct();
        $this->group_name = "hostel";
        $this->encryption_decryption_object = new Encryption();
    }

    function index() {
        $data = $this->get_group_post(); //get all the post for this group from MY_Fixedgroup cotroller

        $college_id = $this->college_id;

        //get hostel description
        $sql_query = "SELECT `hostel_description` FROM `college_hostel_description` WHERE `college_id` = '$college_id'";
        $query_result = $this->data_fetch->data_query($sql_query);

        $data['hostel_description'] = $query_result;

        $this->load->view("front-end/hostel_page", $data);
    }

    function hostel_description_submit() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['hostel_description'])) {
            $college_id = $this->college_id;

            $hostel_description = mysql_real_escape_string($posted_data['hostel_description']);

            //check for college hostel details existance
            $sql_query = "SELECT `id` FROM `college_hostel_description` WHERE `college_id` = '$college_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            //if exist then update else insert
            if (count($query_result)) {
                $sql_query = "UPDATE `college_hostel_description` SET `hostel_description` = '$hostel_description' WHERE `college_id` = '$college_id'";
                $query_result = $this->data_update->data_query($sql_query);
            } else {
                $sql_query = "INSERT INTO `college_hostel_description`(`hostel_description`,`college_id`) VALUE('$hostel_description','$college_id')";
                $query_result = $this->data_insert->data_query($sql_query);
            }
            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

}

?>