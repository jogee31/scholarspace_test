<?php

Class Dashboard extends MY_Controller {

    private $user = null;

    function __construct() {
        parent::__construct();
        $this->user = $this->ion_auth->user()->row();
        if ($this->user->user_type != "college-admin" && $this->user->user_type != "front-end-user") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
    }

    public function index() {
        $data['user'] = $this->user;
        $this->load->view("front-end/intranet", $data);
    }

}

?>