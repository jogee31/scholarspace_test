<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Data_update extends CI_Model {

    //normal query function 
    public function data_query($sql_query) {
        return $this->db->query($sql_query);
    }

}
