<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public $permissions;

    public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('login');
        }
        $this->load->library('My_PHPMailer');
    }

    public function send_email($email, $subject, $body) {
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->IsHTML(true);
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = "jogendra@hexwhale.com";
        $mail->Password = "getinside";
        $mail->setFrom('jogendra@hexwhale.com', 'Scholler Space');
        $mail->addReplyTo('jogendra@hexwhale.com', 'Scholler Space');
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->addAddress($email);
        $mail->send();
    }

}
