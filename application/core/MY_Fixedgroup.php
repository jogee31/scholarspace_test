<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Fixedgroup extends MY_Controller {

    private $user = null;
    public $group_name = null;
    public $encryption_decryption_object = null;

    function __construct() {
        parent::__construct();
        $this->user = $this->ion_auth->user()->row();
        $this->college_id = $this->session->userdata('college_id');

        if ($this->user->user_type != "college-admin") {
            show_404();
        }
        $this->load->model('data_fetch');
        $this->load->model('data_insert');
        $this->load->model('data_update');
        $this->load->model('data_delete');
    }

    public function get_group_post() {
        $college_id = $this->college_id;

        //group name
        $post_group_name = $this->group_name;

        //current user
        $user_id = $this->user->user_id;
        $sql_query = "SELECT a.*,b.`post_message`,c.`first_name`,c.`last_name`,c.`profile_picture`,d.`file_path` FROM `college_fixed_group_posts` AS a LEFT JOIN `college_fixed_group_posts_details` as b ON a.`post_id` = b.`post_id` INNER JOIN `users` as c ON a.`user_id` = c.`id` LEFT JOIN `college_fixed_group_posts_files` as d ON a.`post_id` = d.`post_id` WHERE a.`college_id` = '$college_id' AND a.`post_group_name` = '$post_group_name' group by a.`post_id` ORDER BY a.`timestamp` DESC LIMIT 0,4"; // LIMIT 0,4
//        $sql_query = "SELECT a.*,b.`post_message`,c.`first_name`,c.`last_name`,c.`profile_picture` FROM `college_fixed_group_posts` AS a INNER JOIN `college_fixed_group_posts_details` as b ON a.`post_id` = b.`post_id` INNER JOIN `users` as c ON a.`user_id` = c.`id` WHERE a.`college_id` = '$college_id' AND a.`post_group_name` = 'jobopening' ORDER BY a.`timestamp` DESC LIMIT 0,4"
        $query_result = $this->data_fetch->data_query($sql_query);
        $data['posts_list'] = $query_result;

        $data['posts_file_list'] = array();
        $data['posts_reply_list'] = array();
        $data['posts_like_current_user'] = array();
        $data['posts_like_all_user'] = array();
        $data['posts_reply_like_all_user'] = array();
        $data['posts_reply_like_current_user'] = array();

        //fetch POST reply and like list
        foreach ($data['posts_list'] as $value) {
            $post_id = $value->post_id;

            //post files list
            $sql_query = "SELECT `id`,`file_path` FROM `college_fixed_group_posts_files` WHERE `post_id` = '$post_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            $data['posts_file_list'][$post_id] = $query_result;

            //post reply msg
            $sql_query = "SELECT `post_reply_id`,`user_id`,`reply_message`,`reply_like`,`timestamp` FROM `college_fixed_group_posts_reply` WHERE `college_id`='$college_id' AND `post_id` ='$post_id' ORDER BY `timestamp` ASC";
            $query_result = $this->data_fetch->data_query($sql_query);
            $data['posts_reply_list'][$post_id] = $query_result;

            //post reply like
            foreach ($data['posts_reply_list'][$post_id] as $inner_value) {
                $post_reply_id = $inner_value->post_reply_id;

                $sql_query = "SELECT `user_id` FROM `college_fixed_group_posts_reply_like_details` WHERE `post_id` = '$post_id' AND `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id'";
                $query_result = $this->data_fetch->data_query($sql_query);
                $data['posts_reply_like_all_user'][$post_id][$post_reply_id] = $query_result;

                //post like for the current user
                $sql_query = "SELECT `id` FROM `college_fixed_group_posts_reply_like_details` WHERE `post_id` = '$post_id' AND `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
                $query_result = $this->data_fetch->data_query($sql_query);

                if (count($query_result)) {
                    $data['posts_reply_like_current_user'][$post_id][$post_reply_id] = 1;
                } else {
                    $data['posts_reply_like_current_user'][$post_id][$post_reply_id] = 0;
                }
            }

            //post reply like
            $sql_query = "SELECT `post_reply_id`,`user_id` FROM `college_fixed_group_posts_reply_like_details` WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            foreach ($query_result as $value) {
                $data['posts_reply_like'][$post_id][$value->post_reply_id] = $value->user_id;
            }

            //post like for all the user
            $sql_query = "SELECT `id`,`user_id` FROM `college_fixed_group_posts_like_details` WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
            $query_result = $this->data_fetch->data_query($sql_query);
            $data['posts_like_all_user'][$post_id] = $query_result;

            //post like for the current user
            $sql_query = "SELECT `id` FROM `college_fixed_group_posts_like_details` WHERE `post_id` = '$post_id' AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
            $query_result = $this->data_fetch->data_query($sql_query);

            if (count($query_result)) {
                $data['posts_like_current_user'][$post_id] = 1;
            } else {
                $data['posts_like_current_user'][$post_id] = 0;
            }
        }

        return $data;
//        $this->load->view("front-end/jobopening", $data);
    }

    //fetch posts on scroll up
    public function get_post_on_scroll() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['target_flag'])) {
            $college_id = $this->college_id;
            $target_flag = 4 * $posted_data['target_flag'];

            //group name
            $post_group_name = $this->group_name;

            //current user
            $user_id = $this->user->user_id;

            $sql_query = "SELECT a.*,b.`post_message`,c.`first_name`,c.`last_name`,c.`profile_picture`,d.`file_path` FROM `college_fixed_group_posts` AS a LEFT JOIN `college_fixed_group_posts_details` as b ON a.`post_id` = b.`post_id` INNER JOIN `users` as c ON a.`user_id` = c.`id` LEFT JOIN `college_fixed_group_posts_files` as d ON a.`post_id` = d.`post_id` WHERE a.`college_id` = '$college_id' AND a.`post_group_name` = '$post_group_name' GROUP BY a.`post_id` ORDER BY a.`timestamp` DESC LIMIT " . $target_flag . ",2";
            //$sql_query = "SELECT a.*,b.`post_message`,c.`first_name`,c.`last_name`,c.`profile_picture` FROM `college_fixed_group_posts` AS a INNER JOIN `college_fixed_group_posts_details` as b ON a.`post_id` = b.`post_id` INNER JOIN `users` as c ON a.`user_id` = c.`id` WHERE a.`college_id` = '$college_id' AND a.`post_group_name` = 'jobopening' ORDER BY a.`timestamp` DESC LIMIT " . $target_flag . ",2";
            $query_result = $this->data_fetch->data_query($sql_query);
            $data['posts_list'] = $query_result;
            if (!count($data['posts_list'])) {
                echo 0;
            } else {
                //fetch POST reply and like list
                foreach ($data['posts_list'] as $value) {
                    $post_id = $value->post_id;
                    $sql_query = "SELECT `post_reply_id`,`user_id`,`reply_message`,`reply_like`,`timestamp` FROM `college_fixed_group_posts_reply` WHERE `college_id`='$college_id' AND `post_id` ='$post_id' ORDER BY `timestamp` ASC";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    $data['posts_reply_list'][$post_id] = $query_result;

                    //post reply like
                    foreach ($data['posts_reply_list'][$post_id] as $inner_value) {
                        $post_reply_id = $inner_value->post_reply_id;

                        $sql_query = "SELECT `user_id` FROM `college_fixed_group_posts_reply_like_details` WHERE `post_id` = '$post_id' AND `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id'";
                        $query_result = $this->data_fetch->data_query($sql_query);
                        $data['posts_reply_like_all_user'][$post_id][$post_reply_id] = $query_result;

                        //post like for the current user
                        $sql_query = "SELECT `id` FROM `college_fixed_group_posts_reply_like_details` WHERE `post_id` = '$post_id' AND `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
                        $query_result = $this->data_fetch->data_query($sql_query);

                        if (count($query_result)) {
                            $data['posts_reply_like_current_user'][$post_id][$post_reply_id] = 1;
                        } else {
                            $data['posts_reply_like_current_user'][$post_id][$post_reply_id] = 0;
                        }
                    }

                    //post reply like
                    $sql_query = "SELECT `post_reply_id`,`user_id` FROM `college_fixed_group_posts_reply_like_details` WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    foreach ($query_result as $value) {
                        $data['posts_reply_like'][$post_id][$value->post_reply_id] = $value->user_id;
                    }

                    //post like for all the user
                    $sql_query = "SELECT `id`,`user_id` FROM `college_fixed_group_posts_like_details` WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
                    $query_result = $this->data_fetch->data_query($sql_query);
                    $data['posts_like_all_user'][$post_id] = $query_result;

                    //post like for the current user
                    $sql_query = "SELECT `id` FROM `college_fixed_group_posts_like_details` WHERE `post_id` = '$post_id' AND `college_id` = '$college_id' AND `user_id` = '$user_id' LIMIT 1";
                    $query_result = $this->data_fetch->data_query($sql_query);

                    if (count($query_result)) {
                        $data['posts_like_current_user'][$post_id] = 1;
                    } else {
                        $data['posts_like_current_user'][$post_id] = 0;
                    }
                }

                foreach ($data['posts_list'] as $value) {
                    ?>
                    <div class="post_content_div">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="top_user_img">
                                    <img src="<?php echo base_url() . $value->profile_picture; ?>">
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="top_user_details">
                                    <h5><a href="#" class="top_name"><?php echo $value->first_name . " " . $value->last_name; ?></a> </h5>
                                    <div class="user_popup res_top_name">
                                        <span class="point_top"></span> 
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="intra_user_img" style="padding: 4px 9px;">
                                                    <img src="<?php echo base_url() . $value->profile_picture; ?>" alt="">
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="pop_user_info">
                                                    <h5><?php echo $value->first_name . " " . $value->last_name; ?></h5>
                                                    <h6>Web developer</h6>
                                                    <h6>Groups: Web development design</h6>
                                                    <h6>Email : <a href="mailto:haneef@hexwhale.com"> haneef@hexwhale.com</a></h6>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="pop_send">
                                                    <a href="#">Send Message</a>
                                                    <a href="#"> <span class="tick"></span> Following</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <h6>
                                        <span class="post_time" timestamp="<?php echo strtotime($value->timestamp); ?>">
                                            <?php
                                            $posted_date = date('M j Y', strtotime($value->timestamp));
                                            echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($value->timestamp));
                                            ?>
                                        </span>
                                    </h6>
                                    <p class="post_message_p">
                                        <?php
                                        if (str_word_count($value->post_message) >= 110) {
                                            ?>
                                            <span class="truncated_body">
                                                <?php
                                                echo preg_replace('/\s+?(\S+)?$/', '', substr($value->post_message, 0, 500)) . "&nbsp;<a class=\"expand_msg\"> Expand >></a>";
                                                ?>
                                            </span>
                                            <span class="complete_body">
                                                <?php
                                                echo $value->post_message . "&nbsp;<a class=\"collapse_msg\"> << Collapse</a>";
                                                ?>
                                            </span>
                                            <?php
                                        } else {
                                            echo $value->post_message;
                                        }
                                        ?>
                                    </p>
                                    <div class="post_action_div" post_id ="<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>">
                                        <a class="post_like" href="javascript:void(0);" like_count="<?php echo count($data['posts_like_all_user'][$value->post_id]); ?>" like_action ="<?php echo ($data['posts_like_current_user'][$value->post_id] == 1) ? "unlike" : "like"; ?>" title="Like"><?php echo ($data['posts_like_current_user'][$value->post_id] == 1) ? "Unlike" : "Like."; ?></a>
                                        <a href="#" title="Reply">Reply.</a>
                                        <a href="#" title="Share">Share.</a>
                                        <a href="javascript:void(0);" class="click_more" title="More">More</a>
                                        <div class="more_list">
                                            <ul>
                                                <li class="delete_post"> <a href="javascript:void(0);"> <h6> <span class="stop_icon"></span> Delete</h6> </a> </li>
                                                <li> <a href="#">  <h6> <span class="stop_icon"></span> Select a file on </h6> </a> </li>
                                                <li> <a href="#"> <h6> <span class="stop_icon"></span> Select a note on</h6> </a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="commenting_box">
                                        <div id="post_like_div-<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>" class="post_like_div" style="display: <?php echo (!empty($data['posts_like_all_user'][$value->post_id])) ? "block" : "none"; ?>">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="post_like_body-<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>" class="post_like_body">
                                                        <i class="fa fa-thumbs-up"></i>
                                                        <span class="current_user_like">
                                                            <?php
                                                            //current user like
                                                            if ($data['posts_like_current_user'][$value->post_id] == 1 && count($data['posts_like_all_user'][$value->post_id]) == 2) {
                                                                ?>
                                                                You and
                                                                <?php
                                                            } else if ($data['posts_like_current_user'][$value->post_id] == 1) {
                                                                ?>You
                                                                <?php
                                                            }
                                                            ?>
                                                        </span>
                                                        <?php
                                                        //other user like status
                                                        if (count($data['posts_like_all_user'][$value->post_id]) > 1) {
                                                            foreach ($data['posts_like_all_user'][$value->post_id] as $inner_value) {
                                                                ?>
                                                                <span><?php echo $inner_value->user_id; ?></span>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        like this.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="all_reply">
                                            <?php
                                            foreach ($data['posts_reply_list'][$value->post_id] as $post_reply_key => $post_reply_value) {
                                                $user_details = $this->ion_auth->user($post_reply_value->user_id)->row();
                                                ?>
                                                <div class="post_content_reply_div">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="top_user_img pull-right">
                                                                <img class="reply_user_img" src="<?php echo base_url() . $user_details->profile_picture; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-10">
                                                            <div class="top_user_details">
                                                                <h5><a href="#" class="top_name"><?php echo $user_details->first_name . " " . $user_details->last_name; ?></a> </h5>
                                                                <h6>
                                                                    <span class="post_time" timestamp="<?php echo strtotime($post_reply_value->timestamp); ?>">
                                                                        <?php
                                                                        $posted_date = date('M j Y', strtotime($post_reply_value->timestamp));
                                                                        echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A', strtotime($post_reply_value->timestamp));
                                                                        ?>
                                                                    </span>
                                                                </h6>
                                                                <p class="post_message_p">
                                                                    <?php
                                                                    if (str_word_count($post_reply_value->reply_message) >= 80) {
                                                                        ?>
                                                                        <span class="truncated_body">
                                                                            <?php
                                                                            echo preg_replace('/\s+?(\S+)?$/', '', substr($post_reply_value->reply_message, 0, 200)) . "&nbsp;<a class=\"expand_msg\"> Expand >></a>";
                                                                            ?>
                                                                        </span>
                                                                        <span class="complete_body">
                                                                            <?php
                                                                            echo $post_reply_value->reply_message . "&nbsp;<a class=\"collapse_msg\"> << Collapse</a>";
                                                                            ?>
                                                                        </span>
                                                                        <?php
                                                                    } else {
                                                                        echo $post_reply_value->reply_message;
                                                                    }
                                                                    ?>
                                                                </p>
                                                                <div post_id="<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>" post_reply_id ="<?php echo $this->encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>" class="post_action_div">
                                                                    <a class="post_reply_like" href="javascript:void(0);" like_count ="<?php echo count($data['posts_reply_like_all_user'][$value->post_id][$post_reply_value->post_reply_id]); ?>" like_action="<?php echo ($data['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1) ? "unlike" : "like"; ?>" title="Like"><?php echo ($data['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1) ? "Unlike." : "Like."; ?></a>
                                                                    <a href="#" title="Reply">Reply.</a>
                                                                    <a href="#" title="Share">Share.</a>
                                                                    <a href="javascript:void(0);" class="click_more" title="More">More</a>
                                                                    <div class="more_list">
                                                                        <ul>
                                                                            <li class="delete_post_reply"> <a href="javascript:void(0);"> <h6> <span class="stop_icon"></span> Delete</h6> </a> </li>
                                                                            <li> <a href="#">  <h6> <span class="stop_icon"></span> Select a file on </h6> </a> </li>
                                                                            <li> <a href="#"> <h6> <span class="stop_icon"></span> Select a note on</h6> </a> </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="post_reply_like_div-<?php echo $this->encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>" style="display: <?php echo (!empty($data['posts_reply_like_all_user'][$value->post_id][$post_reply_value->post_reply_id])) ? "block" : "none"; ?>" class="post_reply_like_div">
                                                        <div class="row">
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-10">
                                                                <div id="post_reply_like_body-<?php echo $this->encryption_decryption_object->encode($post_reply_value->post_reply_id); ?>" class="post_like_body">
                                                                    <i class="fa fa-thumbs-up"></i>
                                                                    <span class="current_user_post_reply_like">
                                                                        <?php
                                                                        //current user like
                                                                        if ($data['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1 && count($data['posts_reply_like_all_user'][$value->post_id][$post_reply_value->post_reply_id]) == 2) {
                                                                            ?>
                                                                            You and
                                                                            <?php
                                                                        } else if ($data['posts_reply_like_current_user'][$value->post_id][$post_reply_value->post_reply_id] == 1) {
                                                                            ?>You
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </span>
                                                                    like this.
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <!-- reply comment form  -->
                                        <div class="reply_box">
                                            <div class="announce_img">
                                                <img src="<?php echo base_url() . $value->profile_picture; ?>" alt="">
                                            </div>
                                            <div class="share_box2 comment_in1">
                                                <a href="javascript:void(0);" >Write a reply...</a>
                                                <span class="reply_pin"></span>
                                            </div>
                                            <div class="more_share">
                                                <form class="reply_form">
                                                    <a href="#"><?php echo $value->first_name . " " . $value->last_name; ?> is replying.</a>
                                                    <div class="text_poll2">
                                                        <input type="hidden" name="post_id" value="<?php echo $this->encryption_decryption_object->encode($value->post_id); ?>"/>
                                                        <textarea name="reply_message" class="share_text2 reply_textarea"></textarea>
                                                        <span class="pin2"></span> 
                                                        <input type="text" placeholder="Notify additional people" class="poll_text" name="note">
                                                    </div>
                                                    <button type="submit" class="btn reply_post_button">Post</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
        } else {
            echo 0;
        }
    }

    //FUNCTION IS TO SAVE THE UPDATE FILES
    public function save_update_post() {
        $posted_data = $this->input->post();

        if (!empty($posted_data) && (!empty($posted_data['update_message'])) || !empty($posted_data['uploaded_file'])) {
            $user = $this->user;
            $user_id = $user->user_id;

            //group name
            $post_group_name = $this->group_name;

            $sql_query = "SELECT `college_id` FROM `college_admin` WHERE `user_id` = '$user_id'";
            $query_result = $this->data_fetch->data_query($sql_query);

            $college_id = $query_result[0]->college_id;

            $update_message = mysql_real_escape_string($posted_data['update_message']);

            //Insert into jobopening group table
            $sql_query = "INSERT INTO `college_fixed_group_posts`(`post_group_name`,`user_id`,`college_id`) VALUES('$post_group_name','$user_id','$college_id')";
            $query_result = $this->data_insert->data_query($sql_query);

            if ($query_result) {
                $post_id = $this->db->insert_id();

                /* for post message saving */
                if ($update_message != '') {
                    $sql_query = "INSERT INTO `college_fixed_group_posts_details`(`post_message`,`post_id`) VALUE('$update_message','$post_id')";
                    $query_result = $this->data_insert->data_query($sql_query);
                }

                /* for post file upload saving */
                if (!empty($posted_data['uploaded_file'])) {
                    foreach ($posted_data['uploaded_file'] as $file_value) {
                        $sql_query = "INSERT INTO `college_fixed_group_posts_files` (`file_path`,`post_id`) VALUE('$file_value','$post_id')";
                        $query_result = $this->data_insert->data_query($sql_query);
                    }
                }
            }

            if ($query_result) {
                ?>
                <div class="post_content_div">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="top_user_img">
                                <img src="<?php echo base_url() . $user->profile_picture; ?>">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="top_user_details">
                                <h5> <a href="#" id="top_name"> <?php echo $user->first_name . " " . $user->last_name; ?> </a> </h5>
                                <div class="user_popup" id="res_top_name">
                                    <span class="point_top"></span> 
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="intra_user_img" style="padding: 4px 9px;">
                                                <img src="<?php echo base_url() . $user->profile_picture; ?>" alt="">
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="pop_user_info">
                                                <h5>Haneef</h5>
                                                <h6>Web developer</h6>
                                                <h6>Groups: Web development design</h6>
                                                <h6>Email : <a href="mailto:haneef@hexwhale.com"> haneef@hexwhale.com</a></h6>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="pop_send">
                                                <a href="#">Send Message</a>
                                                <a href="#"> <span class="tick"></span> Following</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h6>
                                    <span class="post_time" timestamp="">
                                        <?php
                                        $posted_date = date('M j Y');
                                        echo (($posted_date == date('M j Y')) ? "Today" : $posted_date) . " at " . date('h:i A');
                                        ?>
                                    </span>
                                </h6>
                                <p class="post_message_p">
                                    <?php
                                    if (str_word_count($posted_data['update_message']) >= 110) {
                                        ?>
                                        <span class="truncated_body">
                                            <?php
                                            echo preg_replace('/\s+?(\S+)?$/', '', substr($posted_data['update_message'], 0, 500)) . "&nbsp;<a style=\"cursor:pointer\" class=\"expand_msg\"> Expand >></a>";
                                            ?>
                                        </span>
                                        <span class="complete_body">
                                            <?php
                                            echo $posted_data['update_message'] . "&nbsp;<a style=\"cursor:pointer\" class=\"collapse_msg\"> << Collapse</a>";
                                            ?>
                                        </span>
                                        <?php
                                    } else {
                                        echo $posted_data['update_message'];
                                    }
                                    ?>
                                </p>
                                <?php
                                $file_count = (!empty($posted_data['uploaded_file'])) ? count($posted_data['uploaded_file']) : 0;
                                if ($file_count) {
                                    ?>
                                    <div class="post_file_div">
                                        <div class="row">
                                            <?php
                                            $count = 1;
                                            $inner_count = 2;
                                            foreach ($posted_data['uploaded_file'] as $file_value) {
                                                if ($count == 1) {
                                                    ?>
                                                    <div class="col-md-12">
                                                        <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo base_url() . $file_value; ?>">
                                                            <img style="max-width: 99%; min-width: auto; object-fit: cover; margin-top: 7px; margin-bottom: 2px;" src="<?php echo base_url() . $file_value; ?>"/>
                                                        </a>
                                                    </div>
                                                    <?php
                                                    $count++;
                                                } else {
                                                    if ($inner_count == 2) {
                                                        ?>
                                                        <div class="col-md-12">
                                                            <?php
                                                        }
                                                        ?>
                                                        <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo base_url() . $file_value; ?>">
                                                            <img style="margin-bottom: 2px; width: 32.5%; object-fit: cover;" src="<?php echo base_url() . $file_value; ?>"/>
                                                        </a>
                                                        <?php
                                                        if ($file_count == $inner_count) {
                                                            ?>
                                                        </div>
                                                        <?php
                                                    }
                                                    $inner_count++;
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="post_action_div" post_id ="<?php echo $this->encryption_decryption_object->encode($post_id); ?>">
                                    <a class="post_like" href="javascript:void(0);" like_count="0" like_action ="like" title="Like">Like.</a>
                                    <a href="#" title="Reply">Reply.</a>
                                    <a href="#" title="Share">Share.</a>
                                    <a href="javascript:void(0);" class="click_more" title="More">More</a>
                                    <div class="more_list">
                                        <ul>
                                            <li class="delete_post"> <a href="javascript:void(0);"> <h6> <span class="stop_icon"></span> Delete</h6> </a> </li>
                                            <li> <a href="#">  <h6> <span class="stop_icon"></span> Select a file on </h6> </a> </li>
                                            <li> <a href="#"> <h6> <span class="stop_icon"></span> Select a note on</h6> </a> </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="commenting_box">
                                    <div id="post_like_div-<?php echo $this->encryption_decryption_object->encode($post_id); ?>" class="post_like_div" style="display: none">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="post_like_body-<?php echo $this->encryption_decryption_object->encode($post_id); ?>" class="post_like_body">
                                                    <i class="fa fa-thumbs-up"></i>
                                                    <span class="current_user_like"></span>
                                                    like this.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="all_reply"></div>
                                    <div class="reply_box">
                                        <div class="announce_img">
                                            <img src="<?php echo base_url() . $user->profile_picture; ?>" alt="">
                                        </div>
                                        <div class="share_box2 comment_in1">
                                            <a href="javascript:void(0);" >Write a reply...</a>
                                            <span class="reply_pin"></span>
                                        </div>
                                        <div class="more_share">
                                            <form class="reply_form">
                                                <a href="#"><?php echo $user->first_name . " " . $user->first_name; ?> is replying.</a>
                                                <div class="text_poll2">  
                                                    <input type="hidden" name="post_id" value="<?php echo $post_id; ?>"/>
                                                    <textarea name="reply_message" class="share_text2 reply_textarea"></textarea>
                                                    <!--<span class="pin2"></span>--> 
                                                    <input type="text" placeholder="Notify additional people" class="poll_text" name="note">
                                                </div>
                                                <button type="submit" class="btn reply_post_button">Post</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                echo 0;
            }
        }
    }

    //function to save the post reply
    public function save_post_reply() {
        $posted_data = $this->input->post();

        if (!empty($posted_data) && !empty($posted_data['reply_message']) && $this->encryption_decryption_object->is_valid_input($posted_data['post_id'])) {
            $post_id = $this->encryption_decryption_object->is_valid_input($posted_data['post_id']);
            $reply_message = mysql_real_escape_string($posted_data['reply_message']);

            $user_id = $this->user->user_id;
            $college_id = $this->college_id;

            $sql_query = "INSERT INTO `college_fixed_group_posts_reply`(`post_id`,`user_id`,`college_id`,`reply_message`) VALUE ('$post_id','$user_id','$college_id','$reply_message')";
            $query_result = $this->data_insert->data_query($sql_query);

            if ($query_result) {
                $inserted_post_id = $this->db->insert_id();
                ?>
                <div class="post_content_reply_div">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="top_user_img pull-right">
                                <img class="reply_user_img" src="<?php echo base_url() . $this->user->profile_picture; ?>">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="top_user_details">
                                <h5><a href="#" class="top_name"><?php echo $this->user->first_name . " " . $this->user->last_name; ?></a> </h5>
                                <h6>
                                    <span class="post_time" timestamp="">
                                        <?php
                                        $posted_date = date('M j Y');
                                        echo (($posted_date == date('M j Y')) ? "Today" : date('h:i A')) . " at " . date('h:i A');
                                        ?>
                                    </span>
                                </h6>
                                <p class="post_message_p">
                                    <?php
                                    if (str_word_count($posted_data['reply_message']) >= 80) {
                                        ?>
                                        <span class="truncated_body">
                                            <?php
                                            echo preg_replace('/\s+?(\S+)?$/', '', substr($posted_data['reply_message'], 0, 200)) . "&nbsp;<a class=\"expand_msg\"> Expand >></a>";
                                            ?>
                                        </span>
                                        <span class="complete_body">
                                            <?php
                                            echo $posted_data['reply_message'] . "&nbsp;<a class=\"collapse_msg\"> << Collapse</a>";
                                            ?>
                                        </span>
                                        <?php
                                    } else {
                                        echo $posted_data['reply_message'];
                                    }
                                    ?>
                                </p>
                                <div post_id="<?php echo $this->encryption_decryption_object->encode($post_id); ?>" post_reply_id ="<?php echo $this->encryption_decryption_object->encode($inserted_post_id); ?>" class="post_action_div">
                                    <!--<div post_reply_id ="<?php echo $inserted_post_id; ?>" class="post_action_div">-->
                                    <a class="post_reply_like" href="javascript:void(0);" like_count ="0" like_action="like" title="Like">Like.</a>
                                    <a href="#" title="Reply">Reply.</a>
                                    <a href="#" title="Share">Share.</a>
                                    <a href="javascript:void(0);" class="click_more" title="More">More</a>
                                    <div class="more_list">
                                        <ul>
                                            <li class="delete_post_reply"> <a href="javascript:void(0);"> <h6> <span class="stop_icon"></span> Delete</h6> </a> </li>
                                            <li> <a href="#">  <h6> <span class="stop_icon"></span> Select a file on </h6> </a> </li>
                                            <li> <a href="#"> <h6> <span class="stop_icon"></span> Select a note on</h6> </a> </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="post_reply_like_div-<?php echo $this->encryption_decryption_object->encode($inserted_post_id); ?>" style="display: none" class="post_reply_like_div">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <div id="post_reply_like_body-<?php echo $this->encryption_decryption_object->encode($inserted_post_id); ?>" class="post_like_body">
                                    <i class="fa fa-thumbs-up"></i>
                                    <span class="current_user_post_reply_like"></span>
                                    like this.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                echo 0;
            }
        }
    }

    public function delete_post() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['post_id']) && $this->encryption_decryption_object->is_valid_input($posted_data['post_id'])) {
            $post_id = $this->encryption_decryption_object->is_valid_input($posted_data['post_id']);
            $user_id = $this->user->user_id;
            $college_id = $this->college_id;

            //delete query for post and post reply
            $sql_query = "DELETE FROM `college_fixed_group_posts` WHERE `post_id` = '$post_id' AND `college_id` = '$college_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            //delete from the post details table
            $sql_query = "DELETE FROM `college_fixed_group_posts_reply` WHERE `post_id` = '$post_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            //delete from the post likes table
            $sql_query = "DELETE FROM `college_fixed_group_posts_like_details` WHERE `post_id` = '$post_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            if ($query_result) {
                //select files
                $sql_query = "SELECT * FROM `college_fixed_group_posts_files` WHERE `post_id` = '$post_id'";
                $query_result = $this->data_fetch->data_query($sql_query);

                if (count($query_result)) {
                    foreach ($query_result as $files_value) {
                        $file_path = $files_value->file_path;
                        unlink($file_path);
                    }

                    $sql_query = "DELETE FROM `college_fixed_group_posts_files` WHERE `post_id` ='$post_id'";
                    $query_result = $this->data_delete->data_query($sql_query);
                }

                //delete all the post reply from post reply table
                $sql_query = "DELETE FROM `college_fixed_group_posts_reply` WHERE `post_id` ='$post_id'";
                $query_result = $this->data_delete->data_query($sql_query);

                //delete all the post reply likes from post reply table
                $sql_query = "DELETE FROM `college_fixed_group_posts_reply_like_details` WHERE `post_id` ='$post_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }
            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    //delete post reply message
    public function delete_post_reply() {
        $posted_data = $this->input->post();

        if (!empty($posted_data) && !empty($posted_data['post_reply_id']) && $this->encryption_decryption_object->is_valid_input('post_reply_id')) {
            $post_reply_id = $this->encryption_decryption_object->is_valid_input($posted_data['post_reply_id']);
            $user_id = $this->user->user_id;
            $college_id = $this->college_id;

            //delete query for post reply
            $sql_query = "DELETE FROM `college_fixed_group_posts_reply` WHERE `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            //delete all the likes from post reply like table
            $sql_query = "DELETE FROM `college_fixed_group_posts_reply_like_details` WHERE `post_reply_id` = '$post_reply_id'";
            $query_result = $this->data_delete->data_query($sql_query);

            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    //like the post
    public function like_post() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['post_id']) && $this->encryption_decryption_object->is_valid_input($posted_data['post_id']) && !empty($posted_data['like_action'])) {
            $post_id = $this->encryption_decryption_object->is_valid_input($posted_data['post_id']);
            $like_action = $posted_data['like_action'];

            //other details
            $user_id = $this->user->user_id;
            $college_id = $this->college_id;

            //like database operation
            if ($like_action == 'like') {
                // if action is "like"
                $sql_query = "INSERT INTO `college_fixed_group_posts_like_details`(`post_id`,`college_id`,`user_id`) VALUES('$post_id','$college_id','$user_id')";
                $query_result = $this->data_insert->data_query($sql_query);
            } else {
                // if action is "unlike"
                $sql_query = "DELETE FROM `college_fixed_group_posts_like_details` WHERE `post_id` = '$post_id' AND `user_id` = '$user_id' AND `college_id` = '$college_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }

            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    //post reply like action function
    public function like_post_reply() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['post_reply_id']) && !empty($posted_data['post_id']) && $this->encryption_decryption_object->is_valid_input('post_id') && $this->encryption_decryption_object->is_valid_input('post_reply_id')) {
            $post_id = $this->encryption_decryption_object->is_valid_input($posted_data['post_id']);
            $post_reply_id = $this->encryption_decryption_object->is_valid_input($posted_data['post_reply_id']);
            $like_action = $posted_data['like_action'];

            //other details
            $user_id = $this->user->user_id;
            $college_id = $this->college_id;

            //like database operation
            if ($like_action == 'like') {
                $sql_query = "INSERT INTO `college_fixed_group_posts_reply_like_details`(`post_reply_id`,`user_id`,`college_id`,`post_id`) VALUE('$post_reply_id','$user_id','$college_id','$post_id')";
                $query_result = $this->data_insert->data_query($sql_query);
            } else {
                $sql_query = "DELETE FROM `college_fixed_group_posts_reply_like_details` WHERE `post_reply_id` = '$post_reply_id' AND `college_id` = '$college_id' AND `user_id` = '$user_id' AND `post_id` = '$post_id'";
                $query_result = $this->data_delete->data_query($sql_query);
            }

            if ($query_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    //upload the image
    public function image_upload_form() {
        //attachment file
        if ($_FILES['upload_post_file']['name']) {
            $target = "assets_front/image/group_post_files/";
            $target.=date("dmYhsi") . "-" . basename($_FILES['upload_post_file']['name']);

            if (move_uploaded_file($_FILES['upload_post_file']['tmp_name'], $target)) {
                echo $target;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    //remove uploaded file
    public function remove_file_upload() {
        $posted_data = $this->input->post();
        if (!empty($posted_data) && !empty($posted_data['file_path'])) {
            $file_path = $posted_data['file_path'];
            echo unlink($file_path);
        }
    }

}
?>
