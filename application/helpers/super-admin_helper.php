<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('test_method')) {

    function college_registration_request() {
        $ci = & get_instance();
        $ci->load->model('data_fetch');
        $sql_query = "SELECT `id` FROM `contemporary_college_registration` WHERE `is_checked` = '0'";
        $query_result = $ci->data_fetch->data_query($sql_query);
        return count($query_result);
    }

}