<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('test_method')) {

    function college_membership_request() {
        $ci = & get_instance();
        $ci->load->model('data_fetch');

        $sql_query = "SELECT `id` FROM `college_user_request_to_admin`";
        $query_result = $ci->data_fetch->data_query($sql_query);

        return count($query_result);
    }

    function intranet_default_group() {
        $ci = & get_instance();
        $ci->load->model('data_fetch');

        $sql_query = "SELECT * FROM `intranet_default_groups`";
        $query_result = $ci->data_fetch->data_query($sql_query);

        return $query_result;
    }

}