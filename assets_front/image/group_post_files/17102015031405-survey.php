<?php
require 'header.php';
//require_once 'classes/base.php';
//
//$base = new base();
if (isset($_GET['user']) && isset($_GET['survey']) && isset($_GET['id'])) {

    /* Get variables from url , decode and sanitize */
    $survey = $base->sanitizeint(base64_decode($_GET['survey']));
    $user = $base->sanitize(base64_decode($_GET['user']));
    $id = $base->sanitizeint(base64_decode($_GET['id']));

    /* Get survey record from survey table */
    $msql = "SELECT * FROM  `survey` WHERE id=" . $survey;
    $mres = $base->execute_query($msql);
    $srow = mysql_fetch_array($mres);

    $dd = strtotime($srow['surveyend']); // Survey End Date
    $td = strtotime(date('Y-m-d')); // Todays Date
    $comp = unserialize($srow['competency']); // Store competency list to $comp 
    $total = count($comp); // total no of competencies

    $c = 0;

    $list = join(',', $comp);  // Comma seperated competency list to $list 

    if ($user == "part") { // if user is participate
        // Get participates record
        $psql = "SELECT * FROM  `participate` WHERE id=" . $id;
        $pres = $base->execute_query($psql);
        $prow = mysql_fetch_array($pres);
        $asesseename = $prow['name'];
        $ratername = $prow['name'];
    }

    if ($user == "riwter") { // if user is rater
        // Get raters record
        $psql = "SELECT * FROM  `raters` WHERE id=" . $id;
        $pres = $base->execute_query($psql);
        $prow = mysql_fetch_array($pres);
        $ratername = $prow['name'];

        // Get participates record
        $apsql = "SELECT * FROM  `participate` WHERE id=" . $prow['participate'];
        $apres = $base->execute_query($apsql);
        $aprow = mysql_fetch_array($apres);
        $asesseename = $aprow['name'];
    }

    // Get competency records
    $sql = "SELECT * FROM  `competency` WHERE  id in (" . $list . ") ORDER BY FIELD(id, " . $list . " )";
    $res4comp = $base->execute_query($sql);
}


// if save button is clicked
if (isset($_POST['save'])) {
    //Get competency records
    $sql = "SELECT * FROM  `competency` WHERE  id in (" . $list . ") ORDER BY FIELD(id, " . $list . " )";
    $res4comp = $base->execute_query($sql);

    while ($rwcomp = mysql_fetch_array($res4comp)) { // for each competency record
        // Get behavior records
        $sql4beh = "SELECT * FROM `behavior` WHERE `cid`=" . $rwcomp['id'] . " ORDER BY `order`";
        $res4 = $base->execute_query($sql4beh);

        $i = 0;
        $markval = 0;
        $num_q = 0;
        $resultval = 0;

        while ($behrow = mysql_fetch_array($res4)) { // for each behaviors
            // Get survey results stored
            $sql3 = "SELECT * FROM survey_result_full WHERE beh_id='" . $behrow['id'] . "' AND survey_id='$survey' AND rater_id='$id' AND rater_type='$user'";
            $resq = $base->execute_query($sql3);
            $ciq = mysql_num_rows($resq);
            $rsval = mysql_fetch_array($resq);

            $i++;
            $mark = "mark" . $behrow['id']; // ID of the behaviour radio button


            if (isset($_POST[$mark])) {

                if ($_POST[$mark] != 0) {
                    $num_q++;
                    $markval+=$_POST[$mark];
                }

                if ($ciq <= 0) {
                    $insq = "INSERT INTO `survey_result_full`(`survey_id`, `comp_id`, `rater_id`, `rater_type`, `result`,`beh_id`) VALUES ('$survey','" . $rwcomp['id'] . "','$id','$user','$_POST[$mark]','$behrow[id]')";
                    $base->execute_query($insq);
                } else {
                    $upsql = "UPDATE survey_result_full SET `result`='$_POST[$mark]' WHERE beh_id='$behrow[id]' AND rater_type='$user' AND rater_id='$id'";
                    $base->execute_query($upsql);
                }
            }
        }

        if ($markval != 0 && $num_q != 0) {
            $resultval = $markval / $num_q;
        }

        $insq = "INSERT INTO `survey_result`(`survey_id`, `comp_id`, `rater_id`, `rater_type`, `result`) VALUES ('$survey','" . $rwcomp['id'] . "','$id','$user','$resultval')";
        $base->execute_query($insq);

        if (isset($_GET['user'])) {

            $compId = 'importence' . $rwcomp['id'];
            $sqlo = "SELECT * FROM importence WHERE rater_id='" . base64_decode($_GET['id']) . "' AND comp_id='" . $rwcomp['id'] . "' AND `type`='" . base64_decode($_GET['user']) . "'";
            $re = $base->execute_query($sqlo);
            $ciq1 = mysql_num_rows($re);
            if ($ciq1 <= 0) {
                $sqli = "INSERT INTO `importence`(`survey_id`, `rater_id`, `comp_id`, `importence`,`type`) VALUES ('$survey','" . base64_decode($_GET['id']) . "','" . $rwcomp['id'] . "','" . $_POST[$compId] . "','" . base64_decode($_GET['user']) . "')";
            } else {
                $sqli = "UPDATE importence SET importence='" . $_POST[$compId] . "' WHERE rater_id='" . base64_decode($_GET['id']) . "' AND comp_id='" . $rwcomp['id'] . "' AND `type`='" . base64_decode($_GET['user']) . "'";
            }
            $base->execute_query($sqli);
        }

        //  coments  
        $comment = "comment" . $rwcomp['id'];

        $sql3 = "SELECT * FROM survey_comments WHERE beh_id='" . $rwcomp['id'] . "' AND survey_id='$survey' AND rater_id='" . base64_decode($_GET['id']) . "' AND rater_type='" . base64_decode($_GET['user']) . "'";
        $resq = $base->execute_query($sql3);
        $ciq2 = mysql_num_rows($resq);

        if ($ciq2 <= 0) {
            $insq1 = "INSERT INTO `survey_comments`(`survey_id`, `comp_id`, `rater_id`, `rater_type`, `result`,`beh_id`,`comments`) VALUES ('$survey','" . $rwcomp['id'] . "','" . base64_decode($_GET['id']) . "','" . base64_decode($_GET['user']) . "','$_POST[$mark]','" . $rwcomp['id'] . "','$_POST[$comment]')";
            // echo $insq1;
            $base->execute_query($insq1);
        } else {
            $upsql = "UPDATE survey_comments SET `result`='$_POST[$mark]',comments='$_POST[$comment]' WHERE beh_id='" . $rwcomp['id'] . "' AND rater_type='" . base64_decode($_GET['user']) . "' AND rater_id='" . base64_decode($_GET['id']) . "'";
            $base->execute_query($upsql);
        }


        //  strengths  
        $strength = "strength" . $rwcomp['id'];

        $sql3 = "SELECT * FROM survey_strengths WHERE beh_id='" . $rwcomp['id'] . "' AND survey_id='$survey' AND rater_id='" . base64_decode($_GET['id']) . "' AND rater_type='" . base64_decode($_GET['user']) . "'";
        $resq = $base->execute_query($sql3);
        $ciq2 = mysql_num_rows($resq);

        if ($ciq2 <= 0) {
            $insq1 = "INSERT INTO `survey_strengths`(`survey_id`, `comp_id`, `rater_id`, `rater_type`, `result`,`beh_id`,`comments`) VALUES ('$survey','" . $rwcomp['id'] . "','" . base64_decode($_GET['id']) . "','" . base64_decode($_GET['user']) . "','$_POST[$mark]','" . $rwcomp['id'] . "','$_POST[$strength]')";
            // echo $insq1;
            $base->execute_query($insq1);
        } else {
            $upsql = "UPDATE survey_strengths SET `result`='$_POST[$mark]',comments='$_POST[$strength]' WHERE beh_id='" . $rwcomp['id'] . "' AND rater_type='" . base64_decode($_GET['user']) . "' AND rater_id='" . base64_decode($_GET['id']) . "'";
            $base->execute_query($upsql);
        }
    }


    header("location:save.php?survey=$survey&user=$user&id=$id");
}

if (isset($_POST['finish'])) {
    $sql = "SELECT * FROM  `competency` WHERE  id in (" . $list . ") ORDER BY FIELD(id, " . $list . " )";
    //echo $sql;
    $res4comp = $base->execute_query($sql);
    while ($rwcomp = mysql_fetch_array($res4comp)) {
        $sql4beh = "SELECT * FROM `behavior` WHERE `cid`=" . $rwcomp['id'] . " ORDER BY `order`";
        $res4 = $base->execute_query($sql4beh);
        $i = 0;
        $markval = 0;
        $num_q = 0;

        $resultval = 0;
        while ($behrow = mysql_fetch_array($res4)) {
            $sql3 = "SELECT * FROM survey_result_full WHERE beh_id='" . $behrow['id'] . "' AND survey_id='$survey' AND rater_id='" . base64_decode($_GET['id']) . "' AND rater_type='" . base64_decode($_GET['user']) . "'";
            $resq = $base->execute_query($sql3);
            $ciq = mysql_num_rows($resq);
            $rsval = mysql_fetch_array($resq);
            $i++;
            $mark = "mark" . $behrow['id'];

            //echo $mark;
            if (isset($_POST[$mark])) {
                if ($_POST[$mark] != 0) {
                    $num_q++;
                    $markval+=$_POST[$mark];
                }
                if ($ciq <= 0) {
                    $insq = "INSERT INTO `survey_result_full`(`survey_id`, `comp_id`, `rater_id`, `rater_type`, `result`,`beh_id`) VALUES ('$survey','" . $rwcomp['id'] . "','" . base64_decode($_GET['id']) . "','" . base64_decode($_GET['user']) . "','$_POST[$mark]','$behrow[id]')";
                    //   echo $insq;
                    $base->execute_query($insq);
                } else {
                    $upsql = "UPDATE survey_result_full SET `result`='$_POST[$mark]' WHERE beh_id='$behrow[id]' AND rater_type='" . base64_decode($_GET['user']) . "' AND rater_id='" . base64_decode($_GET['id']) . "'";
                    $base->execute_query($upsql);
                }
            }
        }
        if ($markval != 0 && $num_q != 0) {
            $resultval = $markval / $num_q;
        }


        $insq = "INSERT INTO `survey_result`(`survey_id`, `comp_id`, `rater_id`, `rater_type`, `result`) VALUES ('$survey','" . $rwcomp['id'] . "','" . base64_decode($_GET['id']) . "','" . base64_decode($_GET['user']) . "','$resultval')";
        //  echo $insq;
        $base->execute_query($insq);


        if (isset($_GET['user'])) {

            $compId = 'importence' . $rwcomp['id'];
            //echo $compId."<br/>";
            $sqlo = "SELECT * FROM importence WHERE rater_id='" . base64_decode($_GET['id']) . "' AND comp_id='" . $rwcomp['id'] . "'  AND `type`='" . base64_decode($_GET['user']) . "'";
            $re = $base->execute_query($sqlo);
            $ciq1 = mysql_num_rows($re);
            if ($ciq1 <= 0) {
                $sqli = "INSERT INTO `importence`(`survey_id`, `rater_id`, `comp_id`, `importence`,`type`) VALUES ('$survey','" . base64_decode($_GET['id']) . "','" . $rwcomp['id'] . "','" . $_POST[$compId] . "','" . base64_decode($_GET['user']) . "')";
            } else {
                $sqli = "UPDATE importence SET importence='" . $_POST[$compId] . "' WHERE rater_id='" . base64_decode($_GET['id']) . "' AND comp_id='" . $rwcomp['id'] . "'  AND `type`='" . base64_decode($_GET['user']) . "'";
            }

            $base->execute_query($sqli);
        }
        ///////////   coments   /////////
        $comment = "comment" . $rwcomp['id'];

        $sql3 = "SELECT * FROM survey_comments WHERE beh_id='" . $rwcomp['id'] . "' AND survey_id='$survey' AND rater_id='" . base64_decode($_GET['id']) . "' AND rater_type='" . base64_decode($_GET['user']) . "'";
        $resq = $base->execute_query($sql3);
        $ciq2 = mysql_num_rows($resq);

        if ($ciq2 <= 0) {
            $insq1 = "INSERT INTO `survey_comments`(`survey_id`, `comp_id`, `rater_id`, `rater_type`, `result`,`beh_id`,`comments`) VALUES ('$survey','" . $rwcomp['id'] . "','" . base64_decode($_GET['id']) . "','" . base64_decode($_GET['user']) . "','$_POST[$mark]','" . $rwcomp['id'] . "','$_POST[$comment]')";
            // echo $insq1;
            $base->execute_query($insq1);
        } else {
            $upsql = "UPDATE survey_comments SET `result`='$_POST[$mark]',comments='$_POST[$comment]' WHERE beh_id='" . $rwcomp['id'] . "' AND rater_type='" . base64_decode($_GET['user']) . "' AND rater_id='" . base64_decode($_GET['id']) . "'";
            $base->execute_query($upsql);
        }
        ///////////   coments   /////////
        $strength = "strength" . $rwcomp['id'];

        $sql3 = "SELECT * FROM survey_strengths WHERE beh_id='" . $rwcomp['id'] . "' AND survey_id='$survey' AND rater_id='" . base64_decode($_GET['id']) . "' AND rater_type='" . base64_decode($_GET['user']) . "'";
        $resq = $base->execute_query($sql3);
        $ciq2 = mysql_num_rows($resq);

        if ($ciq2 <= 0) {
            $insq1 = "INSERT INTO `survey_strengths`(`survey_id`, `comp_id`, `rater_id`, `rater_type`, `result`,`beh_id`,`comments`) VALUES ('$survey','" . $rwcomp['id'] . "','" . base64_decode($_GET['id']) . "','" . base64_decode($_GET['user']) . "','$_POST[$mark]','" . $rwcomp['id'] . "','$_POST[$strength]')";
            // echo $insq1;
            $base->execute_query($insq1);
        } else {
            $upsql = "UPDATE survey_strengths SET `result`='$_POST[$mark]',comments='$_POST[$strength]' WHERE beh_id='" . $rwcomp['id'] . "' AND rater_type='" . base64_decode($_GET['user']) . "' AND rater_id='" . base64_decode($_GET['id']) . "'";
            $base->execute_query($upsql);
        }

        ////////
    }
    $sqlqq = "SELECT * FROM question ";
    $resqq = $base->execute_query($sqlqq);
    $k = 0;
    while ($rowq = mysql_fetch_array($resqq)) {
        $ansq = "quest" . $rowq['id'];
        $insqlqq = "INSERT INTO answers (q_id,survey,`type`,rater_id,answer) VALUES ('" . $rowq['id'] . "','$survey','$user','" . base64_decode($_GET['id']) . "','" . $_POST[$ansq] . "')";
        $base->execute_query($insqlqq);
    }
    header("location:back.php?survey=$survey&user=$user&id=$id");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
    <meta style="visibility: hidden !important; display: block !important; width: 0px !important; height: 0px !important; border-style: none !important;"></meta><head>
        <meta charset="utf-8">
            <title>360 Degree : Survey</title>


            <!-- Le styles -->
            <link href="./css/bootstrap_1.css" rel="stylesheet" />
            <style type="text/css">

                body {
                    padding-bottom: 0px;
                    background-color: #A5A5A5;
                }
                .sidebar-nav {
                    padding: 9px 0;
                }
                .input-xlarge
                {

                }

                .container {
                    background-color: #F7F7F7;
                    padding: 20px;
                    padding-left: 25px;

                }

                .container table { border:1px solid #D8E4AB;}
                .container tr { border-bottom:1px solid #D8E4AB;}
                .container th { background-color:#D8E4AB;}

                .table th, .table td {border: 0;}

                .progress-bar {
                    float: left;
                    width: 0;
                    height: 100%;
                    font-size: 12px;
                    color: #ffffff;
                    text-align: center;
                    background-color: #428bca;
                    -webkit-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);
                    box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);
                    -webkit-transition: width 0.6s ease;
                    transition: width 0.6s ease;
                }

                #focus11 {
                    background-color: #ECECEC;
                    padding: 2% 0px;
                    margin-left: 0%;
                    border-radius: 10px;
                }

                #headimg {
                    width:100%;
                    height: 121px;
                    background-image: url('images/header.jpg');
                    background-size: 100%;
                }

                .lead { color: #533B3B;font-size: 14px;}
                .span2 { display: inline-block;width: auto;margin-left: 0;margin-right: 12px;}
            </style>
            <script src="http://code.jquery.com/jquery-latest.js"></script>
            <script src="js/bootstrap.min.js"></script>

            <script src="js/bootstrap-prompts-alert.js"></script>
            <!--<script src="js/jquery-2.0.3.min.js" type="text/javascript"></script>-->
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#wrap1').show();
                    $('.next').click(function(e) {
                        e.preventDefault();
                        var f = 1;

                        var commentempty = 0;
                        var form2 = $(this).data("id");
                        var ccccid = form2.split("/");
                        var form = parseInt(ccccid[0]);
                        var id = '#wrap' + form;
                        var compid = parseInt(ccccid[1]);
                        var comments = 0;
                        var comments = $.trim($(':input[name="comment' + compid + '"]').val());
                        if (comments.length == 0) {
                            commentempty = 1;
                        }


                        $(':input[name="comment' + compid + '"]').click(function(e) {
                            $(':input[name="comment' + compid + '"]').css('background', 'white');
                        })


                        var strengthempty = 0;
                        var strengths = 0;
                        strengths = $.trim($(':input[name="strength' + compid + '"]').val());//alert(" str "+strengths.length+" aod "+comments.length);
                        if (strengths.length == 0) {
                            strengthempty = 1;
                        }


                        $(':input[name="strength' + compid + '"]').click(function(e) {
                            $(':input[name="strength' + compid + '"]').css('background', 'white');
                        })




                        var n = $('.ttr' + form).size();
                        //alert(n);

                        for (i = 1; i <= n; i++)
                        {

                            var cl = 'radion' + form + "" + i;
                            //alert(cl);
                            if (!$('input.' + cl + ':checked').val()) {
                                f = 2;
                                $('.' + cl).parents('.chhhh').css('background-color', '#FBB');
                                alert('ha');
                                break;

                            }
                        }
                        var k = $('select.imp' + form + " option:selected").val();
                        var k = $('select.per' + form + " option:selected").val();

                        if (f == 2)
                        {
                            alert('Please Select an option ');
                            $("html, body").animate({scrollTop: 0}, 500);
                            return false;

                        }
                        else if (strengthempty == 1)
                        {
                            alert('Please enter your comments on strenghts');
                            $(':input[name="strength' + compid + '"]').css('background', '#FBB');
                        }
                        else if (commentempty == 1)
                        {
                            alert('Please enter your comments on areas of development');
                            $(':input[name="comment' + compid + '"]').css('background', '#FBB');
                        }

                        else
                        {
                            var k = $('select.imp' + form + " option:selected").val();


                            if (k != "!")
                            {
                                var next_form = form + 1;
                                $('.hide').fadeOut();
                                $('#wrap' + next_form).fadeIn();
                                $("html, body").animate({scrollTop: 0}, 500);
                            }
                            else
                            {
                                alert('Please select  the option for importance of the competency');
                                var n = $(document).height();
                                $('html, body').animate({scrollTop: n}, '50');
                            }
                        }
                    });
                    $('.chet').click(function(e) {

                        $(this).parents('.chhhh').css('background-color', 'transparent');

                    });
                    $('.pre').click(function(e) {
                        e.preventDefault();
                        var form = $(this).data("id");
                        var next_form = form;
                        $('.hide').fadeOut();
                        $('#wrap' + next_form).fadeIn();
                        $("html, body").animate({scrollTop: 0}, 500);
                    });


                });
            </script>
    </head>
    <body screen_capture_injected="true">

        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="span3"><a class="brand" href="#"> 360&#176; Feedback </a></div>
                    <div class="btn-group pull-right">

  <!-- <li><a href="#"><i class="icon-edit"></i> Settings</a></li> 
   <li class="divider"></li>-->
                        <a href="index.php?status=loggedout" class="btn"><i class="icon-off "></i> LOG OUT</a>

                    </div>
                </div>
            </div>
        </div>
        <br/><br/>
        <!-- Part 1: Wrap all page content here -->
        <?php if (isset($_GET['user']) && isset($_GET['survey']) && isset($_GET['id']) && $prow['status'] != 2 && $td <= $dd) {
            ?>
            <form action="survey.php?<?php echo "survey=" . base64_encode($survey) . "&user=" . base64_encode($user) . "&id=" . base64_encode($id); ?>" method="POST" name="survey">
                <?php
                $j = 0;
                while ($corow = mysql_fetch_array($res4comp)) {
                    $j++;
                    $c++;
                    ?>
                    <div id="wrap<?php echo $j; ?>" class="hide">

                        <!-- Begin page content -->
                        <div class="container">
                            <div id="headimg"></div>
                            <div class="page-header">
                                <table style="width:100%;"  cellpadding="10">
                                    <tr>
                                        <th align="left">Survey name</th>
                                        <th align="left">Assessee</th>
                                        <th align="left">Rater</th>
                                    </tr>
                                    <tr>
                                        <td><?php echo $srow['surveyname']; ?></td>
                                        <td><?php echo $asesseename; ?></td>
                                        <td><?php echo $ratername ?></td>
                                    </tr>
                                </table>


                                <h2 style="color: #2C2C2C;font-size: 22px;line-height: 0px;padding-top: 30px;"> <?php echo ucfirst($corow['name']); ?></h2>
                                <div class="span11" style=" margin-top: 20px;
                                     margin-left: 0;"><?php echo $corow['definition']; ?></div>
                            </div>

                        </div>

                        <div class="container">

                            <table cellpadding="10" style="width:100%">

                                <tr>
                                    <th align="left">Sl No.</th>             
                                    <th align="left">Statements</th>
                                     <!--<th align="left" style="width:620px;">Rating scale</th>-->
                                </tr>

                                <?php if ($prow['status'] != 2) {
                                    ?>


                                    <?php
                                    $sql4beh = "SELECT * FROM `behavior` WHERE `cid`=" . $corow['id'] . " ORDER BY `order`";
                                    $res4 = $base->execute_query($sql4beh);
                                    $i = 0;
                                    while ($behrow = mysql_fetch_array($res4)) {
                                        $i++;
                                        $sql3 = "SELECT * FROM survey_result_full WHERE beh_id='" . $behrow['id'] . "' AND survey_id='$survey' AND rater_id='" . base64_decode($_GET['id']) . "' AND rater_type='" . base64_decode($_GET['user']) . "'";
                                        $resq = $base->execute_query($sql3);
                                        $ciq = mysql_num_rows($resq);
                                        $rsval = mysql_fetch_array($resq);
                                        ?>
                                        <tr  style="border-bottom:2px solid #BDCB87;" class=" ttr<?php echo $j ?> chhhh" >
                                            <td style="border-right: 1px solid #D8E4AB; vertical-align: top;padding-top: 16px;"><?php echo $i . "."; ?> </td>
                                            <td>

                                                <p class="lead">  <?php echo $behrow['behavior']; ?> </p>
                                                <hr style="border: 1px solid #D8E4AB;" />
                                                <div >
                                                    <div class="span2" ><label class="radio" ><input class="radion<?php echo $j . $i; ?> chet" type="radio" name="mark<?php echo $behrow['id']; ?>" value="0" <?php
                                                            if ($ciq == 1) {
                                                                if ($rsval['result'] == 0) {
                                                                    echo 'checked="checked"';
                                                                }
                                                            }
                                                            ?>/><small > Not Applicable </small></label></div>

                                                    <div class="span2" ><label class="radio" ><input class="radion<?php echo $j . $i; ?> chet" type="radio" name="mark<?php echo $behrow['id']; ?>" value="1" <?php
                                                            if ($ciq == 1) {
                                                                if ($rsval['result'] == 1) {
                                                                    echo 'checked="checked"';
                                                                }
                                                            }
                                                            ?>/><small > Strongly Disagree (1) </small></label></div>
                                                    <div class="span2" ><label class="radio" ><input class="radion<?php echo $j . $i; ?> chet" type="radio" name="mark<?php echo $behrow['id']; ?>" value="2" <?php
                                                            if ($ciq == 1) {
                                                                if ($rsval['result'] == 2) {
                                                                    echo 'checked="checked"';
                                                                }
                                                            }
                                                            ?> /><small > Disagree (2) </small></label></div>
                                                    <div class="span2" ><label class="radio" ><input class="radion<?php echo $j . $i; ?> chet" type="radio" name="mark<?php echo $behrow['id']; ?>" value="3" <?php
                                                            if ($ciq == 1) {
                                                                if ($rsval['result'] == 3) {
                                                                    echo 'checked="checked"';
                                                                }
                                                            }
                                                            ?>/><small > Agree (3) </small></label></div>
                                                    <div class="span2" ><label class="radio" ><input class="radion<?php echo $j . $i; ?> chet" type="radio" name="mark<?php echo $behrow['id']; ?>" value="4" <?php
                                                            if ($ciq == 1) {
                                                                if ($rsval['result'] == 4) {
                                                                    echo 'checked="checked"';
                                                                }
                                                            }
                                                            ?> /><small > Strongly Agree (4) </small></label></div>
                                                    <!-- <div class="span2" ><label class="radio" ><input class="radion<?php echo $j . $i; ?> chet" type="radio" name="mark<?php echo $behrow['id']; ?>" value="5" <?php
                                                    if ($ciq == 1) {
                                                        if ($rsval['result'] == 4) {
                                                            echo 'checked="checked"';
                                                        }
                                                    }
                                                    ?> /><small > Strongly  Agree (5) </small></label></div> -->
                                    <!--                 <div class="span2" ><label class="radio" ><input class="radion<?php echo $j . $i; ?> chet" type="radio" name="mark<?php echo $behrow['id']; ?>" value="5" <?php
                                                    if ($ciq == 1) {
                                                        if ($rsval['result'] == 5) {
                                                            echo 'checked="checked"';
                                                        }
                                                    }
                                                    ?> /><small > Strongly Agree (5 ) </small></label></div>
                                                    --><!--     
                                                                   <div class="span2" ><label class="radio" ><input class="radion<?php echo $j . $i; ?> chet" type="radio" name="mark<?php echo $behrow['id']; ?>" value="0" <?php
                                                    if ($ciq == 1) {
                                                        if ($rsval['result'] == 0) {
                                                            echo 'checked="checked"';
                                                        }
                                                    }
                                                    ?>/><small > Could not Observe (0) </small></label></div>
                                                                   <div class="span2" ><label class="radio" ><input class="radion<?php echo $j . $i; ?> chet" type="radio" name="mark<?php echo $behrow['id']; ?>" value="1" <?php
                                                    if ($ciq == 1) {
                                                        if ($rsval['result'] == 1) {
                                                            echo 'checked="checked"';
                                                        }
                                                    }
                                                    ?> /><small > Strongly Disagree (1) </small></label></div>
                                                                   <div class="span2" ><label class="radio" ><input class="radion<?php echo $j . $i; ?> chet" type="radio" name="mark<?php echo $behrow['id']; ?>" value="2" <?php
                                                    if ($ciq == 1) {
                                                        if ($rsval['result'] == 2) {
                                                            echo 'checked="checked"';
                                                        }
                                                    }
                                                    ?>/><small > Disagree (2) </small></label></div>
                                                                   <div class="span2" ><label class="radio" ><input class="radion<?php echo $j . $i; ?> chet" type="radio" name="mark<?php echo $behrow['id']; ?>" value="3" <?php
                                                    if ($ciq == 1) {
                                                        if ($rsval['result'] == 3) {
                                                            echo 'checked="checked"';
                                                        }
                                                    }
                                                    ?> /><small > Neutral (3) </small></label></div>
                                                                   <div class="span2" ><label class="radio" ><input class="radion<?php echo $j . $i; ?> chet" type="radio" name="mark<?php echo $behrow['id']; ?>" value="4" <?php
                                                    if ($ciq == 1) {
                                                        if ($rsval['result'] == 4) {
                                                            echo 'checked="checked"';
                                                        }
                                                    }
                                                    ?> /><small > Agree (4) </small></label></div>
                                                                   <div class="span2" ><label class="radio" ><input class="radion<?php echo $j . $i; ?> chet" type="radio" name="mark<?php echo $behrow['id']; ?>" value="5" <?php
                                                    if ($ciq == 1) {
                                                        if ($rsval['result'] == 5) {
                                                            echo 'checked="checked"';
                                                        }
                                                    }
                                                    ?> /><small > Strongly Agree (5 ) </small></label></div>
                                                    -->
                                                </div>
                                            </td>

                                                                                                                                <!--                                            <td>
                                                                                                                                                                
                                                                                                                                                            </td>-->

                                        </tr>
                                        <?php
                                    }
                                    ?>

                                </table>

                                <div class="row"> &nbsp;<br/></div>
                                <div class="row">

                                    <div class="span8" >
                                        <?php
                                        $sql3 = "SELECT * FROM survey_strengths WHERE beh_id='" . $corow['id'] . "' AND survey_id='$survey' AND rater_id='" . base64_decode($_GET['id']) . "' AND rater_type='" . base64_decode($_GET['user']) . "'";
                                        $resq = $base->execute_query($sql3);
                                        $ciq = mysql_num_rows($resq);
                                        $rsval = mysql_fetch_array($resq);
                                        ?>

                                        <textarea name="strength<?php echo $corow['id']; ?>" class="span12" style="padding:10px;width:916px;" placeholder="Comment on Strengths"><?php
                                            if ($ciq == 1) {
                                                echo $rsval['comments'];
                                            }
                                            ?></textarea>
                                    </div>
                                </div>       
                                <div class="row">

                                    <div class="span8" >
                                        <?php
                                        $sql3 = "SELECT * FROM survey_comments WHERE beh_id='" . $corow['id'] . "' AND survey_id='$survey' AND rater_id='" . base64_decode($_GET['id']) . "' AND rater_type='" . base64_decode($_GET['user']) . "'";
                                        $resq = $base->execute_query($sql3);
                                        $ciq = mysql_num_rows($resq);
                                        $rsval = mysql_fetch_array($resq);
                                        ?>

                                        <textarea name="comment<?php echo $corow['id']; ?>" class="span12" style="padding:10px;width:916px;" placeholder="Comment on Areas of Development"><?php
                                            if ($ciq == 1) {
                                                echo $rsval['comments'];
                                            }
                                            ?></textarea>
                                    </div>
                                </div>
                                <hr />
                                <!--<?php
                                if ($user == "part") {
                                    ?>
                                                                                                                      <div class="row " id="focus1<?php echo $j ?>"><div class="span8" > <strong>How important is this specific competency  for you ?</strong>
                                                                                                                       <br/>
                                                                                                                       <small style="font-size: 12px;font-weight: bold;color: #718F00;">Rate the competency  on a scale of 1-5  where 1 is LOW  and  5 is HIGH </small></div> 
                                                                                                                       <div class="span3" >
                                    <?php
                                    $sqlo = "SELECT * FROM importence WHERE rater_id='" . base64_decode($_GET['id']) . "' AND comp_id='" . $corow['id'] . "'";
                                    $re = $base->execute_query($sqlo);
                                    $ciq1 = mysql_num_rows($re);
                                    $imp = mysql_fetch_array($re)
                                    ?>
                                                                                                                        <select name="importence<?php echo $corow['id'] ?>" class="imp<?php echo $j ?>">
                                                                                                                          <option value="!">Select</option>  
                                                                                                                          <option value="1" <?php
                                    if ($ciq1 == 1) {
                                        if ($imp['importence'] == 1) {
                                            echo 'selected="selected"';
                                        }
                                    }
                                    ?> >1</option>
                                                                                                                          <option value="2" <?php
                                    if ($ciq1 == 1) {
                                        if ($imp['importence'] == 2) {
                                            echo 'selected="selected"';
                                        }
                                    }
                                    ?> >2</option>
                                                                                                                          <option value="3" <?php
                                    if ($ciq1 == 1) {
                                        if ($imp['importence'] == 3) {
                                            echo 'selected="selected"';
                                        }
                                    }
                                    ?> >3</option>
                                                                                                                          <option value="4" <?php
                                    if ($ciq1 == 1) {
                                        if ($imp['importence'] == 4) {
                                            echo 'selected="selected"';
                                        }
                                    }
                                    ?> >4</option>
                                                                                                                          <option value="5" <?php
                                    if ($ciq1 == 1) {
                                        if ($imp['importence'] == 5) {
                                            echo 'selected="selected"';
                                        }
                                    }
                                    ?> >5</option>
                                                                                                        
                                                                                                                        </select>
                                                                                                        
                                                                                                                      </div></div>
                                <?php } ?> -->


                                <hr />


                                <div class="row" style="position:relative;" >
                                    <div class="span12" style="position:fixed;bottom:0;padding:15px 23px;background-color:#732A73;margin-left: -5px;" >
                                        <div class="progress pull-left" style="width: 70%">

                                            <div class="progress-bar" style="background-color:#C1D441;width: <?php echo $j / ($total + 1) * 100; ?>%"></div>
                                        </div>
                                        <div class="pull-right">

                                            <input type="hidden" name="compid" id="competency_id" value="<?php echo $corow['id'] ?>"/>
                                            <input type="hidden" name="compcount" value="<?php echo $c; ?>">
                                                <?php if ($j != 1) { ?>
                                                    <button class="btn pre btn-info" type="button" name="button"  style="background:#C1D441;color:#333;" data-id="<?php echo $j - 1; ?>">
                                                        <i class="icon-arrow-left "></i> Previous

                                                    </button>
                                                <?php } ?>
                                                <?php if ($j <= $total + 1) { ?>
                                                    <button class="btn next btn-info" type="button" style="background:#C1D441;color:#333;" name="button" data-id="<?php echo $j; ?>/<?php echo $corow['id']; ?>">Next <i class="icon-arrow-right " ></i></button>
                                                <?php } ?>
                                                    <?php if ($c <= $total - 1) { ?> <button class="btn save btn-success" style="background:#C1D441;color:#333;"  type="submit" name="save" data-id="<?php echo $j; ?>">Save</button><?php } ?>

                                        </div> </div> </div> </div> <?php } ?>

                    </div>



                    </div>
                <?php }
                ?>
                <div id="wrap<?php echo $j + 1; ?>" class="hide ">
                    <div class="container">
                        <div class="page-header">
                            <h2 style="color:#732A73;">Significant Remarks</h2>
                        </div>

                    </div>

                    <div class="container">
                        <?php
                        $sqlqq = "SELECT * FROM question ";
                        $resqq = $base->execute_query($sqlqq);
                        $k = 0;
                        while ($rowq = mysql_fetch_array($resqq)) {
                            $k++;
                            ?>
                            <div class="row">
                                <div class="span12"><p class="lead"> <?php echo $k; ?> .  <?php echo $rowq['question']; ?>              </p></div>

                            </div>
                            <div class="row">

                                <div class="span8" >

                                    <textarea name="quest<?php echo $rowq['id']; ?>" class="span12" placeholder="Enter your Answer , if any"></textarea>
                                </div>
                            </div> <hr />
                        <?php } ?>
                        <div class="row" style="position:relative;" >

                            <div class="span12" style="position:fixed;bottom:0;padding:15px 23px;background-color:#732A73;margin-left: -5px;" >

                                <div class="progress pull-left" style="width: 80%">
                                    <div class="progress-bar" style="background-color:#C1D441;width: 100%;"></div>
                                </div>

                                <div class="pull-right">

                                    <input type="hidden" name="compid" value="<?php echo $corow['id'] ?>"/>
                                    <input type="hidden" name="compcount" value="<?php echo $c; ?>">
                                        <?php if ($j != 1) { ?>
                                            <button class="btn pre btn-info" type="button"  style="background:#C1D441;color:#333;" name="button" data-id="<?php echo $j; ?>">
                                                <i class="icon-arrow-left "></i> Previous

                                            </button>
                                        <?php } ?>
                                        <?php if ($j < $total) { ?>
                                            <button class="btn next btn-info" type="button"  style="background:#C1D441;color:#333;" name="button" data-id="<?php echo $j; ?>">Next <i class="icon-arrow-right icon-white" ></i></button>
                                        <?php } else { ?>
                                            <button class="btn" type="submit" name="finish"  style="background:#C1D441;color:#333;" data-id="<?php echo $j; ?>">Finish</button>
                                        <?php } ?>


                                </div> </div> </div>
                    </div>

                </div>


            </form>

        <?php } else {
            ?>
            <div class="container" style="margin-top:10%">
                <div class="page-header" style="border: 0px;">
                    <h2>This survey has already been taken.</h2> 
                </div>
            </div> 

        <?php }
        ?> 
        <div id="footer">

        </div>

        <script type="text/javascript">
            $(document).ready(function() {
                $(".span12").keyup(function() {
                    var yourInput = $(this).val();
                    re = /[`~!@#$%^&*()_|+\-=?;:'"<>\{\}\[\]\\\/]/gi;
                    var isSplChar = re.test(yourInput);
                    if (isSplChar)
                    {
                        var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'"<>\{\}\[\]\\\/]/gi, '');
                        $(this).val(no_spl_char);
                        alert("You can't use following special characters : /[`~!@#$%^&*()_|+\-=?;:'\"<>\{\}\[\]\\\/]");
                    }
                });
            });
        </script>

    </body>
</html>