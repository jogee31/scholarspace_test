-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 21, 2015 at 04:21 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `scholerspace`
--

-- --------------------------------------------------------

--
-- Table structure for table `college`
--

CREATE TABLE IF NOT EXISTS `college` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active_status` int(11) NOT NULL DEFAULT '1',
  `college_name` varchar(80) NOT NULL,
  `institute_type` int(11) NOT NULL,
  `naac_accredition` varchar(100) NOT NULL,
  `address_1` text NOT NULL,
  `address_2` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(20) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `email` varchar(10) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `fax` varchar(10) NOT NULL,
  `industry_interaction` text NOT NULL,
  `fee` text NOT NULL,
  `placement_details` text NOT NULL,
  `quality_of_students_and_crowd` text NOT NULL,
  `international_exposure` text NOT NULL,
  `international_or_india_tie_ups` text NOT NULL,
  `hostel_facility` text NOT NULL,
  `aicte_approved` text NOT NULL,
  `scholarships` text NOT NULL,
  `facility_to_stay_near_by` text NOT NULL,
  `sports_infrastructure` text NOT NULL,
  `library` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1819 ;

--
-- Dumping data for table `college`
--

INSERT INTO `college` (`id`, `active_status`, `college_name`, `institute_type`, `naac_accredition`, `address_1`, `address_2`, `city`, `state`, `zipcode`, `email`, `phone`, `fax`, `industry_interaction`, `fee`, `placement_details`, `quality_of_students_and_crowd`, `international_exposure`, `international_or_india_tie_ups`, `hostel_facility`, `aicte_approved`, `scholarships`, `facility_to_stay_near_by`, `sports_infrastructure`, `library`) VALUES
(1, 1, 'Indian Institute of Technology', 1, '', '', '', 'Kharagpur', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2, 1, 'Jadavpur University', 1, '', '', '', 'Kolkata', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, 1, 'Indian Institute of Engineering Science and Techno', 1, '', '', '', 'Shibpur', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, 1, 'National Institute of Technology', 1, '', '', '', 'Durgapur', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(5, 1, 'Indian Institute of Science Education and Research', 1, '', '', '', 'Kolkata', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6, 1, 'Bidhan Chandra Krishi Viswa Vidyalaya', 1, '', '', '', 'Nadia', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7, 1, 'Kalyani Government Engineering College', 1, '', '', '', 'Nadia', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8, 1, 'Institute of Engineering and Management', 1, '', '', '', 'Kolkata', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9, 1, 'Heritage Institute of Technology', 1, '', '', '', 'Kolkata', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10, 1, 'University of Kalyani', 1, '', '', '', 'Kalyani', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11, 1, 'University Institute of Technology', 1, '', '', '', 'Burdwan', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12, 1, 'Techno India University', 1, '', '', '', 'Kolkata', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13, 1, 'National Power Training Institute', 1, '', '', '', 'Durgapur', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14, 1, 'Jalpaiguri Government Engineering College', 1, '', '', '', 'Jalpaiguri', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15, 1, 'Haldia Institute of Technology', 1, '', '', '', 'Haldia', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(16, 1, 'Government College of Engineering Textile Technolo', 1, '', '', '', 'Serampore', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(17, 1, 'Government College of Engineering and Textile Tech', 1, '', '', '', 'Berhampore', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18, 1, 'Government College of Engineering and Leather Tech', 1, '', '', '', 'Kolkata', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(19, 1, 'Government College of Engineering and Ceramic Tech', 1, '', '', '', 'Kolkata', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(20, 1, 'Uttar Banga Krishi Vishwavidyalaya', 1, '', '', '', 'Pundibari', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(21, 1, 'RCC Institute of Information Technology', 1, '', '', '', 'Beliaghata', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(22, 1, 'Netaji Subhash Engineering College', 1, '', '', '', 'Garia', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(23, 1, 'Meghnad Saha Institute of Technology', 1, '', '', '', 'South24Parganas', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(24, 1, 'MCKV Institute of Engineering', 1, '', '', '', 'Howrah', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(25, 1, 'Future Institute of Engineering and Management', 1, '', '', '', 'Kolkata', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(26, 1, 'BP Poddar Institute of Management and Technology ', 1, '', '', '', 'Kolkata', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(27, 1, 'Bengal College of Engineering and Technology', 1, '', '', '', 'Durgapur', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(28, 1, 'Aliah University - Faculty of Engineering', 1, '', '', '', 'Kolkata', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(29, 1, 'BCDA College of Pharmacy and Technology', 1, '', '', '', 'Hridaypur', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(30, 1, 'Bengal College of Engineering and Technology for W', 1, '', '', '', 'Durgpur', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(31, 1, 'Bengal College of Pharmaceutical Science and Resea', 1, '', '', '', 'Kolkata', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(32, 1, 'Bengal School of Technology', 1, '', '', '', 'Hooghly', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(33, 1, 'Bharat Technology', 1, '', '', '', 'Howrah', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(34, 1, 'Bipradas Pal Chowdhury Institute of Technology', 1, '', '', '', 'Nadia', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(35, 1, 'Calcutta Institute of Pharmaceutical Technology an', 1, '', '', '', 'Howrah', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(36, 1, 'Dr. BC Roy College of Pharmacy and Allied Health S', 1, '', '', '', 'Durgapur', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(37, 1, 'Elite Polytechnic Institute', 1, '', '', '', 'Hooghly', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(38, 1, 'Gupta College of Technological Sciences', 1, '', '', '', 'Asansol', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(39, 1, 'Guru Nanak Institute of Pharmaceutical Science and', 1, '', '', '', 'North24Paraganas', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(40, 1, 'Hoogly Institute of Technology', 1, '', '', '', 'Hoogly', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(41, 1, 'Institute of Pharmacy', 1, '', '', '', 'Jalpaiguri', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(42, 1, 'Marine Engineering and Research Institute', 1, '', '', '', 'Kolkata', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(43, 1, 'Netaji Subhas Chandra Bose Institute of Pharmacy', 1, '', '', '', 'Nadia', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(44, 1, 'Nopany Institute of Management Studies', 1, '', '', '', 'Kolkata', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(45, 1, 'NSHM College of Pharmaceutical Technology', 1, '', '', '', 'Kolkata', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(46, 1, 'Rani Rashmoni School of Architecture', 1, '', '', '', 'Durgapur', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(47, 1, 'Regional Engineering College', 1, '', '', '', 'Durgapur', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(48, 1, 'Siliguri Institute of Technology', 1, '', '', '', 'Darjeeling', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(49, 1, 'Techno India College of Technology', 1, '', '', '', 'North24Parganas', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(50, 1, 'Techno India Hooghly', 1, '', '', '', '', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(51, 1, 'The Institutions of Engineers India', 1, '', '', '', 'Kolakta', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(52, 1, 'The New Horizons Institute of Technology', 1, '', '', '', 'Durgapur', 'West Bangal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(54, 1, 'University College of Science, Technology and Agri', 1, '', '', '', 'Kolkata', 'West Bengal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(190, 1, 'Indian Institute of Technology', 1, '', '', '', 'Patna', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(191, 1, 'National Institute of Technology', 1, '', '', '', 'Patna', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(192, 1, 'Bhagalpur College of Engineering', 1, '', '', '', 'Bhagalpur', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(193, 1, 'Rajendra Agricultural University', 1, '', '', '', 'Samastipur', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(194, 1, 'Muzaffarpur Institute of Technology', 1, '', '', '', 'Muzaffarpur', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(195, 1, 'Loknayak Jai Prakash Institute of Technology', 1, '', '', '', 'Chhapra', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(196, 1, 'Gaya College of Engineering', 1, '', '', '', 'Gaya', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(197, 1, 'Darbhanga College of Engineering', 1, '', '', '', 'Darbhanga', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(198, 1, 'Birla Institute of Technology', 1, '', '', '', 'Patna', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(199, 1, 'Amrapali Institute of Technology', 1, '', '', '', 'Lakhisarai', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(200, 1, 'Aryabhatta Knowledge University', 1, '', '', '', 'Patna', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(201, 1, 'Azmet College of Engineering and Technology', 1, '', '', '', 'Kishanganj', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(202, 1, 'Bhupendra Narayan Mandal University', 1, '', '', '', 'Madhepura', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(203, 1, 'Bihar Agricultural University', 1, '', '', '', 'Bhagalpur', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(204, 1, 'Dr. Jagganath Mishra Institute of Technology', 1, '', '', '', 'Darbhanga', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(205, 1, 'Kishanganj College of Engineering and Technology', 1, '', '', '', 'KishanganjÂ ', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(206, 1, 'KK College of Engineering and Management', 1, '', '', '', 'Nalanda', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(207, 1, 'Maulana Azad College of Engineering and Technology', 1, '', '', '', 'Patna', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(208, 1, 'Millia Institute of Technology', 1, '', '', '', 'Purnea', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(209, 1, 'Motihari College of Engineering', 1, '', '', '', 'Motihari', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(210, 1, 'Nalanda College of Engineering', 1, '', '', '', 'Chandi', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(211, 1, 'Netaji Subhas Institute of Technology', 1, '', '', '', 'Patna', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(212, 1, 'Patna Sahib Technical Campus', 1, '', '', '', 'Vaishali', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(213, 1, 'RP Sharma Institute of Technology', 1, '', '', '', 'Patna', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(214, 1, 'Sityog Institute of Technology', 1, '', '', '', 'Aurangabad', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(215, 1, 'Siwan Engineering and Technical Institute', 1, '', '', '', 'Siwan', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(216, 1, 'Vidya Vihar Institute of Technology', 1, '', '', '', 'Purnea', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(217, 1, 'Vidyadaan Institute of Technology & Management', 1, '', '', '', 'Buxar', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(218, 1, 'Womenâ€™s Institute of Technology', 1, '', '', '', 'Darbhanga', 'Bihar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(219, 1, 'Indian Institute of Technology', 1, '', '', '', 'Indore', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(220, 1, 'Maulana Azad National Institute of Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(221, 1, 'Indian Institute of Science Education and Research', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(222, 1, 'ABV- Indian Institute of Information Technology an', 1, '', '', '', 'Gwalior', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(223, 1, 'Shri Vaishnav Institute of Technology and Science', 1, '', '', '', 'Indore', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(224, 1, 'Shri GS Institute of Technology and Science', 1, '', '', '', 'Indore', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(225, 1, 'Medi-Caps Institute of Technology and Management', 1, '', '', '', 'Indore', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(226, 1, 'Madhav Institute of Technology and Science', 1, '', '', '', 'Gwalior', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(227, 1, 'Jiwaji University', 1, '', '', '', 'Gwalior', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(228, 1, 'Jabalpur Engineering College', 1, '', '', '', 'Jabalpur', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(229, 1, 'Institute of Engineering and Technology', 1, '', '', '', 'Indore', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(230, 1, 'Institute of Engineering and Science', 1, '', '', '', 'IPSAcademy', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(231, 1, 'Indian Institute of Information Technology', 1, '', '', '', 'DesignandManufacturing', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(232, 1, 'ITM University', 1, '', '', '', 'Gwalior', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(233, 1, 'Vikram University', 1, '', '', '', 'Ujjain', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(234, 1, 'Ujjain Engineering College', 1, '', '', '', 'Ujjain', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(235, 1, 'Technocrats Institute of Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(236, 1, 'Shiv Kumar Singh Institute of Technology and Scien', 1, '', '', '', 'Indore', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(237, 1, 'Samrat Ashok Technological Institute', 1, '', '', '', 'Vidisha', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(238, 1, 'Sagar Institute of Research and Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(239, 1, 'Rustamji Institute of Technology', 1, '', '', '', 'Gwalior', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(240, 1, 'Rajiv Gandhi Proudyogiki Vishwavidyalaya', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(241, 1, 'Oriental Institute of Science and Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(242, 1, 'Oriental College of Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(243, 1, 'Nowgong Engineering College', 1, '', '', '', 'Chhatarpur', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(244, 1, 'Nagaji Institute of Technology and Management', 1, '', '', '', 'Datia', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(245, 1, 'Mittal Institute of Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(246, 1, 'Lakshmi Narain College of Technology', 1, 'ABCD', 'Anand Nagar', 'asdasdas', 'Bhopal', 'Madhya Pradesh', '462022', 'lnct@lnct.', '9739930818', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(247, 1, 'Kailash Narayan Patidar College of Science and Tec', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(248, 1, 'IES College of Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(249, 1, 'Gyan Ganga Institute of Technology and Sciences', 1, '', '', '', 'Jabalpur', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(250, 1, 'Global Engineering and Management College', 1, '', '', '', 'Jabalpur', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(251, 1, 'Chameli Devi School of Engineering', 1, '', '', '', 'Indore', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(252, 1, 'Bhopal Institute of Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(253, 1, 'Barkatullah University Institute of Technology', 1, '', '', '', 'BarkatullahUniversity', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(254, 1, 'Bansal Institute of Science and Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(255, 1, 'AISECT University', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(256, 1, 'Acropolis Institute of Technology and Research', 1, '', '', '', 'Indore', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(257, 1, 'TRUBA Institute of Engineering and Information Tec', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(258, 1, 'Sushila Devi Bansal College of Technology', 1, '', '', '', 'Indore', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(259, 1, 'Sagar Institute of Science Technology and Engineer', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(260, 1, 'RKDF College of Engineering', 1, '', '', '', 'Jatkhedi', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(261, 1, 'Rewa Institute of Technology', 1, '', '', '', 'Rewa', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(262, 1, 'Rewa Engineering College', 1, '', '', '', 'Rewa', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(263, 1, 'Radharaman Institute of Technology and Science', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(264, 1, 'Patel College of Science and Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(265, 1, 'Nalin Institute of Technology', 1, '', '', '', 'Indore', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(266, 1, 'Maxim Institute of Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(267, 1, 'Indore Institute of Science and Technology', 1, '', '', '', 'Indore', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(268, 1, 'Admissions Open Now', 1, '', '', '', '', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(269, 1, 'Admission open for Batch 2015 at Sharda University', 1, '', '', '', '', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(270, 1, 'LIST OF COLLEGES IN MADHYA PRADESH WHICH WE HAVE N', 1, '', '', '', '', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(271, 1, 'AISECT Institute of Science and Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(272, 1, 'AKS University- Faculty of Engineering and Technol', 1, '', '', '', 'Satna', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(273, 1, 'Amity University', 1, '', '', '', 'Gwalior', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(274, 1, 'Amity University- Department of Architecture Gwali', 1, '', '', '', '', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(275, 1, 'Bansal Institute of Research', 1, '', '', '', 'TechnologyandScience', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(276, 1, 'Barkatullah University', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(277, 1, 'Bethesda Institute of Technology and Science', 1, '', '', '', 'Gwalior', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(278, 1, 'Bhopal Institute of Technology and Management', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(279, 1, 'Crescent College of Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(280, 1, 'Educational Multimedia Research Centre', 1, '', '', '', 'DAVV', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(281, 1, 'Girdhar Shiksha Evam Samaj Kalyan Samiti Group of ', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(282, 1, 'Global Engineering College', 1, '', '', '', 'Jabalpur', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(283, 1, 'Government Polytechnic', 1, '', '', '', 'Rewa', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(284, 1, 'Hindustan Institute of Technology Science and Mana', 1, '', '', '', 'Gwalior', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(285, 1, 'HL Agrawal College of Engineering', 1, '', '', '', 'Itarsi', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(286, 1, 'Indira Gandhi Government Engineering College', 1, '', '', '', 'Sagar', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(287, 1, 'Jawaharlal Nehru Krishi Vishwavidyalaya', 1, '', '', '', 'Jabalpur', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(288, 1, 'Laxmipati Institute of Science and Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(289, 1, 'Maa Kaila Devi Institute of Information Technology', 1, '', '', '', 'Gwalior', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(290, 1, 'New-Tech Institute of Engineering and Technology', 1, '', '', '', 'Dewas', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(291, 1, 'Peopleâ€™s College of Research and Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(292, 1, 'Prakash Institute of Engineering and Technology', 1, '', '', '', 'Jabalpur', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(293, 1, 'Radharaman Institute of Research and Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(294, 1, 'RKDF College of Technology and Research', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(295, 1, 'RKDF College of Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(296, 1, 'Sakshi Institute of Technology and Management', 1, '', '', '', 'Guna', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(297, 1, 'Satyam Education and Social Welfare society Group ', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(298, 1, 'School of Planning and Architecture', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(299, 1, 'Sha-Shib College of Technology', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(300, 1, 'Shri Govindram Seksaria Institute of Technology an', 1, '', '', '', 'Indore', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(301, 1, 'Sri Parashuram Institute of Technology and Researc', 1, '', '', '', 'Khandwa', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(302, 1, 'Star Academy of Technology and Management', 1, '', '', '', 'Indore', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(303, 1, 'Surabhi Group of Institutions- Faculty of Engineer', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(304, 1, 'Swami Vivekanand University', 1, '', '', '', 'Sagar', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(305, 1, 'Tapasya Shiksha Samiti', 1, '', '', '', 'Bhopal', 'Madhya Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(393, 1, 'National Institute of Technology', 1, '', '', '', 'Raipur', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(394, 1, 'Guru Ghasidas Vishwavidyalaya', 1, '', '', '', 'Bilaspur', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(395, 1, 'Bhilai Institute of Technology', 1, '', '', '', 'Durg', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(396, 1, 'Shri Shankaracharya Technical Campus', 1, '', '', '', 'Bhilai', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(397, 1, 'Rungta College of Engineering and Technology', 1, '', '', '', 'Bhilai', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(398, 1, 'OP Jindal University', 1, '', '', '', 'Raigarh', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(399, 1, 'Government Engineering College', 1, '', '', '', 'Raipur', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(400, 1, 'Government Engineering College', 1, '', '', '', 'Bilaspur', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(401, 1, 'Government Engineering College', 1, '', '', '', 'Bastar', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(402, 1, 'Bhilai Institute of Technology', 1, '', '', '', 'Raipur', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(403, 1, 'Raipur Institute of Technology', 1, '', '', '', 'Raipur', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(404, 1, 'Chhatrapati Shivaji Institute of Technology', 1, '', '', '', 'Durg', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(405, 1, 'Bharti College of Agricultural Engineering', 1, '', '', '', 'Durg', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(406, 1, 'Chhattisgarh Swami Vivekanand Technical University', 1, '', '', '', 'Bhilai', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(407, 1, 'Indira Gandhi Krishi Vishwavidyalaya', 1, '', '', '', 'Raipur', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(408, 1, 'International Institute of Information Technology', 1, '', '', '', 'NayaRaipur', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(409, 1, 'ITM University', 1, '', '', '', 'Raipur', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(410, 1, 'Kalinga University', 1, '', '', '', 'Raipur', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(411, 1, 'Krishna Engineering College', 1, '', '', '', 'Bhilai', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(412, 1, 'Ravi Shankar Institute of Technology and Managemen', 1, '', '', '', 'Raipur', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(413, 1, 'RSR Rungta College of Engineering & Technology', 1, '', '', '', 'Raipur', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(414, 1, 'Sarguja University', 1, '', '', '', 'Sarguja', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(415, 1, 'Shri Rawatpura Sarkar Institute of Technology -2 ', 1, '', '', '', 'Raipur', 'Chhattisgarh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(416, 1, 'National Institute of Technology', 1, '', '', '', 'Warangal', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(417, 1, 'Indian Institute of Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(418, 1, 'University College of Engineering', 1, '', '', '', 'OsmaniaUniversity', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(419, 1, 'JNTUH College of Engineering', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(420, 1, 'International Institute of Information Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(421, 1, 'BITS Pilani- Hyderabad Campus', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(422, 1, 'VNR Vignana Jyothi Institute of Engineering and Te', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(423, 1, 'Vasavi College of Engineering', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(424, 1, 'Vardhaman College of Engineering', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(425, 1, 'Sreenidhi Institute of Science and Technology', 1, '', '', '', 'Ghatkesar', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(426, 1, 'Maturi Venkata Subba Rao Engineering College', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(427, 1, 'Kakatiya Institute of Technology and Science', 1, '', '', '', 'Warangal', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(428, 1, 'JNTUH College of Engineering', 1, '', '', '', 'Karimnagar', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(429, 1, 'Gokaraju Rangaraju Institute of Engineering and Te', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(430, 1, 'CVR College of Engineering', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(431, 1, 'Chaitanya Bharathi Institute of Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(432, 1, 'University College of Technology', 1, '', '', '', 'OsmaniaUniversity', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(433, 1, 'Malla Reddy College of Engineering and Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(434, 1, 'Mahatma Gandhi Institute of Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(435, 1, 'KU College of Engineering and Technology', 1, '', '', '', 'Warangal', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(436, 1, 'Keshav Memorial Institute of Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(437, 1, 'JNTU College of Engineering', 1, '', '', '', 'Sultanpur', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(438, 1, 'JNTU College of Engineering', 1, '', '', '', 'Manthani', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(439, 1, 'Institute of Aeronautical Engineering', 1, '', '', '', 'Dundigal', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(440, 1, 'ICFAI Foundation for Higher Education', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(441, 1, 'Guru Nanak Institute of Technology', 1, '', '', '', 'Secunderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(442, 1, 'G Narayanamma Institute of Technology and Science ', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(443, 1, 'CMR College of Engineering and Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(444, 1, 'BVRIT Padmasri Dr BV Raju Institute of Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(445, 1, 'Vidya Jyothi Institute of Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(446, 1, 'JB Institute of Engineering and Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(447, 1, 'Guru Nanak Institutions Technical Campus', 1, '', '', '', 'Secunderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(448, 1, 'Adusumalli Vijaya College of Engineering and Resea', 1, '', '', '', 'Nalgonda', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(449, 1, 'Adusumilli Vijay Institute of Technology and Resea', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(450, 1, 'Aizza College of Engineering and Technology', 1, '', '', '', 'Adilabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(451, 1, 'Al-Habeeb College of Engineering and Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(452, 1, 'Anwarul-Uloom College of Engineering and Technolog', 1, '', '', '', 'Vikarabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(453, 1, 'Arkay College of Engineering and Technology', 1, '', '', '', 'Nizamabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(454, 1, 'Asifia College of Engineering and Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(455, 1, 'Avanthi Scientific Technological and Research Acad', 1, '', '', '', 'Rangareddy', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(456, 1, 'Ayaan College of Engineering and Technology', 1, '', '', '', 'RangaReddy', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(457, 1, 'Basavaraju Krishna Bai (Laxmi) Educational Society', 1, '', '', '', 'Ibrahimpatnam', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(458, 1, 'Bharat Engineering College', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(459, 1, 'Center for Spatial Information Technology', 1, '', '', '', 'JawaharlalNehruTechnologicalUniversity', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(460, 1, 'Christu Jyothi Institute of Technology and Science', 1, '', '', '', 'Warangal', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(461, 1, 'City Women College of Engineering and Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(462, 1, 'Deccan Group of Institutions-Faculty of Engineerin', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(463, 1, 'Dhruva Institute of Engineering and Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(464, 1, 'Dr VRK Womens College of Engineering and Technolog', 1, '', '', '', 'Moinabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(465, 1, 'Dr. VRK College of Engineering and Technology', 1, '', '', '', 'Karimnagar', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(466, 1, 'Flytech Aviation Academy', 1, '', '', '', 'Secunderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(467, 1, 'Gayathri Institute of Technology and Sciences', 1, '', '', '', 'Mahabubnagar', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(468, 1, 'GITAM School of Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(469, 1, 'Green Fort Engineering College', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(470, 1, 'Hi-Point College of Engineering and Technology', 1, '', '', '', 'Moinabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(471, 1, 'Jawaharlal Nehru Architecture and Fine Arts Univer', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(472, 1, 'Khader Memorial College of Engineering and Technol', 1, '', '', '', 'Nalgonda', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(473, 1, 'Lumbini Group of Institutes- Faculty of Engineerin', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(474, 1, 'Lumbini School of Architecture and Town Planning', 1, '', '', '', 'Nalgonda', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(475, 1, 'Mahindra Ã‰cole Centrale', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(476, 1, 'Maulana Azad National Urdu University', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(477, 1, 'Medak College of Engineering and Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(478, 1, 'Mohammadiya Institutions- Institute of Engineering', 1, '', '', '', 'Khammam', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(479, 1, 'Mona College of Engineering and Technology', 1, '', '', '', 'Nalgonda', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(480, 1, 'Mumtaz College of Engineering and Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(481, 1, 'Nawab Shah Alam Khan College of Engineering and Te', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(482, 1, 'Noor College of Engineering and Technology', 1, '', '', '', 'Mahabubnagar', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(483, 1, 'Panineeya Institute of Technology and Science', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(484, 1, 'Patanam Rajender Reddy Memorial Engineering Colleg', 1, '', '', '', 'RangaReddy', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(485, 1, 'Pragna Bharath Institute of Technology', 1, '', '', '', 'RangaReddy', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(486, 1, 'Prienceton College of Engineering and Technology ', 1, '', '', '', 'Ghatkesar', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(487, 1, 'Princeton Institute of Engineering and Technology', 1, '', '', '', 'Ghatkesar', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(488, 1, 'Rajiv Gandhi University of Knowledge Technologies', 1, '', '', '', 'Basar', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(489, 1, 'Rajiv Gandhi University of Knowledge Technologies', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(490, 1, 'Royal Institute of Technology and Science', 1, '', '', '', 'Chevella', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(491, 1, 'Sana Engineering College', 1, '', '', '', 'Kodad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(492, 1, 'School of Information Technology', 1, '', '', '', 'JawaharlalNehruTechnologicalUniversity', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(493, 1, 'Shaaz College of Engineering and Technology', 1, '', '', '', 'Moinabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(494, 1, 'Shadan College of Engineering and Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(495, 1, 'Shahjehan College of Engineering and Technology', 1, '', '', '', 'Chevella', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(496, 1, 'Suprabhath College of Engineering and Technology', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(497, 1, 'Swarandhra Institue of Engineering and Technology', 1, '', '', '', 'Narsapur', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(498, 1, 'Syed Hashim College of Science and Technology', 1, '', '', '', 'Siddipet', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(499, 1, 'Tirumala Integrated Campus', 1, '', '', '', 'Nizamabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(500, 1, 'Tudi Ram Reddy Institute of Technology and Science', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(501, 1, 'Vidya Vikas Engineering College', 1, '', '', '', 'Chevella', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(502, 1, 'VIF College of Engineering and Technology', 1, '', '', '', 'Moinabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(503, 1, 'Vishwa Bharathi PG College of Engineering and Mana', 1, '', '', '', 'Hyderabad', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(504, 1, 'Vivekananda Institute of Technological Science', 1, '', '', '', 'Malkapur', 'Telangana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(505, 1, 'Indian Institute of Technology', 1, '', '', '', 'Madras', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(506, 1, 'VIT University', 1, '', '', '', 'Vellore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(507, 1, 'Thiagarajar College of Engineering', 1, '', '', '', 'Madurai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(508, 1, 'SRM University', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(509, 1, 'SASTRA University', 1, '', '', '', 'Thanjavur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(510, 1, 'PSG College of Technology', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(511, 1, 'National Institute of Technology', 1, '', '', '', 'Tiruchirappalli', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(512, 1, 'College of Engineering', 1, '', '', '', 'AnnaUniversity', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(513, 1, 'Amrita School of Engineering', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(514, 1, 'SSN College of Engineering', 1, '', '', '', 'Kalavakkam', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(515, 1, 'Madras Institute of Technology', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(516, 1, 'Karunya University', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(517, 1, 'Coimbatore Institute of Technology', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(518, 1, 'Kalasalingam University', 1, '', '', '', 'Virudhunagar', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(519, 1, 'Veltech Multitech Dr.Rangarajan Dr.Sakunthala Engi', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(520, 1, 'Velammal Engineering College', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(521, 1, 'Sri Venkateswara College of Engineering', 1, '', '', '', 'Sriperumbudur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(522, 1, 'Sri Sai Ram Engineering College', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(523, 1, 'Sri Krishna College of Engineering and Technology', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(524, 1, 'RMK Engineering College', 1, '', '', '', 'Thiruvallur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(525, 1, 'Panimalar Engineering College', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(526, 1, 'Mepco Schlenk Engineering College', 1, '', '', '', 'Sivakasi', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(527, 1, 'Kumaraguru College of Technology', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(528, 1, 'Kongu Engineering College', 1, '', '', '', 'Erode', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(529, 1, 'Jeppiaar Engineering College', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(530, 1, 'Institute of Road and Transport Technology', 1, '', '', '', 'Erode', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(531, 1, 'Indian Institute of Information Technology', 1, '', '', '', 'DesignandManufacturing', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(532, 1, 'Government College of Technology', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(533, 1, 'Government College of Engineering', 1, '', '', '', 'Salem', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(534, 1, 'Easwari Engineering College', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(535, 1, 'Anna University Tiruchirappalli', 1, '', '', '', 'BITCampus', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(536, 1, 'Alagappa College of Technology', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(537, 1, 'Alagappa Chettiar College of Engineering and Techn', 1, '', '', '', 'Karaikudi', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(538, 1, 'Sona College of Technology', 1, '', '', '', 'Salem', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(539, 1, 'Vels University', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(540, 1, 'Vel Tech Dr Rangrajan Dr Sakunthala Technical Univ', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(541, 1, 'Valliammai Engineering College', 1, '', '', '', 'Kancheepuram', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(542, 1, 'Thanthai Periyar Government Institute of Technolog', 1, '', '', '', 'Vellore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `college` (`id`, `active_status`, `college_name`, `institute_type`, `naac_accredition`, `address_1`, `address_2`, `city`, `state`, `zipcode`, `email`, `phone`, `fax`, `industry_interaction`, `fee`, `placement_details`, `quality_of_students_and_crowd`, `international_exposure`, `international_or_india_tie_ups`, `hostel_facility`, `aicte_approved`, `scholarships`, `facility_to_stay_near_by`, `sports_infrastructure`, `library`) VALUES
(543, 1, 'Sri Ramakrishna Engineering College', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(544, 1, 'Sri Eshwar College of Engineering', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(545, 1, 'Saveetha University', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(546, 1, 'Sathyabama University', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(547, 1, 'Saranathan College of Engineering', 1, '', '', '', 'Tiruchirappalli', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(548, 1, 'Rajalakshmi Engineering College', 1, '', '', '', 'KancheepuramÂ ', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(549, 1, 'PSNA College of Engineering and Technology', 1, '', '', '', 'Dindigul', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(550, 1, 'M Kumarasamy College of Engineering', 1, '', '', '', 'Karur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(551, 1, 'Hindustan University', 1, '', '', '', 'Kelambakkam', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(552, 1, 'Government College of Engineering', 1, '', '', '', 'Tirunelveli', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(553, 1, 'Dr. Sivanthi Aditanar College of Engineering', 1, '', '', '', 'Thoothukudi', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(554, 1, 'Dr. Mahalingam College of Engineering and Technolo', 1, '', '', '', 'Pollachi', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(555, 1, 'Central Institute of Plastics Engineering and Tech', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(556, 1, 'BS Abdur Rahman University', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(557, 1, 'Avinashilingam Institute for Home Science and High', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(558, 1, 'Arunai Engineering College', 1, '', '', '', 'Tiruvannamalai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(559, 1, 'Adhiyamaan College of Engineering', 1, '', '', '', 'Hosur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(560, 1, 'Adhiparasakthi Engineering College', 1, '', '', '', 'Kancheepuram', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(561, 1, 'VSB Engineering College', 1, '', '', '', 'Karur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(562, 1, 'AAA College of Engineering and Technology', 1, '', '', '', 'Thiruthangal', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(563, 1, 'Aarupadai Veedu Institute of Technology', 1, '', '', '', 'KancheepuramÂ ', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(564, 1, 'Adhiyamaan College of Engineering- Department of A', 1, '', '', '', 'Hosur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(565, 1, 'Agricultural Engineering College and Research Inst', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(566, 1, 'Agricultural Engineering College and Research Inst', 1, '', '', '', 'Kumulur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(567, 1, 'Aishwarya College of Engineering and Technology', 1, '', '', '', 'Erode', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(568, 1, 'Amrita Institute of Computer Technology', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(569, 1, 'Annai Mira College of Engineering and Technology', 1, '', '', '', 'Nagapattinam', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(570, 1, 'Annapoorana Engineering College', 1, '', '', '', 'Salem', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(571, 1, 'Apollo Priyadarshanam Institute of Technology', 1, '', '', '', 'KancheepuramÂ ', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(572, 1, 'AR Engineering College', 1, '', '', '', 'Villupuram', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(573, 1, 'Archana Institute of Technology', 1, '', '', '', 'KrishnagiriÂ ', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(574, 1, 'Ariyalur Engineering College', 1, '', '', '', 'Ariyalur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(575, 1, 'Arul College of Technology', 1, '', '', '', 'Radhapuram', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(576, 1, 'Arulmigu Meenakshi Amman College of Engineering- D', 1, '', '', '', 'Tiruvannamalai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(577, 1, 'Arulmurugan College of Engineering', 1, '', '', '', 'Karur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(578, 1, 'Arunai College of Engineering', 1, '', '', '', 'Tiruvannamalai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(579, 1, 'Asian College of Engineering and Technology', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(580, 1, 'ASL Pauls College of Engineering and Technology', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(581, 1, 'Balamani Arunachalam Engineering College', 1, '', '', '', 'KancheepuramÂ ', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(582, 1, 'Bharath University- School of Architecture', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(583, 1, 'CARE School of Architecture', 1, '', '', '', 'Tiruchirappalli', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(584, 1, 'Chandy College of Engineering', 1, '', '', '', 'Thoothukudi', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(585, 1, 'Chendu College of Engineering and Technology', 1, '', '', '', 'KancheepuramÂ ', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(586, 1, 'Chennai Institute of Technology', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(587, 1, 'Cheran College of Engineering', 1, '', '', '', 'Karur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(588, 1, 'Christ the King Engineering College', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(589, 1, 'CR Engineering College', 1, '', '', '', 'Madurai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(590, 1, 'Crescent School of Architecture', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(591, 1, 'CSI College of Engineering', 1, '', '', '', 'Nilgiris', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(592, 1, 'Dhanalakshmi Srinivasan College of Engineering', 1, '', '', '', 'Perambalur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(593, 1, 'Dhanalakshmi Srinivasan Institute of Research and ', 1, '', '', '', 'Perambalur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(594, 1, 'Dhanalakshmi Srinivasan Institute of Research and ', 1, '', '', '', 'Perambalur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(595, 1, 'Dhanalakshmi Srinivasan Institute of Technology', 1, '', '', '', 'Tiruchirappalli', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(596, 1, 'Dhanalakshmi Srinivasan Institute of Technology- D', 1, '', '', '', 'Tiruchirappalli', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(597, 1, 'Dhaya College of Engineering', 1, '', '', '', 'Madurai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(598, 1, 'Dhirajlal Gandhi College of Technology', 1, '', '', '', 'Salem', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(599, 1, 'Dr. GU Pope College of Engineering', 1, '', '', '', 'Thoothukudi', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(600, 1, 'Dr. MGR Educational and Research Institute Univers', 1, '', '', '', 'ChennaiÂ ', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(601, 1, 'Elizabeth College of Engineering and Technology', 1, '', '', '', 'Annamangalam', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(602, 1, 'Excel College of Architecture and Planning', 1, '', '', '', 'Namakkal', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(603, 1, 'Excel College of Engineering for Women', 1, '', '', '', 'Namakkal', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(604, 1, 'Excel College of Technology', 1, '', '', '', 'Namakkal', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(605, 1, 'Ganesh College of Engineering', 1, '', '', '', 'Salem', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(606, 1, 'GGR College of Engineering', 1, '', '', '', 'Vellore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(607, 1, 'Gopal Ramalingam Memorial Engineering College', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(608, 1, 'Government College of Engineering', 1, '', '', '', 'Bodinayakanur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(609, 1, 'GRT Institute of Engineering and Technology', 1, '', '', '', 'Thiruvallur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(610, 1, 'Haji Sheik Ismail Engineering College', 1, '', '', '', 'Thirukuvalai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(611, 1, 'Hindustan Institute of Engineering Technology', 1, '', '', '', 'Sriperumbudur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(612, 1, 'Hosur Institute of Technology and Science', 1, '', '', '', 'Hosur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(613, 1, 'Jairupaa College of Engineering', 1, '', '', '', 'Tirupur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(614, 1, 'Jawahar Engineering College', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(615, 1, 'Jaya Suriya Engineering College', 1, '', '', '', 'Tiruvallur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(616, 1, 'Jayaraj Annapackiam CSI College of Engineering', 1, '', '', '', 'Nazareth', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(617, 1, 'Jeppiaar Institute of Technology', 1, '', '', '', 'Kunnam', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(618, 1, 'K Ramakrishnan College of Technology', 1, '', '', '', 'Tiruchirappalli', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(619, 1, 'Karaikudi Institute of Technology', 1, '', '', '', 'Karaikudi', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(620, 1, 'Karur College of Engineering', 1, '', '', '', 'Karur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(621, 1, 'KIT and KIM Technical Campus', 1, '', '', '', 'Karaikudi', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(622, 1, 'KKC College of Engineering and Technology', 1, '', '', '', 'Jayankondam', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(623, 1, 'KSK College of Engineering and Technology', 1, '', '', '', 'CuddaloreÂ ', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(624, 1, 'Kumaran Institute of Technology', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(625, 1, 'Loyola-ICAM College of Engineering and Technology', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(626, 1, 'Mahalakshmi Engineering College', 1, '', '', '', 'Tiruchirappalli', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(627, 1, 'MAM School of Architecture', 1, '', '', '', 'Tiruchirappalli', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(628, 1, 'MAM School of Engineering', 1, '', '', '', 'Siruganur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(629, 1, 'Manonmaniam Sundaranar University', 1, '', '', '', 'Tirunelveli', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(630, 1, 'Maria College of Engineering and Technology', 1, '', '', '', 'Kanyakumari', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(631, 1, 'Meenakshi College of Engineering- Department of Ar', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(632, 1, 'Meenakshi University', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(633, 1, 'Muthayammal Technical Campus', 1, '', '', '', 'Namakkal', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(634, 1, 'Nadar Saraswathi College of Engineering and Techno', 1, '', '', '', 'Theni', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(635, 1, 'Nightingale Institute of Technology', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(636, 1, 'NSN College of Engineering and Technology', 1, '', '', '', 'Karur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(637, 1, 'OAS Institute of Technology and Management', 1, '', '', '', 'Tiruchirappalli', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(638, 1, 'Paavaai Group of Institutions- School of Engineeri', 1, '', '', '', 'Pachal', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(639, 1, 'Pallava Raja College of Engineering', 1, '', '', '', 'Kancheepuram', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(640, 1, 'PERI Institute of Technology', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(641, 1, 'PMR Institute of Technology', 1, '', '', '', 'Thiruvallur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(642, 1, 'Pollachi Institute of Engineering and Technology', 1, '', '', '', 'Pollachi', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(643, 1, 'Professional Educational Trustâ€™s Group of Instit', 1, '', '', '', 'Tirupur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(644, 1, 'Professional Group of Institutions', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(645, 1, 'Rajalakshmi College of Architecture', 1, '', '', '', 'KancheepuramÂ ', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(646, 1, 'Rathinam Institute of Technology', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(647, 1, 'Renganayagi Varatharaj College of Engineering', 1, '', '', '', 'Sivakasi', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(648, 1, 'Rohini College of Engineering and Technology', 1, '', '', '', 'Kanyakumari', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(649, 1, 'RVS College of Engineering and Technology', 1, '', '', '', 'Dindigul', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(650, 1, 'RVS Institute of Higher Education Engineering Coll', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(651, 1, 'RVS Padhmavathy College of Engineering and Technol', 1, '', '', '', 'Kavaraipettai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(652, 1, 'RVS Padhmavathy School of Architecture', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(653, 1, 'RVS School of Architecture', 1, '', '', '', 'Dindigul', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(654, 1, 'RVS School of Architecture', 1, '', '', '', 'Kannampalayam', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(655, 1, 'RVS School of Engineering', 1, '', '', '', 'Dindigul', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(656, 1, 'Saraswathy College of Engineering and Technology', 1, '', '', '', 'Villupuram', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(657, 1, 'SCAD Institute of Technology', 1, '', '', '', 'Tirupur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(658, 1, 'School of Architecture and Planning', 1, '', '', '', 'AnnaUniversity', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(659, 1, 'Shree Sathyam College of Engineering and Technolog', 1, '', '', '', 'Salem', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(660, 1, 'Shreenivasa Engineering College', 1, '', '', '', 'Dharmapuri', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(661, 1, 'Shri Andal Alagar College of Engineering', 1, '', '', '', 'Mamandur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(662, 1, 'Sir Issac Newton College of Engineering and Techno', 1, '', '', '', 'Nagapattinam', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(663, 1, 'SIVA Institute of Frontier Technology', 1, '', '', '', 'Thiruvallur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(664, 1, 'SMR East Coast College of Engineering and Technolo', 1, '', '', '', 'Thanjavur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(665, 1, 'Sree Sakthi Engineering College', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(666, 1, 'SRI College of Engineering and Technology', 1, '', '', '', 'Tiruvannamalai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(667, 1, 'Sri Krishna Engineering College', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(668, 1, 'Sri Padmavathi College of Engineering', 1, '', '', '', 'Kancheepuram', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(669, 1, 'Sri Raaja Raajan College of Engineering and Techno', 1, '', '', '', 'Sivaganga', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(670, 1, 'Sri Ramakrishna College of Engineering', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(671, 1, 'Sri Ramana Maharishi College of Engineering', 1, '', '', '', 'Tiruvanamalai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(672, 1, 'Sri Ranganathar Institute of Engineering and Techn', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(673, 1, 'Sri Shanmugha College of Engineering and Technolog', 1, '', '', '', 'Salem', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(674, 1, 'Sri Venkateswara College of Technology', 1, '', '', '', 'Sriperumbudur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(675, 1, 'Srinivasa Ramanujan Centre', 1, '', '', '', 'Tanjavur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(676, 1, 'SRM University', 1, '', '', '', 'RamapuramCampus', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(677, 1, 'SRM University', 1, '', '', '', 'VadapalaniCampus', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(678, 1, 'SSM Institute of Engineering and Technology', 1, '', '', '', 'Dindigul', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(679, 1, 'St.Peters University', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(680, 1, 'Star Lion College of Engineering and Technology', 1, '', '', '', 'Thanjavur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(681, 1, 'Suguna College of Engineering', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(682, 1, 'Sureya College of Engineering', 1, '', '', '', 'Trichy', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(683, 1, 'Surya Group of Institutions - School of Architectu', 1, '', '', '', 'Villpuram', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(684, 1, 'Surya School of Engineering and Technology', 1, '', '', '', 'Villpuram', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(685, 1, 'SVS School of Architecture', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(686, 1, 'Tamilnadu School of Architecture', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(687, 1, 'Thamirabharani Engineering College', 1, '', '', '', 'Tirunelveli', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(688, 1, 'Thiagarajar College of Engineering- Department of ', 1, '', '', '', 'Madurai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(689, 1, 'TRP Engineering College', 1, '', '', '', 'Trichirappalli', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(690, 1, 'TSM Jain College of Technology', 1, '', '', '', 'Melur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(691, 1, 'University College of Engineering', 1, '', '', '', 'Kancheepuram', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(692, 1, 'University College of Engineering', 1, '', '', '', 'Thoothukudi', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(693, 1, 'Vaigai College of Engineering', 1, '', '', '', 'Madurai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(694, 1, 'Vedhantha Institute of Technology', 1, '', '', '', 'Villupuram', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(695, 1, 'Veerammal College of Engineering', 1, '', '', '', 'Dindigul', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(696, 1, 'Vidhya Mandhir Institute of Technology', 1, '', '', '', 'Erode', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(697, 1, 'Vinayaka Missions University', 1, '', '', '', 'Salem', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(698, 1, 'VIT University', 1, '', '', '', 'Chennai', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(699, 1, 'Vivekanandha Engineering College for Women', 1, '', '', '', 'Tiruchengode', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(700, 1, 'VKK Vijayan College of Engineering', 1, '', '', '', 'Sriperumpudur', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(701, 1, 'VPV College of Engineering', 1, '', '', '', 'Theni', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(702, 1, 'VSB College of Engineering- Technical Campus', 1, '', '', '', 'Coimbatore', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(703, 1, 'VV College of Engineering', 1, '', '', '', 'Tirunelveli', 'Tamil Nadu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(704, 1, 'National Institute of Technology', 1, '', '', '', 'Calicut', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(705, 1, 'School of Engineering Cochin University of Science', 1, '', '', '', 'Ernakulam', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(706, 1, 'Indian Institute of Science Education and Research', 1, '', '', '', 'Thiruvananthapuram', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(707, 1, 'College of Engineering', 1, '', '', '', 'Trivandrum', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(708, 1, 'TKM College of Engineering', 1, '', '', '', 'Kollam', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(709, 1, 'Rajiv Gandhi Institute of Technology', 1, '', '', '', 'Kottayam', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(710, 1, 'Indian Institute of Space Science and Technology', 1, '', '', '', 'Thiruvananthapuram', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(711, 1, 'Government Engineering College', 1, '', '', '', 'Thrissur', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(712, 1, 'Sree Chitra Thirunal College of Engineering', 1, '', '', '', 'ThiruvananthapuramÂ ', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(713, 1, 'SCMS School of Engineering and Technology', 1, '', '', '', 'Ernakulum', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(714, 1, 'Rajagiri School of Engineering and Technology', 1, '', '', '', 'Ernakulam', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(715, 1, 'NSS College of Engineering', 1, '', '', '', 'Palakkad', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(716, 1, 'MES College of Engineering', 1, '', '', '', 'Malappuram', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(717, 1, 'Mar Athanasius College of Engineering', 1, '', '', '', 'Kothamangalam', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(718, 1, 'Government Model Engineering College', 1, '', '', '', 'Thrikkakara', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(719, 1, 'Government Engineering College', 1, '', '', '', 'Wayanad', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(720, 1, 'Government Engineering College', 1, '', '', '', 'Kozhikode', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(721, 1, 'Government Engineering College Barton Hill', 1, '', '', '', 'ThiruvananthapuramÂ ', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(722, 1, 'Government College of Engineering', 1, '', '', '', 'Kannur', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(723, 1, 'Vedavyasa Institute of Technology', 1, '', '', '', 'Malappuram', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(724, 1, 'Mookambika Technical Campus', 1, '', '', '', 'Ernakulam', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(725, 1, 'LBS Institute of Technology for Women', 1, '', '', '', 'Thiruvananthapuram', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(726, 1, 'LBS College of Engineering', 1, '', '', '', 'Kasargod', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(727, 1, 'Kelappaji College of Agricultural Engineering and ', 1, '', '', '', 'Tavanur', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(728, 1, 'Government Engineering College', 1, '', '', '', 'Sreekrishnapuram', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(729, 1, 'Government Engineering College', 1, '', '', '', 'Idukki', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(730, 1, 'College of Engineering', 1, '', '', '', 'Chengannur', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(731, 1, 'College of Engineering', 1, '', '', '', 'Adoor', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(732, 1, 'College of Dairy Science and Technology Mannuthi', 1, '', '', '', 'Thrissur', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(733, 1, 'ACE College of Engineering', 1, '', '', '', 'Thiruvananthapuram', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(734, 1, 'Alsalama Institute of Architecture', 1, '', '', '', 'Malappuram', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(735, 1, 'Amrita Center for Nanosciences and Molecular Medic', 1, '', '', '', 'Kochi', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(736, 1, 'Amrita School of Engineering', 1, '', '', '', 'Amritapuri', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(737, 1, 'Bishop Jerome Institute- School of Architecture', 1, '', '', '', 'Kollam', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(738, 1, 'CAT College of Architecture', 1, '', '', '', 'Trivandrum', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(739, 1, 'Cochin University College of Engineering', 1, '', '', '', 'Kuttanad', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(740, 1, 'Cochin University of Science and Technology. Kunja', 1, '', '', '', 'Ernakulam', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(741, 1, 'College of Architecture', 1, '', '', '', 'Vellanad', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(742, 1, 'College of Engineering and Technology- Department ', 1, '', '', '', 'Payyanur', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(743, 1, 'College of Engineering- Department of Architecture', 1, '', '', '', '', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(744, 1, ' Cochin University of Science and Technology', 1, '', '', '', 'Ernakulam', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(745, 1, 'Department of Ship Technology Cochin University of', 1, '', '', '', 'Ernakulam', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(746, 1, 'DG College of Architecture', 1, '', '', '', 'Malappuram', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(747, 1, 'ER&DCI Institute of Technology', 1, '', '', '', 'ThiruvananthapuramÂ ', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(748, 1, 'Eranad Knowledge City Technical Campus', 1, '', '', '', 'Malappuram', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(749, 1, 'Global Institute of Architecture', 1, '', '', '', 'Palakkad', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(750, 1, 'Holy Crescent College of Architecture', 1, '', '', '', 'Aluva', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(751, 1, 'IES College of Architecture', 1, '', '', '', 'Thrissur', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(752, 1, 'Indian Naval Academy', 1, '', '', '', 'Kannur', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(753, 1, 'KMCT College of Architecture', 1, '', '', '', 'Manassery', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(754, 1, 'MES College of Engineering- School of Architecture', 1, '', '', '', 'Malappuram', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(755, 1, 'Muthoot Institute of Technology and Sciences', 1, '', '', '', 'Ernakulam', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(756, 1, 'Pinnacle School of Engineering and Technology', 1, '', '', '', 'Anchal', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(757, 1, 'PRS College of Engineering and Technology', 1, '', '', '', 'ThiruvananthapuramÂ ', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(758, 1, 'School of Technology and Applied Sciences', 1, '', '', '', 'MahatmaGandhiUniversity', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(759, 1, 'Sneha College of Architecture', 1, '', '', '', 'Palakkad', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(760, 1, 'Sree Narayana Institute of Technology', 1, '', '', '', 'Kollam', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(761, 1, 'TKM College of Engineering- Faculty of Architectur', 1, '', '', '', 'Kollam', 'Kerala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(762, 1, 'National Institute of Technology', 1, '', '', '', 'Surathkal', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(763, 1, 'Manipal Institute of Technology', 1, '', '', '', 'Manipal', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(764, 1, 'RV College of Engineering', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(765, 1, 'PES University', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(766, 1, 'MS Ramaiah Institute of Technology', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(767, 1, 'International Institute of Information Technology', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(768, 1, 'BMS College of Engineering', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(769, 1, 'The National Institute of Engineering', 1, '', '', '', 'Mysore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(770, 1, 'Sri Sairam College Of Engineering', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(771, 1, 'Siddaganga Institute of Technology', 1, '', '', '', 'Tumkur', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(772, 1, 'Nitte Meenakshi Institute of Technology', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(773, 1, 'JSS Mahavidyapeetha Sri Jayachamarajendra College ', 1, '', '', '', 'Mysore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(774, 1, 'Bangalore Institute of Technology', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(775, 1, 'School of Engineering and Technology Jain Universi', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(776, 1, 'NMAM Institute of Technology', 1, '', '', '', 'KarkalaTaluk', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(777, 1, 'MVJ College of Engineering', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(778, 1, 'Alliance College of Engineering and Design', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(779, 1, 'Vidyavardhaka College of Engineering', 1, '', '', '', 'Mysore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(780, 1, 'University Visvesvaraya College of Engineering', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(781, 1, 'The Oxford College of Engineering', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(782, 1, 'Sir M Visvesvaraya Institute of Technology', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(783, 1, 'SDM College of Engineering and Technology', 1, '', '', '', 'Dharwad', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(784, 1, 'RNS Institute of Technology', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(785, 1, 'Reva Institute of Technology and Management', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(786, 1, 'JSS Academy of Technical Education', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(787, 1, 'Dr. Ambedkar Institute of Technology', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(788, 1, 'Dayanand Sagar College of Engineering', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(789, 1, 'Christ University', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(790, 1, 'BV Bhoomaraddi College of Engineering and Technolo', 1, '', '', '', 'Hubli', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(791, 1, 'BNM Institute of Technology', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(792, 1, 'BMS Institute of Technology and Management', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(793, 1, 'Basaveshwar Engineering College', 1, '', '', '', 'Bagalkot', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(794, 1, 'Ballari Institute of Technology and Management', 1, '', '', '', 'Bellary', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(795, 1, 'Acharya Institute of Technology', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(796, 1, 'UBDT College of Engineering', 1, '', '', '', 'Davangere', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(797, 1, 'Sri Revana Siddeshwara Institute of Technology', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(798, 1, 'SJB Institute of Technology', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(799, 1, 'Sapthagiri College of Engineering', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(800, 1, 'Sai Vidya Institute of Technology', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(801, 1, 'Sahyadri College of Engineering and Management', 1, '', '', '', 'Mangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(802, 1, 'Rajarajeswari College of Engineering', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(803, 1, 'PES College of Engineering', 1, '', '', '', 'Mandya', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(804, 1, 'PA College of Engineering', 1, '', '', '', 'Mangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(805, 1, 'NIE Institute of Technology', 1, '', '', '', 'Mysore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(806, 1, 'New Horizon College of Engineering', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(807, 1, 'Malnad College of Engineering', 1, '', '', '', 'Hassan', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(808, 1, 'KLS Gogte Institute of Technology', 1, '', '', '', 'Belgaum', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(809, 1, 'Guru Nanak Dev Engineering College', 1, '', '', '', 'Bidar', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(810, 1, 'Government Sri Krishnarajendra Silver Jubilee Tech', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(811, 1, 'Coorg Institute of Technology', 1, '', '', '', 'Kodagu', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(812, 1, 'CMR Institute of Technology', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(813, 1, 'Bapuji Institute of Engineering and Technology', 1, '', '', '', 'Davangere', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(814, 1, 'A C S College of Engineering', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(815, 1, 'Aakar Academy of Architecture', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(816, 1, 'Amrita School of Engineering', 1, '', '', '', 'Bengaluru', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(817, 1, 'Central University of Karnataka', 1, '', '', '', 'Gulbarga', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(818, 1, 'GITAM School of Technology', 1, '', '', '', 'BengaluruCampus', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(819, 1, 'Government Engineering College', 1, '', '', '', 'Bellary', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(820, 1, 'Government Engineering College', 1, '', '', '', 'Coorg', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(821, 1, 'Government Engineering College', 1, '', '', '', 'Mandya', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(822, 1, 'Jain AGM Institute of Technology', 1, '', '', '', 'Jamakandi', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(823, 1, 'Karnataka Veterinary  Animal and Fisheries Science', 1, '', '', '', 'Bidar', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(824, 1, 'KLE Institute of Technology', 1, '', '', '', 'Hubli', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(825, 1, 'Nadgir Institute of Engineering and Technology', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(826, 1, 'PNS Institute of Technology', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(827, 1, 'Proudadevaraya Institute of Technology', 1, '', '', '', 'Hospet', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(828, 1, 'RV School of Architecture', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(829, 1, 'Siddhartha Institute of Aeronautical Engineering a', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(830, 1, 'Sri Krishna School of Engineering and Management', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(831, 1, 'Sri Siddhartha Academy of Higher Education', 1, '', '', '', 'Tumkur', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(832, 1, 'The Oxford College of Engineering- Department of A', 1, '', '', '', 'Bangalore', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(833, 1, 'Vijayanagara Sri Krishnadevaraya University', 1, '', '', '', 'Bellary', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(834, 1, 'Visvesvaraya Technological University', 1, '', '', '', 'Belgaum', 'Karnataka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(835, 1, 'Jawaharlal Nehru Technological University College ', 1, '', '', '', 'Kakinada', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(836, 1, 'AU College of Engineering', 1, '', '', '', 'Vishakhapatnam', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(837, 1, 'GMR Institute of Technology', 1, '', '', '', 'Rajam', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(838, 1, 'Velagapudi Ramakrishna Siddhartha Engineering Coll', 1, '', '', '', 'Vijaywada', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(839, 1, 'Sri Venkateswara University', 1, '', '', '', 'Tirupati', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(840, 1, 'Sree Vidyanikethan Engineering College', 1, '', '', '', 'Tirupati', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(841, 1, 'JNTUA Engineering College', 1, '', '', '', 'Ananthapur', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(842, 1, 'JNTU', 1, '', '', '', 'UniversityCollegeofEngineering', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(843, 1, 'GITAM University', 1, '', '', '', 'Visakhapatnam', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(844, 1, 'Gayatri Vidya Parishad College of Engineering', 1, '', '', '', 'Vishakhapatnam', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(845, 1, 'Vishnu Institute of Technology', 1, '', '', '', 'Bhimavaram', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(846, 1, 'Vignan University', 1, '', '', '', 'Guntur', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(847, 1, 'Vasireddy Venkatadari Institute of Technology', 1, '', '', '', 'Guntur', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(848, 1, 'Sagi Ramakrishnam Raju Engineering College', 1, '', '', '', 'Bhimavaram', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(849, 1, 'RVR and JC College of Engineering', 1, '', '', '', 'Guntur', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(850, 1, 'Prasad V Potluri Siddhartha Institute of Technolog', 1, '', '', '', 'Vijaywada', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(851, 1, 'Maharaj Vijayaram Gajapathi Raj College of Enginee', 1, '', '', '', 'Vizianagaram', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(852, 1, 'Lakireddy Bali Reddy College of Engineering', 1, '', '', '', 'Mylavaram', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(853, 1, 'K L University', 1, '', '', '', 'Guntur', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(854, 1, 'JNTUA College of Engineering', 1, '', '', '', 'Pulivendula', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(855, 1, 'Gudlavalleru Engineering College', 1, '', '', '', 'Gudlavalleru', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(856, 1, 'G Pulla Reddy Engineering College', 1, '', '', '', 'Kurnool', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(857, 1, 'College of Agricultural Engineering.  Acharya NG R', 1, '', '', '', 'MadakasiraÂ ', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(858, 1, 'College of Agricultural Engineering. Acharya NG Ra', 1, '', '', '', 'Bapatla', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(859, 1, 'Bapatla Engineering College', 1, '', '', '', 'Bapatala', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(860, 1, 'AU College of Engineering for Women', 1, '', '', '', 'Vishakapattnam', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(861, 1, 'ANU College of Engineering and Technology', 1, '', '', '', 'Guntur', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(862, 1, 'Annamacharya Institute of Technology and Sciences', 1, '', '', '', 'Rajampet', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(863, 1, 'Anil Neerukonda Institute of Technology and Scienc', 1, '', '', '', 'Vishakhapatnam', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(864, 1, 'Sri Krishnadevaraya University', 1, '', '', '', 'Anantapur', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `college` (`id`, `active_status`, `college_name`, `institute_type`, `naac_accredition`, `address_1`, `address_2`, `city`, `state`, `zipcode`, `email`, `phone`, `fax`, `industry_interaction`, `fee`, `placement_details`, `quality_of_students_and_crowd`, `international_exposure`, `international_or_india_tie_ups`, `hostel_facility`, `aicte_approved`, `scholarships`, `facility_to_stay_near_by`, `sports_infrastructure`, `library`) VALUES
(865, 1, 'Rajeev Gandhi Memorial College of Engineering and ', 1, '', '', '', 'Kurnool', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(866, 1, 'Pragati Engineering College', 1, '', '', '', 'Kakinada', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(867, 1, 'Madanpalle Institute of Technology and Science', 1, '', '', '', 'Madanapalle', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(868, 1, 'Audisankara College of Engineering and Technology', 1, '', '', '', 'Gudur', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(869, 1, 'Aditya Institute of Technology and Management', 1, '', '', '', 'Tekkali', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(870, 1, 'Aditya Engineering College', 1, '', '', '', 'Surampalem', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(871, 1, 'A1 Global Institute of Engineering and Technology', 1, '', '', '', 'Prakasam', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(872, 1, 'Academy of Aviation Engineering and Technology', 1, '', '', '', 'Vishakhapatnam', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(873, 1, 'Acharya College of Engineering', 1, '', '', '', 'Badvel', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(874, 1, 'Anjan Narasimha Munaga Kancharia Thummapudi Instit', 1, '', '', '', 'Cuddapah', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(875, 1, 'ASK College of Technology and Management', 1, '', '', '', 'Visakhapatnam', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(876, 1, 'Baba Institute of Technology and Sciences', 1, '', '', '', 'Visakhapatnam', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(877, 1, 'Badari Institute of Technology and Sciences for Wo', 1, '', '', '', 'Kakinada', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(878, 1, 'Benaiah Institute of Technology and Sciences', 1, '', '', '', 'Rajahmundry', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(879, 1, 'Bonam Venkata Chalamayya College of Engineering', 1, '', '', '', 'Rajahmundry', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(880, 1, 'Chadalwada Venkta Subbaiah College of Engineering', 1, '', '', '', 'Tirupati', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(881, 1, 'Chintalapudi Engineering College', 1, '', '', '', 'Guntur', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(882, 1, 'Chiranjeevi Reddy Group of Institutions- Faculty o', 1, '', '', '', 'Anatapur', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(883, 1, 'Dr Y S Rajasekhar Reddy Institute of Engineering a', 1, '', '', '', 'Cuddapah', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(884, 1, 'Faculty of Engineering Acharya Nagarjuna Universit', 1, '', '', '', 'Vijayawada', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(885, 1, 'Government Institute of Chemical Engineering', 1, '', '', '', 'Vishakhapatnam', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(886, 1, 'Grandhi Varalakshmi Venkata Rao Institute of Techn', 1, '', '', '', 'Bhimavaram', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(887, 1, 'Intellectual Institute of Technology', 1, '', '', '', 'Anantapur', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(888, 1, 'Kakinada Institute of Engineering and Technology -', 1, '', '', '', 'Korangi', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(889, 1, 'Madina Engineering College', 1, '', '', '', 'Cuddapah', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(890, 1, 'Mandava Institute of Engineering and Technology', 1, '', '', '', 'Jaggayyapet', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(891, 1, 'NIMRA College of Engineering and Technology', 1, '', '', '', 'Vijaywada', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(892, 1, 'NRI Institute of Technology', 1, '', '', '', 'Vijaywada', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(893, 1, 'Praveenya Institute of Marine Engineering and Mari', 1, '', '', '', 'Vizag', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(894, 1, 'Priyadarshini Institute of Science and Technology', 1, '', '', '', 'Tenali', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(895, 1, 'Quba College of Engineering and Technology', 1, '', '', '', 'Nellore', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(896, 1, 'Rajiv Gandhi University of Knowledge Technologies', 1, '', '', '', 'Nuzivid', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(897, 1, 'Rajiv Gandhi University of Knowledge Technologies-', 1, '', '', '', 'Cuddapah', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(898, 1, 'School of Planning and Architecture', 1, '', '', '', 'Vijayawada', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(899, 1, 'Seshachala Institute of Engineering and Technology', 1, '', '', '', 'Puttur', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(900, 1, 'Sri Padmavati Mahila Visvavidyalayam', 1, '', '', '', 'Tirupati', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(901, 1, 'Sri Prakash College of Technology', 1, '', '', '', 'EastGodavari', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(902, 1, 'Sri Satya Narayana College of Engineering and Tech', 1, '', '', '', 'Ongole', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(903, 1, 'Sri Venkateshwara University-DOMS', 1, '', '', '', 'Tirupati', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(904, 1, 'Sri Venkateswara Veterinary University', 1, '', '', '', 'Tirupati', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(905, 1, 'Visaka Engineering College', 1, '', '', '', 'Visakhapatnam', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(906, 1, 'Visakha Technical Campus', 1, '', '', '', 'Vishakhapatnam', 'Andhra Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(907, 1, 'Indian Institute of Technology', 1, '', '', '', 'Roorkee', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(908, 1, 'College of Technology GB Pant University of Agriculture and Technology', 1, '', '', '', 'Pantnagar', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(909, 1, 'National Institute of Technology', 1, '', '', '', 'Uttarakhand', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(910, 1, 'Govind Ballabh Pant Engineering College', 1, '', '', '', 'Pauri-Garhwal', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(911, 1, 'DIT University', 1, '', '', '', 'Dehradun', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(912, 1, 'College of Engineering Studies University of Petroleum and Energy Studies', 1, '', '', '', 'Dehradun', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(913, 1, 'THDC Institute of Hydro Power Engineering and Technology', 1, '', '', '', 'Tehri-Garhwal', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(914, 1, 'College of Engineering Roorkee', 1, '', '', '', 'Roorkee', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(915, 1, 'Bipin Tripathi Kumaon Institute of Technology', 1, '', '', '', 'Dwarahat', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(916, 1, 'Shivalik College of Engineering', 1, '', '', '', 'Dehradun', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(917, 1, 'Seemant Institute of Technology', 1, '', '', '', 'Pithoragarh', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(918, 1, 'Roorkee College of Engineering', 1, '', '', '', 'Roorkee', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(919, 1, 'Quantum School of Technology', 1, '', '', '', 'Roorkee', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(920, 1, 'Institute of Technology', 1, '', '', '', 'Gopeshwar', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(921, 1, 'Gurukul Kangri Vishwavidyalaya', 1, '', '', '', 'Haridwar', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(922, 1, 'Graphic Era University', 1, '', '', '', 'Dehradun', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(923, 1, 'Birla Institute of Applied Sciences', 1, '', '', '', 'Nainital', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(924, 1, 'Balwant Singh Mukhiya (BSM) College of Engineering', 1, '', '', '', 'Roorkee', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(925, 1, 'Bishamber Sahai Institute of Technology', 1, '', '', '', 'Roorkee', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(926, 1, 'Department of Architecture and Planning', 1, '', '', '', 'UniversityofRoorkee', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(927, 1, 'Department of Engineering', 1, '', '', '', 'UniversityofRoorkee', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(928, 1, 'Doon College of Engineering and Technology', 1, '', '', '', 'Dehradun', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(929, 1, 'Doon University', 1, '', '', '', 'Dehradun', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(930, 1, 'Drona College of Management and Technical Education', 1, '', '', '', 'Dehradun', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(931, 1, 'Graphic Era Hill University', 1, '', '', '', 'Dehradun', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(932, 1, 'GSBA Engineering Pharmacy and Management Institute', 1, '', '', '', 'Roorkee', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(933, 1, 'Hemwati Nandan Bahuguna Garhwal University', 1, '', '', '', 'PauriGarhwal', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(934, 1, 'Himgiri Zee University', 1, '', '', '', 'Dehradun', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(935, 1, 'Institute of Technology Roorkee', 1, '', '', '', 'Roorkee', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(936, 1, 'Maya Institute of Technology and Management', 1, '', '', '', 'Dehradun', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(937, 1, 'MIET Kumaon', 1, '', '', '', 'Haldwani', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(938, 1, 'Ramanand Institute of Pharmacy and Management', 1, '', '', '', 'Haridwar', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(939, 1, 'Surajmal College of Engineering and Management', 1, '', '', '', 'Kichha', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(940, 1, 'Techwords- Wali Gramodyog Vikas Sansthanâ€™s Group of Institutions', 1, '', '', '', 'Roorkee', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(941, 1, 'Uttarakhand Technical University', 1, '', '', '', 'Dehradun', 'Uttarakhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(942, 1, 'Thapar University', 1, '', '', '', 'Patiala', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(943, 1, 'Indian Institute of Technology', 1, '', '', '', 'Ropar', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(944, 1, 'Sant Longowal Institute of Engineering and Technology', 1, '', '', '', 'Longowal', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(945, 1, 'Indian Institute of Science Education and Research', 1, '', '', '', 'Mohali', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(946, 1, 'Dr B R Ambedkar National Institute of Technology', 1, '', '', '', 'Jalandhar', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(947, 1, 'Lovely Professional University', 1, '', '', '', 'Phagwara', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(948, 1, 'Guru Nanak Dev University', 1, '', '', '', 'Amritsar', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(949, 1, 'Guru Nanak Dev Engineering College', 1, '', '', '', 'Ludhiana', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(950, 1, 'DAV Institute of Engineering and Technology', 1, '', '', '', 'Jalandhar', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(951, 1, 'College of Agricultural Engineering and Technology Punjab Agricultural Universit', 1, '', '', '', 'Ludhiana', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(952, 1, 'Chitkara University', 1, '', '', '', 'Patiala', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(953, 1, 'Punjab College of Engineering and Technology', 1, '', '', '', 'Mohali', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(954, 1, 'Amritsar College of Engineering and Technology', 1, '', '', '', 'Amritsar', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(955, 1, 'University College of Engineering Punjabi University', 1, '', '', '', 'Patiala', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(956, 1, 'Panjab University SSG Regional Centre', 1, '', '', '', 'Hoshiarpur', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(957, 1, 'Guru Teg Bahadur Khalsa Institute of Engineering and Technology', 1, '', '', '', 'Malout', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(958, 1, 'Guru Nanak Institute of Engineering and Management', 1, '', '', '', 'Hoshiarpur', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(959, 1, 'Guru Nanak Dev University Regional Campus', 1, '', '', '', 'Jalandhar', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(960, 1, 'Giani Zail Singh Punjab Technical University Campus', 1, '', '', '', 'Bathinda', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(961, 1, 'Beant College of Engineering and Technology', 1, '', '', '', 'Gurdaspur', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(962, 1, 'Punjab Institute of Engineering and Applied Research', 1, '', '', '', 'Mohali', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(963, 1, 'Swami Parmanand College of Engineering and Technology', 1, '', '', '', 'Mohali', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(964, 1, 'Shaheed Bhagat Singh State Technical Campus', 1, '', '', '', 'Firozpur', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(965, 1, 'School of Engineering and Technology', 1, '', '', '', 'IETBaddalTechnicalCampus', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(966, 1, 'Rayat Bahra Faculty of Engineering', 1, '', '', '', 'PatialaCampus', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(967, 1, 'Ram Devi Jindal Group of Professional Institutions', 1, '', '', '', 'Mohali', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(968, 1, 'Indo Global College of Engineering', 1, '', '', '', 'Mohali', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(969, 1, 'Gurukul Vidyapeeth Institute of Engineering and Technology', 1, '', '', '', 'Patiala', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(970, 1, 'College of Engineering Punjabi University', 1, '', '', '', 'RampuraPhul', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(971, 1, 'Chandigarh Engineering College', 1, '', '', '', 'LandranCampus', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(972, 1, 'BIS College of Engineering and Technology', 1, '', '', '', 'Gagra', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(973, 1, 'Adesh Institute of Engineering and Technology', 1, '', '', '', 'Faridkot', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(974, 1, 'Gulzar Institute of Engineering and Technology', 1, '', '', '', 'Khanna', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(975, 1, 'Gulzar College of Engineering', 1, '', '', '', 'Khanna', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(976, 1, 'Adesh University', 1, '', '', '', 'Bhatinda', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(977, 1, 'Bharat Institute of Engineering and Technology', 1, '', '', '', 'Mansa', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(978, 1, 'Central University of Punjab', 1, '', '', '', 'Bathinda', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(979, 1, 'CGC Institute of Architecture', 1, '', '', '', 'Gharuan', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(980, 1, 'Chitkara School of Planning and Architecture', 1, '', '', '', 'Patiala', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(981, 1, 'Gian Jyoti Institute of Engineering and Technology', 1, '', '', '', 'Patiala', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(982, 1, 'Guru Kashi University', 1, '', '', '', 'TalwandiSabo', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(983, 1, 'Guru Nanak Institute of Technology', 1, '', '', '', 'Hoshiarpur', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(984, 1, 'Indo Global College of Architecture', 1, '', '', '', 'Mohali', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(985, 1, 'Jasdev Singh Sandhu Institute of Engineering and Technology', 1, '', '', '', 'Patiala', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(986, 1, 'KCT College of Engineering and Technology', 1, '', '', '', 'Sangrur', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(987, 1, 'Kings Group of Institutions- Faculty of Engineering', 1, '', '', '', 'Barnala', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(988, 1, 'Ludhiana Group of Colleges- Faculty of Engineering and Technology', 1, '', '', '', 'Ludhiana', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(989, 1, 'Lyallpur Khalsa College of Engineering', 1, '', '', '', 'Jalandhar', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(990, 1, 'MK Education Societyâ€™s Group of Institutions- Faculty of Engineering', 1, '', '', '', 'Amritsar', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(991, 1, 'Punjab Institute of Technical Education', 1, '', '', '', 'FatehgarhSahibÂ ', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(992, 1, 'Punjab Technical University', 1, '', '', '', 'Jalandhar', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(993, 1, 'Radical Technical Institute ', 1, '', '', '', 'Amritsar', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(994, 1, 'Regional Institute of Management and Technology', 1, '', '', '', 'Gobindgarh', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(995, 1, 'RIMT- College of Architecture', 1, '', '', '', 'FatehgarhSahib', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(996, 1, 'SECT Group of Institutes- Faculty of Engineering', 1, '', '', '', 'Patiala', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(997, 1, 'Shaheed Udham Singh Women Engineering College', 1, '', '', '', 'Mohali', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(998, 1, 'Shree Krishna Educational and Charitable Society Groups of Institutions- Faculty', 1, '', '', '', 'BarnalaÂ ', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(999, 1, 'Sri Guru Granth Sahib World University', 1, '', '', '', 'FatehgarhSahib', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1000, 1, 'Sri Sai Institute of Engineering and Technology', 1, '', '', '', 'Amritsar', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1001, 1, 'Surya School of Architecture', 1, '', '', '', 'Patiala', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1002, 1, 'Swami Sarvanand Institute of Management and Technology', 1, '', '', '', 'GurdaspurÂ ', 'Punjab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1003, 1, 'National Institute of Technology', 1, '', '', '', 'Kurukshetra', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1004, 1, 'YMCA University of Science and Technology', 1, '', '', '', 'Faridabad', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1005, 1, 'University Institute of Engineering and Technology Kurukshetra University', 1, '', '', '', 'Kurukshetra', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1006, 1, 'National Dairy Research Institute', 1, '', '', '', 'Karnal', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1007, 1, 'Deenbandhu Chhotu Ram University of Science and Technology', 1, '', '', '', 'Murthal', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1008, 1, 'Faculty of Engineering and Technology Manav Rachna International University', 1, '', '', '', 'Faridabad', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1009, 1, 'University Institute of Engineering and Technology Maharshi Dayanand University', 1, '', '', '', 'Rohtak', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1010, 1, 'Manav Rachna College of Engineering', 1, '', '', '', 'Faridabad', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1011, 1, 'Maharishi Markandeshwar University', 1, '', '', '', 'AmbalaÂ ', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1012, 1, 'ITM University', 1, '', '', '', 'Gurgaon', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1013, 1, 'Guru Jambheshwar University of Science and Technology', 1, '', '', '', 'Hisar', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1014, 1, 'Chaudhary Charan Singh Haryana Agricultural University', 1, '', '', '', 'Hisar', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1015, 1, 'Ambala College of Engineering and Applied Research', 1, '', '', '', 'Ambala', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1016, 1, 'Kurukshetra Institute of Technology and Management', 1, '', '', '', 'Kurukshetra', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1017, 1, 'Vaish College of Engineering', 1, '', '', '', 'Rohtak', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1018, 1, 'The Technological Institute of Textile and Sciences', 1, '', '', '', 'Bhiwani', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1019, 1, 'Suraj College of Engineering and Technology', 1, '', '', '', 'Mahendragarh', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1020, 1, 'Seth Jai Parkash Mukand Lal Institute of Engineering and Technology', 1, '', '', '', 'Yamunanagar', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1021, 1, 'SD Mewat Institute of Engineering and Technology- Technical Campus', 1, '', '', '', 'Mewat', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1022, 1, 'RP Inderaprastha Institute of Technology', 1, '', '', '', 'Karnal', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1023, 1, 'Rattan Institute of Technology and Management', 1, '', '', '', 'Faridabad', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1024, 1, 'Panipat Institute of Engineering and Technology', 1, '', '', '', 'Panipat', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1025, 1, 'NC College of Engineering', 1, '', '', '', 'Israna', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1026, 1, 'Jind Institute of Engineering and Technology', 1, '', '', '', 'Jind', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1027, 1, 'Jai Parkash Mukand Lal Innovative Engineering and Technology Institute', 1, '', '', '', 'Radaur', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1028, 1, 'Institute of Information Technology and Management', 1, '', '', '', 'Gurgaon', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1029, 1, 'Hindu College of Engineering', 1, '', '', '', 'Sonipat', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1030, 1, 'Haryana College of Technology and Management', 1, '', '', '', 'Kaithal', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1031, 1, 'Ganga Institute of Technology and Management', 1, '', '', '', 'Jhajjar', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1032, 1, 'Dronacharya College of Engineering', 1, '', '', '', 'Gurgaon', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1033, 1, 'Delhi College of Technology and Management', 1, '', '', '', 'Palwal', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1034, 1, 'Ch. Devi Lal Memorial Government Engineering College', 1, '', '', '', 'Sirsa', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1035, 1, 'CBS Group of Instiutions', 1, '', '', '', 'Jhajjar', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1036, 1, 'BM College of Technology and Management', 1, '', '', '', 'Gurgaon', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1037, 1, 'Asia Pacific Institute of Information Technology', 1, '', '', '', 'Panipat', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1038, 1, 'Al-Falah School of Engineering and Technology', 1, '', '', '', 'Faridabad', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1039, 1, 'Ansal University', 1, '', '', '', 'Gurgaon', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1040, 1, 'Baba Mastnath University', 1, '', '', '', 'Rohtak', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1041, 1, 'Bhagat Phool Singh Mahila Vishwavidyalaya', 1, '', '', '', 'KhanpurKalan', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1042, 1, 'Bimla Devi Educational Societyâ€™s Group of Institutions', 1, '', '', '', 'JBKnowledgePark', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1043, 1, 'Brown Hills College of Engineering and Technology', 1, '', '', '', 'Faridabad', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1044, 1, 'Ganga Institute of Architecture and Town Planning', 1, '', '', '', 'Jhajjar', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1045, 1, 'Gurgaon College of Engineering for Women', 1, '', '', '', 'Gurgaon', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1046, 1, 'ICL Institute of Architecture and Town Planning', 1, '', '', '', 'Ambala', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1047, 1, 'Institute of Technology and Sciences', 1, '', '', '', 'Bhiwani', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1048, 1, 'Jagannath University', 1, '', '', '', 'Jhajjar', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1049, 1, 'Lingayaâ€™s Institute of Management and Technology for Women', 1, '', '', '', 'Faridabad', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1050, 1, 'Maharishi Markandeshwar Group of Institutions', 1, '', '', '', 'Karnal', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1051, 1, 'Mahaveer Swami Institute of Technology', 1, '', '', '', 'Sonipat', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1052, 1, 'MVN University', 1, '', '', '', 'Palwal', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1053, 1, 'National Institute of Food Technology Entrepreneurship and Management', 1, '', '', '', 'Sonepat', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1054, 1, 'NIILM University', 1, '', '', '', 'Kaithal', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1055, 1, 'PD Memorial College of Engineering', 1, '', '', '', 'Bahadurgarh', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1056, 1, 'PDM College of Engineering', 1, '', '', '', 'Bahadurgarh', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1057, 1, 'Sat Priya School of Architecture and Design', 1, '', '', '', 'Rohtak', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1058, 1, 'Savera Educational Trust Group of Institutions- Faculty of Architecture and Plan', 1, '', '', '', 'Gurgaon', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1059, 1, 'School of Engineering Sciences', 1, '', '', '', 'BPSMahilaVishwaVidyalaya', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1060, 1, 'SGT University', 1, '', '', '', 'Gurgaon', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1061, 1, 'Shree Ram Institute of Engineering and Technology', 1, '', '', '', 'YamunaNagar', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1062, 1, 'Shree Ram Mulkh College of Technical Education', 1, '', '', '', 'Ambala', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1063, 1, 'SRM University', 1, '', '', '', 'Sonipat', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1064, 1, 'Sushant School of Art and Architecture', 1, '', '', '', 'Gurgaon', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1065, 1, 'Yaduvanshi College of Engineering and Technology', 1, '', '', '', 'Mahendergarh', 'Haryana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1066, 1, 'Indian Institute of Technology', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1067, 1, 'Delhi Technological University', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1068, 1, 'Netaji Subhas Institute of Technology', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1069, 1, 'Indraprastha Institute of Information Technology', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1070, 1, 'Guru Gobind Singh Indraprastha University', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1071, 1, 'National Institute of Technology', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1072, 1, 'Maharaja Agrasen Institute of Technology', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1073, 1, 'Jamia Millia Islamia', 1, '', '', '', 'NewDelhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1074, 1, 'Maharaja Surajmal Institute of Technology', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1075, 1, 'Indira Gandhi Delhi Technical University for Women', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1076, 1, 'Amity School of Engineering and Technology', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1077, 1, 'Ambedkar Institute of Advanced Communication Technologies and Research', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1078, 1, 'Sri Guru Tegh Bahadur Institute of Management and Information Technology', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1079, 1, 'National Power Training Institute', 1, '', '', '', 'Faridabad', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1080, 1, 'GB Pant Government Engineering College', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1081, 1, 'CH Brahm Prakash Government Engineering College', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1082, 1, 'Bhagwan Parshuram Institute of Technology', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1083, 1, 'Aditya Institute of Technology', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1084, 1, 'Ambedkar Institute of Technology', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1085, 1, 'Bhim Rao Ambedkar College', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1086, 1, 'Deen Dayal Upadhyaya College', 1, '', '', '', 'NewDelhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1087, 1, 'Gargi College', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1088, 1, 'Hansraj College', 1, '', '', '', 'DelhiÂ ', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1089, 1, 'Jamia Millia Islamia Department of Architecture', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1090, 1, 'Kamala Nehru College', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1091, 1, 'Lady Irwin College', 1, '', '', '', 'NewDelhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1092, 1, 'Lady Shriram College for Women', 1, '', '', '', 'NewDelhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1093, 1, 'Maharaja Agrasen College', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1094, 1, 'MBS School of Planning and Architecture', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1095, 1, 'Miranda House', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1096, 1, 'Ram Lal Anand College', 1, '', '', '', 'NewDelhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1097, 1, 'Shaheed Sukhdev College of Business Studies', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1098, 1, 'Shivaji College', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1099, 1, 'Sri Guru Gobind Singh College of Commerce', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1100, 1, 'Sri Guru Tegbahadur Khalsa College', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1101, 1, 'Sri Venkateswara College', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1102, 1, 'University School of Bio-Technology', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1103, 1, 'Vastu Kala Academy', 1, '', '', '', 'Delhi', 'Delhi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1296, 1, 'Indian Institute of Technology', 1, '', '', '', 'Kanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1297, 1, 'Indian Institute of Technology Banaras Hindu University', 1, '', '', '', 'Varanasi', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1298, 1, 'Motilal Nehru National Institute of Technology', 1, '', '', '', 'Allahabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1299, 1, 'Jaypee Institute of Information Technology', 1, '', '', '', 'Noida', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1300, 1, 'Indian Institute of Information Technology', 1, '', '', '', 'Allahabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1301, 1, 'Harcourt Butler Technological Institute', 1, '', '', '', 'Kanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1302, 1, 'Zakir Hussain College of Engineering and Technology Aligarh Muslim University', 1, '', '', '', 'Aligarh', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1303, 1, 'Shiv Nadar University', 1, '', '', '', 'Dadri', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1304, 1, 'Krishna Institute of Engineering and Technology', 1, '', '', '', 'Ghaziabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1305, 1, 'Kamla Nehru Institute of Technology', 1, '', '', '', 'Sultanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1306, 1, 'JSS Academy of Technical Education', 1, '', '', '', 'Noida', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1307, 1, 'Institute of Engineering and Technology', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1308, 1, 'Dayalbagh Educational Institute', 1, '', '', '', 'Agra', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1309, 1, 'Amity School of Engineering and Technology', 1, '', '', '', 'Noida', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1310, 1, 'Ajay Kumar Garg Engineering College', 1, '', '', '', 'Ghaziabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1311, 1, 'Sharda University', 1, '', '', '', 'GreaterNoida', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1312, 1, 'University Institute of Engineering and Technology CSJMU', 1, '', '', '', 'Kanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1313, 1, 'Shri Ram Murti Smarak College of Engineering and Technology', 1, '', '', '', 'Bareilly', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1314, 1, 'Shobhit University', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1315, 1, 'Sam Higginbottom Institute of Agriculture Technology and Science', 1, '', '', '', 'Allahabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1316, 1, 'Raj Kumar Goel Institute of Technology', 1, '', '', '', 'Ghaziabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1317, 1, 'Meerut Institute of Engineering and Technology', 1, '', '', '', 'Â Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1318, 1, 'Mahatma Jyotiba Phule Rohilkhand University', 1, '', '', '', 'Bareilly', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1319, 1, 'Madan Mohan Malaviya University of Technology', 1, '', '', '', 'Gorakhpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1320, 1, 'Integral University', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1321, 1, 'Indian Institute of Carpet Technology', 1, '', '', '', 'Bhadohi', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1322, 1, 'Inderprastha Engineering College', 1, '', '', '', 'Â Ghaziabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1323, 1, 'IMS Engineering College', 1, '', '', '', 'Ghaziabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1324, 1, 'GLA University', 1, '', '', '', 'Mathura', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1325, 1, 'Galgotias University', 1, '', '', '', 'GreaterNoida', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1326, 1, 'Galgotias College of Engineering and Technology', 1, '', '', '', 'GreaterNoida', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1327, 1, 'Feroze Gandhi Institute of Engineering and Technology', 1, '', '', '', 'Raibareli', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1328, 1, 'Dr. Bhimrao Ambedkar Engineering College of Information Technology', 1, '', '', '', 'Banda', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1329, 1, 'Dr. Ambedkar Institute of Technology for Handicapped', 1, '', '', '', 'Â KanpurÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1330, 1, 'Central Institute of Plastics Engineering and Technology', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1331, 1, 'Bundelkhand University', 1, '', '', '', 'Jhansi', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1332, 1, 'Bundelkhand Institute of Engineering and Technology', 1, '', '', '', 'Jhansi', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1333, 1, 'ABES Engineering College', 1, '', '', '', 'Ghaziabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1334, 1, 'Priyadarshani College Of Computer Sciences', 1, '', '', '', 'Â GreaterNoidaÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1335, 1, 'Nitra Technical Campus', 1, '', '', '', 'Ghaziabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1336, 1, 'Lucknow Model Institute of Technology & Management', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1337, 1, 'Krishna Institute of Technology', 1, '', '', '', 'Kanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1338, 1, 'Kashi Institute of Technology', 1, '', '', '', 'Varanasi', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1339, 1, 'K.P. Engineering College', 1, '', '', '', 'Agra', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1340, 1, 'JRE Group of Institutions', 1, '', '', '', 'GreaterNoida', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1341, 1, 'International College of Engineering', 1, '', '', '', 'Ghaziabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1342, 1, 'IIMT Engineering College', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1343, 1, 'Dr. Ram Manohar Lohia Avadh University', 1, '', '', '', 'Faizabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1344, 1, 'Dr. KN Modi Institute of Engineering and Technology', 1, '', '', '', 'Â ModinagarÂ Â ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1345, 1, 'Bharat Institute of Technology', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1346, 1, 'Babu Sunder Singh Institute of Technology & Management', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1347, 1, 'Babu Banarasi Das Northern India Institute of Technology', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1348, 1, 'Babu Banarasi Das National Institute of Technology and Management', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1349, 1, 'Ansal Technical Campus', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1350, 1, 'KCC Institute of Technology And Management', 1, '', '', '', 'GreaterNoida', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1351, 1, 'United Institute of Technology', 1, '', '', '', 'Â AllahabadÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1352, 1, 'ACE College of Engineering & Management', 1, '', '', '', 'Agra', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1353, 1, 'Adhunik College of Engineering', 1, '', '', '', 'Ghaziabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1354, 1, 'Alpine College of Engineering', 1, 'adasda', 'asdasds', 'asdsdas', 'GreaterNoida', 'Uttar Pradesh', '560032', 'asdasdad', '9739930818', 'asdsdas', 'asdsdsda', 'sadsdda', 'asdasdasd', 'asdasdasdasd', 'asdasdas', 'asdasdasd', 'adasdasds', 'asdsadasd', 'asdasdasdas', 'asdasdasdas', 'asdasdsdas', 'sdasdsadas'),
(1355, 1, 'Amani Engineering College', 1, '', '', '', 'Amroha', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1356, 1, 'Amardeep College of Engineering & Management', 1, '', '', '', 'Firozabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1357, 1, 'Anand College of Architecture', 1, '', '', '', 'Agra', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1358, 1, 'Anubhav Institute Of Engineering & Management', 1, '', '', '', 'Kanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1359, 1, 'Apeejay Institute of Technology School of Architecture and Planning', 1, '', '', '', 'GreaterNoida', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1360, 1, 'Apex Institute of Technology', 1, '', '', '', 'Rampur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1361, 1, 'Apostle Institute of Technology (for Women)', 1, '', '', '', 'GreaterNoida', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1362, 1, 'Aryabhatt College of Engineering & Technology', 1, '', '', '', 'Bagpat', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1363, 1, 'Ashoka Institute of Technology and Management', 1, '', '', '', 'VaranasiÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1364, 1, 'Asia School of Engineering and Management', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1365, 1, 'Asian Institute of Engineering and Technology', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1366, 1, 'Axis Institute Of Architecture', 1, '', '', '', 'Kanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1367, 1, 'Ayodhya Prasad Management Institute & Technology', 1, '', '', '', 'Allahabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1368, 1, 'Babu Banarsi Das University- School of Architecture', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1369, 1, 'Bansal Institute of Engineering and Technology', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1370, 1, 'Bhagwati Institute of Technology & Science', 1, '', '', '', 'Ghaziabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `college` (`id`, `active_status`, `college_name`, `institute_type`, `naac_accredition`, `address_1`, `address_2`, `city`, `state`, `zipcode`, `email`, `phone`, `fax`, `industry_interaction`, `fee`, `placement_details`, `quality_of_students_and_crowd`, `international_exposure`, `international_or_india_tie_ups`, `hostel_facility`, `aicte_approved`, `scholarships`, `facility_to_stay_near_by`, `sports_infrastructure`, `library`) VALUES
(1371, 1, 'Bon Maharaj Engineering College', 1, '', '', '', 'Mathura', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1372, 1, 'Brahmanand Group of Institutions', 1, '', '', '', 'Bulandshahr', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1373, 1, 'Chandra Shekhar Azad University of Agriculture and Technology', 1, '', '', '', 'Kanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1374, 1, 'Chaudhary Beeri Singh College of Engineering & Management', 1, '', '', '', 'Agra', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1375, 1, 'Devender Singh Institute of Technology and Management', 1, '', '', '', 'Ghaziabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1376, 1, 'DNM-Institute Of Engineering & Technology', 1, '', '', '', 'LucknowÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1377, 1, 'DNS College of Engineering and Technology', 1, '', '', '', 'Â Amroha', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1378, 1, 'Dr KN Modi Girls Engineering College', 1, '', '', '', 'Modinagar', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1379, 1, 'Dr. G.P.R.D. Patel Institute of Technology and Management', 1, '', '', '', 'KanpurÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1380, 1, 'Dr. Virendra Swaroop Memorial Trust Group of Institutions', 1, '', '', '', 'Unnao', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1381, 1, 'ESAR College of Engineering', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1382, 1, 'Excel Institute of Management & Technology', 1, '', '', '', 'Mathura', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1383, 1, 'Faculty of Architecture GB Technical University', 1, '', '', '', 'Hazratganj', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1384, 1, 'Faculty of Engineering Shanti Niketan Trustâ€™s Group of Institutions ', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1385, 1, 'Focus Institute of Engineering & Management', 1, '', '', '', 'Bareilly', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1386, 1, 'G L Bajaj Group of Institutions', 1, '', '', '', 'Mathura', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1387, 1, 'Ganeshi Lal Narayandas Agarwal Institute of Technology', 1, '', '', '', 'Mathura', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1388, 1, 'Gautam Buddha University', 1, '', '', '', 'GreaterNoida', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1389, 1, 'GCRG Memorial Trusts Group of Institutions Faculty of Engineering', 1, '', '', '', 'Â LucknowÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1390, 1, 'Glocal School of Technology', 1, '', '', '', 'Saharanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1391, 1, 'Glocal University', 1, '', '', '', 'Saharanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1392, 1, 'Gokaran Narvadeshwar Institute of Technology and Management', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1393, 1, 'Gyan Bharti Institute of Technology', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1394, 1, 'Hardayal Technical Campus', 1, '', '', '', 'Mathura', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1395, 1, 'Heeralal Yadav Institute of Technology & Management', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1396, 1, 'Hindustan Institute of Technology and Management', 1, '', '', '', 'Agra', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1397, 1, 'Hindustan Institute of Technology', 1, '', '', '', 'GreaterNoida', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1398, 1, 'HMFA Memorial Institute of Engineering & Technology', 1, '', '', '', 'Allahabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1399, 1, 'Ideal School of Architecture', 1, '', '', '', 'Ghaziabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1400, 1, 'IFTM University', 1, '', '', '', 'Moradabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1401, 1, 'Indian Institute of Information Technology', 1, '', '', '', 'AmethiCampus', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1402, 1, 'Infinity Institute of Technology', 1, '', '', '', 'Allahbad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1403, 1, 'Institute of Advanced Management and Technology', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1404, 1, 'Institute of Architecture and Town Planning', 1, '', '', '', 'Â SitapurÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1405, 1, 'Institute of Engineering & Management', 1, '', '', '', 'MathuraÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1406, 1, 'Institute of Engineering and Rural Technology', 1, '', '', '', 'Â Sitapur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1407, 1, 'Institute of Technology and Management', 1, '', '', '', 'Â MaharajganjÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1408, 1, 'International Maritime Institute', 1, '', '', '', 'GreaterNoida', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1409, 1, 'Invertis University', 1, '', '', '', 'Bareilly', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1410, 1, 'ITM Institute of Architecture and Town Planning', 1, '', '', '', 'LucknowÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1411, 1, 'Jahangirabad Educational Trust Group of Institutions', 1, '', '', '', 'Barabanki', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1412, 1, 'Jauhar College of Engineering & Technology', 1, '', '', '', 'Rampur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1413, 1, 'Jaypee Institute of Information Technology', 1, '', '', '', 'Noida', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1414, 1, 'JS Institute of Management and Technology', 1, '', '', '', 'Shikohabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1415, 1, 'Khandelwal College of Architecture & Design', 1, '', '', '', 'Bareilly', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1416, 1, 'Khandelwal College of Management Science and Technology', 1, '', '', '', 'Bareilly', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1417, 1, 'Kishan Institute of Information Technology', 1, '', '', '', 'Â Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1418, 1, 'KLS Institute of Engineering and Technology', 1, '', '', '', 'Â Bijnor', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1419, 1, 'Lakshya Technical Campus', 1, '', '', '', '', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1420, 1, 'Lucknow University Faculty of Arts', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1421, 1, 'Madhu Vachaspati Institute of Engieneering & Technology', 1, '', '', '', 'Kaushambi', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1422, 1, 'Mahamaya Technical University', 1, '', '', '', 'Noida', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1423, 1, 'Maharaja Agrasen College of Engineering and Technology', 1, '', '', '', 'Moradabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1424, 1, 'Maharana Pratap College For Women', 1, '', '', '', 'Kanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1425, 1, 'Maharana Pratap College of Engineering', 1, '', '', '', 'Kanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1426, 1, 'Maharana Pratap Mangla Devi Institute of Computer Science Technology and Managem', 1, '', '', '', 'Gorakhpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1427, 1, 'Mahaveer Institute of Engineering and Technology', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1428, 1, 'Mangalayatan University', 1, '', '', '', 'Aligarh', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1429, 1, 'Mass College of Engineering & Management', 1, '', '', '', 'Hathras', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1430, 1, 'Millennium Institute of Technology', 1, '', '', '', 'Saharanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1431, 1, 'Modern Institute Of Management For Women', 1, '', '', '', 'Â GhaziabadÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1432, 1, 'Mohammad Ali Jauhar University', 1, '', '', '', 'Rampur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1433, 1, 'Monad University', 1, '', '', '', 'Hapur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1434, 1, 'Moradabad Educational Trust (MET) Group Of Institutions- Faculty Of Architecture', 1, '', '', '', 'MoradabadÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1435, 1, 'MP School of Engineering', 1, '', '', '', 'Varanasi', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1436, 1, 'Murli Manohar Agrawal Institute of Technology', 1, '', '', '', 'Mathura', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1437, 1, 'Narendra Deva University of Agriculture and Technology', 1, '', '', '', 'Faizabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1438, 1, 'Neelam College of Engineering & Technology', 1, '', '', '', 'Agra', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1439, 1, 'Neelkanth Institute of Technology', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1440, 1, 'Nehru Gram Bharati University', 1, '', '', '', 'Allahabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1441, 1, 'Nikhil Institute of Engineering & Management', 1, '', '', '', 'Mathura', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1442, 1, 'Noida International University', 1, '', '', '', 'GreaterNoida', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1443, 1, 'North India Institute of Technology', 1, '', '', '', 'Bijnor', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1444, 1, 'Om Sai Institute of Technology And Science', 1, '', '', '', 'Bagpat', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1445, 1, 'Panchwati Institute of Engineering & Technology', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1446, 1, 'Prabhat Engineering College', 1, '', '', '', 'Kanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1447, 1, 'Prasad Institute of Management and Technology', 1, '', '', '', 'LucknowÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1448, 1, 'Prayag Institute of Technology And Management', 1, '', '', '', 'Allahabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1449, 1, 'R.D. Foundation Group Of Institutions Faculty Of Engineering', 1, '', '', '', 'Â GhaziabadÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1450, 1, 'Raja Balwant Singh College', 1, '', '', '', 'Â Agra', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1451, 1, 'Rajendra Institute of Science and Technology', 1, '', '', '', 'Allahabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1452, 1, 'Rama Engineering College', 1, '', '', '', 'Ghaziabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1453, 1, 'Rama Institute Of Technology', 1, '', '', '', 'Kanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1454, 1, 'Rishi Chadha Vishvas Girls Institute of Technology', 1, '', '', '', 'Ghaziabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1455, 1, 'Rishi Institute Of Engineering and Technology', 1, '', '', '', 'Â MeerutÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1456, 1, 'RV Institute of Technology', 1, '', '', '', 'Â BijnorÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1457, 1, 'Sardar Vallabhbhai Patel University of Agriculture and Technology', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1458, 1, 'Satyam College of Engineering', 1, '', '', '', 'Ghaziabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1459, 1, 'School of Architecture and Planning', 1, '', '', '', 'ShardaUniversity', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1460, 1, 'Shamli Institute of Engineering & Technology', 1, '', '', '', 'Shamli', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1461, 1, 'Shanti Institute of Technology', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1462, 1, 'Shree Bankey Bihari Institute Of Architecture', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1463, 1, 'Shree Bankey Bihari Institute of Technology', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1464, 1, 'Shri Girraj Maharaj College of Engineering and Management', 1, '', '', '', 'Mathura', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1465, 1, 'Shri Gopichand Institute of Technology & Management', 1, '', '', '', 'Bagpat', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1466, 1, 'Shri Jeet Ram Smarak Institute of Engineering and Technology', 1, '', '', '', 'Bareilly', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1467, 1, 'Shri Krishna College of Engineering', 1, '', '', '', 'Bagpat', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1468, 1, 'Shri Ramswaroop Memorial University', 1, '', '', '', 'Barabanki', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1469, 1, 'Shrinathji Institute for Technical Education', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1470, 1, 'Shriram Institute of Technology ', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1471, 1, 'SMS Institute of Technology', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1472, 1, 'SR Institute of Management & Technology', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1473, 1, 'Sri Krishna Yogi Raj Technical Institute', 1, '', '', '', 'Â HathrasÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1474, 1, 'Sri Sri Institute of Technology and Management', 1, '', '', '', 'Â KanshiramNagarÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1475, 1, 'SSLD Varshney Institute of Management & Engineering', 1, '', '', '', 'Aligarh', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1476, 1, 'Stallian College for Engineering and Technology', 1, '', '', '', 'Saharanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1477, 1, 'Subharti Institute of Technology and Engineering', 1, '', '', '', 'Meerut', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1478, 1, 'Sunder Deep College of Engineering and Research Centre', 1, '', '', '', 'GhaziabadÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1479, 1, 'Sunrise Institute of Engineering Technology & Management', 1, '', '', '', 'Â UnnaoÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1480, 1, 'SVS Group Of Institutions- School of Engineering', 1, '', '', '', 'MeerutÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1481, 1, 'Uttar Pradesh Technical University', 1, '', '', '', 'Lucknow', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1482, 1, 'Vedant Institute of Management and Technology', 1, '', '', '', 'Ghaziabad', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1483, 1, 'Veer Kunwar Institute of Technology', 1, '', '', '', 'Bijnor', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1484, 1, 'Vidya Bhavan College for Engineering Technology', 1, '', '', '', 'Kanpur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1485, 1, 'Vision Institute Of Technology', 1, '', '', '', 'Â AligarhÂ ', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1486, 1, 'Vivekananda College of Technology & Management', 1, '', '', '', 'Aligarh', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1487, 1, 'Women''s Institute of Engineering and Technology', 1, '', '', '', 'Sitapur', 'Uttar Pradesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1488, 1, 'Birla Institute of Technology and Science', 1, '', '', '', 'Pilani', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1489, 1, 'Malaviya National Institute of Technology', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1490, 1, 'Indian Institute of Technology', 1, '', '', '', 'Jodhpur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1491, 1, 'The LNM Institute of Information Technology', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1492, 1, 'Rajasthan Technical University', 1, '', '', '', 'Kota', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1493, 1, 'MBM Engineering College', 1, '', '', '', 'Jodhpur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1494, 1, 'Jaipur Engineering College and Research Centre', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1495, 1, 'College of Technology and Engineering Maharana Pratap University of Agriculture ', 1, '', '', '', 'Udaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1496, 1, 'Banasthali University', 1, '', '', '', 'Banasthali', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1497, 1, 'Mody University of Science and Technology', 1, '', '', '', 'Sikar', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1498, 1, 'JK Lakshmipat University', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1499, 1, 'Swami Keshvanand Institute of Technology Management and Gramothan', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1500, 1, 'Poornima College of Engineering', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1501, 1, 'NIMS University', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1502, 1, 'NIIT University', 1, '', '', '', 'Neemrana', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1503, 1, 'Manipal University', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1504, 1, 'Jaipur National University', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1505, 1, 'Government Women Engineering College', 1, '', '', '', 'Ajmer', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1506, 1, 'Government Engineering College', 1, '', '', '', 'Bikaner', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1507, 1, 'Government Engineering College', 1, '', '', '', 'Ajmer', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1508, 1, 'Government College of Engineering and Technology', 1, '', '', '', 'Bikaner', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1509, 1, 'Global Institute of Technology', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1510, 1, 'Jodhpur Institute of Engineering and Technology', 1, '', '', '', 'Jodhpur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1511, 1, 'SLBS Engineering College', 1, '', '', '', 'Jodhpur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1512, 1, 'Shree Digamber Institute of Technology', 1, '', '', '', 'Dausa', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1513, 1, 'Shekhawati Institute of Engineering and Technology', 1, '', '', '', 'Sikar', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1514, 1, 'Rajasthan Institute of Engineering and Technology', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1515, 1, 'Rajasthan College of Engineering for Women', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1516, 1, 'Poornima Group of Institutions', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1517, 1, 'MLV Government Textile and Engineering College', 1, '', '', '', 'Bhilwara', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1518, 1, 'Gyan Vihar School of Engineering and Technology', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1519, 1, 'Government Engineering College', 1, '', '', '', 'Jhalawar', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1520, 1, 'Ajmer Institute of Technology', 1, '', '', '', 'Ajmer', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1521, 1, 'Advait Vedanta Institute of Technology', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1522, 1, 'Amity University', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1523, 1, 'Amity University- Faculty of Engineering', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1524, 1, 'Apex Group of Institutions', 1, '', '', '', 'Sitapura', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1525, 1, 'BLM Institute of Technology and Management Sciences', 1, '', '', '', 'Jhunjhunu', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1526, 1, 'Buddha Group of Institutions- College of Architecture and Town Planning', 1, '', '', '', 'Udaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1527, 1, 'Career Point Technical Campus', 1, '', '', '', 'RajsamandÂ ', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1528, 1, 'Career Point University', 1, '', '', '', 'Kota', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1529, 1, 'Central University of Rajasthan', 1, '', '', '', 'Ajmer', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1530, 1, 'Chanakya Technical Campus', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1531, 1, 'College of Dairy and Food Science Technology', 1, '', '', '', 'Udaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1532, 1, 'Government Engineering College Banswara', 1, '', '', '', 'Banswara', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1533, 1, 'Govindam Institute of Engineering and Technology', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1534, 1, 'ICFAI University', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1535, 1, 'Institute of Advanced Studies in Education', 1, '', '', '', 'Churu', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1536, 1, 'Jagannath University', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1537, 1, 'Janardan Rai Nagar Rajasthan Vidyapeeth University', 1, '', '', '', 'Udaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1538, 1, 'JECRC University', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1539, 1, 'Maharana Pratap University of Agriculture and Technology', 1, '', '', '', 'Udaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1540, 1, 'Maharani Girls Engineering College', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1541, 1, 'Mahatma Jyoti Rao Phoole University', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1542, 1, 'NIMT Institute of Engineering and Technology', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1543, 1, 'Northern Institute of Engineering Technical Campus', 1, '', '', '', 'Alwar', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1544, 1, 'Pacific University', 1, '', '', '', 'Udaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1545, 1, 'Poddar Management and Technical Campus', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1546, 1, 'Poornima University', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1547, 1, 'Pratap Institute of Technology and Science', 1, '', '', '', 'Sikar', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1548, 1, 'Pratap University', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1549, 1, 'Sangam University', 1, '', '', '', 'Bhilwara', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1550, 1, 'Sawai Madhopur College of Engineering and Technology', 1, '', '', '', 'SawaiMadhopur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1551, 1, 'School of Aeronautics', 1, '', '', '', 'Neemrana', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1552, 1, 'School of Architecture- Central University of Rajasthan', 1, '', '', '', 'Ajmer', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1553, 1, 'School of Engineering and Technology- Central University of Rajasthan', 1, '', '', '', 'Ajmer', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1554, 1, 'Shekhawati Girls Engineering College', 1, '', '', '', 'Dundlod', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1555, 1, 'Shri Jagdishprasad Jhabarmal Tibrewala University', 1, '', '', '', 'Jhunjhunu', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1556, 1, 'Shridhar University', 1, '', '', '', 'Pilani', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1557, 1, 'Shridhar University- Faculty of Engineering', 1, '', '', '', 'Pilani', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1558, 1, 'Shyam Institute of Engineering and Technology', 1, '', '', '', 'Dausa', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1559, 1, 'Singhania University', 1, '', '', '', 'Jhunjhunu', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1560, 1, 'SJ College Engineering and Technology', 1, '', '', '', 'Dausa', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1561, 1, 'St. Wilfredâ€™s Institute of Architecture', 1, '', '', '', 'Ajmer', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1562, 1, 'SunRise University', 1, '', '', '', 'Alwar', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1563, 1, 'Suresh Gyan Vihar University', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1564, 1, 'Swasthya Kalyan Technical Campus', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1565, 1, 'University of Engineering and Management', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1566, 1, 'Vedic Gurukul Institute of Engineering and Technology', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1567, 1, 'Vivekananda Global University', 1, '', '', '', 'Jaipur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1568, 1, 'Vyas Engineering College for Girls', 1, '', '', '', 'Jodhpur', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1569, 1, 'Yaduvanshi College of Engineering and Technology', 1, '', '', '', 'Sohali', 'Rajasthan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1570, 1, 'Indian Institute of Technology', 1, '', '', '', 'Bombay', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1571, 1, 'Institute of Chemical Technology', 1, '', '', '', 'Mumbai', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1572, 1, 'College of Engineering', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1573, 1, 'Visvesvaraya National Institute of Technology', 1, '', '', '', 'Nagpur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1574, 1, 'Veermata Jijabai Technological Institute', 1, '', '', '', 'Mumbai', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1575, 1, 'Indian Institute of Science Education and Research', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1576, 1, 'Walchand College of Engineering', 1, '', '', '', 'Sangli', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1577, 1, 'Symbiosis International University', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1578, 1, 'Sinhgad College of Engineering', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1579, 1, 'Shivaji University', 1, '', '', '', 'Kolhapur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1580, 1, 'Government College of Engineering', 1, '', '', '', 'Aurangabad', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1581, 1, 'Yeshwantrao Chavan College of Engineering', 1, '', '', '', 'Nagpur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1582, 1, 'Vishwakarma Institute of Information Technology', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1583, 1, 'Thakur College of Engineering and Technology', 1, '', '', '', 'Mumbai', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1584, 1, 'Smt. Kashibai Navale College of Engineering', 1, '', '', '', 'Vadgaon', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1585, 1, 'Shri Ramdeobaba College of Engineering and Management', 1, '', '', '', 'Nagpur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1586, 1, 'Padmashree Dr. DY Patil Institute of Engineering and Technology', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1587, 1, 'Mukesh Patel School of Technology Management and Engineering', 1, '', '', '', 'Mumbai', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1588, 1, 'Maharashtra Institute of Technology', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1589, 1, 'KK Wagh Institute of Engineering Education and Research', 1, '', '', '', 'Nashik', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1590, 1, 'KJ Somaiya College of Engineering', 1, '', '', '', 'Mumbai', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1591, 1, 'KDK College of Engineering', 1, '', '', '', 'Nagpur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1592, 1, 'Government College of Engineering', 1, '', '', '', 'Karad', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1593, 1, 'Government College of Engineering', 1, '', '', '', 'Amravati', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1594, 1, 'Fr. Conceicao Rodrigues College of Engineering', 1, '', '', '', 'Mumbai', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1595, 1, 'Fr. C Rodrigues Institute of Technology', 1, '', '', '', 'NaviMumbai', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1596, 1, 'Don Bosco Institute of Technology', 1, '', '', '', 'Mumbai', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1597, 1, 'Bhartiya Vidya Bhavans Sardar Patel College of Engineering', 1, '', '', '', 'Mumbai', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1598, 1, 'Bharati Vidyapeeth Deemed University College of Engineering', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1599, 1, 'University Institute of Chemical Technology North Maharashtra University', 1, '', '', '', 'Jalgaon', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1600, 1, 'Trinity College of Engineering and Research', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1601, 1, 'SKN Sinhgad College of Engineering', 1, '', '', '', 'Korti', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1602, 1, 'Shri Sant Gajanan Maharaj College of Engineering', 1, '', '', '', 'Shegaon', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1603, 1, 'Shri Guru Gobind Singhji Institute of Engineering and Technology', 1, '', '', '', 'Vishnupuri', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1604, 1, 'Sanjivani College of Engineering', 1, '', '', '', 'Kopargaon', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1605, 1, 'Priyadarshini College of Engineering', 1, '', '', '', 'Nagpur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1606, 1, 'MIT Academy of Engineering', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1607, 1, 'MCTâ€™s Rajiv Gandhi Institute of Technology', 1, '', '', '', 'Mumbai', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1608, 1, 'Maharshi Karve Stree Shikshan Samstha Cummins College of Engineering for Women', 1, '', '', '', 'Nagpur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1609, 1, 'Kasegaon Education Societyâ€™s Rajarambapu Institute of Technology', 1, '', '', '', 'Sangli', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1610, 1, 'Government College of Engineering', 1, '', '', '', 'Jalgaon', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1611, 1, 'Government College of Engineering', 1, '', '', '', 'Chandrapur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1612, 1, 'GH Raisoni College of Engineering', 1, '', '', '', 'Nagpur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1613, 1, 'Dr. Babasaheb Ambedkar Technological University', 1, '', '', '', 'Lonere', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1614, 1, 'DKTE Societyâ€™s Textile and Engineering Institute', 1, '', '', '', 'Ichalkaranji', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1615, 1, 'Army Institute of Technology', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1616, 1, 'Amrutvahini College of Engineering', 1, '', '', '', 'Ahmednagar', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1617, 1, 'Agnihotri College of Engineering', 1, '', '', '', 'Wardha', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1618, 1, 'Amrutananad Adhikari Institute of Technology', 1, '', '', '', 'Wardha', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1619, 1, 'Ashokrao Mane Group of Institutions- Faculty of Engineering', 1, '', '', '', 'Kolhapur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1620, 1, 'Bharati Vidyapeethâ€™s College of Engineering for Women', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1621, 1, 'Bhausaheb Mulik College of Engineering', 1, '', '', '', 'Butibori', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1622, 1, 'Central India College of Engineering and Technology', 1, '', '', '', 'Nagpur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1623, 1, 'College of Agricultural Engineering and Technology', 1, '', '', '', 'Akola', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1624, 1, 'College of Agricultural Engineering and Technology Marathwada Agricultural Unive', 1, '', '', '', 'Parbhani', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1625, 1, 'College of Engineering and Technology - School of Architecture', 1, '', '', '', 'Akola', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1626, 1, 'College of Engineering and Technology', 1, '', '', '', 'Akola', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1627, 1, 'College of Engineering Pandharpur', 1, '', '', '', 'Solapur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1628, 1, 'Defence Institute of Advanced Technology', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1629, 1, 'Deogiri Technical Campus for Engineering and Management Studies', 1, '', '', '', 'Aurangabad', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1630, 1, 'DN Patel College of Engineering', 1, '', '', '', 'Nandurbar', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1631, 1, 'Dr Balasaheb Sawant Konkan Krishi Vidyapeeth', 1, '', '', '', 'Dapoli', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1632, 1, 'Dr DY Patil Vidyapeeth', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1633, 1, 'Dr Panjabrao Deshmukh Agricultural University', 1, '', '', '', 'Akola', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1634, 1, 'Dr. Annasaheb Shinde College of Agricultural Engineering', 1, '', '', '', 'Ahmednagar', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1635, 1, 'Dr. Bhanuben Nanavati College of Architecture For Women', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1636, 1, 'Dr. DY Patil Biotechnology and Bioinformatics Institute', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1637, 1, 'Dr. DY Patil School of Architecture', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1638, 1, 'Dr. DY Patil School of Engineering', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1639, 1, 'DY Patil College of Engineering and Technology- Department of Architecture', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1640, 1, 'GH Raisoni College of Engineering and Management', 1, '', '', '', 'Amravati', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1641, 1, 'Hi-Tech Institute of Technology', 1, '', '', '', 'Aurangabad', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1642, 1, 'Homi Bhabha National Institute', 1, '', '', '', 'Mumbai', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1643, 1, 'Jayawant Shikshan Prasarak Mandal', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1644, 1, 'Kamla Raheja Vidhyandhi Institute for Architecture', 1, '', '', '', 'Mumbai', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1645, 1, 'KC College of Engineering and Management Studies', 1, '', '', '', 'Thane', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1646, 1, 'Late Shri Bapuraoji Deshmukh College of Architechure Sewagram', 1, '', '', '', 'Wardha', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1647, 1, 'Maharashtra Animal and Fishery Sciences University', 1, '', '', '', 'Nagpur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1648, 1, 'Mahatma Gandhi Missionâ€™s Jawaharlal Nehru Engineering College- Faculty of Arch', 1, '', '', '', 'AurangabadÂ ', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1649, 1, 'Manav School of Architecture', 1, '', '', '', 'Akola', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1650, 1, 'Marathwada Mitra Mandalâ€™s College of Architecture', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1651, 1, 'Mulshi Institute of Technology and Research', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1652, 1, 'Narsee Monjee Institute of Management Studies', 1, '', '', '', 'Shirpur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1653, 1, 'Navneet Bahuuddeshya Sanstha Kinkar Institute of Technology', 1, '', '', '', 'Wardha', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1654, 1, 'NBN Sinhgad School of Engineering', 1, '', '', '', 'Ambegaon', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1655, 1, 'NDMVP Samajâ€™s College of Architecture', 1, '', '', '', 'Nashik', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1656, 1, 'Pad. Dr. DY Patil College of Agricultural Engineering and Technology', 1, '', '', '', 'Kolhapur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1657, 1, 'Padmashree Dr. DY Patil College of Architecture', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1658, 1, 'PR Pote (Patil) Education and Welfare Trustâ€™s Group of Institutions College of', 1, '', '', '', 'Amravati', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1659, 1, 'Rajaram Shinde Degree College of Architecture', 1, '', '', '', 'Mumbai', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1660, 1, 'Rajiv Gandhi College of Engineering and Research', 1, '', '', '', 'Nagpur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1661, 1, 'Revera Institute of Technology', 1, '', '', '', 'NaviMumbai', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1662, 1, 'Rizvi College of Architecture', 1, '', '', '', 'Bandra', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1663, 1, 'S K Somaiya Degree College Of Arts Science and Commerce', 1, '', '', '', 'Mumbai', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1664, 1, 'Sharadchandra Pawar College of Engineering', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1665, 1, 'Shri Pandit Anna Patil College of Engineering', 1, '', '', '', 'Paldhi', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1666, 1, 'Shri PD Patil College of Engineering', 1, '', '', '', 'Paldhi', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1667, 1, 'Shri Shivaji Institute of Engineering and Management Studies', 1, '', '', '', 'Parbhani', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1668, 1, 'Shri. Babulalji Agnihotri College of Engineering', 1, '', '', '', 'Wardha', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1669, 1, 'Sinhgad College of Architecture', 1, '', '', '', 'Pune', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1670, 1, 'SND College of Engineering and Research Centre', 1, '', '', '', 'Nashik', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1671, 1, 'SNDT Womens University', 1, '', '', '', 'Mumbai', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1672, 1, 'Tulsiramji Gaikwad-Patil College of Architecture', 1, '', '', '', 'Nagpur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1673, 1, 'Vidarbha Institute of Technology', 1, '', '', '', 'Nagpur', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1674, 1, 'VJ Shinde Engineering College', 1, '', '', '', 'Osmanabad', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1675, 1, 'VPMâ€™s Maharshi Parshuram College of Engineering', 1, '', '', '', 'Ratnagiri', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1676, 1, 'Yashwantrao Chavan Maharashtra Open University', 1, '', '', '', 'Nashik', 'Maharashtra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1677, 1, 'Indian Institute of Technology', 1, '', '', '', 'Gandhinagar', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1678, 1, 'Sardar Vallabhbhai National Institute of Technology', 1, '', '', '', 'Surat', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1679, 1, 'Nirma University', 1, '', '', '', 'Ahmedabad', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1680, 1, 'Maharaja Sayajirao University of Baroda', 1, '', '', '', 'Vadodara', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `college` (`id`, `active_status`, `college_name`, `institute_type`, `naac_accredition`, `address_1`, `address_2`, `city`, `state`, `zipcode`, `email`, `phone`, `fax`, `industry_interaction`, `fee`, `placement_details`, `quality_of_students_and_crowd`, `international_exposure`, `international_or_india_tie_ups`, `hostel_facility`, `aicte_approved`, `scholarships`, `facility_to_stay_near_by`, `sports_infrastructure`, `library`) VALUES
(1681, 1, 'Dhirubhai Ambani Institute of Information and Communication Technology', 1, '', '', '', 'Gandhinagar', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1682, 1, 'Pandit Deendayal Petroleum University', 1, '', '', '', 'Gandhinagar', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1683, 1, 'Dharmsinh Desai University', 1, '', '', '', 'Nadiad', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1684, 1, 'Charotar University of Science and Technology', 1, '', '', '', 'Petlad', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1685, 1, 'CEPT University', 1, '', '', '', 'Ahmedabad', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1686, 1, 'Vishwakarma Government Engineering College', 1, '', '', '', 'Ahmedabad', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1687, 1, 'LD College of Engineering', 1, '', '', '', 'Ahmedabad', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1688, 1, 'Institute of Infrastructure Technology Research and Management', 1, '', '', '', 'Ahmedabad', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1689, 1, 'Institute of Information and Communication Technology', 1, '', '', '', 'Ahmedabad', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1690, 1, 'Government Engineering College', 1, '', '', '', 'Surat', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1691, 1, 'Shree Swami Atmanand Saraswati Institute of Technology', 1, '', '', '', 'Surat', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1692, 1, 'Sarvajanik College of Engineering and Technology', 1, '', '', '', 'Surat', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1693, 1, 'Sardar Vallabhbhai Institute of Technology', 1, '', '', '', 'Anand', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1694, 1, 'Navrachana University', 1, '', '', '', 'Vadodara', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1695, 1, 'Lukhdhirji Engineering College', 1, '', '', '', 'Morbi', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1696, 1, 'LDRP Institute of Technology and Research', 1, '', '', '', 'Gandhinagar', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1697, 1, 'ITM Vocational University', 1, '', '', '', 'Vadodara', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1698, 1, 'Indus Institute of Technology and Engineering', 1, '', '', '', 'Ahmedabad', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1699, 1, 'Government Engineering College', 1, '', '', '', 'Valsad', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1700, 1, 'Government Engineering College', 1, '', '', '', 'Rajkot', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1701, 1, 'Government Engineering College', 1, '', '', '', 'Modasa', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1702, 1, 'Government Engineering College', 1, '', '', '', 'Gandhinagar', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1703, 1, 'Government Engineering College', 1, '', '', '', 'Bhavnagar', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1704, 1, 'Government Engineering College', 1, '', '', '', 'Bharuch', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1705, 1, 'GH Patel College of Engineering and Technology', 1, '', '', '', 'Anand', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1706, 1, 'Chandubhai S Patel Institute of Technology', 1, '', '', '', 'Anand', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1707, 1, 'Birla Vishvakarma Mahavidyalaya', 1, '', '', '', 'Anand', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1708, 1, 'AD Patel Institute of Technology', 1, '', '', '', 'Anand', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1709, 1, 'Arvindbhai Patel Institute of Environmental Design', 1, '', '', '', 'Anand', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1710, 1, 'AY Dadabhai Technical Institute', 1, '', '', '', 'Surat', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1711, 1, 'Central Institute of Plastics Engineering and Technology', 1, '', '', '', 'Ahmedabad', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1712, 1, 'CEPT University- Faculty of Architecture', 1, '', '', '', 'Ahmedabad', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1713, 1, 'Gujarat Forensic Sciences University', 1, '', '', '', 'Gandhinagar', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1714, 1, 'Hemchandracharya North Gujarat University', 1, '', '', '', 'Patan', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1715, 1, 'Indus University', 1, '', '', '', 'Ahmedabad', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1716, 1, 'ITM- School of Architecture Art and Design', 1, '', '', '', 'Vadodara', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1717, 1, 'Krantiguru Shyamji Krishna Verma Kachchh University', 1, '', '', '', 'Bhuj', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1718, 1, 'Navsari Agricultural University', 1, '', '', '', 'Navsari', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1719, 1, 'Parul Institute of Architecture and Research', 1, '', '', '', 'Vadodara', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1720, 1, 'Rai University', 1, '', '', '', 'Ahmedabad', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1721, 1, 'RK University', 1, '', '', '', 'Rajkot', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1722, 1, 'SAL Engineering and Technical Institute', 1, '', '', '', 'Ahmedabad', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1723, 1, 'Sardar Patel College Of Engineering', 1, '', '', '', 'Bakrol', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1724, 1, 'Sardarkrushinagar Dantiwada Agricultural University', 1, '', '', '', 'Sardarkrushinagar', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1725, 1, 'Shri JM Sabva Institute of Engineering and Technology', 1, '', '', '', 'Botad', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1726, 1, 'Shri Sâ€™ad Vidya Mandal Institute of Technology', 1, '', '', '', 'Bharuch', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1727, 1, 'UKA Tarsadia University', 1, '', '', '', 'Bardoli', 'Gujarat', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1728, 1, 'Veer Surendra Sai University of Technology', 1, '', '', '', 'Sambalpur', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1729, 1, 'Silicon Institute of Technology', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1730, 1, 'National Institute of Technology', 1, '', '', '', 'Rourkela', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1731, 1, 'National Institute of Science Education and Research', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1732, 1, 'Indira Gandhi Institute of Technology', 1, '', '', '', 'Sarang', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1733, 1, 'Indian Institute of Technology', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1734, 1, 'College of Engineering and Technology ', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1735, 1, 'Parala Maharaja Engineering College', 1, '', '', '', 'Berhampur', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1736, 1, 'Orissa School of Mining Engineering', 1, '', '', '', 'Keonjhar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1737, 1, 'Kalinga Institute of Industrial Technology', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1738, 1, 'International Institute of Information Technology', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1739, 1, 'Government College of Engineering', 1, '', '', '', 'Kalahandi', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1740, 1, 'CV Raman College of Engineering', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1741, 1, 'Orissa Engineering College', 1, '', '', '', 'Khordha', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1742, 1, 'National Institute of Science and Technology', 1, '', '', '', 'Berhampur', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1743, 1, 'Gandhi Institute of Engineering and Technology', 1, '', '', '', 'Gunupur', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1744, 1, 'Gandhi Institute for Technological Advancement', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1745, 1, 'College of Engineering', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1746, 1, 'Centurion University', 1, '', '', '', 'Paralakhemundi', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1747, 1, 'Central Institute of Plastic Engineering and Technology', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1748, 1, 'Vignan Institute of Technology and Management', 1, '', '', '', 'Berhampur', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1749, 1, 'Trident Academy of Technology', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1750, 1, 'Silicon Institute of Technology', 1, '', '', '', 'Sambalpur', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1751, 1, 'Radhakrishna Institute of Engineering and Technology', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1752, 1, 'Padmanava College of Engineering', 1, '', '', '', 'Rourkela', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1753, 1, 'Krupajal Engineering College', 1, '', '', '', 'Khordha', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1754, 1, 'Gandhi Institute for Education and Technology', 1, '', '', '', 'Khordha', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1755, 1, 'Gandhi Engineering College', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1756, 1, 'Bhubaneswar Institute of Technology', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1757, 1, 'Aum Sai Institute of Technical Education', 1, '', '', '', 'Berhampur', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1758, 1, 'Bhubaneshwar Institute of Industrial Technology', 1, '', '', '', 'Bhubaneshwar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1759, 1, 'Biju Patnaik University of Technology', 1, '', '', '', 'Rourkela', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1760, 1, 'Centurion Institute of Technology', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1761, 1, 'Dhaneshwar Rath Institute of Engineering and Management', 1, '', '', '', 'Cuttack', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1762, 1, 'Gandhi Institute of Excellent Technocrats', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1763, 1, 'Ganesh Institute of Engineering and Technology', 1, '', '', '', 'Bhubaneswar', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1764, 1, 'Gopal Krishna College of Engineering and Technology', 1, '', '', '', 'Koraput', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1765, 1, 'Gurukul College of Engineering for Women', 1, '', '', '', 'Khordha', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1766, 1, 'Kaustav Institute of Self Domain', 1, '', '', '', 'Khordha', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1767, 1, 'Koustuv Institute of Self Domain', 1, '', '', '', 'Patia', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1768, 1, 'Koustuv School of Engineering', 1, '', '', '', 'Patia', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1769, 1, 'Piloo Mody College of Architecture', 1, '', '', '', 'Cuttack', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1770, 1, 'Rahul Institute of Engineering and Technology', 1, '', '', '', 'Behrampur', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1771, 1, 'Ramarani Institute of Technology', 1, '', '', '', 'Balasore', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1772, 1, 'Rourkela Institute of Technology', 1, '', '', '', 'Rourkela', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1773, 1, 'Sambalpur University', 1, '', '', '', 'Sambalpur', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1774, 1, 'Vikash Institute of Technology Or Vikash College of Engineering for Women', 1, '', '', '', 'Bargarh', 'Orissa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1775, 1, 'Indian School of Mines', 1, '', '', '', 'Dhanbad', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1776, 1, 'Birla Institute of Technology', 1, '', '', '', 'Mesra', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1777, 1, 'National Institute of Technology', 1, '', '', '', 'Jamshedpur', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1778, 1, 'National Institute of Foundry and Forge Technology', 1, '', '', '', 'Ranchi', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1779, 1, 'BIT Sindri', 1, '', '', '', 'Dhanbad', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1780, 1, 'Alice Institute of Technology', 1, '', '', '', 'Ranchi', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1781, 1, 'BA College of Engineering and Technology', 1, '', '', '', 'EastSinghbhum', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1782, 1, 'Cambridge Institute of Technology', 1, '', '', '', 'Ranchi', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1783, 1, 'Central University of Jharkhand', 1, '', '', '', 'Ranchi', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1784, 1, 'DAV Institute of Engineering and Technology', 1, '', '', '', 'Palamu', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1785, 1, 'Guru Gobind Singh Educational Societyâ€™s Technical Campus', 1, '', '', '', 'Seraikela', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1786, 1, 'ICFAI University', 1, '', '', '', 'Jharkhand', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1787, 1, 'Indo Danish Tool Room', 1, '', '', '', 'EastSinghbhum', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1788, 1, 'Jharkhand Rai University', 1, '', '', '', 'Ranchi', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1789, 1, 'KK College of Engineering and Management', 1, '', '', '', 'Dhanbad', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1790, 1, 'Maryland Institute of Technology and Management', 1, '', '', '', 'Galudih', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1791, 1, 'Nilai Institute of Technology', 1, '', '', '', 'Ranchi', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1792, 1, 'Ramachandra Chandravansi Institute of Technology', 1, '', '', '', 'Palamu', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1793, 1, 'Ramgovind Institute of Technology', 1, '', '', '', 'Koderma', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1794, 1, 'Ranchi University', 1, '', '', '', 'Ranchi', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1795, 1, 'RTC Institute of Technology', 1, '', '', '', 'Ranchi', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1796, 1, 'RVS College of Engineering and Technology', 1, '', '', '', 'EastSinghbhum', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1797, 1, 'Sai Nath University', 1, '', '', '', 'Ranchi', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1798, 1, 'Vinoba Bhave University', 1, '', '', '', 'Hazaribagh', 'Jharkhand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1799, 1, 'National Institute of Technology', 1, '', '', '', 'Goa', 'Goa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1800, 1, 'BITS Pilani', 1, '', '', '', 'GoaCampus', 'Goa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1801, 1, 'Goa College of Engineering', 1, '', '', '', 'Farmagudi', 'Goa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1802, 1, 'Agnel Institute of Technology and Design', 1, '', '', '', 'Goa', 'Goa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1803, 1, 'Don Bosco College of Engineering', 1, '', '', '', 'Goa', 'Goa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1804, 1, 'Goa College of Architecture', 1, '', '', '', 'Goa', 'Goa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1805, 1, 'Institute of Shipbuilding Technology', 1, '', '', '', 'Goa', 'Goa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1806, 1, 'Padre Conceicao College of Engineering', 1, '', '', '', 'Goa', 'Goa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1807, 1, 'Shree Rayeshwar Institute of Engineering and Information Technology', 1, '', '', '', 'Goa', 'Goa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1816, 1, 'HKBK', 0, '', '', '', 'Bangalore', 'Karnataka', '560084', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1817, 1, 'Jai narain college of technology', 0, '', '', '', 'Bhopal', 'Madhya Pradesh', '462001', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1818, 1, 'Test College', 1, 'asdasd', 'asdsadas', 'asdasdas', 'dasdas', 'Madhya Pradesh', 'asdasdsad', 'sadasd@asd', '9739930818', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `college_admin`
--

CREATE TABLE IF NOT EXISTS `college_admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `college_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `college_admin`
--

INSERT INTO `college_admin` (`id`, `college_id`, `user_id`) VALUES
(1, 246, 33);

-- --------------------------------------------------------

--
-- Table structure for table `college_alumni_users_intranet`
--

CREATE TABLE IF NOT EXISTS `college_alumni_users_intranet` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `college_users_intranet_id` bigint(20) NOT NULL,
  `stream_id` bigint(20) NOT NULL,
  `stream_course_id` bigint(20) NOT NULL,
  `year_of_passing` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `college_alumni_users_intranet`
--

INSERT INTO `college_alumni_users_intranet` (`id`, `college_users_intranet_id`, `stream_id`, `stream_course_id`, `year_of_passing`, `timestamp`) VALUES
(1, 1, 1, 101, 2008, '2015-10-06 10:59:48');

-- --------------------------------------------------------

--
-- Table structure for table `college_clubs_and_activities`
--

CREATE TABLE IF NOT EXISTS `college_clubs_and_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `college_id` int(11) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `college_clubs_and_activities`
--

INSERT INTO `college_clubs_and_activities` (`id`, `college_id`, `title`, `description`) VALUES
(1, 1818, '', ''),
(2, 246, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `college_extra_curricular`
--

CREATE TABLE IF NOT EXISTS `college_extra_curricular` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `college_id` int(11) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `college_extra_curricular`
--

INSERT INTO `college_extra_curricular` (`id`, `college_id`, `title`, `description`) VALUES
(1, 1818, '', ''),
(2, 246, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `college_faculty`
--

CREATE TABLE IF NOT EXISTS `college_faculty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `college_id` int(11) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `college_faculty`
--

INSERT INTO `college_faculty` (`id`, `college_id`, `title`, `description`) VALUES
(1, 1818, '', ''),
(2, 246, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `college_festivals`
--

CREATE TABLE IF NOT EXISTS `college_festivals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `college_id` int(11) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `college_festivals`
--

INSERT INTO `college_festivals` (`id`, `college_id`, `title`, `description`) VALUES
(1, 1818, '', ''),
(2, 246, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `college_fixed_group_posts`
--

CREATE TABLE IF NOT EXISTS `college_fixed_group_posts` (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_group_name` varchar(15) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `college_id` bigint(20) NOT NULL,
  `post_like` bigint(20) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `college_fixed_group_posts`
--

INSERT INTO `college_fixed_group_posts` (`post_id`, `post_group_name`, `user_id`, `college_id`, `post_like`, `timestamp`) VALUES
(1, 'jobopening', 33, 246, 0, '2015-10-17 10:17:52'),
(2, 'jobopening', 33, 246, 0, '2015-10-17 10:18:09'),
(3, 'jobopening', 33, 246, 0, '2015-10-17 10:19:21'),
(4, 'jobopening', 33, 246, 0, '2015-10-17 10:22:14'),
(5, 'jobopening', 33, 246, 0, '2015-10-17 10:22:47'),
(6, 'jobopening', 33, 246, 0, '2015-10-19 05:55:59'),
(7, 'jobopening', 33, 246, 0, '2015-10-20 09:09:07'),
(8, 'hostel', 33, 246, 0, '2015-10-21 13:53:29'),
(9, 'hostel', 33, 246, 0, '2015-10-21 13:53:52'),
(10, 'hostel', 33, 246, 0, '2015-10-21 13:57:42');

-- --------------------------------------------------------

--
-- Table structure for table `college_fixed_group_posts_details`
--

CREATE TABLE IF NOT EXISTS `college_fixed_group_posts_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_message` text NOT NULL,
  `post_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `college_fixed_group_posts_details`
--

INSERT INTO `college_fixed_group_posts_details` (`id`, `post_message`, `post_id`) VALUES
(1, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 1),
(2, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 2),
(3, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 3),
(4, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 4),
(5, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 5),
(6, 'If it has been determined that you will join Boston College on a temporary basis, please submit your application and resume. Your resume must be in Word or PDF format in order to be attached. Instructions are provided.', 6),
(7, 'If it has been determined that you will join Boston College on a temporary basis, please submit your application and resume. Your resume must be in Word or PDF format in order to be attached. Instructions are provided.', 7),
(8, 'Lorem ipsum dolor sit amet, elit. Praesent eu nibh in sapien blandit finibus. Integer in ipsum efficitur, volutpat dui a, varius lectus. Nam aliquet accumsan accumsan. Ut non nulla eu metus consectetur maximus. Mauris id odio vel odio fermentum laoreet. Etiam tempor leo placerat ipsum interdum, eget ultrices sem ultrices. In id leo neque. Aliquam tempus consequat mauris quis rhoncus. Maecenas nisl nulla, bibendum in metus nec,cubilia Curae;', 8),
(9, 'Lorem ipsum dolor sit amet, elit. Praesent eu nibh in sapien blandit finibus. Integer in ipsum efficitur, volutpat dui a, varius lectus. Nam aliquet accumsan accumsan. Ut non nulla eu metus consectetur maximus. Mauris id odio vel odio fermentum laoreet. Etiam tempor leo placerat ipsum interdum, eget ultrices sem ultrices. In id leo neque. Aliquam tempus consequat mauris quis rhoncus. Maecenas nisl nulla, bibendum in metus nec,cubilia Curae;', 9),
(10, 'Lorem ipsum dolor sit amet, elit. Praesent eu nibh in sapien blandit finibus. Integer in ipsum efficitur, volutpat dui a, varius lectus. Nam aliquet accumsan accumsan. Ut non nulla eu metus consectetur maximus. Mauris id odio vel odio fermentum laoreet. Etiam tempor leo placerat ipsum interdum, eget ultrices sem ultrices. In id leo neque. Aliquam tempus consequat mauris quis rhoncus. Maecenas nisl nulla, bibendum in metus nec,cubilia Curae; Lorem ipsum dolor sit amet, elit. Praesent eu nibh in sapien blandit finibus. Integer in ipsum efficitur, volutpat dui a, varius lectus. Nam aliquet accumsan accumsan. Ut non nulla eu metus consectetur maximus. Mauris id odio vel odio fermentum laoreet. Etiam tempor leo placerat ipsum interdum, eget ultrices sem ultrices. In id leo neque. Aliquam tempus consequat mauris quis rhoncus. Maecenas nisl nulla, bibendum in metus nec,cubilia Curae;', 10);

-- --------------------------------------------------------

--
-- Table structure for table `college_fixed_group_posts_files`
--

CREATE TABLE IF NOT EXISTS `college_fixed_group_posts_files` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_path` text NOT NULL,
  `file_type` varchar(100) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `college_fixed_group_posts_files`
--

INSERT INTO `college_fixed_group_posts_files` (`id`, `file_path`, `file_type`, `post_id`) VALUES
(1, 'assets_front/image/group_post_files/17102015030648-Technology-Wallpaper-6.jpg', '', 2),
(2, 'assets_front/image/group_post_files/17102015031149-wallpaper-photos-4.jpg', '', 3),
(3, 'assets_front/image/group_post_files/17102015031649-Technology-Wallpaper-6.jpg', '', 3),
(4, 'assets_front/image/group_post_files/17102015030852-Technology-Wallpaper-6.jpg', '', 4),
(5, 'assets_front/image/group_post_files/17102015034252-Anonymous-Wallpaper-HD-1920x1080.jpg', '', 5);

-- --------------------------------------------------------

--
-- Table structure for table `college_fixed_group_posts_like_details`
--

CREATE TABLE IF NOT EXISTS `college_fixed_group_posts_like_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `college_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `college_fixed_group_posts_like_details`
--

INSERT INTO `college_fixed_group_posts_like_details` (`id`, `post_id`, `college_id`, `user_id`, `timestamp`) VALUES
(5, 5, 246, 33, '2015-10-20 09:07:59'),
(6, 6, 246, 33, '2015-10-20 09:08:54'),
(8, 7, 246, 33, '2015-10-20 12:53:41');

-- --------------------------------------------------------

--
-- Table structure for table `college_fixed_group_posts_reply`
--

CREATE TABLE IF NOT EXISTS `college_fixed_group_posts_reply` (
  `post_reply_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `college_id` bigint(20) NOT NULL,
  `reply_message` text NOT NULL,
  `reply_like` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`post_reply_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `college_fixed_group_posts_reply`
--

INSERT INTO `college_fixed_group_posts_reply` (`post_reply_id`, `post_id`, `user_id`, `college_id`, `reply_message`, `reply_like`, `timestamp`) VALUES
(1, 6, 33, 246, 'asdasdasdas', 0, '2015-10-20 09:07:37'),
(2, 6, 33, 246, 'sdasdasdasd', 0, '2015-10-20 09:08:14');

-- --------------------------------------------------------

--
-- Table structure for table `college_fixed_group_posts_reply_like_details`
--

CREATE TABLE IF NOT EXISTS `college_fixed_group_posts_reply_like_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_reply_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `college_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `college_hostel_description`
--

CREATE TABLE IF NOT EXISTS `college_hostel_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostel_description` text NOT NULL,
  `college_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `college_hostel_description`
--

INSERT INTO `college_hostel_description` (`id`, `hostel_description`, `college_id`) VALUES
(1, 'Lorem ipsum dolor sit amet, elit. Praesent eu nibh in sapien blandit finibus. Integer in ipsum efficitur, volutpat dui a, varius lectus. Nam aliquet accumsan accumsan. Ut non nulla eu metus consectetur maximus. Mauris id odio vel odio fermentum laoreet. Etiam tempor leo placerat ipsum interdum, eget ultrices sem ultrices. In id leo neque. Aliquam tempus consequat mauris quis rhoncus. Maecenas nisl nulla, bibendum in metus nec,cubilia Curae; Lorem ipsum dolor sit amet, elit. Praesent eu nibh in sapien blandit finibus. Integer in ipsum efficitur, volutpat dui a, varius lectus. Nam aliquet accumsan accumsan. Ut non nulla eu metus consectetur maximus. Mauris id odio vel odio fermentum laoreet. Etiam tempor leo placerat ipsum interdum, eget ultrices sem ultrices. In id leo neque. Aliquam tempus consequat mauris quis rhoncus. Maecenas nisl nulla, bibendum in metus nec,cubilia Curae;', 246);

-- --------------------------------------------------------

--
-- Table structure for table `college_image`
--

CREATE TABLE IF NOT EXISTS `college_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `college_id` int(11) NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `college_institute_type`
--

CREATE TABLE IF NOT EXISTS `college_institute_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `college_institute_type`
--

INSERT INTO `college_institute_type` (`id`, `title`) VALUES
(1, 'Affiliated'),
(2, 'Autonomous');

-- --------------------------------------------------------

--
-- Table structure for table `college_jobopenings_list`
--

CREATE TABLE IF NOT EXISTS `college_jobopenings_list` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `college_id` bigint(20) NOT NULL,
  `job_title` text NOT NULL,
  `job_description` text NOT NULL,
  `job_added_timestamp` varchar(50) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `college_jobopenings_list`
--

INSERT INTO `college_jobopenings_list` (`job_id`, `college_id`, `job_title`, `job_description`, `job_added_timestamp`, `timestamp`) VALUES
(2, 246, 'Software Developer', 'Information about applying for positions at Boston College, and tips on using our online tool, are available for Scholar Space employees and external applicants.And then I display them in a foreach loop . But it is only able to display the 2nd query results or in this case display only categories in one select box and not the items in other select box.I am doing this for the first time so I don''t know how to do it.', '2015-10-19 01:59:16', '2015-10-19 11:40:25'),
(4, 246, 'Software Tester', 'And then I display them in a foreach loop . But it is only able to display the 2nd query results or in this case display only categories in one select box and not the items in other select box.I am doing this for the first time so I don''t know how to do it.', '2015-10-19 05:10:15', '2015-10-19 11:40:15'),
(5, 246, 'Software Tester - 1', 'And then I display them in a foreach loop . But it is only able to display the 2nd query results or in this case display only categories in one select box and not the items in other select box.I am doing this for the first time so I don''t know how to do it.And then I display them in a foreach loop . But it is only able to display the 2nd query results or in this case display only categories in one select box and not the items in other select box.I am doing this for the first time so I don''t know how to do it.And then I display them in a foreach loop . But it is only able to display the 2nd query results or in this case display only categories in one select box and not the items in other select box.I am doing this for the first time so I don''t know how to do it.', '2015-10-19 05:14:54', '2015-10-19 11:44:54'),
(7, 246, 'Software Tester - 2', 'And then I display them in a foreach loop . But it is only able to display the 2nd query results or in this case display only categories in one select box and not the items in other select box.I am doing this for the first time so I don''t know how to do it.And then I display them in a foreach loop . But it is only able to display the 2nd query results or in this case display only categories in one select box and not the items in other select box.I am doing this for the first time so I don''t know how to do it.And then I display them in a foreach loop . But it is only able to display the 2nd query results or in this case display only categories in one select box and not the items in other select box.I am doing this for the first time so I don''t know how to do it.', '2015-10-19 05:37:06', '2015-10-19 12:07:50'),
(9, 246, 'Software Tester - 3', 'And then I display them in a foreach loop . But it is only able to display the 2nd query results or in this case display only categories in one select box and not the items in other select box.I am doing this for the first time so I don''t know how to do it.And then I display them in a foreach loop . But it is only able to display the 2nd query results or in this case display only categories in one select box and not the items in other select box.I am doing this for the first time so I don''t know how to do it.And then I display them in a foreach loop . But it is only able to display the 2nd query results or in this case display only categories in one select box and not the items in other select box.I am doing this for the first time so I don''t know how to do it.', '2015-10-20 01:22:45', '2015-10-20 07:52:45'),
(10, 246, 'Software Tester - 4', 'And then I display them in a foreach loop . But it is only able to display the 2nd query results or in this case display only categories in one select box and not the items in other select box.I am doing this for the first time so I don''t know how to do it.And then I display them in a foreach loop . But it is only able to display the 2nd query results or in this case display only categories in one select box and not the items in other select box.I am doing this for the first time so I don''t know how to do it.And then I display them in a foreach loop . But it is only able to display the 2nd query results or in this case display only categories in one select box and not the items in other select box.I am doing this for the first time so I don''t know how to do it.', '2015-10-20 01:23:13', '2015-10-20 08:49:10');

-- --------------------------------------------------------

--
-- Table structure for table `college_laboratories`
--

CREATE TABLE IF NOT EXISTS `college_laboratories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `college_id` int(11) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `college_laboratories`
--

INSERT INTO `college_laboratories` (`id`, `college_id`, `title`, `description`) VALUES
(1, 1818, '', ''),
(2, 246, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `college_registration_request_by_users`
--

CREATE TABLE IF NOT EXISTS `college_registration_request_by_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `college_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `college_registration_request_by_users`
--

INSERT INTO `college_registration_request_by_users` (`id`, `college_id`, `user_id`) VALUES
(6, 1816, 37),
(7, 1817, 36);

-- --------------------------------------------------------

--
-- Table structure for table `college_streams`
--

CREATE TABLE IF NOT EXISTS `college_streams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `college_id` int(11) NOT NULL,
  `stream_id` int(11) NOT NULL,
  `stream_type` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `college_streams`
--

INSERT INTO `college_streams` (`id`, `college_id`, `stream_id`, `stream_type`) VALUES
(13, 1818, 6, 'existing'),
(14, 1818, 1, 'existing'),
(16, 246, 1, 'existing'),
(17, 246, 9, 'existing');

-- --------------------------------------------------------

--
-- Table structure for table `college_stream_course`
--

CREATE TABLE IF NOT EXISTS `college_stream_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_type` varchar(500) NOT NULL,
  `course_id` int(11) NOT NULL,
  `stream_id` int(11) NOT NULL,
  `stream_type` varchar(500) NOT NULL,
  `college_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `college_stream_course`
--

INSERT INTO `college_stream_course` (`id`, `course_type`, `course_id`, `stream_id`, `stream_type`, `college_id`) VALUES
(25, 'existing', 253, 6, 'existing', 1818),
(26, 'contemporary', 7, 6, 'existing', 1818),
(27, 'existing', 31, 1, 'existing', 1818),
(28, 'existing', 32, 1, 'existing', 1818),
(33, 'existing', 79, 1, 'existing', 246),
(34, 'existing', 96, 1, 'existing', 246),
(35, 'existing', 101, 1, 'existing', 246),
(36, 'existing', 104, 1, 'existing', 246),
(37, 'existing', 255, 9, 'existing', 246),
(38, 'existing', 256, 9, 'existing', 246);

-- --------------------------------------------------------

--
-- Table structure for table `college_student_users_intranet`
--

CREATE TABLE IF NOT EXISTS `college_student_users_intranet` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `college_users_intranet_id` bigint(20) NOT NULL,
  `stream_id` bigint(20) NOT NULL,
  `stream_course_id` bigint(20) NOT NULL,
  `study_type` varchar(100) NOT NULL,
  `semester_or_year` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `college_student_users_intranet`
--

INSERT INTO `college_student_users_intranet` (`id`, `college_users_intranet_id`, `stream_id`, `stream_course_id`, `study_type`, `semester_or_year`, `timestamp`) VALUES
(1, 2, 1, 101, 'semester', 1, '2015-10-06 11:00:26'),
(2, 3, 1, 101, 'semester', 1, '2015-10-15 08:38:34');

-- --------------------------------------------------------

--
-- Table structure for table `college_teacher_users_intranet`
--

CREATE TABLE IF NOT EXISTS `college_teacher_users_intranet` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `college_users_intranet_id` bigint(20) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='college_teacher_users_intranet' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `college_users_intranet`
--

CREATE TABLE IF NOT EXISTS `college_users_intranet` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `college_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `intranet_user_type` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `college_users_intranet`
--

INSERT INTO `college_users_intranet` (`id`, `college_id`, `user_id`, `intranet_user_type`, `timestamp`) VALUES
(1, 246, 37, 'alumni', '2015-10-12 13:23:00'),
(2, 246, 39, 'student', '2015-10-12 13:23:00'),
(3, 246, 41, 'student', '2015-10-15 08:38:34');

-- --------------------------------------------------------

--
-- Table structure for table `college_user_request_to_admin`
--

CREATE TABLE IF NOT EXISTS `college_user_request_to_admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `college_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `read_status` int(11) NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `requested_user_type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `college_user_request_to_admin`
--

INSERT INTO `college_user_request_to_admin` (`id`, `college_id`, `user_id`, `read_status`, `timestamp`, `requested_user_type`) VALUES
(24, 246, 38, 0, '2015-10-06 10:48:36', 'student');

-- --------------------------------------------------------

--
-- Table structure for table `college_user_request_to_admin_for_alumni`
--

CREATE TABLE IF NOT EXISTS `college_user_request_to_admin_for_alumni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `stream_id` int(11) NOT NULL,
  `stream_course_id` int(11) NOT NULL,
  `year_of_passing` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `college_user_request_to_admin_for_student`
--

CREATE TABLE IF NOT EXISTS `college_user_request_to_admin_for_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `stream_id` int(11) NOT NULL,
  `stream_course_id` int(11) NOT NULL,
  `study_type` varchar(100) NOT NULL,
  `semester_or_year` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `college_user_request_to_admin_for_student`
--

INSERT INTO `college_user_request_to_admin_for_student` (`id`, `request_id`, `stream_id`, `stream_course_id`, `study_type`, `semester_or_year`) VALUES
(11, 24, 1, 101, 'semester', 1);

-- --------------------------------------------------------

--
-- Table structure for table `college_user_type`
--

CREATE TABLE IF NOT EXISTS `college_user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `college_user_type`
--

INSERT INTO `college_user_type` (`id`, `user_type`) VALUES
(1, 'student'),
(2, 'teacher'),
(3, 'alumni');

-- --------------------------------------------------------

--
-- Table structure for table `college_video`
--

CREATE TABLE IF NOT EXISTS `college_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `college_id` int(11) NOT NULL,
  `video_link` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `college_video`
--

INSERT INTO `college_video` (`id`, `college_id`, `video_link`) VALUES
(1, 1818, ''),
(2, 246, '');

-- --------------------------------------------------------

--
-- Table structure for table `contemporary_college_registration`
--

CREATE TABLE IF NOT EXISTS `contemporary_college_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `college_name` varchar(1000) NOT NULL,
  `college_city` varchar(1000) NOT NULL,
  `college_state` varchar(1000) NOT NULL,
  `college_zipcode` varchar(1000) NOT NULL,
  `requested_user_id` int(11) NOT NULL,
  `is_checked` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contemporary_stream`
--

CREATE TABLE IF NOT EXISTS `contemporary_stream` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contemporary_stream`
--

INSERT INTO `contemporary_stream` (`id`, `title`) VALUES
(1, 'ABCD');

-- --------------------------------------------------------

--
-- Table structure for table `contemporary_stream_courses`
--

CREATE TABLE IF NOT EXISTS `contemporary_stream_courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `contemporary_stream_id` int(11) NOT NULL,
  `stream_type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `contemporary_stream_courses`
--

INSERT INTO `contemporary_stream_courses` (`id`, `title`, `contemporary_stream_id`, `stream_type`) VALUES
(4, 'adasdas', 1, 'contemporary'),
(5, 'adasdasasdsa', 1, 'contemporary'),
(6, 'asdasd', 1, 'contemporary'),
(7, 'ABCDEFGHI', 6, 'existing');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `institution_types`
--

CREATE TABLE IF NOT EXISTS `institution_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `active_status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `institution_types`
--

INSERT INTO `institution_types` (`id`, `title`, `active_status`) VALUES
(1, 'Autonomus', 1),
(2, 'Job Opening', 0),
(3, 'Internship', 0);

-- --------------------------------------------------------

--
-- Table structure for table `intranet_default_groups`
--

CREATE TABLE IF NOT EXISTS `intranet_default_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_title` varchar(1000) NOT NULL,
  `flag` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `intranet_default_groups`
--

INSERT INTO `intranet_default_groups` (`id`, `group_title`, `flag`) VALUES
(1, 'Courses', 'courses'),
(2, 'Job Opening', 'jobopening'),
(3, 'Internship', 'internship'),
(4, 'Hostel', 'hostel'),
(5, 'Fees', 'fees'),
(6, 'Tools', 'tools');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE IF NOT EXISTS `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `title`) VALUES
(1, 'Andhra Pradesh'),
(2, 'Arunachal Pradesh'),
(3, 'Assam'),
(4, 'Bihar'),
(5, 'Chhattisgarh'),
(6, 'Delhi'),
(7, 'Goa'),
(8, 'Gujarat'),
(9, 'Haryana'),
(10, 'Himachal Pradesh'),
(11, 'Jammu & Kashmir'),
(12, 'Jharkhand'),
(13, 'Karnataka'),
(14, 'Kerala'),
(15, 'Madhya Pradesh'),
(16, 'Maharashtra'),
(17, 'Manipur'),
(18, 'Meghalaya'),
(19, 'Mizoram'),
(20, 'Nagaland'),
(21, 'Orissa'),
(22, 'Punjab'),
(23, 'Rajasthan'),
(24, 'Tamil Nadu'),
(25, 'Telangana'),
(26, 'Tripura'),
(27, 'Uttar Pradesh'),
(28, 'Uttarakhand'),
(29, 'West Bengal');

-- --------------------------------------------------------

--
-- Table structure for table `stream`
--

CREATE TABLE IF NOT EXISTS `stream` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `active_status` varchar(5) NOT NULL,
  `study_type` varchar(100) NOT NULL,
  `study_type_count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `stream`
--

INSERT INTO `stream` (`id`, `title`, `active_status`, `study_type`, `study_type_count`) VALUES
(1, 'Engineering', '1', 'semester', 8),
(6, 'BCA', '1', 'semester', 6),
(7, 'MBBS', '1', 'semester', 11),
(9, 'BSC', '1', 'semester', 6);

-- --------------------------------------------------------

--
-- Table structure for table `stream_courses`
--

CREATE TABLE IF NOT EXISTS `stream_courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1000) NOT NULL,
  `stream_id` int(11) NOT NULL,
  `active_status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=257 ;

--
-- Dumping data for table `stream_courses`
--

INSERT INTO `stream_courses` (`id`, `title`, `stream_id`, `active_status`) VALUES
(31, 'Sugar Technology', 1, 1),
(32, 'Systems Science Engineering', 1, 1),
(33, 'Transportation Engineering', 1, 1),
(34, 'Water Management', 1, 1),
(35, 'Automation and Robotics', 1, 1),
(36, 'Disaster management', 1, 1),
(37, 'Environmental Engineering ', 1, 1),
(38, 'Environmental Engineering', 1, 1),
(39, 'Leather Technology', 1, 1),
(40, 'Materials Science and Technology', 1, 1),
(41, 'Nuclear Engineering', 1, 1),
(42, 'Plastic Engineering', 1, 1),
(43, 'Precision Manufacturing', 1, 1),
(44, 'Rubber Technology', 1, 1),
(45, 'Safety and Fire Engineering ', 1, 1),
(46, 'Safety and Fire Engineering', 1, 1),
(47, 'Thermal Engineering', 1, 1),
(48, 'Applied Electronics and Instrumentation Engineering', 1, 1),
(49, 'Electronics Control Systems Engineering', 1, 1),
(50, 'Electronics Engineering ', 1, 1),
(51, 'Electronics Engineering', 1, 1),
(52, 'Electronics Engineering- Design and Manufacturing', 1, 1),
(53, 'Electronics Product Design Technology', 1, 1),
(54, 'Electronics and Communication Engineering ', 1, 1),
(55, 'Electronics and Communication Engineering', 1, 1),
(56, 'Electronics and Computer Engineering', 1, 1),
(57, 'Electronics and Electrical Communication', 1, 1),
(58, 'Electronics and Instrumentation Engineering', 1, 1),
(59, 'Electronics and Media Technology', 1, 1),
(60, 'Electronics and Power Engineering', 1, 1),
(61, 'Electronics and Telecommunication Engineering', 1, 1),
(62, 'Medical Electronics', 1, 1),
(63, 'Power Electronics', 1, 1),
(64, 'Telecommunication', 1, 1),
(65, 'Bio Engineering', 1, 1),
(66, 'Bio-Medical and Instrumentation Engineering', 1, 1),
(67, 'Biochemical Engineering', 1, 1),
(68, 'Biological Sciences and Bioengineering', 1, 1),
(69, 'Biomedical Engineering ', 1, 1),
(70, 'Biomedical Engineering', 1, 1),
(71, 'Biotechnology Engineering', 1, 1),
(72, 'Biotechnology and Biochemical Engineering', 1, 1),
(73, 'Electronics and Biomedical Engineering', 1, 1),
(74, 'Genetic Engineering', 1, 1),
(75, 'Industrial Biotechnology', 1, 1),
(76, 'Pages', 1, 1),
(77, 'Micro-Biology and Immunology Engineering', 1, 1),
(78, 'Mechanical (Automobile)', 1, 1),
(79, 'Mechanical Engineering', 1, 1),
(80, 'Mechanical Engineering- Design and Manufacturing', 1, 1),
(81, 'Mechanical and Automation Engineering', 1, 1),
(82, 'Mechanical and Industrial Engineering', 1, 1),
(83, 'Mechanical(Production) Engineering', 1, 1),
(84, 'Mechanics', 1, 1),
(85, 'Mechatronics Engineering', 1, 1),
(86, 'Tool Engineering', 1, 1),
(87, 'Carpet and Textile Technology', 1, 1),
(88, 'Jute and Fiber Technology', 1, 1),
(89, 'Man Made Fiber Technology', 1, 1),
(90, 'Textile Chemistry', 1, 1),
(91, 'Textile Engineering', 1, 1),
(92, 'Textile Production', 1, 1),
(93, 'Textile Technology ', 1, 1),
(94, 'Textile Technology', 1, 1),
(95, 'Building and Construction Engineering', 1, 1),
(96, 'Civil Engineering', 1, 1),
(97, 'Civil Environmental Engineering', 1, 1),
(98, 'Civil and Structural Engineering', 1, 1),
(99, 'Construction Technology', 1, 1),
(100, 'Construction Technology and Management', 1, 1),
(101, 'Computer Science Engineering ', 1, 1),
(102, 'Computer Science Engineering', 1, 1),
(103, 'Information Science Engineering', 1, 1),
(104, 'Information Technology ', 1, 1),
(105, 'Information Technology', 1, 1),
(106, 'Information and Communication Technology', 1, 1),
(107, 'Mathematics and Computing Engineering', 1, 1),
(108, 'Software Engineering', 1, 1),
(109, 'Electrical Engineering ', 1, 1),
(110, 'Electrical Engineering', 1, 1),
(111, 'Electrical and Communications Engineering', 1, 1),
(112, 'Electrical and Electronics Engineering ', 1, 1),
(113, 'Electrical and Electronics Engineering', 1, 1),
(114, 'Electrical and Instrumentation Engineering', 1, 1),
(115, 'Power Engineering', 1, 1),
(116, 'Industrial Design', 1, 1),
(117, 'Industrial Engineering', 1, 1),
(118, 'Industrial and Production Engineering', 1, 1),
(119, 'Production Engineering', 1, 1),
(120, 'Production Engineering and Management', 1, 1),
(121, 'Chemical Ceramic Technology', 1, 1),
(122, 'Chemical Engineering ', 1, 1),
(123, 'Chemical Engineering', 1, 1),
(124, 'Chemical and Polymer Engineering', 1, 1),
(125, 'Petrochemical Engineering', 1, 1),
(126, 'Dairy Technology ', 1, 1),
(127, 'Dairy Technology', 1, 1),
(128, 'Food Processing and Technology', 1, 1),
(129, 'Food Technology', 1, 1),
(130, 'Food and Biochemical Engineering', 1, 1),
(131, 'Printing Graphics and Packaging', 1, 1),
(132, 'Printing Technology', 1, 1),
(133, 'Printing and Packaging Technology', 1, 1),
(134, 'Pulp and Paper Technology', 1, 1),
(135, 'Aeronautical Engineering', 1, 1),
(136, 'Aerospace Engineering', 1, 1),
(137, 'Aircraft Manufacturing and Maintenance Engineering', 1, 1),
(138, 'Agricultural Engineering', 1, 1),
(139, 'Agricultural and Food Engineering', 1, 1),
(140, 'Agricultural and Irrigation Engineering', 1, 1),
(141, 'Geo informatics', 1, 1),
(142, 'Geosciences and Remote Sensing', 1, 1),
(143, 'Geotechnical Engineering', 1, 1),
(144, 'Marine Engineering', 1, 1),
(145, 'Naval Architecture and Marine Engineering', 1, 1),
(146, 'Naval Architecture and OceanEngineering', 1, 1),
(147, 'Mineral Engineering', 1, 1),
(148, 'Mining Engineering', 1, 1),
(149, 'Mining Machinery Engineering', 1, 1),
(150, 'Oil and Paint Technology', 1, 1),
(151, 'Paint Technology', 1, 1),
(152, 'Petroleum Engineering', 1, 1),
(153, 'Automobile Engineering ', 1, 1),
(154, 'Automobile Engineering', 1, 1),
(155, 'Automotive Engineering', 1, 1),
(156, 'Ceramic Engineering', 1, 1),
(157, 'Ceramics and Cement Engineering', 1, 1),
(158, 'Fashion Technology ', 1, 1),
(159, 'Fashion Technology', 1, 1),
(160, 'Fashion and Apparel Engineering', 1, 1),
(161, 'Engineering Chemistry', 1, 1),
(162, 'Engineering Physics', 1, 1),
(163, 'Instrumentation Technology', 1, 1),
(164, 'Instrumentation and Control Engineering', 1, 1),
(165, 'Manufacturing Engineering', 1, 1),
(166, 'Manufacturing Process and Automation Engineering', 1, 1),
(167, 'Metallurgical Engineering', 1, 1),
(168, 'Metallurgical and Materials Engineering', 1, 1),
(169, 'Nano Technology Engineering', 1, 1),
(170, 'Nano Technology and Robotics', 1, 1),
(171, 'Architecture Engineering', 1, 1),
(172, 'Energy Management', 1, 1),
(173, 'Pharmaceutical Technology', 1, 1),
(174, 'Polymer Technology', 1, 1),
(175, 'Economics', 1, 1),
(176, 'Administrative Management', 1, 1),
(177, 'Agriculture Business Management', 1, 1),
(178, 'Apparel Management', 1, 1),
(179, 'Aviation Management', 1, 1),
(180, 'Banking Management', 1, 1),
(181, 'Banking and Finance', 1, 1),
(182, 'Banking and Insurance', 1, 1),
(183, 'Business Analytics', 1, 1),
(184, 'Business Design', 1, 1),
(185, 'Business Design and Innovation Management', 1, 1),
(186, 'Business Strategy', 1, 1),
(187, 'Capital Market', 1, 1),
(188, 'Communication Management', 1, 1),
(189, 'Construction Management', 1, 1),
(190, 'Corporate Management', 1, 1),
(191, 'Disaster management', 1, 1),
(192, 'E-Business', 1, 1),
(193, 'Economics', 1, 1),
(194, 'Energy Management', 1, 1),
(195, 'Entrepreneurship', 1, 1),
(196, 'Environment Management', 1, 1),
(197, 'European Studies and Management', 1, 1),
(198, 'Family Business Management', 1, 1),
(199, 'Finance', 1, 1),
(200, 'Financial Market', 1, 1),
(201, 'Forestry Management', 1, 1),
(202, 'General Management', 1, 1),
(203, 'Global Operations', 1, 1),
(204, 'Health Care Management', 1, 1),
(205, 'Hospital Management', 1, 1),
(206, 'Hospitality Management', 1, 1),
(207, 'Human Resource Management', 1, 1),
(208, 'Industrial Marketing', 1, 1),
(209, 'Industry Integrated', 1, 1),
(210, 'Information Systems Management', 1, 1),
(211, 'Infrastructure Management', 1, 1),
(212, 'Insurance', 1, 1),
(213, 'International Business', 1, 1),
(214, 'International Business Communication', 1, 1),
(215, 'International Marketing', 1, 1),
(216, 'Logistics and Supply Chain Management', 1, 1),
(217, 'Marketing Management', 1, 1),
(218, 'Materials Management', 1, 1),
(219, 'Media Management', 1, 1),
(220, 'National Management Program', 1, 1),
(221, 'Operations Management', 1, 1),
(222, 'Organisational Behaviour', 1, 1),
(223, 'Petroleum Management', 1, 1),
(224, 'Pharmaceutical Management', 1, 1),
(225, 'Power Management', 1, 1),
(226, 'Product Management', 1, 1),
(227, 'Public Enterprise', 1, 1),
(228, 'Public Policy and Management', 1, 1),
(229, 'Retail Management', 1, 1),
(230, 'Risk Management', 1, 1),
(231, 'Rural Management', 1, 1),
(232, 'Rural Marketing', 1, 1),
(233, 'Sales', 1, 1),
(234, 'Sales and Marketing', 1, 1),
(235, 'Service Management', 1, 1),
(236, 'Strategic Management', 1, 1),
(237, 'Sustainable Development', 1, 1),
(238, 'Systems Management', 1, 1),
(239, 'Technology Management', 1, 1),
(240, 'Telecom Management', 1, 1),
(241, 'Textile Management', 1, 1),
(242, 'Tourism', 1, 1),
(243, 'MBBS', 1, 1),
(253, 'ABCD', 6, 1),
(254, 'EFGH', 6, 1),
(255, 'Computer Science', 9, 1),
(256, 'Biology', 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `encoded_pwd` varchar(20) NOT NULL,
  `profile_picture` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `user_type`, `user_type_id`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `encoded_pwd`, `profile_picture`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', 'super-admin', 1, '', NULL, NULL, NULL, 1268889823, 1445407637, 1, 'Admin', 'istrator', 'ADMIN', '9755576574', 'cGFzc3dvcmQ=', 'assets_front/image/user_profile_pic/user_default_profile.jpg'),
(11, '::1', '', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', NULL, 'anas@hexwhale.com', 'data-entry', 1, NULL, NULL, NULL, 'vmgn7p.L.JPlDs5W82vQ8.', 1440144455, 1443779382, 1, 'Anas', 'KM', NULL, '9981438759', 'cGFzc3dvcmQ=', 'assets_front/image/user_profile_pic/user_default_profile.jpg'),
(33, '::1', '', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', NULL, 'jogendra@hexwhale.com', 'college-admin', 4, NULL, NULL, NULL, NULL, 1442512445, 1445424645, 1, 'Jogendra', 'Yadav', NULL, '9739930818', 'cGFzc3dvcmQ=', 'assets_front/image/user_profile_pic/user_default_profile.jpg'),
(34, '::1', '', '$2y$08$lWrkiujpDwmmZO904HygO.aJNUJdFxZ2eUWuXYzNnLrjsTpCs3Ary', NULL, 'user1@user.com', 'college-admin', 4, NULL, NULL, NULL, NULL, 1442556979, NULL, 1, 'User1', 'User', NULL, '9424473647', 'TTJGN1FMRUs=', 'assets_front/image/user_profile_pic/user_default_profile.jpg'),
(36, '::1', '', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', NULL, 'mahadev@hexwhale.com', 'front-end-user', 3, NULL, NULL, NULL, NULL, 1442561478, 1445406071, 1, 'Mahadev', 'Mattikali', NULL, '8123320094', 'cGFzc3dvcmQ=', 'assets_front/image/user_profile_pic/user_default_profile.jpg'),
(37, '192.168.1.224', '', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', NULL, 'ameen@hexwhale.com', 'front-end-user', 3, NULL, NULL, NULL, NULL, 1442572880, 1444104734, 1, 'Ameen', 'OT', NULL, '9066682146', 'cGFzc3dvcmQ=', 'assets_front/image/user_profile_pic/user_default_profile.jpg'),
(38, '::1', '', '$2y$08$qpB0mpeye/Xe1SfRzD4Sbe4jV7tlzME3fDm2.k9crQG1ZZATjl3cy', NULL, 'gayethri@hexwhale.com', 'front-end-user', 3, NULL, NULL, NULL, NULL, 1444128438, 1444204409, 1, 'Gayethri', 'R', NULL, '0', 'Z2V0aW5zaWRl', 'assets_front/image/user_profile_pic/user_default_profile.jpg'),
(39, '::1', '', '$2y$08$nOu5GE3uABEMlhtYvuDmgu.nJL.U9V4lyYYCANeNHIF6PZnEv2fdW', NULL, 'anoop@hexwhale.com', 'front-end-user', 3, NULL, NULL, NULL, NULL, 1444128596, 1444128596, 1, 'Anoop', 'Nair', NULL, '0', 'cGFzc3dvcmQ=', 'assets_front/image/user_profile_pic/user_default_profile.jpg'),
(40, '::1', '', '$2y$08$QCNhCwTvMwZc4znXoq22bu5i/3xSuC39.jwboz6HHN1XJjEkEko1.', NULL, 'rashmi@hexwhale.com', 'front-end-user', 3, NULL, NULL, NULL, NULL, 1444128662, 1444128662, 1, 'Rashmi', 'Sonikoppa', NULL, '0', 'cGFzc3dvcmQ=', 'assets_front/image/user_profile_pic/user_default_profile.jpg'),
(41, '::1', '', '$2y$08$Gn8aB.2VrZmURb/CXP1x/edyahnR39Z/at/a4x/dnuWDa1EDMRwYS', NULL, 'vishnu@hexwhale.com', 'front-end-user', 3, NULL, NULL, NULL, NULL, 1444128962, 1444898218, 1, 'Vishnu', 'Ravi', NULL, '0', 'cGFzc3dvcmQ=', 'assets_front/image/user_profile_pic/user_default_profile.jpg'),
(42, '::1', '', '$2y$08$j2GfzfYspXT4AMhC/SKpZunQgY5pRmSzCx2HypRAhApG6hlXmGHDi', NULL, 'asdf@asdf.asdf', 'front-end-user', 3, NULL, NULL, NULL, NULL, 1444656321, NULL, 1, 'ARIFF', 'ALII', NULL, '0', 'R1dCUjQwSEQ=', '');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(10, 11, 2),
(31, 33, 2),
(32, 34, 2),
(34, 36, 2),
(35, 37, 2),
(36, 38, 2),
(37, 39, 2),
(38, 40, 2),
(39, 41, 2),
(40, 42, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE IF NOT EXISTS `user_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `title`, `description`) VALUES
(1, 'super-admin', 'Super Admin'),
(2, 'data-entry', 'Data Entry'),
(3, 'front-end-user', 'Front End User'),
(4, 'college-admin', 'College Admin');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
